/************************************************************************
* draw.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

/* draw a line between x/y and ex/ey of color c */
void draw2d_line(color_t *c, int x, int y, int ex, int ey)
{
	render2d_line(c,x,y,ex,ey);
}

/* draw a rectangle at x/y of size w/h of color c */
void draw2d_rect(color_t *c, int x, int y, int w, int h)
{
	render2d_quad_color(c,x,y,w,h);
}

/* draw a material */
void draw2d_material(material_t *m, int x, int y, int w, int h)
{
	if (!m)
		return;

	if (m->tex) {
		if (h == NTV_SIZE)
			h = m->tex->h ;
		if (w == NTV_SIZE)
			w = m->tex->w;
		render2d_quad_mat(m,x,y,w,h);
	}else{
		color_t c;
		c.r = m->diffuse[0]*255.0;
		c.g = m->diffuse[1]*255.0;
		c.b = m->diffuse[2]*255.0;
		c.a = m->diffuse[3]*255.0;
		render2d_quad_color(&c,x,y,w,h);
	}
}

/* draw a sprite */
void draw2d_sprite(sprite_t *s, int x, int y, int w, int h)
{
	float xf = (float)x;
	float yf = (float)y;
	float wf = (float)w;
	float hf = (float)h;
	int v[8];
	float t[8];

	if (!s || !s->mat)
		return;

	if (w == NTV_SIZE)
		wf = (float)s->w;

	if (h == NTV_SIZE)
		hf = (float)s->h ;

	if (!s->mat->tex) {
		draw2d_material(s->mat,x,y,w,h);
		return;
	}

	v[0] = xf;
	v[1] = yf;
	v[2] = xf+wf;
	v[3] = yf;
	v[4] = v[2];
	v[5] = yf+hf;
	v[6] = xf;
	v[7] = v[5];

	t[0] = s->mat->tex->xf*s->x;
	t[1] = s->mat->tex->yf*s->y;
	t[2] = s->mat->tex->xf*(s->x+s->w);
	t[3] = t[1];
	t[4] = t[2];
	t[5] = s->mat->tex->yf*(s->y+s->h );
	t[6] = t[0];
	t[7] = t[5];

	render2d_quad_mat_vt(s->mat,v,t);
}

/* draw a sprite at an angle */
void draw2d_sprite_angle(sprite_t *s, int x, int y, int w, int h, int angle)
{
	float xf = (float)x;
	float yf = (float)y;
	float wf = (float)w;
	float hf = (float)h;
	float hw;
	float hh;
	float af;
	float hyp;
	int v[8];
	float t[8];

	if (!s || !s->mat || !s->mat->tex)
		return;

	if (w == NTV_SIZE)
		wf = (float)s->w;

	if (h == NTV_SIZE)
		hf = (float)s->h;

	hw = wf/2.0;
	hh = hf/2.0;

	if (angle > 360)
		angle = angle%360;

	af = ((float)angle-45.0)*(M_PI/180.0);
	hyp = math_sqrt((hw*hw)+(hh*hh));

	/* tl */
	v[0] = xf+(hyp*sin(af));
	v[1] = yf+(hyp*cos(af));
	af += 1.57;
	/* tr */
	v[2] = xf+(hyp*sin(af));
	v[3] = yf+(hyp*cos(af));
	af += 1.57;
	/* br */
	v[4] = xf+(hyp*sin(af));
	v[5] = yf+(hyp*cos(af));
	af += 1.57;
	/* bl */
	v[6] = xf+(hyp*sin(af));
	v[7] = yf+(hyp*cos(af));

	t[0] = s->mat->tex->xf*s->x;
	t[1] = s->mat->tex->yf*s->y;
	t[2] = s->mat->tex->xf*(s->x+s->w);
	t[3] = t[1];
	t[4] = t[2];
	t[5] = s->mat->tex->yf*(s->y+s->h );
	t[6] = t[0];
	t[7] = t[5];

	render2d_quad_mat_vt(s->mat,v,t);
}
