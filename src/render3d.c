/************************************************************************
* render3d.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

static array_t *objects = NULL;
static object_t *sorted;

static int render3d_sort(void *e1, void *e2)
{
	camera_t *c;
	v3_t cp;
	v3_t ep;
	GLfloat d1;
	GLfloat d2;
	c = camera_get();
	cp.x = c->x;
	cp.y = c->y;
	cp.z = c->z;

	ep.x = ((object_t*)e1)->pos.x;
	ep.y = ((object_t*)e1)->pos.y;
	ep.z = ((object_t*)e1)->pos.z;
	d1 = math_distance(&cp,&ep);
	if (d1 < 0)
		d1 *= -1;

	/* return -1 if this object is out of visual range */
	if (d1 > wm_res.distance)
		return -1;

	/* TODO: return -1 if this object is completely behind the camera */

	ep.x = ((object_t*)e2)->pos.x;
	ep.y = ((object_t*)e2)->pos.y;
	ep.z = ((object_t*)e2)->pos.z;
	d2 = math_distance(&cp,&ep);
	if (d2 < 0)
		d2 *= -1;

	/* return 1 if e1 is closer to the camera than e2 */
	if (d1 < d2)
		return 1;
	return 0;
}

static void render3d_step(object_t *o)
{
	if (o->m->step(o) && opengl_has_vbo()) {
		int i;
		mesh_t *m;
		for (i=0; i<o->meshes->length; i++) {
			m = ((mesh_t**)o->meshes->data)[i];
			if (m->vbo.state == 1)
				m->vbo.state = 2;
		}
	}
}

/* find a 3d object */
object_t *render3d_object_find(int id)
{
	if (objects) {
		int i;
		object_t **o = objects->data;
		for (i=0; i<objects->length; i++) {
			if (o[i] && o[i]->id == id)
				return o[i];
		}
	}

	return NULL;
}

/* destroy a 3d object */
void render3d_object_free(object_t *o)
{
	mesh_t *m;
	int i = array_find_ptr(objects,o);
	if (i<0)
		return;

	o->ignore = 2;

	while (o->ignore != 3) {
		delay(1);
	}

	((unsigned char**)(objects->data))[i] = NULL;

	while ((m = array_pop_ptr(o->meshes))) {
		mesh_free(m);
	}
	array_free(o->meshes);
	free(o);
}

/* create a new 3d object */
object_t *render3d_object_create()
{
	static int object_ids = 1;
	object_t *o = malloc(sizeof(object_t));

	o->meshes = array_create(RTG_TYPE_PTR);
	o->id = object_ids++;
	o->ignore = 1;
	o->drop = 0;
	o->m = NULL;
	o->pos.x = 0;
	o->pos.y = 0;
	o->pos.z = 0;
	o->rot.x = 0;
	o->rot.y = 0;
	o->rot.z = 0;
	o->scale.x = 1.0;
	o->scale.y = 1.0;
	o->scale.z = 1.0;

	if (!objects)
		objects = array_create(RTG_TYPE_PTR);

	array_push_ptr(objects,o);

	return o;
}

/* render a model */
object_t *render3d_model(model_t *mod, v3_t *pos)
{
	int i;
	mesh_t *m;
	object_t *o = render3d_object_create();
	o->pos.x = pos->x;
	o->pos.y = pos->y;
	o->pos.z = pos->z;

	o->m = mod;

	for (i=0; i<mod->meshes->length; i++) {
		m = ((mesh_t**)(mod->meshes->data))[i];
		m = mesh_copy(m);
		array_push_ptr(o->meshes,m);
	}

	o->ignore = 0;

	return o;
}

/* render a mesh */
object_t *render3d_mesh(mesh_t *m, v3_t *pos)
{
	object_t *o = render3d_object_create();
	o->pos.x = pos->x;
	o->pos.y = pos->y;
	o->pos.z = pos->z;

	o->ignore = 0;

	array_push_ptr(o->meshes,m);

	return o;
}

/* render 3d graphics to the frame */
void render3d()
{
	int i;
#ifdef RTG_CFG_DEBUG
	int c[3] = {0,0,0};
#endif
	object_t *o;
	mesh_t *m;
	GLenum d;
	if (!objects || !objects->length)
		return;

	sorted = NULL;

	for (i=0; i<objects->length; i++) {
		o = ((object_t**)(objects->data))[i];
		/* check if we're meant to touch it */
		if (!o)
			continue;
		if (o->drop)
			o->ignore = 2;
		if (o->ignore) {
			o->drop = 0;
			if (o->ignore == 2)
				o->ignore++;
			continue;
		}
		/* call step on each object, this may modify the data (such as with animated models) */
		if (o->m && o->m->step)
			render3d_step(o);
		/* sort objects by distance, ignoring anything that can't be seen */
		sorted = list_insert_cmp(&sorted,o,render3d_sort);
	}

	/* enable default client pointers, vertex was enabled in render() */
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	o = sorted;
	/* now render objects, furthest first */
	if (opengl_has_vbo()) {
		while (o) {
			glPushMatrix();
			glTranslatef(o->pos.x, o->pos.y, o->pos.z);
			glRotatef(o->rot.z,0,0,1);
			glRotatef(o->rot.y,0,1,0);
			glRotatef(o->rot.x,1,0,0);
			glColor4f(1.0,1.0,1.0,1.0);
			glScalef(o->scale.x,o->scale.y,o->scale.z);
			for (i=0; i<o->meshes->length; i++) {
				m = ((mesh_t**)o->meshes->data)[i];
				/* create buffers if necessary and fill with data */
				if (m->vbo.state != 1) {
					d = (o->m && o->m->step) ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW;
					if (m->t && m->t->length) {
						if (!m->vbo.state || !m->vbo.texcoords)
							opengl_gen_buffers(1, &m->vbo.texcoords);
						if (m->vbo.state && m->vbo.colours) {
							opengl_delete_buffers(1,&m->vbo.colours);
							m->vbo.colours = 0;
						}
						opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.texcoords);
						opengl_buffer_data(GL_ARRAY_BUFFER, m->t->length*4, m->t->data, d);
					}else if (m->c && m->c->length) {
						if (!m->vbo.state || !m->vbo.colours)
							opengl_gen_buffers(1, &m->vbo.colours);
						if (m->vbo.state && m->vbo.texcoords) {
							opengl_delete_buffers(1,&m->vbo.texcoords);
							m->vbo.texcoords = 0;
						}
						opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.colours);
						opengl_buffer_data(GL_ARRAY_BUFFER, m->c->length*4, m->c->data, d);
						if (m->vbo.state && m->vbo.texcoords)
							opengl_delete_buffers(1,&m->vbo.texcoords);
						m->vbo.texcoords = 0;
					}else{
						if (m->vbo.state && m->vbo.colours)
							opengl_delete_buffers(1,&m->vbo.colours);
						if (m->vbo.state && m->vbo.texcoords)
							opengl_delete_buffers(1,&m->vbo.texcoords);
						m->vbo.texcoords = 0;
						m->vbo.colours = 0;
					}

					if (m->n && m->n->length) {
						if (!m->vbo.state || !m->vbo.normals)
							opengl_gen_buffers(1, &m->vbo.normals);
						opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.normals);
						opengl_buffer_data(GL_ARRAY_BUFFER, m->n->length*4, m->n->data, d);
					}else{
						if (m->vbo.state && m->vbo.normals)
							opengl_delete_buffers(1,&m->vbo.normals);
						m->vbo.normals = 0;
					}

					if (m->v && m->v->length) {
						if (!m->vbo.state || !m->vbo.vertices)
							opengl_gen_buffers(1, &m->vbo.vertices);
						opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.vertices);
						opengl_buffer_data(GL_ARRAY_BUFFER, m->v->length*4, m->v->data, d);
					}else{
						if (m->vbo.state && m->vbo.vertices)
							opengl_delete_buffers(1,&m->vbo.vertices);
						m->vbo.vertices = 0;
					}

					if (!m->vbo.state || !m->vbo.indices)
						opengl_gen_buffers(1, &m->vbo.indices);
					opengl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, m->vbo.indices);
					opengl_buffer_data(GL_ELEMENT_ARRAY_BUFFER, m->i->length*4, m->i->data, d);

					opengl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
					opengl_bind_buffer(GL_ARRAY_BUFFER, 0);

					m->vbo.state = 1;
				}

#ifdef RTG_CFG_DEBUG
				c[0]++;
				c[1] += m->v->length/3;
				c[2] += m->i->length/3;
#endif

				/* use vertex, normal, and texcoord or colour arrays */
				if (m->vbo.normals) {
					opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.normals);
					glNormalPointer(GL_FLOAT, 0, NULL);
				}else{
					glDisableClientState(GL_NORMAL_ARRAY);
				}
				if (m->vbo.texcoords) {
					mat_use(m->mat);
					opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.texcoords);
					glTexCoordPointer(2, GL_FLOAT, 0, NULL);
				}else{
					glDisable(GL_TEXTURE_2D);
					glDisableClientState(GL_TEXTURE_COORD_ARRAY);
					if (m->vbo.colours) {
						glEnableClientState(GL_COLOR_ARRAY);
						opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.colours);
						glColorPointer(4, GL_FLOAT, 0, NULL);
					}else if (m->col) {
						glColor4ub(m->col->r,m->col->g,m->col->b,m->col->a);
					}
				}
				opengl_bind_buffer(GL_ARRAY_BUFFER, m->vbo.vertices);
				glVertexPointer(3, GL_FLOAT, 0, NULL);
				/* the actual drawing */
				opengl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, m->vbo.indices);
				if (m->option) {
					switch(m->mode) {
					case GL_POINTS:
						glPointSize(m->option);
						break;
					case GL_LINES:
						glLineWidth(m->option);
						break;
					default:;
					}
					glDrawElements(m->mode,m->i->length,GL_UNSIGNED_INT,NULL);
					switch(m->mode) {
					case GL_POINTS:
						glPointSize(1);
						break;
					case GL_LINES:
						glLineWidth(1);
						break;
					default:;
					}
				}else{
					glDrawElements(m->mode,m->i->length,GL_UNSIGNED_INT,NULL);
				}
				/* re-enable default client pointers */
				if (!m->vbo.normals)
					glEnableClientState(GL_NORMAL_ARRAY);
				if (!m->vbo.texcoords) {
					glEnable(GL_TEXTURE_2D);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
					if (m->vbo.colours) {
						glDisableClientState(GL_COLOR_ARRAY);
					}else if (m->col) {
						glColor4f(1.0,1.0,1.0,1.0);
					}
				}
			}
			glPopMatrix();
			o = o->next;
		}
		opengl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		opengl_bind_buffer(GL_ARRAY_BUFFER, 0);
	}else{
		while (o) {
			glPushMatrix();
			glTranslatef(o->pos.x, o->pos.y, o->pos.z);
			glRotatef(o->rot.z,0,0,1);
			glRotatef(o->rot.y,0,1,0);
			glRotatef(o->rot.x,1,0,0);
			glColor4f(1.0,1.0,1.0,1.0);
			glScalef(o->scale.x,o->scale.y,o->scale.z);
			for (i=0; i<o->meshes->length; i++) {
				m = ((mesh_t**)o->meshes->data)[i];
#ifdef RTG_CFG_DEBUG
				c[0]++;
				c[1] += m->v->length/3;
				c[2] += m->i->length/3;
#endif
				/* use vertex, normal, and texcoord or colour arrays */
				if (m->n && m->n->length) {
					glNormalPointer(GL_FLOAT, 0, m->n->data);
				}else{
					glDisableClientState(GL_NORMAL_ARRAY);
				}
				if (m->t && m->t->length) {
					mat_use(m->mat);
					glTexCoordPointer(2, GL_FLOAT, 0, m->t->data);
				}else{
					glDisable(GL_TEXTURE_2D);
					glDisableClientState(GL_TEXTURE_COORD_ARRAY);
					if (m->c && m->c->length) {
						glEnableClientState(GL_COLOR_ARRAY);
						glColorPointer(4, GL_FLOAT, 0, m->c->data);
					}else if (m->col) {
						glColor4ub(m->col->r,m->col->g,m->col->b,m->col->a);
					}
				}
				glVertexPointer(3, GL_FLOAT, 0, m->v->data);
				/* the actual drawing */
				glDrawElements(m->mode,m->i->length,GL_UNSIGNED_INT,m->i->data);
				/* re-enable default client pointers */
				if (!m->n || !m->n->length)
					glEnableClientState(GL_NORMAL_ARRAY);
				if (!m->t || !m->t->length) {
					glEnable(GL_TEXTURE_2D);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
					if (m->c && m->c->length) {
						glDisableClientState(GL_COLOR_ARRAY);
					}else if (m->col) {
						glColor4f(1.0,1.0,1.0,1.0);
					}
				}
			}
			glPopMatrix();
			o = o->next;
		}
	}
	/* disable default client pointers, vertex is disabled in render() */
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
#ifdef RTG_CFG_DEBUG
	printf2d(10,20,1,14,"meshes: %d, vertices: %d, polys: %d",c[0],c[1],c[2]);
#endif
}
