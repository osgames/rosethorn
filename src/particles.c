/************************************************************************
* particles.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2013 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"
#include "rtg_physics.h"

typedef struct particle_s {
	int active;
	float life;
	color_t col;
	float fade;
	v3_t pos;
	v3_t speed;
} particle_t;

typedef struct particles_s {
	struct particles_s *prev;
	struct particles_s *next;
	particle_t *p;
	color_t *col;
	int count;
	model_t *mdl;
	unsigned int msec;
	v3_t speed;
	v3_t grav;
} particles_t;

static color_t particle_colours[12]= {
	{255, 128, 128, 255},
	{255, 192, 128, 255},
	{255, 255, 128, 255},
	{192, 255, 128, 255},
	{128, 255, 128, 255},
	{128, 255, 192, 255},
	{128, 255, 255, 255},
	{128, 192, 255, 255},
	{128, 128, 255, 255},
	{192, 128, 255, 255},
	{255, 128, 255, 255},
	{255, 128, 192, 255}
};
static particles_t *particles = NULL;

static int particles_advance_frame(object_t *o)
{
	int i;
	int k;
	int inactive = 0;
	unsigned int msec = time_ticks();
	particles_t *p = o->m->data;
	v3_t g;
	float elapsed = (anim_factor*60.0)/1000.0;
	mesh_t *m = array_get_ptr(o->meshes,0);
	m->c->length = 0;
	m->v->length = 0;
	m->i->length = 0;
	for (i=0; i<p->count; i++) {
		if (p->p[i].active) {
			if (p->p[i].life < 0.0) {
				if (p->msec && p->msec < msec) {
					inactive++;
					p->p[i].active = 0;
					continue;
				}

				p->p[i].active = 1;
				p->p[i].life = 1.0;

				if (p->col) {
					p->p[i].col.r = p->col->r;
					p->p[i].col.g = p->col->g;
					p->p[i].col.b = p->col->b;
				}else{
					k = math_rand_range(0,11);
					p->p[i].col.r = particle_colours[k].r;
					p->p[i].col.g = particle_colours[k].g;
					p->p[i].col.b = particle_colours[k].b;
				}
				p->p[i].col.a = 255;
				array_push_color(m->c,&p->p[i].col);

				p->p[i].fade = math_rand_rangef(0.003,0.1);

				p->p[i].speed.x = p->speed.x+math_rand_rangef(-0.6,0.6);
				p->p[i].speed.y = p->speed.y+math_rand_rangef(-0.6,0.6);
				p->p[i].speed.z = p->speed.z+math_rand_rangef(-0.6,0.6);

				p->p[i].pos.x = 0.0;
				p->p[i].pos.y = 0.0;
				p->p[i].pos.z = 0.0;
				mesh_push_point(m,&p->p[i].pos);
				continue;
			}
			p->p[i].life -= p->p[i].fade*anim_factor;
			p->p[i].col.a = 255*p->p[i].life;
			array_push_color(m->c,&p->p[i].col);

			p->p[i].pos.x += p->p[i].speed.x*elapsed;
			p->p[i].pos.y += p->p[i].speed.y*elapsed;
			p->p[i].pos.z += p->p[i].speed.z*elapsed;
			mesh_push_point(m,&p->p[i].pos);

			gravity_at(&p->p[i].pos,&g);

			p->p[i].speed.x += (g.x*0.03)*elapsed;
			p->p[i].speed.y += (g.y*0.03)*elapsed;
			p->p[i].speed.z += (g.z*0.03)*elapsed;
		}
	}

	if (inactive == p->count) {
		o->drop = 1;
		free(p->p);
		if (p->col)
			free(p->col);
		particles = list_remove(&particles,p);
		free(p);
		model_free(o->m);
		return 0;
	}

	return 1;
}

/* create a new particle effect of colour, at pos moving at speed m/s
 * for msec milliseconds, and count particles of size pixels */
int particles_create(color_t *colour, v3_t *pos, v3_t *speed, unsigned int msec, int count, int size)
{
	int i;
	int m;
	int k;
	particles_t *p;
	mesh_t *ms;

	if (!opengl_has_particles())
		return 0;

	/* TODO: get the maximum count of particles per effect,
	 * adjust if necessary */
	m = opengl_particles_max();
	if (count < m)
		m = count;

	if (m < 1)
		return 0;

	p = malloc(sizeof(particles_t));

	p->p = malloc(sizeof(particle_t)*m);
	p->count = m;

	p->mdl = model_create();

	p->mdl->step = particles_advance_frame;

	p->mdl->data = p;

	if (colour) {
		p->col = malloc(sizeof(color_t));
		p->col->r = colour->r;
		p->col->g = colour->g;
		p->col->b = colour->b;
	}else{
		p->col = NULL;
	}
	ms = mesh_create(GL_POINTS);
	ms->c = array_create(RTG_TYPE_FLOAT);
	if (size < 1)
		size = 1;
	ms->option = size;

	array_push_ptr(p->mdl->meshes,ms);

	if (msec) {
		p->msec = time_ticks()+msec;
	}else{
		p->msec = 0;
	}

	p->speed.x = speed->x;
	p->speed.y = speed->y;
	p->speed.z = speed->z;

	p->grav.x = 0.0f;
	p->grav.y = -0.8f;
	p->grav.z = 0.0f;

	for (i=0; i<m; i++) {
		p->p[i].active = 1;
		p->p[i].life = 1.0;

		if (colour) {
			p->p[i].col.r = colour->r;
			p->p[i].col.g = colour->g;
			p->p[i].col.b = colour->b;
		}else{
			k = math_rand_range(0,11);
			p->p[i].col.r = particle_colours[k].r;
			p->p[i].col.g = particle_colours[k].g;
			p->p[i].col.b = particle_colours[k].b;
		}
		p->p[i].col.a = 255;
		array_push_color(ms->c,&p->p[i].col);

		p->p[i].fade = math_rand_rangef(0.003,0.1);

		p->p[i].speed.x = speed->x+math_rand_rangef(-0.6,0.6);
		p->p[i].speed.y = speed->y+math_rand_rangef(-0.6,0.6);
		p->p[i].speed.z = speed->z+math_rand_rangef(-0.6,0.6);

		p->p[i].pos.x = 0.0;
		p->p[i].pos.y = 0.0;
		p->p[i].pos.z = 0.0;
		mesh_push_point(ms,&p->p[i].pos);
	}

	particles = list_push(&particles,p);

	render3d_model(p->mdl,pos);

	return 1;
}
