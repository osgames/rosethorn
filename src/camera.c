/************************************************************************
* camera.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

static camera_t camera = {
	0.0,
	0.0,
	0.0,
	0.0,
	0.0
};

/* move to the camera position */
void camera_centre()
{
	glRotatef(camera.pitch,1,0,0);
	glRotatef(-camera.yaw,0,1,0);
	glTranslatef(-camera.x, -camera.y, -camera.z);
}

/* get the camera data */
camera_t *camera_get()
{
	return &camera;
}

/* set the position of the camera */
void camera_set_pos(v3_t *p)
{
	camera.x = p->x;
	camera.y = p->y;
	camera.z = p->z;
}

/* set the yaw - y rotation - of the camera */
void camera_set_yaw(GLfloat yaw)
{
	camera.yaw = yaw;
}

/* set the pitch - x rotation - of the camera */
void camera_set_pitch(GLfloat pitch)
{
	camera.pitch = pitch;
}
