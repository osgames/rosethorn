/************************************************************************
* main.c
* rtg - rosethorn game engine example
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

static anim_instance_t *orc = NULL;

int direction(int bx, int by, int ex, int ey)
{
	bx /= 32;
	by /= 32;
	ex /= 32;
	ey /= 32;
	if (bx < ex) {
		if (by < ey)
			return 6;
		if (by > ey)
			return 4;
		return 5;
	}
	if (bx > ex) {
		if (by < ey)
			return 0;
		if (by > ey)
			return 2;
		return 1;
	}
	if (by < ey)
		return 7;
	return 3;
}

/* these are some 'game' logic */
void domove(int x, int y)
{
	int cx;
	int cy;
	int alt = 0;

	anim_get_position(orc,&cx,&cy);
	alt = direction(cx,cy,x,y);

	orc->loop = 0;
	orc = anim_spawn(1,alt,-1,cx,cy,x,y);
}

void move()
{
	int x;
	int y;

	x = math_rand_range(0,wm_res.width-128);
	y = math_rand_range(0,wm_res.height-128);
	domove(x,y);
}

/* these are our 'game' events, they get bound to keys and mouse later */
void click()
{
	domove(current_event.x-64,current_event.y-90);
	anim_spawn(2,0,2,current_event.x-32,current_event.y-48,current_event.x-32,current_event.y-48);
}

/* these are our ui events, these are bound to buttons and such when the ui is loaded */
int gui_sound_show(widget_t *w)
{
	if (sound_state(-1) == 1) {
		ui_widget_value(w,"1");
	}else{
		ui_widget_value(w,"0");
	}
	return EVENT_HANDLED;
}
int gui_sound_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.enabled %s",v);
	return EVENT_HANDLED;
}
int gui_music_show(widget_t *w)
{
	int v = sound_volume_music(-1);
	ui_widget_value(w,"%d",v);
	return EVENT_HANDLED;
}
int gui_music_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.music.volume %s",v);
	return EVENT_HANDLED;
}
int gui_effects_show(widget_t *w)
{
	int v = sound_volume_effects(-1);
	ui_widget_value(w,"%d",v);
	return EVENT_HANDLED;
}
int gui_effects_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.effects.volume %s",v);
	return EVENT_HANDLED;
}
int gui_controls_show(widget_t *w)
{
	ctrl_create(w,320,100);
	return EVENT_HANDLED;
}
int gui_controls_hide(widget_t *w)
{
	ctrl_clear(w);
	return EVENT_HANDLED;
}
int gui_video_show(widget_t *w)
{
	char* v;

	v = config_get("wm.fullscreen");
	ui_widget_set_value("video.check.fullscreen",v);

	v = config_get("wm.framecap");
	ui_widget_set_value("video.num.cap",v);

	v = config_get("wm.width");
	ui_widget_set_value("video.num.width",v);

	v = config_get("wm.height");
	ui_widget_set_value("video.num.height",v);

	v = config_get("gl.anisotropic");
	ui_widget_set_value("video.check.ani",v);

	v = config_get("gl.bilinear");
	ui_widget_set_value("video.check.bil",v);

	v = config_get("gl.trilinear");
	ui_widget_set_value("video.check.tri",v);

	v = config_get("gl.mipmaps");
	ui_widget_set_value("video.check.mip",v);

	return EVENT_HANDLED;
}
int gui_video_save(widget_t *w)
{
	char* v;

	v = ui_widget_get_value("video.check.fullscreen");
	config_set_and_apply("wm.fullscreen",v);

	v = ui_widget_get_value("video.num.cap");
	config_set_and_apply("wm.framecap",v);

	v = ui_widget_get_value("video.num.width");
	config_set_and_apply("wm.width",v);

	v = ui_widget_get_value("video.num.height");
	config_set_and_apply("wm.height",v);

	v = ui_widget_get_value("video.check.ani");
	config_set_and_apply("gl.anisotropic",v);

	v = ui_widget_get_value("video.check.bil");
	config_set_and_apply("gl.bilinear",v);

	v = ui_widget_get_value("video.check.tri");
	config_set_and_apply("gl.trilinear",v);

	v = ui_widget_get_value("video.check.mip");
	config_set_and_apply("gl.mipmaps",v);

	return EVENT_HANDLED;
}

int main(int argc, char** argv)
{
	unsigned int cticks;
	unsigned int lticks = 0;
	int s;
	int x;
	int y;
	int dx;
	int dy;
	int bx;
	sprite_t sp;

	rtg_init(argc,argv,RTG_INIT_ALL,"rtg");
	ui_xml_load("main2d.ui");
	sp.mat = mat_from_image("ground.png");
	sp.x = 0;
	sp.y = 0;
	sp.w = 64;
	sp.h = 32;
	anim_load("animations.xml");

	/* export functions used as ui callbacks */
	export_function("gui_sound_show",gui_sound_show);
	export_function("gui_sound_click",gui_sound_click);
	export_function("gui_music_show",gui_music_show);
	export_function("gui_music_click",gui_music_click);
	export_function("gui_effects_show",gui_effects_show);
	export_function("gui_effects_click",gui_effects_click);
	export_function("gui_controls_show",gui_controls_show);
	export_function("gui_controls_hide",gui_controls_hide);
	export_function("gui_video_show",gui_video_show);
	export_function("gui_video_save",gui_video_save);

	/* our sole 'game' event */
	event_create("click","mouse-1",NULL,click,NULL,NULL);

	s = rtg_state;

	while (rtg_state) {
		render_clear();

		if (s != rtg_state) {
			s = rtg_state;
			if (s == RTG_STATE_MENU && orc) {
				orc->loop = 0;
				orc = NULL;
			}
		}
		if (rtg_state == RTG_STATE_PLAY) {
			if (!orc) {
				orc = anim_spawn(1,0,-1,0,0,0,0);
				move();
			}
			cticks = time_ticks();
			if (cticks > lticks+10000) {
				lticks = cticks;
				move();
			}

			bx = wm_res.width/2;

			for (x=0; x<25; x++) {
				dx = bx-(32*x);
				dy = (16*x);

				for (y=0; y<25; y++) {
					draw2d_sprite(&sp,dx,dy,-1,-1);
					dx = dx+32;
					dy = dy+16;
				}
			}
		}

		anim_render();
		printf2d(10,35,1,14,"FPS: %d",wm_res.fps);

		render();
	}

	rtg_exit();

	return 0;
}
