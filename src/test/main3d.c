/************************************************************************
* main.c
* rtg - rosethorn game engine example
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"
#include "rtg_render.h"
#include "rtg_physics.h"

/* these are our 'game' events, they get bound to keys and mouse later */
void motion()
{
	camera_t *cam = camera_get();
	if (rmouse[0] > 0) {
		cam->yaw -= anim_factor;
		if (cam->yaw < 0)
			cam->yaw += 360.0;
	}else if (rmouse[0] < 0) {
		cam->yaw += anim_factor;
		if (cam->yaw > 360.0)
			cam->yaw -= 360.0;
	}
	if (rmouse[1] > 0) {
		if (cam->pitch < 80)
			cam->pitch += anim_factor;
	}else if (rmouse[1] < 0) {
		if (cam->pitch > -80)
			cam->pitch -= anim_factor;
	}
}

void forward()
{
	camera_t *cam = camera_get();
	float f = anim_factor;
	float sf = (float)sin(cam->yaw*PI_OVER_180);
	float cf = (float)cos(cam->yaw*PI_OVER_180);
	if (shift)
		f *= 6.0;
	cam->x -= (sf * 0.05)*f;
	cam->z -= (cf * 0.05)*f;
}

void back()
{
	camera_t *cam = camera_get();
	float f = anim_factor;
	float sf = (float)sin(cam->yaw*PI_OVER_180);
	float cf = (float)cos(cam->yaw*PI_OVER_180);
	if (shift)
		f *= 6.0;
	cam->x += (sf*0.05)*f;
	cam->z += (cf*0.05)*f;
}

void left()
{
	camera_t *cam = camera_get();
	float f = anim_factor;
	float sf = (float)sin(cam->yaw*PI_OVER_180);
	float cf = (float)cos(cam->yaw*PI_OVER_180);
	if (shift)
		f *= 6.0;
	cam->x -= (cf*0.05)*f;
	cam->z += (sf*0.05)*f;
}

void right()
{
	camera_t *cam = camera_get();
	float f = anim_factor;
	float sf = (float)sin(cam->yaw*PI_OVER_180);
	float cf = (float)cos(cam->yaw*PI_OVER_180);
	if (shift)
		f *= 6.0;
	cam->x += (cf*0.05)*f;
	cam->z -= (sf*0.05)*f;
}

void up()
{
	camera_t *cam = camera_get();
	cam->y += anim_factor/6.0;
}

void down()
{
	camera_t *cam = camera_get();
	cam->y -= anim_factor/6.0;
}

void capture()
{
	wm_capture(NULL);
}

void escape()
{
	rtg_state = RTG_STATE_MENU;
	ui_style_set("game.!.*","visible:false;");
	ui_style_set("entry.!.*","visible:true;");
}

/* these are our ui events, these are bound to buttons and such when the ui is loaded */
int gui_sound_show(widget_t *w)
{
	if (sound_state(-1) == 1) {
		ui_widget_value(w,"1");
	}else{
		ui_widget_value(w,"0");
	}
	return EVENT_HANDLED;
}
int gui_sound_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.enabled %s",v);
	return EVENT_HANDLED;
}
int gui_music_show(widget_t *w)
{
	int v = sound_volume_music(-1);
	ui_widget_value(w,"%d",v);
	return EVENT_HANDLED;
}
int gui_music_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.music.volume %s",v);
	return EVENT_HANDLED;
}
int gui_effects_show(widget_t *w)
{
	int v = sound_volume_effects(-1);
	ui_widget_value(w,"%d",v);
	return EVENT_HANDLED;
}
int gui_effects_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.effects.volume %s",v);
	return EVENT_HANDLED;
}
int gui_controls_show(widget_t *w)
{
	ctrl_create(w,320,100);
	return EVENT_HANDLED;
}
int gui_controls_hide(widget_t *w)
{
	ctrl_clear(w);
	return EVENT_HANDLED;
}
int gui_video_show(widget_t *w)
{
	char* v;

	v = config_get("wm.fullscreen");
	ui_widget_set_value("video.check.fullscreen",v);

	v = config_get("wm.framecap");
	ui_widget_set_value("video.num.cap",v);

	v = config_get("wm.width");
	ui_widget_set_value("video.num.width",v);

	v = config_get("wm.height");
	ui_widget_set_value("video.num.height",v);

	v = config_get("gl.anisotropic");
	ui_widget_set_value("video.check.ani",v);

	v = config_get("gl.bilinear");
	ui_widget_set_value("video.check.bil",v);

	v = config_get("gl.trilinear");
	ui_widget_set_value("video.check.tri",v);

	v = config_get("gl.mipmaps");
	ui_widget_set_value("video.check.mip",v);

	return EVENT_HANDLED;
}
int gui_video_save(widget_t *w)
{
	char* v;

	v = ui_widget_get_value("video.check.fullscreen");
	config_set_and_apply("wm.fullscreen",v);

	v = ui_widget_get_value("video.num.cap");
	config_set_and_apply("wm.framecap",v);

	v = ui_widget_get_value("video.num.width");
	config_set_and_apply("wm.width",v);

	v = ui_widget_get_value("video.num.height");
	config_set_and_apply("wm.height",v);

	v = ui_widget_get_value("video.check.ani");
	config_set_and_apply("gl.anisotropic",v);

	v = ui_widget_get_value("video.check.bil");
	config_set_and_apply("gl.bilinear",v);

	v = ui_widget_get_value("video.check.tri");
	config_set_and_apply("gl.trilinear",v);

	v = ui_widget_get_value("video.check.mip");
	config_set_and_apply("gl.mipmaps",v);

	return EVENT_HANDLED;
}

#define MAP_HALFSIZE 200

/* here we go */
int main(int argc, char** argv)
{
	camera_t *cam = camera_get();
	material_t *mat[3];
	object_t *o;
	model_t *m;
	mass_t *mass;
	int x;
	int z;
	int i;
	v3_t q[4];

	color_t c[2] = {
		{255,200,0,255},
		{0,130,187,255}
	};

	/* init rtg, load the ui, add a random sound effect */
	rtg_init(argc,argv,RTG_INIT_ALL,"rtg");
	ui_xml_load("main3d.ui");
	sound_load_effect("explosion.wav","boom");

	/* export functions used as ui callbacks */
	export_function("gui_sound_show",gui_sound_show);
	export_function("gui_sound_click",gui_sound_click);
	export_function("gui_music_show",gui_music_show);
	export_function("gui_music_click",gui_music_click);
	export_function("gui_effects_show",gui_effects_show);
	export_function("gui_effects_click",gui_effects_click);
	export_function("gui_controls_show",gui_controls_show);
	export_function("gui_controls_hide",gui_controls_hide);
	export_function("gui_video_show",gui_video_show);
	export_function("gui_video_save",gui_video_save);

	/* bind some keys/mouse to game event functions above */
	event_create("turn","mouse-motion",NULL,motion,NULL,NULL);
	event_create("forward","w",NULL,NULL,NULL,forward);
	event_create("back","s",NULL,NULL,NULL,back);
	event_create("left","a",NULL,NULL,NULL,left);
	event_create("right","d",NULL,NULL,NULL,right);
	event_create("up","space",NULL,NULL,NULL,up);
	event_create("down","e",NULL,NULL,NULL,down);
	event_create("screenshot","c",NULL,capture,NULL,NULL);
	event_create("exit","esc",NULL,escape,NULL,NULL);

	/* load some textures */
	mat[0] = mat_from_image("grass.tga");
	mat[1] = mat_from_image("mud.png");
	mat[2] = mat_from_image("stone.png");
	q[0].x = -2.0;
	q[0].y = 0.0;
	q[0].z = 0.0;

	/* load a model */
	m = model_load("bob.rtm");
	o = render3d_model(m,&q[0]);
	object_scale(o,0.035,0.035,0.035);
	object_rotate(o,-90.0,0.0,0.0);
	object_set_skeleton(o,1);
	object_set_frame(o,0);
	/* TODO: physics *
	mass = mass_create_obj(o,100.0);

	m = model_load("hunter.rtm");
	o = render3d_model(m,&q[0]);
	object_rotate(o,-90,0,0);*/
	/* create the world */
	q[0].x = 0.0;
	o = object_create(&q[0]);
	cam->y = 1.6;
	i = 0;
	for (x=-MAP_HALFSIZE; x<MAP_HALFSIZE; x++) {
		for (z=-MAP_HALFSIZE; z<MAP_HALFSIZE; z++) {
			q[0].x = x-0.5;
			q[0].y = 0;
			q[0].z = z-0.5;
			q[1].x = x+0.5;
			q[1].y = 0;
			q[1].z = z-0.5;
			q[2].x = x+0.5;
			q[2].y = 0;
			q[2].z = z+0.5;
			q[3].x = x-0.5;
			q[3].y = 0;
			q[3].z = z+0.5;
			if (math_rand_range(0,10)) {
				i = 0;
			}else{
				i = math_rand_range(0,2);
			}
			object_add_mat_quad(o,mat[i],&q[0],&q[1],&q[2],&q[3]);
		}
	}
	object_normalise(o);

	/* set up some particle effects */
	q[0].x = 0.0;
	q[0].y = 5.0;
	q[0].z = 0.0;
	q[1].x = 0.0;
	q[1].y = 0.2;
	q[1].z = 0.0;
	particles_create(&c[1],&q[0],&q[1],0,1000,2);
	q[0].x = 5.0;
	q[0].y = 5.0;
	q[0].z = 5.0;
	q[1].x = 1.0;
	q[1].y = 1.0;
	q[1].z = 1.0;
	particles_create(NULL,&q[0],&q[1],0,1000,1);
	q[0].x = 5.0;
	q[0].y = 5.0;
	q[0].z = 0.0;
	q[1].x = 0.0;
	q[1].y = 5.0;
	q[1].z = 0.0;
	particles_create(&c[0],&q[0],&q[1],6000,1000,1);

	q[2].x = 0.0;
	q[2].y = -1024.0;
	q[2].z = 0.0;
	/* give the world a mass */
	mass = mass_create_obj(o,10000.0);
	/* leave it unaffected by gravity */
	mass_set_isolated(mass,1);
	/* make it a gravity source equivalent to earth */
	mass_set_gravity_source(mass,EARTH_GRAVITY,EARTH_WEIGHT,EARTH_REACH);
	/* and set the pull point to deep underground */
	mass_set_gravity_offset(mass,&q[2]);

	i = rtg_state;

	while (rtg_state != RTG_STATE_EXIT) {
		/* preframe processing - handles events, physics, sound processing etcetera */
		render_clear();

		/* C logic code */
		/* watch for changes in the game state */
		if (i != rtg_state) {
			/* and fire some particles when the game starts */
			if (rtg_state == RTG_STATE_PLAY) {
				sound_play_effect("boom");
				particles_create(&c[0],&q[0],&q[1],6000,1000,1);
			}
			i = rtg_state;
		}

		/* the world and model are already added to the render pipeline
		 * so there's not much to do here */
		printf2d(10,5,1,14,"yaw: %.2f, pitch: %.2f, pos: %.2f,%.2f,%.2f",cam->yaw,cam->pitch,cam->x,cam->y,cam->z);
		printf2d(10,35,1,14,"FPS: %d",wm_res.fps);

		/* finally render eveything to screen, also includes the frame capper */
		render();
	}

	/* deinit rtg */
	rtg_exit();

	return 0;
}
