/************************************************************************
* file.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

/* check if a file exists */
int file_exists(char* p)
{
	FILE *f;
	f = fopen(p,"r");
	if (!f)
		return 0;

	fclose(f);
	return 1;
}

/* check if a directory exists */
int dir_exists(char* p)
{
#ifndef WIN32
	struct stat st;
	if (stat(p,&st) == 0)
		return 1;
#else
/* TODO: win32 code */
#endif
	return 0;
}

/* create a directory or path */
int dir_create(char* p)
{
#ifndef WIN32
	mode_t process_mask = umask(0);
	mode_t mode = S_IRWXU | S_IRWXG | S_IRWXO;
        char *q, *r = NULL, *path = NULL, *up = NULL;
        int ret = 1;
	p = file_path(p);
        if (!strcmp(p, ".") || !strcmp(p, "/")) {
		umask(process_mask);
		return 0;
	}

        if ((path = strdup(p)) == NULL)
                goto fail;

        if ((q = strdup(p)) == NULL)
                goto fail;

        if ((r = dirname(q)) == NULL)
                goto out;

        if ((up = strdup(r)) == NULL)
               goto fail;

        if ((dir_create(up) == -1) && (errno != EEXIST))
                goto out;

        if ((mkdir(path, mode) == -1) && (errno != EEXIST))
                ret = 1;
        else
                ret = 0;

out:
	umask(process_mask);
        if (up != NULL)
                free(up);
        free(q);
        free(path);
        return ret;

fail:
	umask(process_mask);
#else
/* TODO: win32 code */
#endif
	return 1;
}

/* load a file into memory */
file_t *file_load(char *filename)
{
	file_t *ft;
	FILE *f;
	char* fn;

	fn = filename;
	filename = file_path(filename);

	f = fopen(filename, "rb");
	if (f == NULL)
		return NULL;

	ft = malloc(sizeof(file_t));
	if (!ft) {
		fclose(f);
		return NULL;
	}

	/* pos is used by some of the image loading functions */
	ft->pos = 0;

	fseek(f, 0, SEEK_END);
	ft->len = ftell(f);
	fseek(f, 0, SEEK_SET);

	ft->data = malloc(ft->len+1);
	if (!ft->data) {
		fclose(f);
		free(ft);
		return NULL;
	}

	if (ft->len != fread(ft->data, 1, ft->len, f)) {
		fclose(f);
		free(ft->data);
		free(ft);
		return NULL;
	}

	ft->data[ft->len] = 0;

	fclose(f);

	ft->name = strdup(fn);

	return ft;
}

/* load a file to xml data */
xml_tag_t *file_load_xml(char* filename)
{
	xml_tag_t *x;
	file_t *f;

	f = file_load(filename);
	if (!f)
		return NULL;

	x = xml_parse((char*)f->data,NULL);
	file_free(f);

	return x;
}

/* free a loaded file */
void file_free(file_t *file)
{
	if (!file)
		return;

	if (file->data)
		free(file->data);

	if (file->name)
		free(file->name);

	free(file);
}

/* find the next occurance of value from offset in file, return it's position
 * relative to offset */
int file_find(file_t *file, int offset, unsigned char value)
{
	int r;
	for (r=offset; r<file->len; r++) {
		if (file->data[r] == value)
			return (r-offset);
	}
	return -1;
}

/* read from a file buffer to dst */
size_t file_read(file_t *f, size_t l, void* dst)
{
	size_t len;
	if (!f)
		return -1;

	len = f->len - f->pos;
	if (l < len) {
		len = l;
	}

	if (len > 0) {
		memcpy(dst, f->data+f->pos, len);
		f->pos += len;
	}

	return len;
}

/* read integer from a file buffer */
int file_read_int(file_t *f)
{
	int r;
	file_read(f,4,&r);
	return r;
}

/* read short integer from a file buffer */
short int file_read_short(file_t *f)
{
	short int r;
	file_read(f,2,&r);
	return r;
}

/* read char from a file buffer */
char file_read_char(file_t *f)
{
	char r;
	file_read(f,1,&r);
	return r;
}

/* read unsigned integer from a file buffer */
unsigned int file_read_uint(file_t *f)
{
	unsigned int r;
	file_read(f,4,&r);
	return r;
}

/* read float from a file buffer */
float file_read_float(file_t *f)
{
	float r;
	file_read(f,4,&r);
	return r;
}

/* seek to a position in a file buffer */
int file_seek(file_t *f, size_t offset, int origin)
{
	if (!f)
		return -1;

	switch (origin) {
	case SEEK_SET:
		if (f->len < offset)
			offset = f->len;

		f->pos = offset;
		break;
	case SEEK_CUR:
		offset += f->pos;
		if (f->len < offset)
			offset = f->len;

		f->pos = offset;
		break;
	case SEEK_END:
		f->pos = f->len;
		break;
	default:;
	};

	return 0;
}

/* get the position in a file buffer */
size_t file_tell(file_t *f)
{
	if (!f)
		return -1;

	return f->pos;
}

/* get the pointer to the current file position */
void *file_get(file_t *f)
{
	return f->data+f->pos;
}
