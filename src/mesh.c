/************************************************************************
* mesh.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

/* create a new mesh */
mesh_t *mesh_create(GLenum mode)
{
	mesh_t *m = malloc(sizeof(mesh_t));
	m->mode = mode;
	m->mat = NULL;
	m->col = NULL;
	m->c = NULL;
	m->v = array_create(RTG_TYPE_FLOAT);
	m->n = NULL;
	m->t = NULL;
	m->i = array_create(RTG_TYPE_INT);
	m->w = NULL;
	m->m = NULL;

	m->option = 0;

	m->vbo.state = 0;

	m->bounds.upper.x = 0.0;
	m->bounds.upper.y = 0.0;
	m->bounds.upper.z = 0.0;
	m->bounds.lower.x = 0.0;
	m->bounds.lower.y = 0.0;
	m->bounds.lower.z = 0.0;

	return m;
}

/* destroy a mesh */
void mesh_free(mesh_t *m)
{
	if (m->col)
		free(m->col);
	if (m->c)
		array_free(m->c);
	if (m->v)
		array_free(m->v);
	if (m->n)
		array_free(m->n);
	if (m->t)
		array_free(m->t);
	if (m->i)
		array_free(m->i);
	if (m->w)
		array_free(m->w);

	if (m->vbo.state) {
		if (m->vbo.colours)
			opengl_delete_buffers(1,&m->vbo.colours);
		if (m->vbo.vertices)
			opengl_delete_buffers(1,&m->vbo.vertices);
		if (m->vbo.normals)
			opengl_delete_buffers(1,&m->vbo.normals);
		if (m->vbo.texcoords)
			opengl_delete_buffers(1,&m->vbo.texcoords);
		if (m->vbo.indices)
			opengl_delete_buffers(1,&m->vbo.indices);
	}

	free(m);
}

/* create a mesh for a material */
mesh_t *mesh_create_material(material_t *mat)
{
	mesh_t *m = mesh_create(GL_TRIANGLES);
	m->mat = mat;
	m->t = array_create(RTG_TYPE_FLOAT);
	m->n = array_create(RTG_TYPE_FLOAT);
	return m;
}

/* create a mesh for a colour */
mesh_t *mesh_create_color(color_t *c)
{
	mesh_t *m = mesh_create(GL_TRIANGLES);
	m->col = malloc(sizeof(color_t));
	m->col->r = c->r;
	m->col->g = c->g;
	m->col->b = c->b;
	m->col->a = c->a;
	m->c = array_create(RTG_TYPE_FLOAT);
	m->n = array_create(RTG_TYPE_FLOAT);
	return m;
}

/* duplicate a mesh */
mesh_t *mesh_copy(mesh_t *om)
{
	mesh_t *m = mesh_create(om->mode);
	if (om->mat)
		m->mat = om->mat;
	if (om->c)
		m->c = array_copy(om->c);
	m->v = array_copy(om->v);
	if (om->n)
		m->n = array_copy(om->n);
	if (om->t)
		m->t = array_copy(om->t);
	m->i = array_copy(om->i);

	m->option = om->option;

	return m;
}

/* push a vertex onto a mesh */
int mesh_push_point(mesh_t *m, v3_t *v1)
{
	int i;
	for (i=0; i<m->v->length; i+=3) {
		if (
			v1->x == AFLOAT(m->v->data,i)
			&& v1->y == AFLOAT(m->v->data,i+1)
			&& v1->z == AFLOAT(m->v->data,i+2)
		) {
			array_push_int(m->i,i/3);
			return 0;
		}
	}
	array_push_int(m->i,i/3);
	array_push_float(m->v,v1->x);
	array_push_float(m->v,v1->y);
	array_push_float(m->v,v1->z);

	return 1;
}

/* push a poly point onto a mesh */
int mesh_push_polypoint(mesh_t *m, v3_t *v, v2_t *t)
{
	int i;
	int vi;
	int ti;
	for (i=0; (i*3)<m->v->length; i++) {
		vi = i*3;
		ti = i*2;
		if (
			v->x == AFLOAT(m->v->data,vi)
			&& v->y == AFLOAT(m->v->data,vi+1)
			&& v->z == AFLOAT(m->v->data,vi+2)
			&& t->x == AFLOAT(m->t->data,ti)
			&& t->y == AFLOAT(m->t->data,ti+1)
		) {
			array_push_int(m->i,i);
			return 0;
		}
	}
	array_push_int(m->i,i);
	array_push_v3t(m->v,v);
	array_push_v2t(m->t,t);

	return 1;
}

/* push a poly point with normal onto a mesh */
int mesh_push_polypoint_n(mesh_t *m, v3_t *v, v3_t *n, v2_t *t)
{
	int i;
	int vi;
	int ti;
	for (i=0; (i*3)<m->v->length; i++) {
		vi = i*3;
		ti = i*2;
		if (
			v->x == AFLOAT(m->v->data,vi)
			&& v->y == AFLOAT(m->v->data,vi+1)
			&& v->z == AFLOAT(m->v->data,vi+2)
			&& n->x == AFLOAT(m->n->data,vi)
			&& n->y == AFLOAT(m->n->data,vi+1)
			&& n->z == AFLOAT(m->n->data,vi+2)
			&& t->x == AFLOAT(m->t->data,ti)
			&& t->y == AFLOAT(m->t->data,ti+1)
		) {
			array_push_int(m->i,i);
			return 0;
		}
	}
	array_push_int(m->i,i);
	array_push_v3t(m->v,v);
	array_push_v3t(m->n,n);
	array_push_v2t(m->t,t);

	return 1;
}

/* push a poly onto a mesh */
void mesh_push_poly(mesh_t *m, v3_t *v1, v3_t *v2, v3_t *v3)
{
	if (mesh_push_point(m,v1)) {
		if (m->col) {
			array_push_color(m->c,m->col);
		}else if (m->mat) {
			array_push_float(m->t,0);
			array_push_float(m->t,0);
		}
	}
	if (mesh_push_point(m,v2)) {
		if (m->col) {
			array_push_color(m->c,m->col);
		}else if (m->mat) {
			array_push_float(m->t,0);
			array_push_float(m->t,1);
		}
	}
	if (mesh_push_point(m,v3)) {
		if (m->col) {
			array_push_color(m->c,m->col);
		}else if (m->mat) {
			array_push_float(m->t,1);
			array_push_float(m->t,1);
		}
	}
}

/* push a quad onto a mesh */
void mesh_push_quad(mesh_t *m, v3_t *v1, v3_t *v2, v3_t *v3, v3_t *v4)
{
	int i = m->v->length/3;
	array_push_float(m->v,v1->x);
	array_push_float(m->v,v1->y);
	array_push_float(m->v,v1->z);
	array_push_float(m->v,v2->x);
	array_push_float(m->v,v2->y);
	array_push_float(m->v,v2->z);
	array_push_float(m->v,v3->x);
	array_push_float(m->v,v3->y);
	array_push_float(m->v,v3->z);
	array_push_float(m->v,v4->x);
	array_push_float(m->v,v4->y);
	array_push_float(m->v,v4->z);

	array_push_float(m->t,0);
	array_push_float(m->t,0);
	array_push_float(m->t,0);
	array_push_float(m->t,1);
	array_push_float(m->t,1);
	array_push_float(m->t,1);
	array_push_float(m->t,1);
	array_push_float(m->t,0);

	array_push_int(m->i,i);
	array_push_int(m->i,i+1);
	array_push_int(m->i,i+3);
	array_push_int(m->i,i+1);
	array_push_int(m->i,i+2);
	array_push_int(m->i,i+3);
}
