/************************************************************************
* style.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

static char* s_style_selend(char* s, char* e)
{
	while (e > s) {
		if (isspace(*s))
			return s;
		s++;
	}
	return e;
}

static char* s_style_attstart(char* s, char* e)
{
	while (e > s) {
		if (!isspace(*s))
			return s;
		s++;
	}
	return e;
}

static style_rule_t *s_style_rule(style_rule_t **c, char* s)
{
	style_rule_t *r;

	if (*c) {
		r = *c;
		while (r) {
			if (!strcmp(r->selector,s))
				return r;
			r = r->next;
		}
	}

	r = malloc(sizeof(style_rule_t));
	r->attributes = NULL;
	r->elements = NULL;
	r->selector = strdup(s);

	r->data.x = UI_DEFAULT;
	r->data.y = UI_DEFAULT;
	r->data.w = UI_DEFAULT;
	r->data.h = UI_DEFAULT;
	r->data.visible = UI_DEFAULT;
	r->data.bg = NULL;
	r->data.has_bg = UI_DEFAULT;
	r->data.color.r = 255;
	r->data.color.g = 255;
	r->data.color.b = 255;
	r->data.color.a = 255;
	r->data.has_color = 0;
	r->data.hcolor.r = 255;
	r->data.hcolor.g = 255;
	r->data.hcolor.b = 255;
	r->data.hcolor.a = 255;
	r->data.has_hcolor = UI_DEFAULT;
	r->data.border.r = 139;
	r->data.border.g = 139;
	r->data.border.b = 0;
	r->data.border.a = 220;
	r->data.has_border = UI_DEFAULT;
	r->data.text_align = UI_DEFAULT;
	r->data.font_face = UI_DEFAULT;
	r->data.font_size = UI_DEFAULT;
	r->data.ttfont_size = UI_DEFAULT;
	r->data.has_ttbg = UI_DEFAULT;
	r->data.has_ttborder = UI_DEFAULT;
	r->data.has_ttcolor = UI_DEFAULT;
	r->data.slide = NULL;
	r->data.has_slide = UI_DEFAULT;

	r->elements = style_select(s);

	*c = list_push(c,r);

	return r;
}

static style_attribute_t *s_style_attribute(style_attribute_t **s, char* name, char* value)
{
	style_attribute_t *a;

	if (*s) {
		a = *s;
		while (a) {
			if (!strcmp(a->name,name)) {
				if (a->value)
					free(a->value);
				a->value = strdup(value);
				return a;
			}
			a = a->next;
		}
	}

	a = malloc(sizeof(style_attribute_t));
	a->name = strdup(name);
	a->value = strdup(value);

	*s = list_push(s,a);

	return a;
}

static int s_style_bool(char* str)
{
	if (!str || !strcmp(str,"default"))
		return UI_DEFAULT;

	if (!strcmp(str,"true"))
		return 1;

	if (!strcmp(str,"false"))
		return 0;

	return strtol(str,NULL,10);
}

static int s_style_strtofont(char* str)
{
	int f = font_get_id(str);
	if (!f)
		f = font_load(str,str);

	return f;
}

/* convert a string to a colour */
int style_strtocolor(char* str, color_t *c)
{
	int l;
	char r[3];
	char g[3];
	char b[3];
	char a[3];
	if (!str || !c || !strcmp(str,"default"))
		return UI_DEFAULT;

	if (!strcmp(str,"none")) {
		c->a = 0;
		return 0;
	}

	if (str[0] != '#' || (l = strlen(str)) < 7)
		return UI_DEFAULT;

	if (l == 7) {
		r[0] = str[1];
		r[1] = str[2];
		r[2] = 0;
		g[0] = str[3];
		g[1] = str[4];
		g[2] = 0;
		b[0] = str[5];
		b[1] = str[6];
		b[2] = 0;
		c->r = (unsigned char)strtol(r,NULL,16);
		c->g = (unsigned char)strtol(g,NULL,16);
		c->b = (unsigned char)strtol(b,NULL,16);
		c->a = 255;
		return 1;
	}else if (l == 9) {
		r[0] = str[1];
		r[1] = str[2];
		r[2] = 0;
		g[0] = str[3];
		g[1] = str[4];
		g[2] = 0;
		b[0] = str[5];
		b[1] = str[6];
		b[2] = 0;
		a[0] = str[7];
		a[1] = str[8];
		a[2] = 0;
		c->r = (unsigned char)strtol(r,NULL,16);
		c->g = (unsigned char)strtol(g,NULL,16);
		c->b = (unsigned char)strtol(b,NULL,16);
		c->a = (unsigned char)strtol(a,NULL,16);
		return 1;
	}

	rtprintf(CN_DEBUG "invalid colour format '%s'",str);
	return 0;
}

/* convert a colour to a string */
int style_colortostr(char* str, color_t *c)
{
	sprintf(str,"#%.2X%.2X%.2X%.2X",c->r,c->g,c->b,c->a);
	return 1;
}

/* convert a sting to an int - includes keywords like left/right/bottom/etc */
int style_strtoint(char* str)
{
	int r;
	char* p;
	if (!str || !strcmp(str,"default"))
		return UI_DEFAULT;

	if (!strcmp(str,"left")) {
		return UI_ALIGN_LEFT;
	}else if (!strcmp(str,"right")) {
		return UI_ALIGN_RIGHT;
	}else if (!strcmp(str,"centre")) {
		return UI_ALIGN_CENTRE;
	}else if (!strcmp(str,"middle")) {
		return UI_ALIGN_MIDDLE;
	}else if (!strcmp(str,"top")) {
		return UI_ALIGN_TOP;
	}else if (!strcmp(str,"bottom")) {
		return UI_ALIGN_BOTTOM;
	}else if (!strcmp(str,"mouse")) {
		return UI_ALIGN_MOUSE;
	}else if (!strcmp(str,"mouse-centre")) {
		return UI_ALIGN_MOUSE_CENTRE;
	}else if (!strcmp(str,"large")) {
		return LGE_FONT;
	}else if (!strcmp(str,"small")) {
		return SML_FONT;
	}

	p = strchr(str,'p');
	if (p)
		*p = 0;

	r = strtol(str,NULL,10);
	if (p)
		*p = 'p';

	return r;
}

/* convert an int to a string, biasing horizontal if h is non-zero */
int style_inttostr(char* str, int v, int h)
{
	switch (v) {
	case UI_ALIGN_LEFT:
		if (h) {
			strcpy(str,"left");
		}else{
			strcpy(str,"top");
		}
		break;
	case UI_ALIGN_RIGHT:
		if (h) {
			strcpy(str,"right");
		}else{
			strcpy(str,"bottom");
		}
		break;
	case UI_ALIGN_CENTRE:
		if (h) {
			strcpy(str,"centre");
		}else{
			strcpy(str,"middle");
		}
		break;
	case UI_ALIGN_MOUSE:
		strcpy(str,"mouse");
		break;
	case UI_ALIGN_MOUSE_CENTRE:
		strcpy(str,"mouse-centre");
		break;
	default:
		sprintf(str,"%dpx",v);
	}

	return 0;
}

/* convert a string to a sprite */
int style_strtobg(char* str, sprite_t **bg)
{
	if (!str || !bg || !strcmp(str,"default"))
		return UI_DEFAULT;

	if (!*bg)
		*bg = malloc(sizeof(sprite_t));

	if (str[0] == '#') {
		color_t c;
		if (style_strtocolor(str,&c)) {
			(*bg)->mat = mat_from_color(&c);
			(*bg)->x = 0;
			(*bg)->y = 0;
			(*bg)->w = 1;
			(*bg)->h = 1;
			return 1;
		}
	}else if (!strncmp(str,"file:",5)) {
		char* f = str+5;
		char* xs;
		char* ys;
		char* ws;
		char* hs;
		if ((xs = strchr(f,' '))) {
			*xs = 0;
			xs++;
			(*bg)->mat = mat_from_image(f);
			if ((ys = strchr(xs,' '))) {
				*ys = 0;
				ys++;
				if ((ws = strchr(ys,' '))) {
					*ws = 0;
					ws++;
					if ((hs = strchr(ws,' '))) {
						*hs = 0;
						hs++;
						(*bg)->y = strtol(ys,NULL,10);
						(*bg)->w = strtol(ws,NULL,10);
						(*bg)->h = strtol(hs,NULL,10);
						*(hs-1) = ' ';
					}else{
						(*bg)->y = strtol(ys,NULL,10);
						(*bg)->w = strtol(ws,NULL,10);
						(*bg)->h = (*bg)->mat->tex->h;
					}
					*(ws-1) = ' ';
				}else{
					(*bg)->y = strtol(ys,NULL,10);
					(*bg)->w = (*bg)->mat->tex->w;
					(*bg)->h = (*bg)->mat->tex->h;
				}
				*(ys-1) = ' ';
			}else{
				(*bg)->y = 0;
				(*bg)->w = (*bg)->mat->tex->w;
				(*bg)->h = (*bg)->mat->tex->h;
			}
			(*bg)->x = strtol(xs,NULL,10);
			*(xs-1) = ' ';
		}else{
			(*bg)->mat = mat_from_image(f);
			(*bg)->x = 0;
			(*bg)->y = 0;
			(*bg)->w = (*bg)->mat->tex->w;
			(*bg)->h = (*bg)->mat->tex->h;
		}
		return 1;
	}else if (!strcmp(str,"none") || !strcmp(str,"transparent")) {
		return 0;
	}

	return 0;
}

/* convert a sprite to a string */
int style_bgtostr(char* str, sprite_t *bg)
{
	if (!bg) {
		strcpy(str,"transparent");
		return 1;
	}else if (!strncmp(bg->mat->name,"rgba-",5)) {
		color_t c;
		c.r = bg->mat->diffuse[0]*255;
		c.g = bg->mat->diffuse[1]*255;
		c.b = bg->mat->diffuse[2]*255;
		c.a = bg->mat->diffuse[3]*255;
		return style_colortostr(str,&c);
	}else{
		sprintf(str,"file:%s %f %f %f %f",bg->mat->name,bg->x,bg->y,bg->w,bg->h);
		return 1;
	}
	return 0;
}

/* merge a style rule onto a widget */
void style_merge(style_t *src, widget_t *dest)
{
	if (src->x != UI_DEFAULT)
		dest->style.x = src->x;
	if (src->y != UI_DEFAULT)
		dest->style.y = src->y;
	if (src->w != UI_DEFAULT)
		dest->style.w = src->w;
	if (src->h != UI_DEFAULT)
		dest->style.h = src->h;
	if (src->visible != UI_DEFAULT) {
		if (dest->style.visible != src->visible) {
			dest->style.visible = src->visible;
			if (dest->style.visibility_changed == 1) {
				dest->style.visibility_changed = 0;
			}else if (!dest->style.visibility_changed) {
				dest->style.visibility_changed = 1;
			}
		}
	}
	if (src->text_align != UI_DEFAULT)
		dest->style.text_align = src->text_align;
	if (src->font_face != UI_DEFAULT)
		dest->style.font_face = src->font_face;
	if (src->font_size != UI_DEFAULT)
		dest->style.font_size = src->font_size;
	if (src->ttfont_size != UI_DEFAULT)
		dest->style.ttfont_size = src->ttfont_size;

	if (src->has_bg == 1) {
		if (!dest->style.bg)
			dest->style.bg = malloc(sizeof(sprite_t));

		dest->style.bg->x = src->bg->x;
		dest->style.bg->y = src->bg->y;
		dest->style.bg->w = src->bg->w;
		dest->style.bg->h = src->bg->h;
		dest->style.bg->mat = src->bg->mat;

		if (dest->style.h == UI_DEFAULT)
			dest->style.h = dest->style.bg->h;
		if (dest->style.w == UI_DEFAULT) {
			dest->style.w = dest->style.bg->w;
			if (dest->type == UI_WIDGET_CHECK)
				dest->style.w /= 2;
		}
	}else if (!src->has_bg) {
		if (dest->style.bg)
			free(dest->style.bg);
		dest->style.bg = NULL;
	}

	if (src->has_color == 1)
		dest->style.color = src->color;

	if (src->has_border == 1) {
		dest->style.border = src->border;
		dest->style.has_border = 1;
	}else if (!src->has_border) {
		dest->style.has_border = 0;
	}

	if (src->has_ttcolor == 1)
		dest->style.ttcolor = src->ttcolor;

	if (src->has_ttborder == 1) {
		dest->style.ttborder = src->ttborder;
		dest->style.has_ttborder = 1;
	}else if (!src->has_ttborder) {
		dest->style.has_ttborder = 0;
	}

	if (src->has_ttbg == 1) {
		dest->style.ttbg = src->ttbg;
		dest->style.has_ttbg = 1;
	}else if (!src->has_ttborder) {
		dest->style.has_ttbg = 0;
	}

	if (dest->type == UI_WIDGET_HSLIDE) {
		if (src->has_slide == 1) {
			if (!dest->data->sld.sld)
				dest->data->sld.sld = malloc(sizeof(sprite_t));

			dest->data->sld.sld->x = src->slide->x;
			dest->data->sld.sld->y = src->slide->y;
			dest->data->sld.sld->w = src->slide->w;
			dest->data->sld.sld->h = src->slide->h;
			dest->data->sld.sld->mat = src->slide->mat;
		}else if (!src->has_bg) {
			if (dest->data->sld.sld)
				free(dest->data->sld.sld);
			dest->data->sld.sld = NULL;
		}
	}
}

static void s_style_visibility_changes(widget_t *w)
{
	while (w) {
		if (w->style.visibility_changed) {
			w->style.visibility_changed = 0;
			if (w->style.visible) {
				w->events->show(w);
			}else{
				w->events->hide(w);
			}
		}
		if (w->type == UI_WIDGET_CONTAINER)
			s_style_visibility_changes(w->child);
		w = w->next;
	}
}

/* parse a string to a style rule */
style_attribute_t *style_parse_rule(style_rule_t *r, char* str)
{
	style_attribute_t *a = NULL;
	style_attribute_t *t;
	char* p;
	char* n;
	char* ne;
	char* v;
	char* ve;

	p = str;
	while ((ne = strchr(p,':'))) {
		*ne = 0;
		ve = strchr(ne+1,';');
		v = s_style_attstart(ne+1,ve);
		n = s_style_attstart(p,ne);
		if (!ve || !n) {
			*ne = ':';
			break;
		}
		*ve = 0;

		s_style_attribute(&a,n,v);

		*ve = ';';
		p = ve+1;
	}

	if (r) {
		t = a;
		while (t) {
			if (!strcmp(t->name,"x")) {
				r->data.x = style_strtoint(t->value);
			}else if (!strcmp(t->name,"y")) {
				r->data.y = style_strtoint(t->value);
			}else if (!strcmp(t->name,"width")) {
				r->data.w = style_strtoint(t->value);
			}else if (!strcmp(t->name,"height")) {
				r->data.h = style_strtoint(t->value);
			}else if (!strcmp(t->name,"visible")) {
				r->data.visible = s_style_bool(t->value);
			}else if (!strcmp(t->name,"text-align")) {
				r->data.text_align = style_strtoint(t->value);
			}else if (!strcmp(t->name,"font-face")) {
				r->data.font_face = s_style_strtofont(t->value);
			}else if (!strcmp(t->name,"font-size")) {
				r->data.font_size = style_strtoint(t->value);
			}else if (!strcmp(t->name,"background")) {
				r->data.has_bg = style_strtobg(t->value,&r->data.bg);
			}else if (!strcmp(t->name,"color")) {
				r->data.has_color = style_strtocolor(t->value,&r->data.color);
			}else if (!strcmp(t->name,"border")) {
				r->data.has_border = style_strtocolor(t->value,&r->data.border);
			}else if (!strcmp(t->name,"ttcolor")) {
				r->data.has_ttcolor = style_strtocolor(t->value,&r->data.ttcolor);
			}else if (!strcmp(t->name,"ttbackground")) {
				r->data.has_ttbg = style_strtocolor(t->value,&r->data.ttbg);
			}else if (!strcmp(t->name,"ttborder")) {
				r->data.has_ttborder = style_strtocolor(t->value,&r->data.ttborder);
			}else if (!strcmp(t->name,"ttfont-size")) {
				r->data.ttfont_size = style_strtoint(t->value);
			}else if (!strcmp(t->name,"slide")) {
				r->data.has_slide = style_strtobg(t->value,&r->data.slide);
			}else{
				rtprintf(CN_DEBUG "unknown style attribute '%s'",t->name);
			}
			t = t->next;
		}
	}

	return a;
}

/* parse a string to style rules */
style_rule_t *style_parse(char* str)
{
	style_rule_t *r = NULL;
	style_rule_t *cr;
	style_attribute_t *a;
	char* c;
	char* rl;
	char* s;
	char* se;
	char sec;

	c = strtok(str,"}");

	do {
		rl = strchr(c,'{');
		if (rl) {
			*rl = 0;
			s = s_style_attstart(c,rl);
			se = s_style_selend(s,rl);
			sec = *se;
			*se = 0;
			cr = s_style_rule(&r,s);
			a = style_parse_rule(cr,rl+1);
			if (a)
				cr->attributes = a;
			*se = sec;
			*rl = '{';
		}
	} while ((c = strtok(NULL,"}")));

	return r;
}

/* refresh widget styles */
void style_refresh(style_rule_t *rules, xml_tag_t *tags)
{
	style_rule_t *r = rules;
	widget_t **w;
	int i;

	while (r) {
		array_free(r->elements);
		r->elements = style_select(r->selector);
		w = r->elements->data;
		for (i=0; i<r->elements->length; i++) {
			if (w[i])
				style_merge(&r->data,w[i]);
		}
		r = r->next;
	}

	s_style_visibility_changes(containers);
}
