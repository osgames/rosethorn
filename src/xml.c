/************************************************************************
* xml.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

/* create an xml tag */
xml_tag_t *xml_tag_create(char* t, xml_tag_t *p)
{
	xml_tag_t *x = malloc(sizeof(xml_tag_t));
	x->tag = strdup(t);
	x->attributes = NULL;
	x->text = NULL;

	x->data = NULL;

	x->next = NULL;
	x->prev = NULL;
	x->child = NULL;
	if (p && p->tag) {
		x->parent = p;
	}else{
		x->parent = NULL;
	}

	if (p)
		p->child = list_push(&p->child,x);

	return x;
}

/* add or change an attribute */
void xml_attribute_write(xml_tag_t *t, char* name, char* value)
{
	xml_attribute_t *l = NULL;
	xml_attribute_t *a = t->attributes;
	while (a) {
		if (!strcmp(a->name,name)) {
			free(a->value);
			a->value = strdup(value);
			return;
		}
		l = a;
		a = a->next;
	}

	a = malloc(sizeof(xml_attribute_t));
	a->name = strdup(name);
	a->value = strdup(value);
	a->next = NULL;
	if (l) {
		l->next = a;
	}else{
		t->attributes = a;
	}
}

/* find the next tag in a string */
static char* s_xml_next_tag(char* str)
{
	char* t;
	t = strchr(str,'<');
	if (!t)
		return NULL;

	/* ignore comments */
	if (!strncmp(t,"<!-- ",5)) {
		t = strstr(t," -->");
		return s_xml_next_tag(t+4);
	}

	/* ignore xml headers, and closing tags */
	if (!strncmp(t,"<?xml ",6) || !strncmp(t,"</",2))
		return s_xml_next_tag(t+1);

	return t;
}

/* find the closing tag */
static char* s_xml_tag_closure(char* str, char* name)
{
	char* d = NULL;
	char* r;
	char* t;
	char* close;
	char* open;
	int l;
	if (!str || !str[0] || !name || !name[0])
		return NULL;

	l = strlen(name)+5;

	close = alloca(l);
	open = alloca(l);
	r = strchr(str,'\n');
	if (r)
		*r = 0;

	d = strstr(str,"/>");
	if (d) {
		*d = 0;
		if (!(t = strchr(str+1,'<')) || t[1] == ' ') {
			*d = '/';
			if (r)
				*r = '\n';
			return d;
		}
		*d = '/';
	}

	if (r)
		*r = '\n';

	sprintf(open,"<%s",name);
	sprintf(close,"</%s>",name);
	r = str+1;

	while (*r && (d = strstr(r,close)) && (t = strstr(r,open)) && t < d) {
		r = d+1;
	}

	return d;
}

/* find the close of the current tag */
static char* s_xml_tag_close(char* str)
{
	char* t;
	char* e;
	char ec;
	int l;
	if (!str || !str[0])
		return NULL;

	str++;

	t = strchr(str,' ');
	e = strchr(str,'>');
	if (!t) {
		if (!e)
			return NULL;
		t = e;
	}else if (e && e < t) {
		t = e;
	}

	e = t;
	ec = *e;
	*e = 0;

	l = strlen(t)+1;

	t = alloca(l);

	if (str[0] == '<')
		str++;

	strcpy(t,str);

	*e = ec;

	return s_xml_tag_closure(str,t);
}

/* parse an xml string */
xml_tag_t *xml_parse(char* str, xml_tag_t *p)
{
	xml_tag_t *x = NULL;
	xml_tag_t *cx = NULL;
	xml_tag_t tp;
	char* t;
	char* c;
	char* e;
	char cc;
	char* n;
	char* ne;
	char* nes;
	char ncc;
	char* a;
	char* ae;
	char* v;
	char* ve;
	char* xp = str;
	tp.child = NULL;
	tp.tag = NULL;
	if (!p)
		p = &tp;

	while ((t = s_xml_next_tag(xp))) {
		c = s_xml_tag_close(t);
		if (!c)
			break;

		cc = *c;
		*c = 0;
		if (cc == '<') {
			e = strchr(t,'>');
			if (!e) {
				*c = cc;
				break;
			}
		}else{
			e = c-1;
		}

		*e = 0;
		n = t+1;
		ne = e;
		nes = strchr(n,' ');
		if (nes && ne > nes)
			ne = nes;

		ncc = *ne;
		*ne = 0;
		cx = xml_tag_create(n,p);
		if (!x)
			x = cx;
		if (ncc == ' ')
			*ne = ncc;

		xp = ne;
		while ((ae = strchr(xp,'='))) {
			*ae = 0;
			v = ae+2;
			ve = strchr(v,'"');
			a = strrchr(xp,' ');
			if (!ve || !a) {
				*ae = '=';
				break;
			}
			*ve = 0;
			a++;

			xml_attribute_write(cx,a,v);

			*ve = '"';
			*ae = '=';
			xp = ve;
		}

		cx->text = strdup(e+1);

		xml_parse(e+1,cx);
		*e = '>';
		*c = cc;
		xp = c;
	}

	return x;
}

/* free an xml tree */
void xml_free(xml_tag_t *t)
{
	xml_tag_t *n;
	xml_attribute_t *a;
	xml_attribute_t *an;

	while (t) {
		n = t->next;

		if (t->child)
			xml_free(t->child);

		if (t->tag)
			free(t->tag);

		if (t->text)
			free(t->text);

		a = t->attributes;
		while (a) {
			an = a->next;

			if (a->name)
				free(a->name);

			if (a->value)
				free(a->value);

			free(a);

			a = an;
		}

		free(t);

		t = n;
	}
}

/* print an xml structure */
void xml_print(xml_tag_t *t, int s, FILE *f)
{
	xml_attribute_t *a;
	char* buff = alloca(s+1);
	memset(buff,'\t',s);
	buff[s] = 0;
	if (!f)
		f = stdout;
	if (!s)
		fputs("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n",f);
	while (t) {
		fprintf(f,"%s<%s",buff,t->tag);
		a = t->attributes;
		while (a) {
			fprintf(f," %s=\"%s\"",a->name,a->value);
			a = a->next;
		}
		if (t->text && !t->child) {
			fprintf(f,">%s</%s>\n",t->text,t->tag);
		}else if (t->child) {
			fputs(">\n",f);
			xml_print(t->child,s+1,f);
			fprintf(f,"</%s>\n",t->tag);
		}else{
			fputs(" />\n",f);
		}
		t = t->next;
	}
}

/* find an attribute in a tag */
char* xml_attribute(xml_tag_t *tag, char* name)
{
	xml_attribute_t *a;
	if (!tag)
		return NULL;

	a = tag->attributes;
	while (a) {
		if (!strcmp(a->name,name))
			return a->value;
		a = a->next;
	}

	return NULL;
}

/* find an attribute in a tag and return it's integer value */
int xml_attribute_int(xml_tag_t *tag, char* name)
{
	xml_attribute_t *a;
	if (!tag)
		return 0;

	a = tag->attributes;
	while (a) {
		if (!strcmp(a->name,name) && a->value)
			return strtol(a->value,NULL,10);
		a = a->next;
	}

	return 0;
}

/* find an attribute in a tag and return it's floating point value */
float xml_attribute_float(xml_tag_t *tag, char* name)
{
	xml_attribute_t *a;
	if (!tag)
		return 0.0;

	a = tag->attributes;
	while (a) {
		if (!strcmp(a->name,name) && a->value)
			return strtof(a->value,NULL);
		a = a->next;
	}

	return 0.0;
}

/* find a tag where attribute name = value */
xml_tag_t *xml_search_attribute(xml_tag_t *tag, char* name, char* value)
{
	char* v;
	xml_tag_t* r;

	while (tag) {
		v = xml_attribute(tag,name);
		if (v && !strcmp(v,value))
			return tag;
		if (tag->child) {
			r = xml_search_attribute(tag->child,name,value);
			if (r)
				return r;
		}
		tag = tag->next;
	}

	return NULL;
}

/* how many children does this tag have? */
int xml_child_count(xml_tag_t *tag)
{
	xml_tag_t *t = tag->child;
	int r = 0;
	while (t) {
		r++;
		t = t->next;
	}
	return r;
}
