/************************************************************************
* math_vector.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

/* get a vector from start/end points */
void vect_create(v3_t *start, v3_t *end, v3_t *v)
{
	v->x = end->x - start->x;
	v->y = end->y - start->y;
	v->z = end->z - start->z;
	vect_normalise(v);
}

/* get the length of a vector */
float vect_length(v3_t *v)
{
	return sqrtf(v->x*v->x + v->y*v->y + v->z*v->z);
}

/* normalise a vector */
void vect_normalise(v3_t *v)
{
	float l;
	l= vect_length(v);
	if (l==0)
		l = 1;

	v->x/=l;
	v->y/=l;
	v->z/=l;
}

/* get the scalar product of vectors */
float vect_scalarproduct(v3_t *v1, v3_t *v2)
{
    return (v1->x*v2->x + v1->y*v2->y + v1->z*v2->z);
}

/* get the cross product of vectors */
void vect_crossproduct(v3_t *v1, v3_t *v2, v3_t *v3)
{
	v3->x=(v1->y*v2->z)-(v1->z*v2->y);
	v3->y=(v1->z*v2->x)-(v1->x*v2->z);
	v3->z=(v1->x*v2->y)-(v1->y*v2->x);
}

/* get the dot product of vectors */
void vect_dotproduct(v3_t *v1, v3_t *v2, v3_t *v3)
{
	v3->x=(v1->y*v2->z)+(v1->z*v2->y);
	v3->y=(v1->z*v2->x)+(v1->x*v2->z);
	v3->z=(v1->x*v2->y)+(v1->y*v2->x);
}

/* subtract a vector from a vector */
void vect_subtract(v3_t *v1, v3_t *v2, v3_t *v3)
{
	v3->x = (v1->x-v2->x);
	v3->y = (v1->y-v2->y);
	v3->z = (v1->z-v2->z);
}

/* get the diameter of a vector */
float vect_diameter(v3_t *v)
{
	return sqrtf(vect_scalarproduct(v,v));
}

/* get the dot product of vectors */
float math_dotproduct(v3_t *v1, v3_t *v2)
{
	return (v1->x*v2->x + v1->y*v2->y + v1->z*v2->z);
}

/* get the distance between two points */
float math_distance(v3_t *v1, v3_t *v2)
{
	float xd = v2->x-v1->x;
	float yd = v2->y-v1->y;
	float zd = v2->z-v1->z;

	if (xd < 0.0)
		xd *= -1.0;
	if (yd < 0.0)
		yd *= -1.0;
	if (zd < 0.0)
		zd *= -1.0;

	return sqrtf(xd*xd + yd*yd + zd*zd);
}

/* normalise a mesh */
void mesh_normalise(mesh_t *m)
{
	int i;
	GLuint k[9];
	v3_t v1;
	v3_t v2;
	v3_t v3;
	v3_t b1;
	v3_t b2;
	v3_t normal;
	int *con = alloca(m->v->length*sizeof(int));
	memset(con,0,m->v->length*sizeof(int));

	if (m->n) {
		m->n->length = 0;
	}else{
		m->n = array_create(RTG_TYPE_FLOAT);
	}

	for (i=0; i<m->v->length; i++) {
		array_push_float(m->n,0);
	}

	for (i=0; i<m->i->length; i+=3) {
		k[0] = AUINT(m->i->data,i)*3;
		k[1] = k[0]+1;
		k[2] = k[0]+2;
		k[3] = AUINT(m->i->data,i+1)*3;
		k[4] = k[3]+1;
		k[5] = k[3]+2;
		k[6] = AUINT(m->i->data,i+2)*3;
		k[7] = k[6]+1;
		k[8] = k[6]+2;
		v1.x = AFLOAT(m->v->data,k[0]);
		v1.y = AFLOAT(m->v->data,k[1]);
		v1.z = AFLOAT(m->v->data,k[2]);
		v2.x = AFLOAT(m->v->data,k[3]);
		v2.y = AFLOAT(m->v->data,k[4]);
		v2.z = AFLOAT(m->v->data,k[5]);
		v3.x = AFLOAT(m->v->data,k[6]);
		v3.y = AFLOAT(m->v->data,k[7]);
		v3.z = AFLOAT(m->v->data,k[8]);

		b1.x = v2.x-v1.x;
		b1.y = v2.y-v1.y;
		b1.z = v2.z-v1.z;

		b2.x = v3.x-v1.x;
		b2.y = v3.y-v1.y;
		b2.z = v3.z-v1.z;

		vect_crossproduct(&b1,&b2,&normal);
		vect_normalise(&normal);

		con[k[0]] += 1;
		con[k[1]] += 1;
		con[k[2]] += 1;

		AFLOAT(m->n->data,k[0]) += normal.x;
		AFLOAT(m->n->data,k[1]) += normal.y;
		AFLOAT(m->n->data,k[2]) += normal.z;
		AFLOAT(m->n->data,k[3]) += normal.x;
		AFLOAT(m->n->data,k[4]) += normal.y;
		AFLOAT(m->n->data,k[5]) += normal.z;
		AFLOAT(m->n->data,k[6]) += normal.x;
		AFLOAT(m->n->data,k[7]) += normal.y;
		AFLOAT(m->n->data,k[8]) += normal.z;
	}

	for (i=0; i<m->n->length; i+=3) {
		if (con[i]>1) {
			AFLOAT(m->n->data,i) /= con[i];
			AFLOAT(m->n->data,i+1) /= con[i];
			AFLOAT(m->n->data,i+2) /= con[i];
		}
	}
}

/* normalise an object */
void object_normalise(object_t *o)
{
	int i;
	mesh_t *m;
	for (i=0; i<o->meshes->length; i++) {
		m = ((mesh_t**)o->meshes->data)[i];
		mesh_normalise(m);
	}
}

/* calculate the radius of an object */
float object_radius(object_t *o)
{
	int i;
	int k;
	int l;
	mesh_t **m;
	v3_t *v;
	float t = 0.0;
	float r = 0.0;

	m = o->meshes->data;
	for (i=0; i<o->meshes->length; i++) {
		v = m[i]->v->data;
		l = m[i]->v->length/3;
		m[i]->radius = 0.0;
		for (k=0; k<l; k++) {
			t = vect_diameter(&v[k]);
			if (t > m[i]->radius)
				m[i]->radius = t;
		}
		if (m[i]->radius > r)
			r = m[i]->radius;
	}
	return r;
}

/* calculate the bounding box for a mesh */
int mesh_bounds(mesh_t *m)
{
	int i;
	v3_t *v = m->v->data;
	int l = m->v->length/3;
	aabb_t *b = &m->bounds;

	if (
		b->upper.x != 0.0
		&& b->upper.y != 0.0
		&& b->upper.z != 0.0
		&& b->lower.x != 0.0
		&& b->lower.y != 0.0
		&& b->lower.z != 0.0
	)
		return 0;

	b->upper.x = 0.0;
	b->upper.y = 0.0;
	b->upper.z = 0.0;
	b->lower.x = 0.0;
	b->lower.y = 0.0;
	b->lower.z = 0.0;

	for (i=0; i<l; i++) {
		if (v[i].x < b->lower.x)
			b->lower.x = v[i].x;
		if (v[i].y < b->lower.y)
			b->lower.y = v[i].y;
		if (v[i].z < b->lower.z)
			b->lower.z = v[i].z;
		if (v[i].x > b->upper.x)
			b->upper.x = v[i].x;
		if (v[i].y > b->upper.y)
			b->upper.y = v[i].y;
		if (v[i].z > b->upper.z)
			b->upper.z = v[i].z;
	}

	return 0;
}

/* calculate the bounding box for an object */
int object_bounds(object_t *o)
{
	int i;
	int r = 1;
	mesh_t *m;
	aabb_t *b = &o->bounds;

	b->upper.x = 0.0;
	b->upper.y = 0.0;
	b->upper.z = 0.0;
	b->lower.x = 0.0;
	b->lower.y = 0.0;
	b->lower.z = 0.0;

	for (i=0; i<o->meshes->length; i++) {
		m = ((mesh_t**)o->meshes->data)[i];
		if (mesh_bounds(m))
			continue;
		if (m->bounds.lower.x < b->lower.x)
			b->lower.x = m->bounds.lower.x;
		if (m->bounds.lower.y < b->lower.y)
			b->lower.y = m->bounds.lower.y;
		if (m->bounds.lower.z < b->lower.z)
			b->lower.z = m->bounds.lower.z;
		if (m->bounds.upper.x > b->upper.x)
			b->upper.x = m->bounds.upper.x;
		if (m->bounds.upper.y > b->upper.y)
			b->upper.y = m->bounds.upper.y;
		if (m->bounds.upper.z > b->upper.z)
			b->upper.z = m->bounds.upper.z;
		r = 0;
	}

	return r;
}
