/************************************************************************
* ui_xml.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

static widget_t *ui_xml_label(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_LABEL,parent);
	tag->data = w;
	w->x = tag;
	ui_widget_value(w,tag->text);
	w->style.w = print_length(1,SML_FONT,w->text);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_button(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_BUTTON,parent);
	tag->data = w;
	w->x = tag;
	ui_widget_value(w,tag->text);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_text(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_TEXT,parent);
	tag->data = w;
	w->x = tag;
	ui_widget_value(w,tag->text);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_num(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_NUM,parent);
	tag->data = w;
	w->x = tag;
	ui_widget_value(w,tag->text);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}

	w->data->num.max = xml_attribute_int(tag,"max");
	w->data->num.min = xml_attribute_int(tag,"min");

	return w;
}

static widget_t *ui_xml_check(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_CHECK,parent);
	tag->data = w;
	w->x = tag;

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_console(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_CONSOLE,parent);
	tag->data = w;
	w->x = tag;

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_header(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_HEADER,parent);
	tag->data = w;
	w->x = tag;
	ui_widget_value(w,tag->text);
	w->style.w = print_length(1,LGE_FONT,w->text);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_pbar(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_PBAR,parent);
	tag->data = w;
	w->x = tag;
	v = xml_attribute(tag,"max");
	if (v)
		w->data->pbr.max = strtol(v,NULL,10);
	v = xml_attribute(tag,"value");
	if (v)
		w->data->pbr.value = strtol(v,NULL,10);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_textbox(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_TEXTBOX,parent);
	tag->data = w;
	w->x = tag;
	ui_widget_value(w,tag->text);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}
	return w;
}

static widget_t *ui_xml_hslide(xml_tag_t *tag, widget_t *parent)
{
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_HSLIDE,parent);
	tag->data = w;
	w->x = tag;
	v = xml_attribute(tag,"value");
	if (v)
		w->data->sld.value = strtol(v,NULL,10);
	v = xml_attribute(tag,"min");
	if (v)
		w->data->sld.min = strtol(v,NULL,10);
	v = xml_attribute(tag,"max");
	if (v)
		w->data->sld.max = strtol(v,NULL,10);

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}

	return w;
}

static widget_t *ui_xml_row(xml_tag_t *tag, widget_t *parent)
{
	xml_tag_t *t;
	int i;
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_LISTROW,parent);
	tag->data = w;
	w->x = tag;

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}

	t = tag->child;
	for (i=0; t && i<parent->data->lst.col_cnt; i++) {
		if (strcmp(t->tag,"column")) {
			rtprintf(CN_RTG "invalid ui list row element '%s'",t->tag);
			i--;
			continue;
		}
		w->data->row.values[i] = t->text;
		t = t->next;
	}

	parent->data->lst.rows++;

	return w;
}

static widget_t *ui_xml_list(xml_tag_t *tag, widget_t *parent)
{
	xml_tag_t *t;
	xml_tag_t *st;
	int c;
	char* v;
	widget_t *w = ui_widget_create(UI_WIDGET_LIST,parent);
	w->style.w = 0;
	w->style.h = 50;
	tag->data = w;
	w->x = tag;
	t = tag->child;

	if ((v = xml_attribute(tag,"hover"))) {
		ui_widget_tooltip(w,v);
	}else if ((v = xml_attribute(tag,"tooltip"))) {
		ui_widget_tooltip(w,v);
	}

	while (t) {
		if (!strcmp(t->tag,"head")) {
			c = xml_child_count(t);
			w->data->lst.col_cnt = c;
			w->data->lst.columns = malloc(sizeof(list_column_t)*c);
			st = t->child;
			for (c=0; st && c<w->data->lst.col_cnt; c++) {
				if (strcmp(st->tag,"column")) {
					rtprintf(CN_DEBUG "invalid ui list head element '%s'",st->tag);
					c--;
					continue;
				}
				if (st->text) {
					w->data->lst.columns[c].name = strdup(st->text);
				}else{
					w->data->lst.columns[c].name = NULL;
				}
				v = xml_attribute(st,"width");
				if (v) {
					w->data->lst.columns[c].width = strtol(v,NULL,10);
				}else{
					w->data->lst.columns[c].width = print_length(1,SML_FONT,w->data->lst.columns[c].name)+10;
				}
				w->style.w += w->data->lst.columns[c].width;
				st = st->next;
			}
		}else if (!strcmp(t->tag,"row")) {
			ui_xml_row(t,w);
		}else{
			rtprintf(CN_DEBUG "invalid ui list element '%s'",t->tag);
		}
		t = t->next;
	}
	return w;
}

static widget_t *ui_xml_container(xml_tag_t *tag)
{
	xml_tag_t *t;
	char* n;
	widget_t *w = ui_widget_container();
	tag->data = w;
	w->x = tag;
	t = tag->child;
	while (t) {
		if (!strcmp(t->tag,"label")) {
			ui_xml_label(t,w);
		}else if (!strcmp(t->tag,"button")) {
			ui_xml_button(t,w);
		}else if (!strcmp(t->tag,"text")) {
			ui_xml_text(t,w);
		}else if (!strcmp(t->tag,"num")) {
			ui_xml_num(t,w);
		}else if (!strcmp(t->tag,"check")) {
			ui_xml_check(t,w);
		}else if (!strcmp(t->tag,"console")) {
			ui_xml_console(t,w);
		}else if (!strcmp(t->tag,"header")) {
			ui_xml_header(t,w);
		}else if (!strcmp(t->tag,"pbar")) {
			ui_xml_pbar(t,w);
		}else if (!strcmp(t->tag,"textbox")) {
			ui_xml_textbox(t,w);
		}else if (!strcmp(t->tag,"slide")) {
			if (!(n = xml_attribute(t,"type")) || !strcmp(n,"horizontal")) {
				ui_xml_hslide(t,w);
			}else{
				rtprintf(CN_DEBUG "unknown ui slide type '%s'",n);
			}
		}else if (!strcmp(t->tag,"list")) {
			ui_xml_list(t,w);
		}else{
			rtprintf(CN_DEBUG "unknown ui element type '%s'",t->tag);
		}
		t = t->next;
	}
	return w;
}

/* create ui from an xml file */
xml_tag_t *ui_xml_load(char* fn)
{
	xml_tag_t *x;
	xml_tag_t *t;
	style_rule_t *s;
	char* a;
	if (!fn)
		return NULL;

	x = file_load_xml(fn);
	if (!x)
		return NULL;

	t = x;
	while (t) {
		if (!strcmp(t->tag,"container")) {
			ui_xml_container(t);
		}else if (!strcmp(t->tag,"style")) {
			s = style_parse(t->text);
			if (s) {
				if (ui.init) {
					ui.styles = list_append(&ui.styles,s);
				}else{
					style_rule_t *tsr = ui.styles;
					ui.styles = NULL;
					ui.styles = list_append(&ui.styles,s);
					if (tsr)
						ui.styles = list_append(&ui.styles,tsr);
					ui.init = 1;
				}
			}
		}else if (!strcmp(t->tag,"script")) {
			a = xml_attribute(t,"type");
			if (a) {
				rtprintf(CN_DEBUG "unknown ui script type '%s'",a);
			}else{
				rtprintf(CN_DEBUG "unknown ui script type");
			}
		}else{
			rtprintf(CN_DEBUG "unknown ui base element '%s'",t->tag);
		}
		t = t->next;
	}

	ui.elements = list_append(&ui.elements,x);

	style_refresh(ui.styles,ui.elements);

	return x;
}
