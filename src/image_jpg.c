/************************************************************************
* image_png.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"

#ifdef RTG_HAVE_JPG

#include <jpeglib.h>
#include <setjmp.h>

/* check for a jpg image */
int image_is_jpg(file_t *f)
{
	if (f->data[0] == 0xFF && f->data[1] == 0xD8)
		return 1;

	return 0;
}

/* error management things */
typedef struct jpg_error_mgr {
	struct jpeg_error_mgr pub;
	jmp_buf setjmp_buffer;
} jpg_error_mgr_t;

static void jpg_error_exit(j_common_ptr cinfo)
{
	jpg_error_mgr_t *err = (jpg_error_mgr_t*)cinfo->err;
	longjmp(err->setjmp_buffer, 1);
}

/* load a jpeg image */
int image_load_jpg(file_t *f, image_t *p)
{
	struct jpeg_decompress_struct cinfo;
	jpg_error_mgr_t jerr;
	JSAMPROW buffer[1];
	int row_stride;
	int i;

	/* error handling */
	cinfo.err = jpeg_std_error(&jerr.pub);
	jerr.pub.error_exit = jpg_error_exit;
	/* setjmp, just like png, return here if something goes wrong,
	 * and (hopefully) exit gracefully */
	if (setjmp(jerr.setjmp_buffer)) {
		jpeg_destroy_decompress(&cinfo);
		return 1;
	}

	/* create the decompress struct, so we can start doing things */
	jpeg_create_decompress(&cinfo);

	/* read from file_t */
	jpeg_mem_src(&cinfo, f->data, f->len);

	/* get image info */
	jpeg_read_header(&cinfo, TRUE);

	/* begin, and store the width/height */
	jpeg_start_decompress(&cinfo);
	p->w = cinfo.output_width;
	p->h = cinfo.output_height;

	/* get the length of a row, and allocate the pixel data */
	row_stride = cinfo.output_width*cinfo.output_components;
	p->pixels = malloc(cinfo.output_width*cinfo.output_height*4);

	/* read it in, align the buffer to the correct pixel data */
	for (i=0; cinfo.output_scanline < cinfo.output_height; i+=row_stride) {
		buffer[0] = p->pixels+i;
		jpeg_read_scanlines(&cinfo, buffer, 1);
	}

	/* finish */
	jpeg_finish_decompress(&cinfo);

	/* ensure it's in RGBA format */
	if (cinfo.output_components != 4) {
		/* RGB image */
		if (cinfo.output_components == 3) {
			int j = 0;
			int m = row_stride*cinfo.output_height;
			unsigned char *px = p->pixels;
			p->pixels = malloc(p->w*p->h*4);
			for (i=0; i<m; i+=3) {
				p->pixels[j++] = px[i];
				p->pixels[j++] = px[i+1];
				p->pixels[j++] = px[i+2];
				p->pixels[j++] = 255;
			}
			free(px);
		/* unsupported image */
		}else{
			free(p->pixels);
			return 1;
		}
	}

	/* free */
	jpeg_destroy_decompress(&cinfo);

	return 0;
}

/* save image to a jpeg file */
int image_save_jpg(image_t *p, char* file)
{
	struct jpeg_compress_struct cinfo;
	jpg_error_mgr_t jerr;
	FILE *f;
	JSAMPROW row_pointer[1];
	int row_stride;
	int i;
	int j;
	int m;
	unsigned char* px;
	char* fn = file_path(file);

	/* error handling */
	cinfo.err = jpeg_std_error(&jerr.pub);
	jerr.pub.error_exit = jpg_error_exit;
	/* setjmp, just like png, return here if something goes wrong,
	 * and (hopefully) exit gracefully */
	if (setjmp(jerr.setjmp_buffer)) {
		jpeg_destroy_compress(&cinfo);
		return 1;
	}

	/* create the compression struct */
	jpeg_create_compress(&cinfo);

	f= fopen(fn,"wb");
	if (!f)
		return 1;

	jpeg_stdio_dest(&cinfo, f);

	/* set up some info */
	cinfo.image_width = p->w;
	cinfo.image_height = p->h;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;
	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, 85, TRUE);

	/* begin */
	jpeg_start_compress(&cinfo, TRUE);

	/* libjpeg doesn't understand RGBA so convert to RGB */
	row_stride = p->w*3;
	m = row_stride*p->h;
	px = alloca(m);
	for (i=0,j=0; i<m; i+=3) {
		px[i] = p->pixels[j++];
		px[i+1] = p->pixels[j++];
		px[i+2] = p->pixels[j++];
		j++;
	}

	for (i=0; cinfo.next_scanline < cinfo.image_height; i+=row_stride) {
		row_pointer[0] = px+i;
		jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	/* finish */
	jpeg_finish_compress(&cinfo);
	fclose(f);

	/* free */
	jpeg_destroy_compress(&cinfo);

	return 1;
}

#endif
