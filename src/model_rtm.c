/************************************************************************
* model_rtm.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

#define RTM_FILE_ID		"RoseThorn Model"
#define RTM_VERSION_MAJOR_MASK	0x00FF00
#define RTM_VERSION_MINOR_MASK	0x0000FF
#define RTM_VERSION_CURRENT	0x0002

#define RTM_CHUNK_VOID		0X00
#define RTM_CHUNK_MESH		0x01
#define RTM_CHUNK_INDEX		0x02
#define RTM_CHUNK_MESHMATERIAL	0x03
#define RTM_CHUNK_VERTEX	0x04
#define RTM_CHUNK_NORMAL	0x05
#define RTM_CHUNK_TEXCOORD	0x06
#define RTM_CHUNK_COLORLIST	0x10
#define RTM_CHUNK_COLOR		0x11
#define RTM_CHUNK_WEIGHTMAP	0x12
#define RTM_CHUNK_WEIGHTS	0x13

#define RTM_CHUNK_MATERIAL	0x20
#define RTM_CHUNK_TEXTURE	0x21
#define RTM_CHUNK_AMBIENT	0x22
#define RTM_CHUNK_DIFFUSE	0x23
#define RTM_CHUNK_SPECULAR	0x24
#define RTM_CHUNK_EMISSION	0x25
#define RTM_CHUNK_IMAGE		0x26

#define RTM_CHUNK_SKELETON	0x30
#define RTM_CHUNK_FRAME		0x31

typedef struct rtm_file_header_s {
	char id[16];
	unsigned int version;
	unsigned int data_header;
} __attribute__ ((packed)) rtm_file_header_t;

typedef struct rtm_data_header1_s {
	unsigned int meshes;
	unsigned int materials;
	unsigned int chunks;
	unsigned int chunk_start;
} __attribute__ ((packed)) rtm_data_header1_t;

typedef struct rtm_data_header_s {
	unsigned int meshes;
	unsigned int materials;
	unsigned int skeletons;
	unsigned int chunks;
	unsigned int chunk_start;
} __attribute__ ((packed)) rtm_data_header_t;

typedef struct rtm_chunk_ref_s {
	unsigned int type;
	unsigned int start;
} __attribute__ ((packed)) rtm_chunk_ref_t;

typedef struct rtm_chunk_s {
	unsigned int type;
	unsigned int size;
	unsigned int count;
} __attribute__ ((packed)) rtm_chunk_t;

/* load a rosethorn model */
model_t *model_load_rtm(file_t *f)
{
	model_t *mdl = model_create();
	material_t *mat;
	mesh_t *m;
	image_t *img;
	rtm_file_header_t fh;
	rtm_data_header_t dh;
	rtm_chunk_ref_t *cr;
	rtm_chunk_t *ch;
	weight_t *wt;
	frame_t *fr;
	skeleton_t *sk;
	joint_t *jt;
	unsigned int s;
	int i;
	int j;
	int k;
	int cnt;
	char* name;
	float c[4];

	file_read(f,sizeof(rtm_file_header_t),&fh);

	/* load the data header */
	if (fh.version == RTM_VERSION_CURRENT) {
		file_read(f,sizeof(rtm_data_header_t),&dh);
	/* version 0.1 this had less data header fields */
	}else if (fh.version == 0x0001) {
		rtm_data_header1_t dh1;
		file_read(f,sizeof(rtm_data_header1_t),&dh1);
		dh.meshes = dh1.meshes;
		dh.materials = dh1.materials;
		dh.skeletons = 0;
		dh.chunks = dh1.chunks;
		dh.chunk_start = dh1.chunk_start;
	}

	if (dh.skeletons)
		mdl->step = object_advance_frame;

	cr = file_get(f);

	for (i=0; i<dh.chunks; i++) {
		if (cr[i].type == RTM_CHUNK_MATERIAL) {
			file_seek(f,cr[i].start,SEEK_SET);
			ch = file_get(f);
			file_seek(f,sizeof(rtm_chunk_t),SEEK_CUR);
			name = file_get(f);
			mat = mat_find_or_create(name);
			file_seek(f,ch->size,SEEK_CUR);
			cnt = ch->count;
			for (k=0; k<cnt; k++) {
				ch = file_get(f);
				file_seek(f,sizeof(rtm_chunk_t),SEEK_CUR);
				switch (ch->type) {
				case RTM_CHUNK_AMBIENT:
					file_read(f,sizeof(float),&c[0]);
					file_read(f,sizeof(float),&c[1]);
					file_read(f,sizeof(float),&c[2]);
					file_read(f,sizeof(float),&c[3]);
					mat_amb3f(mat,c[0],c[1],c[2]);
					break;
				case RTM_CHUNK_DIFFUSE:
					file_read(f,sizeof(float),&c[0]);
					file_read(f,sizeof(float),&c[1]);
					file_read(f,sizeof(float),&c[2]);
					file_read(f,sizeof(float),&c[3]);
					mat_dif3f(mat,c[0],c[1],c[2]);
					mat_alf(mat,c[3]);
					break;
				case RTM_CHUNK_SPECULAR:
					file_read(f,sizeof(float),&c[0]);
					file_read(f,sizeof(float),&c[1]);
					file_read(f,sizeof(float),&c[2]);
					file_read(f,sizeof(float),&c[3]);
					mat_spc3f(mat,c[0],c[1],c[2]);
					break;
				case RTM_CHUNK_EMISSION:
					file_read(f,sizeof(float),&c[0]);
					file_read(f,sizeof(float),&c[1]);
					file_read(f,sizeof(float),&c[2]);
					file_read(f,sizeof(float),&c[3]);
					break;
				case RTM_CHUNK_TEXTURE:
					name = file_get(f);
					mat->tex = tex_from_image(name);
					file_seek(f,ch->size,SEEK_CUR);
					break;
				case RTM_CHUNK_IMAGE:
					if (ch->size == 3) {
						unsigned int *px;
						img = malloc(sizeof(image_t));
						img->pixels = malloc(4*ch->count);
						file_read(f,4,&img->w);
						file_read(f,4,&img->h);

						memset(img->pixels,255,4*ch->count);

						px = (unsigned int*)img->pixels;
						for (j=0; j<ch->count; j++) {
							file_read(f,3,&px[j]);
						}

						mat->tex = tex_from_pixels(img);
					}else if (ch->size == 4) {
						img = malloc(sizeof(image_t));
						img->pixels = malloc(4*ch->count);
						file_read(f,4,&img->w);
						file_read(f,4,&img->h);
						file_read(f,4*ch->count,&img->pixels);
						mat->tex = tex_from_pixels(img);
					}else{
						rtprintf(CN_ERROR "Unknow pixel size for image data '%d'",ch->size);
						file_seek(f,(ch->size*ch->count)+8,SEEK_CUR);
					}
					break;
				default:;
				}
			}
		}
	}
	for (i=0; i<dh.chunks; i++) {
		if (cr[i].type == RTM_CHUNK_MESH) {
			file_seek(f,cr[i].start,SEEK_SET);
			ch = file_get(f);
			file_seek(f,sizeof(rtm_chunk_t),SEEK_CUR);
			m = mesh_create(GL_TRIANGLES);
			array_push_ptr(mdl->meshes,m);
			cnt = ch->count;
			for (k=0; k<cnt; k++) {
				ch = file_get(f);
				file_seek(f,sizeof(rtm_chunk_t),SEEK_CUR);
				switch (ch->type) {
				case RTM_CHUNK_MESHMATERIAL:
					name = file_get(f);
					m->mat = mat_find_or_create(name);
					file_seek(f,ch->size,SEEK_CUR);
					break;
				case RTM_CHUNK_VERTEX:
					if (!m->v)
						m->v = array_create(RTG_TYPE_FLOAT);
					s = ch->size*ch->count;
					m->v->length = s;
					s *= sizeof(float);
					m->v->data = malloc(s);
					file_read(f,s,m->v->data);
					break;
				case RTM_CHUNK_NORMAL:
					if (!m->n)
						m->n = array_create(RTG_TYPE_FLOAT);
					s = ch->size*ch->count;
					m->n->length = s;
					s *= sizeof(float);
					m->n->data = malloc(s);
					file_read(f,s,m->n->data);
					break;
				case RTM_CHUNK_INDEX:
					if (!m->i)
						m->i = array_create(RTG_TYPE_INT);
					s = ch->size*ch->count;
					m->i->length = s;
					s *= sizeof(unsigned int);
					m->i->data = malloc(s);
					file_read(f,s,m->i->data);
					break;
				case RTM_CHUNK_TEXCOORD:
					if (!m->t)
						m->t = array_create(RTG_TYPE_FLOAT);
					s = ch->size*ch->count;
					m->t->length = s;
					s *= sizeof(float);
					m->t->data = malloc(s);
					file_read(f,s,m->t->data);
					break;
				case RTM_CHUNK_COLORLIST:
					if (!m->c)
						m->c = array_create(RTG_TYPE_FLOAT);
					s = ch->size*ch->count;
					m->c->length = s;
					s *= sizeof(float);
					m->c->data = malloc(s);
					file_read(f,s,m->c->data);
					break;
				case RTM_CHUNK_WEIGHTS:
					if (!m->w)
						m->w = array_create(RTG_TYPE_PTR);
					for (j=0; j<ch->count; j++) {
						wt = malloc(sizeof(weight_t));
						array_push_ptr(m->w,wt);
						wt->joint = file_read_int(f);
						wt->bias = file_read_float(f);
						wt->pos.x = file_read_float(f);
						wt->pos.y = file_read_float(f);
						wt->pos.z = file_read_float(f);
					}
					break;
				case RTM_CHUNK_WEIGHTMAP:
					if (!m->m)
						m->m = array_create(RTG_TYPE_INT);
					array_set_int(m->m,0,ch->count*2);
					file_read(f,ch->count*8,m->m->data);
					break;
				default:;
				}
			}
		}
	}

	for (i=0; i<dh.chunks; i++) {
		if (cr[i].type == RTM_CHUNK_SKELETON) {
			file_seek(f,cr[i].start,SEEK_SET);
			ch = file_get(f);
			file_seek(f,sizeof(rtm_chunk_t),SEEK_CUR);
			sk = malloc(sizeof(skeleton_t));
			sk->fps = (float)file_read_uint(f)/60.0;
			sk->name = malloc(ch->size);
			file_read(f,ch->size,sk->name);
			sk->frames = array_create(RTG_TYPE_PTR);
			if (!mdl->skeletons)
				mdl->skeletons = array_create(RTG_TYPE_PTR);
			array_push_ptr(mdl->skeletons,sk);
			cnt = ch->count;
			for (k=0; k<cnt; k++) {
				ch = file_get(f);
				file_seek(f,sizeof(rtm_chunk_t),SEEK_CUR);
				switch (ch->type) {
				case RTM_CHUNK_FRAME:
					fr = malloc(sizeof(frame_t));
					fr->pre_vertex = NULL;
					fr->joints = array_create(RTG_TYPE_PTR);
					array_push_ptr(sk->frames,fr);
					for (j=0; j<ch->count; j++) {
						jt = malloc(sizeof(joint_t));
						jt->pos.x = file_read_float(f);
						jt->pos.y = file_read_float(f);
						jt->pos.z = file_read_float(f);
						jt->rot.x = file_read_float(f);
						jt->rot.y = file_read_float(f);
						jt->rot.z = file_read_float(f);
						jt->rot.w = file_read_float(f);
						array_push_ptr(fr->joints,jt);
					}
					break;
				default:;
				}
			}
		}
	}

	return mdl;
}

/* save a model to a rosethorn model file */
void model_export(model_t *mdl, char* file)
{
	rtm_file_header_t fh;
	rtm_data_header_t dh;
	rtm_chunk_ref_t cr;
	rtm_chunk_t chunk;
	int i;
	int j;
	int k;
	unsigned int u;
	mesh_t *m;
	weight_t *wt;
	skeleton_t *sk;
	frame_t *fr;
	joint_t *jt;
	size_t cp;
	int cc = 0;
	size_t pos;
	FILE *f;
	file = file_path(file);
	f = fopen(file,"w");
	if (!f)
		return;

	strcpy(fh.id,RTM_FILE_ID);
	fh.version = RTM_VERSION_CURRENT;
	fh.data_header = sizeof(rtm_file_header_t);
	fwrite(&fh,sizeof(rtm_file_header_t),1,f);

	dh.meshes = mdl->meshes->length;
	dh.materials = 0;
	dh.skeletons = 0;
	if (mdl->skeletons)
		dh.skeletons = mdl->skeletons->length;
	dh.chunks = 0;

	for (i=0; i<mdl->meshes->length; i++) {
		m = array_get_ptr(mdl->meshes,i);
		dh.chunks++;
		if (m->mat) {
			dh.materials++;
			dh.chunks += 2;
			if (m->mat->tex)
				dh.chunks++;
			if (m->mat->has_amb)
				dh.chunks++;
			if (m->mat->has_dif)
				dh.chunks++;
			if (m->mat->has_spc)
				dh.chunks++;
		}
		if (m->v)
			dh.chunks++;
		if (m->n)
			dh.chunks++;
		if (m->t)
			dh.chunks++;
		if (m->c)
			dh.chunks++;
		if (m->i)
			dh.chunks++;
		if (m->w)
			dh.chunks++;
		if (m->m)
			dh.chunks++;
	}
	if (mdl->skeletons) {
		for (i=0; i<mdl->skeletons->length; i++) {
			dh.chunks++;
			sk = array_get_ptr(mdl->skeletons,i);
			dh.chunks += sk->frames->length;
		}
	}
	dh.chunk_start = sizeof(rtm_file_header_t)+sizeof(rtm_data_header_t);
	fwrite(&dh,sizeof(rtm_data_header_t),1,f);

	cp = ftell(f);

	for (i=0; i<dh.chunks; i++) {
		fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
	}

	for (i=0; i<mdl->meshes->length; i++) {
		m = array_get_ptr(mdl->meshes,i);
		/* mesh chunk reference */
		pos = ftell(f);
		cr.type = RTM_CHUNK_MESH;
		cr.start = pos;
		fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
		fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
		cc++;
		fseek(f,pos,SEEK_SET);
		/* mesh chunk header */
		chunk.type = RTM_CHUNK_MESH;
		chunk.size = 0;
		chunk.count = 0;
		if (m->mat)
			chunk.count++;
		if (m->v)
			chunk.count++;
		if (m->n)
			chunk.count++;
		if (m->t)
			chunk.count++;
		if (m->c)
			chunk.count++;
		if (m->i)
			chunk.count++;
		if (m->w)
			chunk.count++;
		if (m->m)
			chunk.count++;
		fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
		if (m->mat) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_MESHMATERIAL;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_MESHMATERIAL;
			chunk.size = strlen(m->mat->name)+1;
			chunk.count = 1;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->mat->name,chunk.size,1,f);
		}
		if (m->v) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_VERTEX;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_VERTEX;
			chunk.size = 3;
			chunk.count = m->v->length/3;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->v->data,m->v->length*(sizeof(float)),1,f);
		}
		if (m->n) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_NORMAL;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_NORMAL;
			chunk.size = 3;
			chunk.count = m->n->length/3;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->n->data,m->n->length*(sizeof(float)),1,f);
		}
		if (m->t) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_TEXCOORD;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_TEXCOORD;
			chunk.size = 2;
			chunk.count = m->t->length/2;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->t->data,m->t->length*(sizeof(float)),1,f);
		}
		if (m->c) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_COLORLIST;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_COLORLIST;
			chunk.size = 4;
			chunk.count = m->c->length/4;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->c->data,m->c->length*(sizeof(float)),1,f);
		}
		if (m->i) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_INDEX;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_INDEX;
			chunk.size = 3;
			chunk.count = m->i->length/3;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->i->data,m->i->length*4,1,f);
		}
		if (m->w) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_WEIGHTS;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_WEIGHTS;
			chunk.size = 0;
			chunk.count = m->w->length;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			for (k=0; k<m->w->length; k++) {
				wt = array_get_ptr(m->w,k);
				fwrite(&wt->joint,4,1,f);
				fwrite(&wt->bias,4,1,f);
				fwrite(&wt->pos.x,4,1,f);
				fwrite(&wt->pos.y,4,1,f);
				fwrite(&wt->pos.z,4,1,f);
			}
		}
		if (m->m) {
			/* chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_WEIGHTMAP;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* chunk header */
			chunk.type = RTM_CHUNK_WEIGHTMAP;
			chunk.size = 0;
			chunk.count = m->m->length/2;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			fwrite(m->m->data,m->m->length*4,1,f);
		}
	}

	for (i=0; i<mdl->meshes->length; i++) {
		m = array_get_ptr(mdl->meshes,i);
		if (m->mat) {
			/* mat chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_MATERIAL;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* mat chunk header */
			chunk.type = RTM_CHUNK_MATERIAL;
			chunk.size = strlen(m->mat->name)+1;
			chunk.count = 0;
			if (m->mat->tex)
				chunk.count++;
			if (m->mat->has_amb)
				chunk.count++;
			if (m->mat->has_dif)
				chunk.count++;
			if (m->mat->has_spc)
				chunk.count++;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			fwrite(m->mat->name,chunk.size,1,f);
			if (m->mat->tex) {
				/* chunk reference */
				pos = ftell(f);
				cr.type = RTM_CHUNK_TEXTURE;
				cr.start = pos;
				fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
				fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
				cc++;
				fseek(f,pos,SEEK_SET);
				/* chunk header */
				chunk.type = RTM_CHUNK_TEXTURE;
				chunk.size = strlen(m->mat->tex->name)+1;
				chunk.count = 1;
				fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
				/* chunk data */
				fwrite(m->mat->tex->name,chunk.size,1,f);
			}
			if (m->mat->has_amb) {
				/* chunk reference */
				pos = ftell(f);
				cr.type = RTM_CHUNK_AMBIENT;
				cr.start = pos;
				fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
				fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
				cc++;
				fseek(f,pos,SEEK_SET);
				/* chunk header */
				chunk.type = RTM_CHUNK_AMBIENT;
				chunk.size = 0;
				chunk.count = 4;
				fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
				/* chunk data */
				fwrite(m->mat->ambient,16,1,f);
			}
			if (m->mat->has_dif) {
				/* chunk reference */
				pos = ftell(f);
				cr.type = RTM_CHUNK_DIFFUSE;
				cr.start = pos;
				fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
				fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
				cc++;
				fseek(f,pos,SEEK_SET);
				/* chunk header */
				chunk.type = RTM_CHUNK_DIFFUSE;
				chunk.size = 0;
				chunk.count = 4;
				fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
				/* chunk data */
				fwrite(m->mat->diffuse,16,1,f);
			}
			if (m->mat->has_spc) {
				/* chunk reference */
				pos = ftell(f);
				cr.type = RTM_CHUNK_SPECULAR;
				cr.start = pos;
				fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
				fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
				cc++;
				fseek(f,pos,SEEK_SET);
				/* chunk header */
				chunk.type = RTM_CHUNK_SPECULAR;
				chunk.size = 0;
				chunk.count = 4;
				fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
				/* chunk data */
				fwrite(m->mat->specular,16,1,f);
			}
		}
	}

	if (mdl->skeletons) {
		for (i=0; i<mdl->skeletons->length; i++) {
			sk = array_get_ptr(mdl->skeletons,i);
			/* skeleton chunk reference */
			pos = ftell(f);
			cr.type = RTM_CHUNK_SKELETON;
			cr.start = pos;
			fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
			fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
			cc++;
			fseek(f,pos,SEEK_SET);
			/* skeleton chunk header */
			chunk.type = RTM_CHUNK_SKELETON;
			chunk.size = strlen(sk->name)+1;
			chunk.count = sk->frames->length;
			fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
			/* chunk data */
			u = sk->fps*60;
			fwrite(&u,4,1,f);
			fwrite(sk->name,chunk.size,1,f);
			for (k=0; k<sk->frames->length; k++) {
				fr = array_get_ptr(sk->frames,k);
				/* chunk reference */
				pos = ftell(f);
				cr.type = RTM_CHUNK_FRAME;
				cr.start = pos;
				fseek(f,cp+(sizeof(rtm_chunk_ref_t)*cc),SEEK_SET);
				fwrite(&cr,sizeof(rtm_chunk_ref_t),1,f);
				cc++;
				fseek(f,pos,SEEK_SET);
				/* chunk header */
				chunk.type = RTM_CHUNK_FRAME;
				chunk.size = 0;
				chunk.count = fr->joints->length;
				fwrite(&chunk,sizeof(rtm_chunk_t),1,f);
				/* chunk data */
				for (j=0; j<fr->joints->length; j++) {
					jt = array_get_ptr(fr->joints,j);
					fwrite(&jt->pos.x,4,1,f);
					fwrite(&jt->pos.y,4,1,f);
					fwrite(&jt->pos.z,4,1,f);
					fwrite(&jt->rot.x,4,1,f);
					fwrite(&jt->rot.y,4,1,f);
					fwrite(&jt->rot.z,4,1,f);
					fwrite(&jt->rot.w,4,1,f);
				}
			}
		}
	}

	rtprintf("chunks: %d %u",cc,dh.chunks);

	fclose(f);
}
