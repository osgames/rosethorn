/************************************************************************
* wm_w32.c
* window management code win32
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

#ifdef WIN32
static int wm_has_init = 0;

/* initialise the game window */
int wm_init()
{
	wm_has_init = 1;
	return wm_create();
}

/* exit the game window */
void wm_exit()
{
	wm_destroy();
}

/* create a window */
int wm_create()
{
	GLuint PixelFormat;
	WNDCLASS wc;
	DWORD dwExStyle;
	DWORD dwStyle;
	RECT WindowRect;

	wm_res.hDC = NULL;
	wm_res.hRC = NULL;
	wm_res.hWnd = NULL;

	WindowRect.left = (long)0;
	WindowRect.right = (long)wm_res.width;
	WindowRect.top = (long)0;
	WindowRect.bottom=(long)wm_res.height;

	wm_res.hInstance = GetModuleHandle(NULL);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = wm_res.hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "OpenGL";

	if (!RegisterClass(&wc)) {
		rtprintf(CN_RTG "Failed To Register The Window Class");
		return 1;
	}

	if (wm_res.fullscreen) {
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = wm_res.width;
		dmScreenSettings.dmPelsHeight = wm_res.height;
		dmScreenSettings.dmBitsPerPel = 24;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		/* if fullscreen fails, switch to windowed mode */
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
			rtprintf(CN_RTG "Requested Fullscreen Mode Is Not Supported");
			wm_res.fullscreen = 0;
		}
	}

	if (wm_res.fullscreen) {
		dwExStyle = WS_EX_APPWINDOW;
		dwStyle = WS_POPUP;
	}else{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU);
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);

	if (!(wm_res.hWnd = CreateWindowEx(
		dwExStyle,
		"OpenGL",
		rtg_game_name,
		dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		0,
		0,
		WindowRect.right-WindowRect.left,
		WindowRect.bottom-WindowRect.top,
		NULL,
		NULL,
		wm_res.hInstance,
		NULL
	))) {
		wm_destroy();
		rtprintf(CN_RTG "Window Creation Error");
		return 1;
	}

	static	PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		16,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		16,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0,
		0,
		0
	};

	if (!(wm_res.hDC = GetDC(wm_res.hWnd))) {
		wm_destroy();
		rtprintf(CN_RTG "Can't Create A GL Device Context");
		return 1;
	}

	if (!(PixelFormat = ChoosePixelFormat(wm_res.hDC,&pfd))) {
		wm_destroy();
		rtprintf(CN_RTG "Can't Find A Suitable PixelFormat");
		return 1;
	}

	if (!SetPixelFormat(wm_res.hDC,PixelFormat,&pfd)) {
		wm_destroy();
		rtprintf(CN_RTG "Can't Set The PixelFormat");
		return 1;
	}

	if (!(wm_res.hRC = wglCreateContext(wm_res.hDC))) {
		wm_destroy();
		rtprintf(CN_RTG "Can't Create A GL Rendering Context");
		return 1;
	}

	if (!wglMakeCurrent(wm_res.hDC,wm_res.hRC)) {
		wm_destroy();
		rtprintf(CN_RTG "Can't Activate The GL Rendering Context");
		return 1;
	}

	ShowWindow(wm_res.hWnd,SW_SHOW);
	SetForegroundWindow(wm_res.hWnd);
	SetFocus(wm_res.hWnd);

	return 0;
}

/* resize the screen, fullscreen or windowed */
int wm_resize()
{
	return 0;
}

/* flush graphics through and flip buffers */
int wm_update()
{
	glFlush();
	/* update the screen */
	SwapBuffers(wm_res.hDC);
	return 0;
}

/* destroy the current window */
void wm_destroy()
{
	if (wm_res.fullscreen)
		ChangeDisplaySettings(NULL,0);

	if (wm_res.hRC) {
		if (!wglMakeCurrent(NULL,NULL)) {
			rtprintf(CN_RTG "Release Of DC And RC Failed");
		}

		if (!wglDeleteContext(wm_res.hRC)) {
			rtprintf(CN_RTG "Release Rendering Context Failed");
		}
		wm_res.hRC = NULL;
	}

	if (wm_res.hDC && !ReleaseDC(wm_res.hWnd,wm_res.hDC)) {
		rtprintf(CN_RTG "Release Device Context Failed");
		wm_res.hDC = NULL;
	}

	if (wm_res.hWnd && !DestroyWindow(wm_res.hWnd)) {
		rtprintf(CN_RTG "Could Not Release hWnd");
		wm_res.hWnd = NULL;
	}

	if (!UnregisterClass("OpenGL",wm_res.hInstance)) {
		rtprintf(CN_RTG "Could Not Unregister Class");
		wm_res.hInstance = NULL;
	}
}

/* set fullscreen on/off */
void wm_toggle_fullscreen(int fs)
{
	if (fs == wm_res.fullscreen)
		return;

	if (!wm_has_init) {
		wm_res.fullscreen = fs;
		return;
	}
	wm_destroy();
	wm_res.fullscreen = fs;
	wm_create();
}

/* use file as a cursor texture */
void wm_cursor(char* file, int width, int height, int offset_x, int offset_y)
{
	if (!file) {
		if (!wm_res.cursor.mat)
			return;

		wm_res.cursor.mat = NULL;
		/* TODO: w32 wm_cursor */

		return;
	}

	wm_res.cursor.mat = mat_from_image(file);
	wm_res.cursor.w = width;
	wm_res.cursor.h = height;
	wm_res.cursor.x = offset_x;
	wm_res.cursor.y = offset_y;

	if (!wm_res.cursor.mat)
		return;
	/* TODO: w32 wm_cursor */
}

/* grab mouse */
void wm_grab()
{
	/* TODO: w32 wm_grab */
}

/* stop grabbing mouse */
void wm_ungrab()
{
	/* TODO: w32 wm_ungrab */
}

/* set window title */
void wm_title(char* title)
{
	if (title) {
		free(wm_res.title);
		wm_res.title = strdup(title);
	}
	if (!wm_has_init)
		return;

	SetWindowText(wm_res.hWnd,wm_res.title);
}
#endif
