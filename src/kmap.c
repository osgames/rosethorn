/************************************************************************
* kmap.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

/* convert a computed sym to a bind string */
char* sym_to_string(int type, int sym)
{
	static char asym[10];

	if (type == CTRL_MOUSE) {
		if (sym == RTM_LEFT_BUTTON) {
			sprintf(asym,"mouse-1");
		}else if (sym == RTM_RIGHT_BUTTON) {
			sprintf(asym,"mouse-2");
		}else if (sym == RTM_MIDDLE_BUTTON) {
			sprintf(asym,"mouse-3");
		}else if (sym == RTM_SCROLL_UP) {
			sprintf(asym,"scroll-up");
		}else if (sym == RTM_SCROLL_DOWN) {
			sprintf(asym,"scroll-down");
		}else if (sym == RTM_MOTION) {
			sprintf(asym,"mouse-motion");
		}else{
			sprintf(asym,"???");
		}
		return asym;
#ifdef WIN32
	}else if (isgraph(current_event.ch)) {
		sprintf(asym,"%c",current_event.ch);
		return asym;
#else
	}else if (isgraph(sym)) {
		sprintf(asym,"%c",sym);
		return asym;
#endif
	}else if (sym == RTK_Tab) {
		sprintf(asym,"tab");
		return asym;
	}else if (sym == RTK_Escape) {
		sprintf(asym,"esc");
		return asym;
	}else if (sym == RTK_space) {
		sprintf(asym,"spc");
		return asym;
	}else if (sym == RTK_KP_0) {
		sprintf(asym,"kp-0");
		return asym;
	}else if (sym == RTK_KP_1) {
		sprintf(asym,"kp-1");
		return asym;
	}else if (sym == RTK_KP_2) {
		sprintf(asym,"kp-2");
		return asym;
	}else if (sym == RTK_KP_3) {
		sprintf(asym,"kp-3");
		return asym;
	}else if (sym == RTK_KP_4) {
		sprintf(asym,"kp-4");
		return asym;
	}else if (sym == RTK_KP_5) {
		sprintf(asym,"kp-5");
		return asym;
	}else if (sym == RTK_KP_6) {
		sprintf(asym,"kp-6");
		return asym;
	}else if (sym == RTK_KP_7) {
		sprintf(asym,"kp-7");
		return asym;
	}else if (sym == RTK_KP_8) {
		sprintf(asym,"kp-8");
		return asym;
	}else if (sym == RTK_KP_9) {
		sprintf(asym,"kp-9");
		return asym;
	}else if (sym == RTK_Up) {
		sprintf(asym,"up");
		return asym;
	}else if (sym == RTK_Down) {
		sprintf(asym,"down");
		return asym;
	}else if (sym == RTK_Right) {
		sprintf(asym,"right");
		return asym;
	}else if (sym == RTK_Left) {
		sprintf(asym,"left");
		return asym;
	}else{
		sprintf(asym,"???");
		return asym;
	}
	return NULL;
}

/* get the control type from a bind string */
int key_to_type(char* k)
{
	if (!strncmp(k,"mouse",5) || !strncmp(k,"scroll",6)) {
		return CTRL_MOUSE;
	}else{
		return CTRL_KEYBOARD;
	}
}

/* get the sym from a bind string */
int key_to_int(char* k)
{
	if (!strcmp(k,"mouse-1")) {
		return RTM_LEFT_BUTTON;
	}else if (!strcmp(k,"mouse-2")) {
		return RTM_RIGHT_BUTTON;
	}else if (!strcmp(k,"mouse-3")) {
		return RTM_MIDDLE_BUTTON;
	}else if (!strcmp(k,"scroll-up")) {
		return RTM_SCROLL_UP;
	}else if (!strcmp(k,"scroll-down")) {
		return RTM_SCROLL_DOWN;
	}else if (!strcmp(k,"mouse-motion")) {
		return RTM_MOTION;
	}else if (!k[0]) {
		return RTK_UNKNOWN;
	}else if (!k[1] && isgraph(k[0])) {
#ifdef WIN32
		return (int)VkKeyScan(k[0]);
#else
		return (int)k[0];
#endif
	}else if (!strcmp(k,"esc")) {
		return RTK_Escape;
	}else if (!strcmp(k,"tab")) {
		return RTK_Tab;
	}else if (!strcmp(k,"space")) {
		return RTK_space;
	}else if (!strcmp(k,"kp-0")) {
		return RTK_KP_0;
	}else if (!strcmp(k,"kp-1")) {
		return RTK_KP_1;
	}else if (!strcmp(k,"kp-2")) {
		return RTK_KP_2;
	}else if (!strcmp(k,"kp-3")) {
		return RTK_KP_3;
	}else if (!strcmp(k,"kp-4")) {
		return RTK_KP_4;
	}else if (!strcmp(k,"kp-5")) {
		return RTK_KP_5;
	}else if (!strcmp(k,"kp-6")) {
		return RTK_KP_6;
	}else if (!strcmp(k,"kp-7")) {
		return RTK_KP_7;
	}else if (!strcmp(k,"kp-8")) {
		return RTK_KP_8;
	}else if (!strcmp(k,"kp-9")) {
		return RTK_KP_9;
	}else if (!strcmp(k,"up")) {
		return RTK_Up;
	}else if (!strcmp(k,"down")) {
		return RTK_Down;
	}else if (!strcmp(k,"right")) {
		return RTK_Right;
	}else if (!strcmp(k,"left")) {
		return RTK_Left;
	}else{
		return RTK_UNKNOWN;
	}
}

