/************************************************************************
* ctrl.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

typedef struct ctrl_s {
	widget_t *label;
	widget_t *button;
} ctrl_t;
ctrl_t *ctrls = NULL;

/* clear the control settings widgets from w */
int ctrl_clear(widget_t *w)
{
	if (ctrls) {
		if (ctrls[0].label) {
			int i;
			widget_t *p = w->child;
			if (p) {
				while (p->next) {
					if (p->next->id == ctrls[0].label->id)
						break;
					p = p->next;
				}
			}
			if (p)
				p->next = NULL;

			for (i=0; ctrls[i].label; i++) {
				ui_widget_free(ctrls[i].label);
				ui_widget_free(ctrls[i].button);
				ctrls[i].label = NULL;
				ctrls[i].button = NULL;
			}
		}
		free(ctrls);
		ctrls = NULL;
	}

	return EVENT_HANDLED;
}

/* click function for control settings widgets */
static int ctrl_set(widget_t *w)
{
	char d[100];
	sprintf(d,"-- %s --",ui_widget_value(w,NULL));
	ui_widget_value(w,d);
	setter = w;
	return EVENT_HANDLED;
}

/* add a list of control settings widgets to w at x/y */
int ctrl_create(widget_t *w, int x, int y)
{
	event_action_t *e;
	int i;
	int ec;

	if (!w)
		return EVENT_UNHANDLED;

	e = events;
	for (ec=1; e; ec++) {
		e = e->next;
	}

	ctrls = malloc(sizeof(ctrl_t)*ec);
	if (!ctrls)
		return EVENT_UNHANDLED;

	e = events;
	for (i=0; e; i++) {
		ctrls[i].label = ui_widget_create(UI_WIDGET_LABEL,w);
		ui_widget_value(ctrls[i].label,e->name);
		ctrls[i].label->style.h = SML_FONT;
		ctrls[i].label->style.x = x;
		ctrls[i].label->style.y = y+(20*i);

		ctrls[i].button = ui_widget_create(UI_WIDGET_BUTTON,w);
		ui_widget_value(ctrls[i].button,e->bind);
		ctrls[i].button->data = malloc(sizeof(widget_data_t));
		strcpy(ctrls[i].button->data->opt.com,e->name);
		ctrls[i].button->style.w = y;
		ctrls[i].button->style.x = x;
		ctrls[i].button->style.y = y+(20*i);

		ctrls[i].button->events->mclick = &ctrl_set;

		e = e->next;
	}

	style_refresh(ui.styles,ui.elements);

	ctrls[i].label = NULL;
	ctrls[i].button = NULL;

	return EVENT_HANDLED;
}
