/************************************************************************
* texture.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

static texture_t *textures = NULL;
static int tex_ids = 0;

/* generate the hardware texture */
int tex_generate(texture_t *tex)
{
	int b;
	int t;
	int m;
	if (!tex)
		return 1;

	/* delete the old texture, if it exists */
	if (tex->state)
		glDeleteTextures(1, &tex->tex);

	/* create a new texture */
	glGenTextures(1, &tex->tex);

	glBindTexture(GL_TEXTURE_2D, tex->tex);

	b = opengl_has_bilinear();
	t = opengl_has_trilinear();
	m = opengl_has_mipmap();
	if (t) {
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		if (m) {
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		}else{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
	}else if (b) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}else{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	if (opengl_has_anisotropic()) {
		float ma = opengl_max_anisotropic();
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, ma);
	}

        /* draw the pixels to the texture */
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex->px->w, tex->px->h , 0, GL_RGBA, GL_UNSIGNED_BYTE, tex->px->pixels);
	if (glGetError() != GL_NO_ERROR) {
		tex->px = NULL;
		return 1;
	}

	if (m)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 4, tex->px->w, tex->px->h , GL_RGBA, GL_UNSIGNED_BYTE, tex->px->pixels);

	tex->state = 1;

	return 0;
}

/* add a new image to the cache */
texture_t *tex_create()
{
	texture_t *tex = (texture_t*)malloc(sizeof(texture_t));
	if (tex == NULL)
		return NULL;

	tex->w = 0;
	tex->h = 0;
	tex->xf = 0;
	tex->yf = 0;
	tex->name[0] = 0;
	tex->px = NULL;
	tex->id = tex_ids++;
	tex->tex = 0;
	tex->state = 0;

	textures = list_push(&textures,tex);

	return tex;
}

/* free a texture */
void tex_free(texture_t *tex)
{
	if (!tex)
		return;
	textures = list_remove(&textures,tex);
	if (tex->tex)
		glDeleteTextures(1, &tex->tex);
	free(tex);
}

/* load an image to a texture */
texture_t *tex_from_image(char* filename)
{
	texture_t *t;
	/* get the image */
	image_t *p;

	t = textures;
	while (t) {
		if (!strcmp(t->name,filename))
			break;
		t = t->next;
	}

	if (t)
		return t;

	p = image_load(filename);
	if (!p)
		return NULL;

	/* and make a texture out of it */
	t = tex_from_pixels(p);
	strncpy(t->name,filename,100);

	return t;
}

/* create a texture from pixel data */
texture_t *tex_from_pixels(image_t *px)
{
	texture_t *tex;
	if (!px)
		return NULL;

	tex = tex_create();
	if (tex == NULL)
		return NULL;

	tex->w = px->w;
	tex->h = px->h ;
	tex->px = px;
	tex->xf = 1.0/(GLfloat)px->w;
	tex->yf = 1.0/(GLfloat)px->h;

	if (!tex_generate(tex))
		return tex;

	tex_free(tex);

	return NULL;
}

/* update a texture from pixel data */
texture_t *tex_update_pixels(texture_t *tex, image_t *px)
{
	if (!tex)
		return tex_from_pixels(px);

	tex->w = px->w;
	tex->h = px->h ;
	tex->px = px;
	tex->xf = 1.0/(GLfloat)px->w;
	tex->yf = 1.0/(GLfloat)px->h;

	if (!tex_generate(tex))
		return tex;

	tex_free(tex);

	return NULL;
}

/* create a transparent texture */
texture_t *tex_from_rgba(int x, int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	char n[100];
	texture_t *t;
	image_t *p;
	int size;
	int i;

	snprintf(n,100,"rgba-%u-%u-%u-%u-%d-%d",(unsigned int)r,(unsigned int)g,(unsigned int)b,(unsigned int)a,x,y);
	t = textures;
	while (t) {
		if (!strcmp(t->name,n))
			break;
		t = t->next;
	}

	if (t)
		return t;

	p = malloc(sizeof(image_t));
	if (!p)
		return NULL;

	p->w = x;
	p->h = y;

	size = x*y*4;

	p->pixels = malloc(sizeof(unsigned char)*size);
	if (!p->pixels) {
		free(p);
		return NULL;
	}

	i = 0;
	while (i<size) {
		p->pixels[i++] = r;
		p->pixels[i++] = g;
		p->pixels[i++] = b;
		p->pixels[i++] = a;
	}

	t = tex_from_pixels(p);
	strcpy(t->name,n);

	return t;
}
