/************************************************************************
* cmd.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

#include <stdarg.h>

/*

	ui.elements(selector.style.attribute) 'value'
	wm.attribute 'value'
	sys.control(name or command) 'value'
	sound.music.volume 'value'
	sound.effects.volume 'value'
	sound.enabled 'value'

*/

typedef struct setter_s {
	struct setter_s *prev;
	struct setter_s *next;
	void (*func)();
	char* name;
} setter_t;

static setter_t *lsetters = NULL;
static setter_t *vsetters = NULL;

/* register a new command setter function */
int cmd_add_setter(int type, char* name, void (*func)())
{
	setter_t *s = malloc(sizeof(setter_t));
	s->name = strdup(name);
	s->func = func;

	if (type == RTG_SETTER_LITERAL) {
		lsetters = list_push(&lsetters,s);
		return 0;
	}else if (type == RTG_SETTER_VFUNC) {
		vsetters = list_push(&vsetters,s);
		return 0;
	}

	free(s->name);
	free(s);
	return 1;
}

/* apply a command */
int cmd_apply(char* name, char* value)
{
	char* b;
	char* e;
	setter_t *s;

	b = alloca(strlen(name)+1);
	strcpy(b,name);
	name = b;

	if ((b = strchr(name,'(')) && (e = strchr(b,')'))) {
		*b = 0;
		*e = 0;
		s = vsetters;
		while (s) {
			if (!strcmp(s->name,name))
				break;
			s = s->next;
		}
		*b = '(';
		b++;
		if (s)
			s->func(b,value);
		*e = ')';
	}else{
		s = lsetters;
		while (s) {
			if (!strcmp(s->name,name))
				break;
			s = s->next;
		}
		if (s)
			s->func(value);
	}

	return 0;
}

/* execute a command */
void cmd_exec(char* str)
{
	char* cmd = str;
	char* a1 = strchr(cmd,' ');
	char* a2;
	if (!a1)
		return;

	if (cmd[0] == '/')
		cmd++;

	rtprintf(CN_RTG "%s",cmd);

	*a1 = 0;
	a1++;
	a2 = strchr(a1,' ');

	if (a2) {
		*a2 = 0;
		a2++;
		if (!strcmp(cmd,"set")) {
			config_set_and_apply(a1,a2);
		}else if (!strcmp(cmd,"exec")) {
			cmd_apply(a1,a2);
		}
		a2--;
		*a2 = ' ';
	}else{
		if (!strcmp(cmd,"unset")) {
			config_set_and_apply(a1,NULL);
		}else if (!strcmp(cmd,"exec")) {
			cmd_apply(a1,NULL);
		}
	}
	a1--;
	*a1 = ' ';
}

/* execute a command from a formatted string */
void cmd_execf(char* str, ...)
{
	char b[1024];
	va_list ap;
	va_start(ap, str);
	vsnprintf(b,1024,str,ap);
	va_end(ap);

	cmd_exec(b);
}
