/************************************************************************
* ui_script.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

static int ui_script_event_mclick(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->type == UI_WIDGET_LIST && w->data->lst.click) {
		r = w->data->lst.click(w);
	}
	if (w->x)
		v = xml_attribute(w->x,"click");
	if (v && v[0] && ui_script_exec(w,v) == EVENT_HANDLED)
		return EVENT_HANDLED;

	return r;
}

static int ui_script_event_mdown(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->x)
		v = xml_attribute(w->x,"press");
	if (v && v[0])
		r = ui_script_exec(w,v);

	return r;
}

static int ui_script_event_mup(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->type == UI_WIDGET_CHECK) {
		r = ui_widget_event_check_mclick(w);
	}
	if (w->x)
		v = xml_attribute(w->x,"release");
	if (v && v[0] && ui_script_exec(w,v) == EVENT_HANDLED)
		return EVENT_HANDLED;

	return r;
}

static int ui_script_event_sdown(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->type == UI_WIDGET_LIST) {
		r = ui_widget_event_list_sdown(w);
	}else if (w->type == UI_WIDGET_NUM) {
		r = ui_widget_event_numbox_sdown(w);
	}
	if (w->x)
		v = xml_attribute(w->x,"down");
	if (v && v[0] && ui_script_exec(w,v) == EVENT_HANDLED)
		return EVENT_HANDLED;

	return r;
}

static int ui_script_event_sup(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->type == UI_WIDGET_LIST) {
		r = ui_widget_event_list_sup(w);
	}else if (w->type == UI_WIDGET_NUM) {
		r = ui_widget_event_numbox_sup(w);
	}
	if (w->x)
		v = xml_attribute(w->x,"up");
	if (v && v[0] && ui_script_exec(w,v) == EVENT_HANDLED)
		return EVENT_HANDLED;

	return r;
}

static int ui_script_event_mmove(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->type == UI_WIDGET_HSLIDE) {
		r = ui_widget_event_slide_mmove(w);
	}
	if (w->x)
		v = xml_attribute(w->x,"motion");
	if (v && v[0] && ui_script_exec(w,v) == EVENT_HANDLED)
		return EVENT_HANDLED;

	return r;
}

static int ui_script_event_kdown(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->type == UI_WIDGET_NUM) {
		r = ui_widget_event_numbox_kdown(w);
	}else if (w->type == UI_WIDGET_TEXT) {
		r = ui_widget_event_text_kdown(w);
	}else if (w->type == UI_WIDGET_CONSOLE) {
		r = ui_widget_event_console_kdown(w);
	}
	if (w->x)
		v = xml_attribute(w->x,"keydown");
	if (v && v[0] && ui_script_exec(w,v) == EVENT_HANDLED)
		return EVENT_HANDLED;

	return r;
}

static int ui_script_event_kup(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->x)
		v = xml_attribute(w->x,"keyup");
	if (v && v[0])
		r = ui_script_exec(w,v);

	return r;
}

static int ui_script_event_select(widget_t *l)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	widget_t *w;
	int i;
	int y;

	y = mouse[1]-ui_widget_position_y(l);
	y /= l->style.font_size;
	y--;
	y += l->data->lst.offset;

	if (y > l->data->lst.rows || y < 0)
		return r;

	w = l->child;
	for (i=0; w && i<y; i++) {
		w = w->next;
	}

	if (!w)
		return r;

	if (l->x)
		v = xml_attribute(l->x,"select");
	if (v && v[0])
		r = ui_script_exec(w,v);

	return r;
}

static int ui_script_event_show(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->x)
		v = xml_attribute(w->x,"show");
	if (v && v[0])
		r = ui_script_exec(w,v);

	if (r == EVENT_UNHANDLED) {
		if (w->type == UI_WIDGET_CONTAINER) {
			widget_t *c = w->child;
			while (c) {
				if (c->x)
					v = xml_attribute(c->x,"show");
				if (v && v[0])
					r = ui_script_exec(c,v);
				c = c->next;
			}
		}
	}

	return r;
}

static int ui_script_event_hide(widget_t *w)
{
	int r = EVENT_UNHANDLED;
	char* v = NULL;
	if (w->x)
		v = xml_attribute(w->x,"hide");
	if (v && v[0])
		r = ui_script_exec(w,v);

	if (r == EVENT_UNHANDLED) {
		if (w->type == UI_WIDGET_CONTAINER) {
			widget_t *c = w->child;
			while (c) {
				if (c->x)
					v = xml_attribute(c->x,"hide");
				if (v && v[0])
					r = ui_script_exec(c,v);
				c = c->next;
			}
		}
	}

	return r;
}

/* set the default event handlers for a widget */
void ui_script_events(widget_t *w)
{
	if (w->events) {
		w->events->mclick = ui_script_event_mclick;
		w->events->mdown = ui_script_event_mdown;
		w->events->mup = ui_script_event_mup;
		w->events->sdown = ui_script_event_sdown;
		w->events->sup = ui_script_event_sup;
		w->events->mmove = ui_script_event_mmove;
		w->events->kdown = ui_script_event_kdown;
		w->events->kup = ui_script_event_kup;
		w->events->show = ui_script_event_show;
		w->events->hide = ui_script_event_hide;
	}

	if (w->type == UI_WIDGET_LIST)
		w->data->lst.click = ui_script_event_select;
}

/* exec a ui scripted event */
int ui_script_exec(widget_t *w, char* str)
{
	char* t = str;
	char* e = str;
	char* s;
	char* b;
	int r = EVENT_UNHANDLED;

	do {
		e = strchr(t,';');
		if (e)
			*e = 0;

		if (t[0]) {
			while (isspace(*t)) {
				t++;
			}
			rtprintf(CN_RTG "ui_script_exec: '%s'",t);
			if (!strncmp(t,"call:",5)) {
				r = export_call_ui(t+5,w);
			}else if (!strcmp(t,"play()")) {
				rtg_state = RTG_STATE_PLAY;
				r = EVENT_HANDLED;
			}else if (!strcmp(t,"pause()")) {
				rtg_state = RTG_STATE_PAUSED;
				r = EVENT_HANDLED;
			}else if (!strcmp(t,"menu()")) {
				rtg_state = RTG_STATE_MENU;
				r = EVENT_HANDLED;
			}else if (!strcmp(t,"quit()")) {
				rtg_state = RTG_STATE_EXIT;
				r = EVENT_HANDLED;
			}else if (!strncmp(t,"show(",5)) {
				s = t+5;
				b = strchr(s,')');
				if (b) {
					*b = 0;
					ui_style_set(s,"visible:true;");
					*b = ')';
				}
			}else if (!strncmp(t,"hide(",5)) {
				s = t+5;
				b = strchr(s,')');
				if (b) {
					*b = 0;
					ui_style_set(s,"visible:false;");
					*b = ')';
				}
			}else{
				cmd_exec(t);
				r = EVENT_HANDLED;
			}
		}
		if (e) {
			*e = ';';
			e++;
		}
		if (r == EVENT_HANDLED)
			return r;
	} while ((t = e));

	return r;
}
