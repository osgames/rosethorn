/************************************************************************
* main.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

int rtg_state = RTG_STATE_EXIT;
float anim_factor = 0.5;
char* rtg_game_name = PACKAGE;
char* rtg_game_dir = PACKAGE;
res_t wm_res;

/* initialise rosethorn */
int rtg_init(int argc, char** argv, unsigned int flags, char* name)
{

	if (name) {
		int i;
		rtg_game_name = strdup(name);
		wm_res.title = strdup(name);
		rtg_game_dir = strdup(name);
		for (i=0; rtg_game_dir[i]; i++) {
			if (isupper(rtg_game_dir[i])) {
				rtg_game_dir[i] = tolower(rtg_game_dir[i]);
			}else if (isspace(rtg_game_dir[i])) {
				rtg_game_dir[i] = '_';
			}
		}
	}
	time_init();

	wm_res.height = 768;
	wm_res.width = 1024;
	wm_res.frame_cap = 30;
#ifdef RTG3D
	wm_res.distance = 1000.0;
#endif

	if (events_init()) {
		rtprintf(CN_ERROR "could not initialise events");
		return 1;
	}

	config_init(argc,argv);

	if (wm_init()) {
		rtprintf(CN_ERROR "could not initialise screen");
		return 1;
	}

	rtg_state = RTG_STATE_MENU;

	if (flags&RTG_INIT_SOUND) {
		if (sound_init()) {
			rtprintf(CN_ERROR "could not initialise sound");
			return 1;
		}
	}

	return 0;
}

/* exit rosethorn */
void rtg_exit()
{
	wm_exit();
	config_save();
	sound_exit();
	events_exit();
}
