/************************************************************************
* opengl.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

#ifdef WIN32
#ifndef APIENTRYP
#define APIENTRYP *
#endif

typedef void (APIENTRYP PFNGLGENBUFFERSPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRYP PFNGLDELETEBUFFERSPROC) (GLsizei n, const GLuint *buffers);
typedef void (APIENTRYP PFNGLBINDBUFFERPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRYP PFNGLBUFFERDATAPROC) (GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
typedef GLuint (APIENTRYP PFNGLCREATESHADERPROC) (GLenum type);
typedef void (APIENTRYP PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar* const *string, const GLint *length);
typedef void (APIENTRYP PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint (APIENTRYP PFNGLCREATEPROGRAMPROC) (void);
typedef void (APIENTRYP PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (APIENTRYP PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (APIENTRYP PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (APIENTRYP PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (APIENTRYP PFNGLDELETEPROGRAMPROC) (GLuint program);

#endif

static struct {
	int anisotropic;
	float anisotropic_max;
	int vertex_buffer_objects;
	int cfg_anisotropic;
	int cfg_bilinear;
	int cfg_trilinear;
	int cfg_mipmap;
	int cfg_backfacecull;
	int cfg_particles;
	int cfg_particles_max;
} opengl_features = {
	-1,
	1.0,
	-1,
	0,
	0,
	1,
	1,
	0,
	1,
	1000
};

/* command anisotropic setter */
void opengl_anisotropic_setter(char* value)
{
	opengl_features.cfg_anisotropic = strtol(value,NULL,10);
}
/* command bilinear setter */
void opengl_bilinear_setter(char* value)
{
	opengl_features.cfg_bilinear = strtol(value,NULL,10);
}
/* command trilinear setter */
void opengl_trilinear_setter(char* value)
{
	opengl_features.cfg_trilinear = strtol(value,NULL,10);
}
/* command mipmap setter */
void opengl_mipmap_setter(char* value)
{
	opengl_features.cfg_mipmap = strtol(value,NULL,10);
}
/* command backfacecull setter */
void opengl_backfacecull_setter(int v)
{
	int b = !!v;
	if (b != opengl_features.cfg_backfacecull) {
		if (b) {
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CW);
		}else{
			glDisable(GL_CULL_FACE);
			glFrontFace(GL_CW);
		}
	}
	opengl_features.cfg_backfacecull = b;
}
/* command particles setter */
void opengl_particles_setter(char* value)
{
	opengl_features.cfg_particles = strtol(value,NULL,10);
}
/* command particlesmax setter */
void opengl_particles_max_setter(char* value)
{
	opengl_features.cfg_particles_max = strtol(value,NULL,10);
}

/* check for anisotropic filtering support */
int opengl_has_anisotropic()
{
	if (opengl_features.anisotropic < 0) {
		char* t = (char*)glGetString(GL_EXTENSIONS);
		opengl_features.anisotropic = 0;
		if (glGetError() == GL_NO_ERROR && strstr(t,"GL_EXT_texture_filter_anisotropic"))
			opengl_features.anisotropic = 1;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &opengl_features.anisotropic_max);
		if (glGetError() == GL_NO_ERROR)
			opengl_features.anisotropic_max = 1.0;
	}

	if (!opengl_features.anisotropic)
		return 0;

	if (opengl_features.cfg_anisotropic && opengl_features.cfg_mipmap)
		return 1;

	return 0;
}

/* get the maximum anisotropic filtering value */
float opengl_max_anisotropic()
{
	return opengl_features.anisotropic_max;
}

/* check for bilinear filtering support */
int opengl_has_bilinear()
{
	return opengl_features.cfg_bilinear;
}

/* check for trilinear filtering support */
int opengl_has_trilinear()
{
	return opengl_features.cfg_trilinear;
}

/* check for mipmap support */
int opengl_has_mipmap()
{
	return opengl_features.cfg_mipmap;
}

/* check for VBO support */
int opengl_has_vbo()
{
	if (opengl_features.vertex_buffer_objects < 0) {
		char* t = (char*)glGetString(GL_EXTENSIONS);
		opengl_features.vertex_buffer_objects = 0;
		if (glGetError() == GL_NO_ERROR && strstr(t,"GL_ARB_vertex_buffer_object"))
			opengl_features.vertex_buffer_objects = 1;
	}

	return opengl_features.vertex_buffer_objects;
}

/* check if particle effects are enabled */
int opengl_has_particles()
{
	return opengl_features.cfg_particles;
}

/* check if backface culling is enabled */
int opengl_particles_max()
{
	if (!opengl_has_particles())
		return 0;
	return opengl_features.cfg_particles_max;
}

/***********************************************************************
*	some compatibilty code for vertex buffer objects
***********************************************************************/
static PFNGLGENBUFFERSPROC openglGenBuffers = NULL;
static PFNGLDELETEBUFFERSPROC openglDeleteBuffers = NULL;
static PFNGLBINDBUFFERPROC openglBindBuffer = NULL;
static PFNGLBUFFERDATAPROC openglBufferData = NULL;

/* glGenBuffers */
int opengl_gen_buffers(GLsizei n, GLuint *buffers)
{
	if (!openglGenBuffers) {
#ifndef WIN32
		openglGenBuffers = (PFNGLGENBUFFERSPROC)glXGetProcAddress((const GLubyte*)"glGenBuffers");
#else
		openglGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");
#endif
	}
	if (!openglGenBuffers)
		return 1;

	openglGenBuffers(n,buffers);

	return 0;
}

/* glDeleteBuffers */
int opengl_delete_buffers(GLsizei n, const GLuint *buffers)
{
	if (!openglDeleteBuffers) {
#ifndef WIN32
		openglDeleteBuffers = (PFNGLDELETEBUFFERSPROC)glXGetProcAddress((const GLubyte*)"glDeleteBuffers");
#else
		openglDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress("glDeleteBuffers");
#endif
	}
	if (!openglDeleteBuffers)
		return 1;

	openglDeleteBuffers(n,buffers);

	return 0;
}

/* glBindBuffer */
int opengl_bind_buffer(GLenum target, GLuint buffer)
{
	if (!openglBindBuffer) {
#ifndef WIN32
		openglBindBuffer = (PFNGLBINDBUFFERPROC)glXGetProcAddress((const GLubyte*)"glBindBuffer");
#else
		openglBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
#endif
	}
	if (!openglBindBuffer)
		return 1;

	openglBindBuffer(target,buffer);

	return 0;
}

/* glBufferData */
int opengl_buffer_data(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage)
{
	if (!openglBufferData) {
#ifndef WIN32
		openglBufferData = (PFNGLBUFFERDATAPROC)glXGetProcAddress((const GLubyte*)"glBufferData");
#else
		openglBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
#endif
	}
	if (!openglBufferData)
		return 1;

	openglBufferData(target,size,data,usage);

	return 0;
}


/***********************************************************************
*		some compatibilty code for shaders
***********************************************************************/
static PFNGLCREATESHADERPROC openglCreateShader = NULL;
static PFNGLSHADERSOURCEPROC openglShaderSource = NULL;
static PFNGLCOMPILESHADERPROC openglCompileShader = NULL;
static PFNGLCREATEPROGRAMPROC openglCreateProgram = NULL;
static PFNGLATTACHSHADERPROC openglAttachShader = NULL;
static PFNGLDETACHSHADERPROC openglDetachShader = NULL;
static PFNGLLINKPROGRAMPROC openglLinkProgram = NULL;
static PFNGLUSEPROGRAMPROC openglUseProgram = NULL;
static PFNGLDELETEPROGRAMPROC openglDeleteProgram = NULL;

/* glCreateShader */
GLuint opengl_create_shader(GLenum type)
{
	if (!openglCreateShader) {
#ifndef WIN32
		openglCreateShader = (PFNGLCREATESHADERPROC)glXGetProcAddress((const GLubyte*)"glCreateShader");
#else
		openglCreateShader = (PFNGLCREATESHADERPROC)wglGetProcAddress("glCreateShader");
#endif
	}
	if (!openglCreateShader)
		return 0;

	return openglCreateShader(type);
}

/* glShaderSource */
int opengl_shader_source(GLuint shader, GLsizei count, const GLchar **string, const GLint *length)
{
	if (!openglShaderSource) {
#ifndef WIN32
		openglShaderSource = (PFNGLSHADERSOURCEPROC)glXGetProcAddress((const GLubyte*)"glShaderSource");
#else
		openglBufferData = (PFNGLSHADERSOURCEPROC)wglGetProcAddress("glShaderSource");
#endif
	}
	if (!openglShaderSource)
		return 1;

	openglShaderSource(shader,count,string,length);

	return 0;
}

/* glCompileShader */
int opengl_compile_shader(GLuint shader)
{
	if (!openglCompileShader) {
#ifndef WIN32
		openglCompileShader = (PFNGLCOMPILESHADERPROC)glXGetProcAddress((const GLubyte*)"glCompileShader");
#else
		openglCompileShader = (PFNGLCOMPILESHADERPROC)wglGetProcAddress("glCompileShader");
#endif
	}
	if (!openglCompileShader)
		return 1;

	openglCompileShader(shader);

	return 0;
}

/* glCreateProgram */
GLuint opengl_create_program()
{
	if (!openglCreateProgram) {
#ifndef WIN32
		openglCreateProgram = (PFNGLCREATEPROGRAMPROC)glXGetProcAddress((const GLubyte*)"glCreateProgram");
#else
		openglCreateProgram = (PFNGLCREATEPROGRAMPROC)wglGetProcAddress("glCreateProgram");
#endif
	}
	if (!openglCreateProgram)
		return 0;

	return openglCreateProgram();
}

/* glAttachShader */
GLuint opengl_attach_shader(GLuint program, GLuint shader)
{
	if (!openglAttachShader) {
#ifndef WIN32
		openglAttachShader = (PFNGLATTACHSHADERPROC)glXGetProcAddress((const GLubyte*)"glAttachShader");
#else
		openglAttachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress("glAttachShader");
#endif
	}
	if (!openglAttachShader)
		return 1;

	openglAttachShader(program,shader);

	return 0;
}

/* glDetachShader */
GLuint opengl_detach_shader(GLuint program, GLuint shader)
{
	if (!openglDetachShader) {
#ifndef WIN32
		openglDetachShader = (PFNGLATTACHSHADERPROC)glXGetProcAddress((const GLubyte*)"glDetachShader");
#else
		openglDetachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress("glDetachShader");
#endif
	}
	if (!openglDetachShader)
		return 1;

	openglDetachShader(program,shader);

	return 0;
}

/* glLinkProgram */
GLuint opengl_link_program(GLuint program)
{
	if (!openglLinkProgram) {
#ifndef WIN32
		openglLinkProgram = (PFNGLLINKPROGRAMPROC)glXGetProcAddress((const GLubyte*)"glLinkProgram");
#else
		openglLinkProgram = (PFNGLLINKPROGRAMPROC)wglGetProcAddress("glLinkProgram");
#endif
	}
	if (!openglLinkProgram)
		return 1;

	openglLinkProgram(program);

	return 0;
}

/* glUseProgram */
GLuint opengl_use_program(GLuint program)
{
	if (!openglUseProgram) {
#ifndef WIN32
		openglUseProgram = (PFNGLUSEPROGRAMPROC)glXGetProcAddress((const GLubyte*)"glUseProgram");
#else
		openglUseProgram = (PFNGLUSEPROGRAMPROC)wglGetProcAddress("glUseProgram");
#endif
	}
	if (!openglUseProgram)
		return 1;

	openglUseProgram(program);

	return 0;
}

/* glDeleteProgram */
GLuint opengl_delete_program(GLuint program)
{
	if (!openglDeleteProgram) {
#ifndef WIN32
		openglDeleteProgram = (PFNGLDELETEPROGRAMPROC)glXGetProcAddress((const GLubyte*)"glDeleteProgram");
#else
		openglDeleteProgram = (PFNGLDELETEPROGRAMPROC)wglGetProcAddress("glDeleteProgram");
#endif
	}
	if (!openglDeleteProgram)
		return 1;

	openglDeleteProgram(program);

	return 0;
}
