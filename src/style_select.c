/************************************************************************
* style_select.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

static int w_type(char* str)
{
	if (!strcmp(str,"*"))
		return UI_WIDGET_VOID;
	if (!strcmp(str,"label"))
		return UI_WIDGET_LABEL;
	if (!strcmp(str,"button"))
		return UI_WIDGET_BUTTON;
	if (!strcmp(str,"text"))
		return UI_WIDGET_TEXT;
	if (!strcmp(str,"num"))
		return UI_WIDGET_NUM;
	if (!strcmp(str,"check"))
		return UI_WIDGET_CHECK;
	if (!strcmp(str,"console"))
		return UI_WIDGET_CONSOLE;
	if (!strcmp(str,"header"))
		return UI_WIDGET_HEADER;
	if (!strcmp(str,"pbar"))
		return UI_WIDGET_PBAR;
	if (!strcmp(str,"textbox"))
		return UI_WIDGET_TEXTBOX;
	if (!strcmp(str,"slide"))
		return UI_WIDGET_HSLIDE;
	if (!strcmp(str,"list"))
		return UI_WIDGET_LIST;

	return UI_WIDGET_NULL;
}

/*
 {*,container_name}.{!,*,widget_type}.{*,widget_name}[:hover]
* select an array of widgets using a style selector
*/
array_t *style_select(char* selector)
{
	char* sel;
	char* tmp;
	char* cname = NULL;
	char* wtype = NULL;
	char* wname = NULL;
	char* ext = NULL;
	array_t *r;
	widget_t *w;
	if (!selector)
		return NULL;

	sel = alloca(strlen(selector)+1);
	strcpy(sel,selector);

	r = array_create(RTG_TYPE_PTR);

	cname = sel;
	wtype = strchr(cname,'.');
	if (!wtype)
		return NULL;
	*wtype = 0;
	wtype++;
	wname = strchr(wtype,'.');
	if (!wname)
		return NULL;
	*wname = 0;
	wname++;
	ext = strchr(wname,':');
	if (ext) {
		*ext = 0;
		ext++;
	}

	if (!strcmp(cname,"*")) {
		w = containers;
		while (w) {
			array_push_ptr(r,w);
			w = w->next;
		}
	}else{
		w = containers;
		if (cname[0] == '#') {
			int id = strtol(cname+1,NULL,10);
			while (w) {
				if (w->id == id)
					array_push_ptr(r,w);
				w = w->next;
			}
		}else{
			while (w) {
				tmp = xml_attribute(w->x,"name");
				if (tmp && !strcmp(tmp,cname))
					array_push_ptr(r,w);
				w = w->next;
			}
		}
	}

	if (strcmp(wtype,"!")) {
		array_t *l = r;
		int t = w_type(wtype);
		r = array_create(RTG_TYPE_PTR);
		if (t == UI_WIDGET_VOID) {
			while ((w = array_pop_ptr(l))) {
				w = w->child;
				while (w) {
					array_push_ptr(r,w);
					w = w->next;
				}
			}
		}else{
			while ((w = array_pop_ptr(l))) {
				w = w->child;
				while (w) {
					if (w->type == t)
						array_push_ptr(r,w);
					w = w->next;
				}
			}
		}
		array_free(l);
		if (strcmp(wname,"*")) {
			l = r;
			r = array_create(RTG_TYPE_PTR);
			if (wname[0] == '#') {
				int id = strtol(wname+1,NULL,10);
				while ((w = array_pop_ptr(l))) {
					if (w->id == id)
						array_push_ptr(r,w);
				}
			}else{
				while ((w = array_pop_ptr(l))) {
					tmp = xml_attribute(w->x,"name");
					if (tmp && !strcmp(tmp,wname))
						array_push_ptr(r,w);
				}
			}
			array_free(l);
		}
	}

	if (ext) {
		int i;
		array_t *l;
		array_t *col = array_split(ext,":");
		for (i=0; i<col->length; i++) {
			ext = array_get_string(col,i);
			if (!strcmp(ext,"hover")) {
				l = r;
				r = array_create(RTG_TYPE_PTR);
				while ((w = array_pop_ptr(l))) {
					if (w->hover)
						array_push_ptr(r,w);
				}
				array_free(l);
			}else if (!strcmp(ext,"focus")) {
				l = r;
				r = array_create(RTG_TYPE_PTR);
				if (focused_widget) {
					while ((w = array_pop_ptr(l))) {
						if (w->id == focused_widget->id)
							array_push_ptr(r,w);
					}
				}
				array_free(l);
			}else if (!strcmp(ext,"checked")) {
				l = r;
				r = array_create(RTG_TYPE_PTR);
				while ((w = array_pop_ptr(l))) {
					if (w->type == UI_WIDGET_CHECK && w->data->chk.checked)
						array_push_ptr(r,w);
				}
				array_free(l);
			}
		}
		array_free(col);
	}

	return r;
}
