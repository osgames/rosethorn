/************************************************************************
* render2d.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

#include <ctype.h>

typedef struct render_section_s {
	struct render_section_s *prev;
	struct render_section_s *next;
	int type;
	material_t *mat;
	color_t col;
	int has_col;
	char txt[1024];
	int vl;
	int vn;
	GLint *v;
	int tl;
	int tn;
	GLfloat *tc;
} render_section_t;

render_section_t *render2d_stack = NULL;
render_section_t *current_2dsection = NULL;

static render_section_t *render2d_section_create()
{
	render_section_t *r = malloc(sizeof(render_section_t));
	r->mat = NULL;
	r->has_col = 0;
	r->txt[0] = 0;
	r->vl = 0;
	r->vn = 0;
	r->v = NULL;
	r->tl = 0;
	r->tn = 0;
	r->tc = NULL;

	return r;
}

static void render2d_get_section(color_t *c, material_t *m, int t)
{
	if (!current_2dsection) {
		if (!render2d_stack) {
			current_2dsection = render2d_section_create();
			render2d_stack = list_push(&render2d_stack,current_2dsection);
		}else{
			current_2dsection = render2d_stack;
		}
	}

	if (render_get_color())
		c = render_get_color();

	if (!current_2dsection->has_col && !current_2dsection->mat && !current_2dsection->txt[0])
		goto set_sect;

	if (current_2dsection->txt[0])
		goto adv_sect;

	if (c) {
		if (
			!current_2dsection->has_col
			|| current_2dsection->col.r != c->r
			|| current_2dsection->col.g != c->g
			|| current_2dsection->col.b != c->b
			|| current_2dsection->col.a != c->a
		)
			goto adv_sect;
	}else if (current_2dsection->has_col) {
		goto adv_sect;
	}

	if (m) {
		if (current_2dsection->mat != m)
			goto adv_sect;
	}else if (current_2dsection->mat) {
		goto adv_sect;
	}

	if (current_2dsection->type != t)
		goto adv_sect;

	return;
adv_sect:
	if (!current_2dsection->next) {
		current_2dsection = render2d_section_create();
		render2d_stack = list_push(&render2d_stack,current_2dsection);
	}else{
		current_2dsection = current_2dsection->next;
	}
set_sect:
	if (c) {
		current_2dsection->col.r = c->r;
		current_2dsection->col.g = c->g;
		current_2dsection->col.b = c->b;
		current_2dsection->col.a = c->a;
		current_2dsection->has_col = 1;
	}
	if (m) {
		current_2dsection->mat = m;
	}

	current_2dsection->type = t;
}

static void render2d_vertex_push(GLint x, GLint y)
{
	if (current_2dsection->vl <= current_2dsection->vn+2) {
		GLint *v = realloc(current_2dsection->v,sizeof(GLint)*(current_2dsection->vl+8));
		if (!v)
			return;
		current_2dsection->v = v;
		current_2dsection->vl += 8;
	}

	current_2dsection->v[current_2dsection->vn++] = x;
	current_2dsection->v[current_2dsection->vn++] = y;
}

static void render2d_texcoord_push(GLfloat x, GLfloat y)
{
	if (current_2dsection->tl <= current_2dsection->tn+2) {
		GLfloat *t = realloc(current_2dsection->tc,sizeof(GLfloat)*(current_2dsection->tl+8));
		if (!t)
			return;
		current_2dsection->tc = t;
		current_2dsection->tl += 8;
	}

	current_2dsection->tc[current_2dsection->tn++] = x;
	current_2dsection->tc[current_2dsection->tn++] = y;
}

static void render2d_string(int x, int y, int font, int size, char* str)
{
	int i;
	int yo = 0;
	float sc = ((1.0/30.0)*size);
	float modelview_matrix[16];
	array_t *lines;
	font_t *f;
	char* line;

	f = font_get(font);

	glPushMatrix();
	glScalef(sc,sc,0);

	glEnable(GL_TEXTURE_2D);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);
	glListBase(f->list_base);


	lines = array_split(str,"\n");

	for (i=0; i<lines->length; i++) {
		line = array_get_string(lines,i);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(x,(y+yo),0);
		glMultMatrixf(modelview_matrix);
		glCallLists(strlen(line), GL_UNSIGNED_BYTE, line);
		yo += size;
		glPopMatrix();
	}
	array_free(lines);
	glPopMatrix();
}

/* render a 2d line */
void render2d_line(color_t *c, int x, int y, int ex, int ey)
{
	render2d_get_section(c,NULL,RD_LINE);

	render2d_vertex_push(x,y);
	render2d_vertex_push(ex,ey);
}

/* render a 2d quad of colour */
void render2d_quad_color(color_t *c, int x, int y, int w, int h)
{
	render2d_get_section(c,NULL,RD_QUAD);

	render2d_vertex_push(x,y);
	render2d_vertex_push(x+w, y);
	render2d_vertex_push(x+w, y+h);
	render2d_vertex_push(x, y+h);
}

/* render a 2d quad of material */
void render2d_quad_mat(material_t *m, int x, int y, int w, int h)
{
	render2d_get_section(NULL,m,RD_QUAD);

	render2d_vertex_push(x,y+h);
	render2d_vertex_push(x+w, y+h);
	render2d_vertex_push(x+w, y);
	render2d_vertex_push(x, y);

	render2d_texcoord_push(0,1);
	render2d_texcoord_push(1,1);
	render2d_texcoord_push(1,0);
	render2d_texcoord_push(0,0);
}

/* render a 2d quad of material from vertex and texcoord data */
void render2d_quad_mat_vt(material_t *m, GLint v[8], GLfloat t[8])
{
	render2d_get_section(NULL,m,RD_QUAD);

	render2d_vertex_push(v[0],v[1]);
	render2d_vertex_push(v[2],v[3]);
	render2d_vertex_push(v[4],v[5]);
	render2d_vertex_push(v[6],v[7]);

	render2d_texcoord_push(t[0],t[1]);
	render2d_texcoord_push(t[2],t[3]);
	render2d_texcoord_push(t[4],t[5]);
	render2d_texcoord_push(t[6],t[7]);
}

/* render text */
void render2d_text(int x, int y, int font, int size, char* str)
{
	render2d_get_section(NULL,NULL,RD_TEXT);

	strcpy(current_2dsection->txt,str);
	render2d_vertex_push(x,y);
	render2d_vertex_push(font,size);
}

/* render 2d graphics to the frame */
void render2d()
{
	int s = 0;
	int v = 0;

	current_2dsection = render2d_stack;

	while (
		current_2dsection
		&& (
			current_2dsection->has_col
			|| current_2dsection->mat
			|| current_2dsection->txt[0]
		)
	) {
		glColor4f(1.0,1.0,1.0,1.0);
		if (current_2dsection->mat) {
			mat_use(current_2dsection->mat);
		}else{
			glDisable(GL_TEXTURE_2D);
		}
		if (current_2dsection->has_col)
			glColor4ub(current_2dsection->col.r,current_2dsection->col.g,current_2dsection->col.b,current_2dsection->col.a);

		if (current_2dsection->txt[0]) {
			glDisableClientState(GL_VERTEX_ARRAY);
			render2d_string(current_2dsection->v[0],current_2dsection->v[1],current_2dsection->v[2],current_2dsection->v[3],current_2dsection->txt);
			current_2dsection->txt[0] = 0;
			glEnableClientState(GL_VERTEX_ARRAY);
		}else{
			if (current_2dsection->mat) {
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				glVertexPointer(2, GL_INT, 0, current_2dsection->v);
				glTexCoordPointer(2, GL_FLOAT, 0, current_2dsection->tc);
				glDrawArrays(GL_QUADS, 0, current_2dsection->vn/2);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
				v += current_2dsection->vn/2;
			}else{
				glVertexPointer(2, GL_INT, 0, current_2dsection->v);
				if (current_2dsection->type == RD_QUAD) {
					glDrawArrays(GL_QUADS, 0, current_2dsection->vn/2);
				}else{
					glDrawArrays(GL_LINES, 0, current_2dsection->vn/2);
				}
				v += current_2dsection->vn/2;
			}
		}

		current_2dsection->mat = NULL;
		current_2dsection->vn = 0;
		current_2dsection->tn = 0;
		current_2dsection->has_col = 0;
		s++;
		current_2dsection = current_2dsection->next;
	}

	current_2dsection = NULL;
}
