/************************************************************************
* ui_events.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

widget_t *focused_widget = NULL;
widget_t *grabbed_container = NULL;

/* numbox key down event */
int ui_widget_event_numbox_kdown(widget_t *w)
{
	/* uses the textbox key down to get the data */
	if (ui_widget_event_text_kdown(w) == EVENT_UNHANDLED)
		return EVENT_UNHANDLED;

	w->data->num.value = atoi(w->text);
	if (w->data->num.min < w->data->num.max) {
		if (w->data->num.value < w->data->num.min)
			w->data->num.value = w->data->num.min;
		if (w->data->num.value > w->data->num.max)
			w->data->num.value = w->data->num.max;
	}

	free(w->text);
	w->text = malloc(sizeof(char)*20);
	sprintf(w->text,"%d",w->data->num.value);

	return EVENT_HANDLED;
}

/* numbox scroll down (decrement number) */
int ui_widget_event_numbox_sdown(widget_t *w)
{
	w->data->num.value--;
	if (w->data->num.min < w->data->num.max) {
		if (w->data->num.value < w->data->num.min)
			w->data->num.value = w->data->num.min;
		if (w->data->num.value > w->data->num.max)
			w->data->num.value = w->data->num.max;
	}

	free(w->text);
	w->text = malloc(sizeof(char)*20);
	sprintf(w->text,"%d",w->data->num.value);

	focused_widget = w;
	return EVENT_HANDLED;
}

/* numbox scroll down (increment number) */
int ui_widget_event_numbox_sup(widget_t *w)
{
	w->data->num.value++;
	if (w->data->num.min < w->data->num.max) {
		if (w->data->num.value < w->data->num.min)
			w->data->num.value = w->data->num.min;
		if (w->data->num.value > w->data->num.max)
			w->data->num.value = w->data->num.max;
	}

	free(w->text);
	w->text = malloc(sizeof(char)*20);
	sprintf(w->text,"%d",w->data->num.value);

	focused_widget = w;
	return EVENT_HANDLED;
}

/* list scroll down */
int ui_widget_event_list_sdown(widget_t *w)
{
	int h;
	h = w->style.h /w->style.font_size;
	if (w->data->lst.rows-w->data->lst.offset > h)
		w->data->lst.offset++;

	return EVENT_UNHANDLED;
}

/* list scroll up */
int ui_widget_event_list_sup(widget_t *w)
{
	if (w->data->lst.offset)
		w->data->lst.offset--;

	return EVENT_UNHANDLED;
}

/* textbox keydown */
int ui_widget_event_text_kdown(widget_t *w)
{
	if (current_event.sym == RTK_BackSpace) {
		if (w->text && w->text[0]) {
			w->text[strlen(w->text)-1] = 0;
			return EVENT_HANDLED;
		}else{
			return EVENT_UNHANDLED;
		}
	}else if (!isprint(current_event.ch)) {
		return EVENT_UNHANDLED;
	}

	if (w->text) {
		char d[2];
		sprintf(d,"%c",(char)current_event.ch);
		w->text = realloc(w->text,strlen(w->text)+2);
		strcat(w->text,d);
	}else{
		w->text = malloc(2);
		sprintf(w->text,"%c",(char)current_event.ch);
	}

	return EVENT_HANDLED;
}

/* check box click */
int ui_widget_event_check_mclick(widget_t *w)
{
	w->data->chk.checked = !w->data->chk.checked;

	focused_widget = w;
	return EVENT_HANDLED;
}

/* console key down */
int ui_widget_event_console_kdown(widget_t *w)
{
	if (current_event.sym == RTK_Return) {
		if (!w->data->con.cchar)
			return EVENT_UNHANDLED;
		ui_widget_console_write(w,w->data->con.command);
		/* if it's a command exec it, otherwise send to server as chat */
		if (w->data->con.command[0] == '/')
			cmd_exec(w->data->con.command+1);

		w->data->con.command[0] = 0;
		w->data->con.cchar = 0;
		return EVENT_HANDLED;
	}else if (current_event.sym == RTK_BackSpace) {
		if (w->data->con.cchar) {
			w->data->con.cchar--;
			w->data->con.command[w->data->con.cchar] = 0;
			return EVENT_HANDLED;
		}else{
			return EVENT_UNHANDLED;
		}
	}else if ((!isgraph(current_event.ch) && current_event.ch != ' ') || w->data->con.cchar > 98) {
		return EVENT_UNHANDLED;
	}

	w->data->con.command[w->data->con.cchar] = (char)current_event.ch;
	w->data->con.cchar++;
	w->data->con.command[w->data->con.cchar] = 0;

	return EVENT_HANDLED;
}

/* slide motion handler */
int ui_widget_event_slide_mmove(widget_t *w)
{
	if (!button[RTM_LEFT_BUTTON])
		return EVENT_UNHANDLED;

	if (w->type == UI_WIDGET_HSLIDE) {
		int x = mouse[0]-ui_widget_position_x(w);
		w->data->sld.value = ((float)((w->data->sld.max-w->data->sld.min)+1)/(float)w->style.w)*x;

		if (w->data->sld.value > w->data->sld.max)
			w->data->sld.value = w->data->sld.max;

		if (w->data->sld.value < w->data->sld.min)
			w->data->sld.value = w->data->sld.min;

		return EVENT_HANDLED;
	}

	return EVENT_UNHANDLED;
}

/* set a widget event to a C function */
int ui_widget_event_set(
	char* sel,
	int (*mclick)(struct widget_s *w),
	int (*mdown)(struct widget_s *w),
	int (*mup)(struct widget_s *w),
	int (*sdown)(struct widget_s *w),
	int (*sup)(struct widget_s *w),
	int (*mmove)(struct widget_s *w),
	int (*kdown)(struct widget_s *w),
	int (*kup)(struct widget_s *w),
	int (*show)(struct widget_s *w),
	int (*hide)(struct widget_s *w)
)
{
	array_t *a = style_select(sel);
	if (a) {
		widget_t **w = a->data;
		int i;
		for (i=0; i<a->length; i++) {
			if (mclick)
				w[i]->events->mclick = mclick;
			if (mdown)
				w[i]->events->mdown = mdown;
			if (mup)
				w[i]->events->mup = mup;
			if (sdown)
				w[i]->events->sdown = sdown;
			if (sup)
				w[i]->events->sup = sup;
			if (mmove)
				w[i]->events->mmove = mmove;
			if (kdown)
				w[i]->events->kdown = kdown;
			if (kup)
				w[i]->events->kup = kup;
			if (show)
				w[i]->events->show = show;
			if (hide)
				w[i]->events->hide = hide;
		}
		array_free(a);
	}

	return 0;
}

/* ui event handler */
int ui_events()
{
	widget_t *w;
	int cx;
	int cy;
	int ex;
	int ey;
	int hit = 0;
	char* xa;
	int r = EVENT_UNHANDLED;

	/* key events work only on the focused widget, if available */
	if (focused_widget) {
		if (focused_widget->style.visible && focused_widget->parent->style.visible) {
			if (current_event.type == RTE_KEY_DOWN) {
				if (focused_widget->events && focused_widget->events->kdown)
					return focused_widget->events->kdown(focused_widget);
				return EVENT_UNHANDLED;
			}else if (current_event.type == RTE_KEY_UP) {
				if (focused_widget->events && focused_widget->events->kup)
					return focused_widget->events->kup(focused_widget);
				return EVENT_UNHANDLED;
			}
		}else{
			focused_widget = NULL;
		}
	}

	/* otherwise see if the mouse is over a container */
	w = containers;
	if (!w)
		return EVENT_UNHANDLED;
	while (w->next) {
		w = w->next;
	}
	while (w) {
		if (!w->style.visible) {
			w = w->prev;
			continue;
		}
		cx = ui_widget_position_x(w);
		cy = ui_widget_position_y(w);
		if (!hit && mouse[0] > cx && mouse[0] < cx+w->style.w && mouse[1] > cy && mouse[1] < cy+w->style.h ) {
			hit = 1;
			w->hover = 1;
			/* right click + drag moves the container */
			if (grabbed_container && grabbed_container->id == w->id) {
				hit = 2;
				if (current_event.type == RTE_MOUSE_MOTION && button[RTM_RIGHT_BUTTON]) {
					w->style.x = cx + rmouse[0];
					w->style.y = cy + rmouse[1];
					if (w->style.x < 0)
						w->style.x = UI_ALIGN_LEFT;
					if (w->style.y < 0)
						w->style.y = UI_ALIGN_TOP;
					if (w->style.y >= (wm_res.height-w->style.h))
						w->style.y = UI_ALIGN_BOTTOM;
					if (w->style.x >= (wm_res.width-w->style.w))
						w->style.x = UI_ALIGN_RIGHT;
					r = EVENT_HANDLED;
				/* save position */
				}else if (current_event.type == RTE_BUTTON_UP && current_event.button == RTM_RIGHT_BUTTON && w->x) {
					xa = xml_attribute(grabbed_container->x,"name");
					if (xa) {
						ex = w->style.x;
						ey = w->style.y;
						cmd_execf("set ui.elements(%s.!.*.style.x) %d",xa,ex);
						cmd_execf("set ui.elements(%s.!.*.style.y) %d",xa,ey);
					}
					grabbed_container = NULL;
				}
			}else if (current_event.type == RTE_BUTTON_DOWN && current_event.button == RTM_RIGHT_BUTTON) {
				grabbed_container = w;
				r = EVENT_HANDLED;
			}
			if (ui_events_container(w) == EVENT_HANDLED) {
				hit = 1;
				r = EVENT_HANDLED;
			}
		}else{
			w->hover = 0;
		}
		w = w->prev;
	}

	/* unhandled clicks unfocus all widgets... click on game area to stop widgets capturing key events */
	if (!hit && current_event.type == RTE_BUTTON_DOWN)
		focused_widget = NULL;

	if (hit == 1)
		style_refresh(ui.styles,ui.elements);

	return r;
}

/* check for mouse events within a container */
int ui_events_container(widget_t *c)
{
	widget_t *w;
	int wx;
	int wy;
	int hit = 0;
	int r = EVENT_UNHANDLED;

	w = c->child;
	/* see if the mouse is over a widget */
	while (w) {
		if (!w->style.visible) {
			w = w->next;
			continue;
		}
		wx = ui_widget_position_x(w);
		wy = ui_widget_position_y(w);
		if (!hit && mouse[0] > wx && mouse[0] < wx+w->style.w && mouse[1] > wy && mouse[1] < wy+w->style.h ) {
			hit = 1;
			w->hover = 1;
			if (!w->events) {
				w = w->next;
				continue;
			}
			if (current_event.type == RTE_BUTTON_DOWN) {
				/* clicking on a widget makes it focused */
				focused_widget = w;
				if (!w->events)
					r = EVENT_HANDLED;
				if (current_event.button == RTM_LEFT_BUTTON) {
					if (w->events->mdown)
						w->events->mdown(w);
				}else if (current_event.button == RTM_SCROLL_UP) {
					if (w->events->sup)
						w->events->sup(w);
				}else if (current_event.button == RTM_SCROLL_DOWN) {
					if (w->events->sdown)
						w->events->sdown(w);
				}
				r = EVENT_HANDLED;
			}else if (current_event.type == RTE_BUTTON_UP && focused_widget && w->id == focused_widget->id) {
				int ur = EVENT_UNHANDLED;
				int cr = EVENT_UNHANDLED;
				if (w->events && current_event.button == RTM_LEFT_BUTTON && w->events->mup)
					ur = w->events->mup(w);
				if (w->events && current_event.button == RTM_LEFT_BUTTON && w->events->mclick)
					cr = w->events->mclick(w);

				if (current_event.button == RTM_LEFT_BUTTON && (ur == EVENT_HANDLED || cr == EVENT_HANDLED))
					r = EVENT_HANDLED;

			}else if (current_event.type == RTE_MOUSE_MOTION) {
				if (w->events && w->events->mmove)
					r = w->events->mmove(w);
			}
		}else{
			w->hover = 0;
		}
		if (r == EVENT_HANDLED)
			break;
		w = w->next;
	}

	return r;
}
