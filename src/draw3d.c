/************************************************************************
* draw3d.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

/* draw a line of colour c */
int draw3d_line(
	color_t *c,
	GLfloat x,
	GLfloat y,
	GLfloat z,
	GLfloat ex,
	GLfloat ey,
	GLfloat ez
)
{
	object_t *o;
	v3_t pos;
	GLfloat px = ((ex-x)/2);
	GLfloat py = ((ey-y)/2);
	GLfloat pz = ((ez-z)/2);
	mesh_t *m = mesh_create(GL_LINES);

	array_push_float(m->v,-px);
	array_push_float(m->v,-py);
	array_push_float(m->v,-pz);
	array_push_float(m->v,px);
	array_push_float(m->v,py);
	array_push_float(m->v,pz);

	m->c = array_create(RTG_TYPE_FLOAT);
	array_push_color(m->c,c);
	array_push_color(m->c,c);

	array_push_int(m->i,0);
	array_push_int(m->i,1);

	pos.x = x+px;
	pos.y = y+py;
	pos.z = z+pz;

	o = render3d_mesh(m,&pos);
	return o->id;
}

/* draw a rect of colour c */
int draw3d_color(
	color_t *c,
	GLfloat x,
	GLfloat y,
	GLfloat z,
	GLfloat ex,
	GLfloat ey,
	GLfloat ez
)
{
	int i;
	object_t *o;
	v3_t pos;
	GLfloat px = ((ex-x)/2);
	GLfloat py = ((ey-y)/2);
	GLfloat pz = ((ez-z)/2);
	mesh_t *m = mesh_create(GL_TRIANGLES);

	array_push_float(m->v,-px);
	array_push_float(m->v,-py);
	array_push_float(m->v,-pz);

	array_push_float(m->v,px);
	array_push_float(m->v,py);
	array_push_float(m->v,-pz);

	array_push_float(m->v,px);
	array_push_float(m->v,-py);
	array_push_float(m->v,pz);

	array_push_float(m->v,-px);
	array_push_float(m->v,py);
	array_push_float(m->v,pz);

	m->c = array_create(RTG_TYPE_FLOAT);
	for (i=0; i<4; i++) {
		array_push_color(m->c,c);
	}

	array_push_int(m->i,0);
	array_push_int(m->i,1);
	array_push_int(m->i,3);
	array_push_int(m->i,1);
	array_push_int(m->i,2);
	array_push_int(m->i,3);

	m->n = array_create(RTG_TYPE_FLOAT);

	mesh_normalise(m);

	pos.x = x+px;
	pos.y = y+py;
	pos.z = z+pz;

	o = render3d_mesh(m,&pos);

	return o->id;
}

/* draw a material */
int draw3d_material(
	material_t *mat,
	GLfloat x,
	GLfloat y,
	GLfloat z,
	GLfloat ex,
	GLfloat ey,
	GLfloat ez
)
{
	object_t *o;
	v3_t pos;
	GLfloat px = ((ex-x)/2);
	GLfloat py = ((ey-y)/2);
	GLfloat pz = ((ez-z)/2);
	mesh_t *m;

	if (!mat->tex) {
		color_t c;
		c.r = mat->diffuse[0]*255.0;
		c.g = mat->diffuse[1]*255.0;
		c.b = mat->diffuse[2]*255.0;
		c.a = mat->diffuse[3]*255.0;
		return draw3d_color(&c,x,y,z,ex,ey,ez);
	}

	m = mesh_create(GL_TRIANGLES);
	m->mat = mat;

	array_push_float(m->v,-px);
	array_push_float(m->v,-py);
	array_push_float(m->v,-pz);

	array_push_float(m->v,px);
	array_push_float(m->v,py);
	array_push_float(m->v,-pz);

	array_push_float(m->v,px);
	array_push_float(m->v,-py);
	array_push_float(m->v,pz);

	array_push_float(m->v,-px);
	array_push_float(m->v,py);
	array_push_float(m->v,pz);

	m->t = array_create(RTG_TYPE_FLOAT);

	array_push_float(m->t,0);
	array_push_float(m->t,0);

	array_push_float(m->t,0);
	array_push_float(m->t,1);

	array_push_float(m->t,1);
	array_push_float(m->t,1);

	array_push_float(m->t,1);
	array_push_float(m->t,0);

	array_push_int(m->i,0);
	array_push_int(m->i,1);
	array_push_int(m->i,3);
	array_push_int(m->i,1);
	array_push_int(m->i,2);
	array_push_int(m->i,3);

	m->n = array_create(RTG_TYPE_FLOAT);

	mesh_normalise(m);

	pos.x = x+px;
	pos.y = y+py;
	pos.z = z+pz;

	o = render3d_mesh(m,&pos);
	return o->id;
}

/* draw a sprite */
int draw3d_sprite(
	sprite_t *s,
	GLfloat x,
	GLfloat y,
	GLfloat z,
	GLfloat ex,
	GLfloat ey,
	GLfloat ez
)
{
	object_t *o;
	v3_t pos;
	GLfloat px = ((ex-x)/2);
	GLfloat py = ((ey-y)/2);
	GLfloat pz = ((ez-z)/2);
	GLfloat sx;
	GLfloat sy;
	GLfloat sw;
	GLfloat sh;
	mesh_t *m;

	if (!s->mat || !s->mat->tex)
		return 0;

	sx = s->mat->tex->w/s->x;
	sy = s->mat->tex->w/s->y;
	sw = sx+(s->mat->tex->w/s->w);
	sh = sy+(s->mat->tex->w/s->h);

	m = mesh_create(GL_TRIANGLES);
	m->mat = s->mat;

	array_push_float(m->v,-px);
	array_push_float(m->v,-py);
	array_push_float(m->v,-pz);

	array_push_float(m->v,px);
	array_push_float(m->v,py);
	array_push_float(m->v,-pz);

	array_push_float(m->v,px);
	array_push_float(m->v,-py);
	array_push_float(m->v,pz);

	array_push_float(m->v,-px);
	array_push_float(m->v,py);
	array_push_float(m->v,pz);

	m->t = array_create(RTG_TYPE_FLOAT);

	array_push_float(m->t,sx);
	array_push_float(m->t,sy);

	array_push_float(m->t,sx);
	array_push_float(m->t,sh);

	array_push_float(m->t,sw);
	array_push_float(m->t,sh);

	array_push_float(m->t,sw);
	array_push_float(m->t,sy);

	array_push_int(m->i,0);
	array_push_int(m->i,1);
	array_push_int(m->i,3);
	array_push_int(m->i,1);
	array_push_int(m->i,2);
	array_push_int(m->i,3);

	m->n = array_create(RTG_TYPE_FLOAT);

	mesh_normalise(m);

	pos.x = x+px;
	pos.y = y+py;
	pos.z = z+pz;

	o = render3d_mesh(m,&pos);

	return o->id;
}

