/************************************************************************
* exports.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

typedef struct rtg_exported_function_s {
	struct rtg_exported_function_s *prev;
	struct rtg_exported_function_s *next;
	char* name;
	unsigned int h_name;
	int (*fn)(widget_t *);
} rtg_exported_function_t;

static rtg_exported_function_t *funcs = NULL;

static rtg_exported_function_t *export_find(char* name)
{
	rtg_exported_function_t *e = funcs;
	unsigned int h = hash(name);
	while (e) {
		if (h == e->h_name && !strcmp(name,e->name))
			return e;
		e = e->next;
	}

	return NULL;
}

void export_function(char* name, int (*f)(widget_t *))
{
	rtg_exported_function_t *e;

	if (!name)
		return;

	e = export_find(name);

	if (!f) {
		if (!e)
			return;
		funcs = list_remove(&funcs,e);
		free(e->name);
		free(e);
		return;
	}

	if (!f)
		return;

	e = malloc(sizeof(rtg_exported_function_t));
	e->name = strdup(name);
	e->h_name = hash(name);
	e->fn = f;
	funcs = list_push(&funcs,e);
}

int export_call_ui(char* fn, widget_t *w)
{
	rtg_exported_function_t *e = export_find(fn);

	if (!e) {
		rtprintf(CN_RTG "function '%s' not found",fn);
		return EVENT_UNHANDLED;
	}

	return e->fn(w);
}
