/************************************************************************
* wm_x11.c
* window management code for X11 (non-windows) systems
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

/* command width setter */
void wm_width_setter(char* value)
{
	int v = strtol(value,NULL,10);
	if (v > 0) {
		wm_res.width = v;
		wm_resize();
	}
}

/* command height setter */
void wm_height_setter(char* value)
{
	int v = strtol(value,NULL,10);
	if (v > 0) {
		wm_res.height = v;
		wm_resize();
	}
}

/* command frame cap setter */
void wm_cap_setter(char* value)
{
	int v = strtol(value,NULL,10);
	if (v > 0)
		wm_res.frame_cap = v;
}

/* command fullscreen setter */
void wm_fs_setter(char* value)
{
	int v = !!strtol(value,NULL,10);
	wm_toggle_fullscreen(v);
}
#ifdef RTG3D
/* command maximum render distance setter */
void wm_distance_setter(char* value)
{
	float v = strtof(value,NULL);
	if (v > 0)
		wm_res.distance = v;
}
#endif

/* screen capture - take a screenshot, save to file */
void wm_capture(char* file)
{
	char* fmt;
	image_t *p;

	p = image_load_fromscreen(0,0,wm_res.width,wm_res.height,0);
	if (!p) {
		rtprintf(CN_ERROR "Could not grab screen data");
		return;
	}

	fmt = config_get("wm.capture_format");
#ifdef RTG_HAVE_JPG
	if (!fmt || !strcmp(fmt,"jpg")) {
		if (!file)
			file = "screenshot.jpg";
		image_save_jpg(p,file);
	}else
#endif
#ifdef RTG_HAVE_PNG
	if (!fmt || !strcmp(fmt,"png")) {
		if (!file)
			file = "screenshot.png";
		image_save_png(p,file);
	}else
#endif
#ifdef RTG_HAVE_TGA
	if (!fmt || !strcmp(fmt,"tga")) {
		if (!file)
			file = "screenshot.tga";
		image_save_tga(p,file);
	}else
#endif
#ifdef RTG_HAVE_BMP
	if (!fmt || !strcmp(fmt,"bmp")) {
		if (!file)
			file = "screenshot.bmp";
		image_save_bmp(p,file);
	}else
#endif
	if (fmt) {
		rtprintf(CN_ERROR "Unsupported capture format '%s'",fmt);
	}else{
		rtprintf(CN_ERROR "Unsupported capture format");
	}

	free(p->pixels);
	free(p);

}
