/************************************************************************
* wm_x11.c
* window management code for X11 (non-windows) systems
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

#ifndef WIN32

#include <X11/Xatom.h>

static int wm_has_init = 0;

/* attributes for a double buffered visual in RGBA format with at least
 * 4 bits per color and a 16 bit depth buffer */
static int attr_list[] = {
	GLX_RENDER_TYPE, GLX_RGBA_BIT,
	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
	GLX_DOUBLEBUFFER, True,
	GLX_X_RENDERABLE, True,
	GLX_RED_SIZE, 4,
	GLX_GREEN_SIZE, 4,
	GLX_BLUE_SIZE, 4,
	GLX_ALPHA_SIZE, 4,
	GLX_DEPTH_SIZE, 16,
	None
};

/* initialise the game window */
int wm_init()
{
	int min;
	int max;
	char* glXc;
	char* glXv;
	wm_has_init = 1;
	/* get a connection */
	wm_res.dpy = XOpenDisplay(0);
	wm_res.screen = DefaultScreen(wm_res.dpy);
	wm_res.fb_cfg = NULL;
	wm_res.cursor.mat = NULL;
	/* get a connection */
	XF86VidModeQueryVersion(wm_res.dpy, &max, &min);
	rtprintf("Using XF86VidModeExtension %d.%d", max, min);
	XF86VidModeGetAllModeLines(wm_res.dpy, wm_res.screen, &wm_res.mode_count, &wm_res.modes);

	/* save desktop resolution before switching modes */
	wm_res.deskMode = *wm_res.modes[0];

	glXc = (char*)glXGetClientString(wm_res.dpy, GLX_VENDOR);
	glXv = (char*)glXGetClientString(wm_res.dpy, GLX_VERSION);
	rtprintf("Using %s glX %s",glXc,glXv);

	return wm_create();
}

/* exit the game window */
void wm_exit()
{
	wm_destroy();
	XFree(wm_res.modes);
	XCloseDisplay(wm_res.dpy);
}

/* create a window */
int wm_create()
{
	Colormap cmap;
	Atom wmDelete;
	Window winDummy;
	unsigned int borderDummy;
	int x;
	int y;
	unsigned int w;
	unsigned int h;
	unsigned int d;
	int i;

	if (!wm_res.fb_cfg) {
		GLXFBConfig *fb_configs = NULL;
		int num_fb_configs = 0;

		fb_configs = glXChooseFBConfig(wm_res.dpy, wm_res.screen, attr_list, &num_fb_configs);
		if (!fb_configs || !num_fb_configs) {
			fb_configs = glXGetFBConfigs(wm_res.dpy, wm_res.screen, &num_fb_configs);
			if (!fb_configs || !num_fb_configs)
				return 1;
		}
		wm_res.fb_cfg = fb_configs[0];
		glXGetFBConfigAttrib(wm_res.dpy, wm_res.fb_cfg, GLX_DOUBLEBUFFER, &wm_res.dblbuff);
		if (wm_res.dblbuff)
			rtprintf("Using Double Buffering");
		wm_res.vi = glXGetVisualFromFBConfig(wm_res.dpy, wm_res.fb_cfg);

		wm_res.ctx = glXCreateNewContext(wm_res.dpy, wm_res.fb_cfg, GLX_RGBA_TYPE, 0, True);
		if (!wm_res.ctx)
			return 1;
	}

	/* set best mode to current */
	wm_res.mode = 0;
	/* look for mode with requested resolution */
	for (i=0; i<wm_res.mode_count; i++) {
		if ((wm_res.modes[i]->hdisplay == wm_res.width) && (wm_res.modes[i]->vdisplay == wm_res.height)) {
			wm_res.mode = i;
			break;
		}
	}

	/* create a color map */
	cmap = XCreateColormap(
		wm_res.dpy,
		RootWindow(wm_res.dpy, wm_res.vi->screen),
		wm_res.vi->visual,
		AllocNone
	);

	wm_res.attr.colormap = cmap;
	wm_res.attr.border_pixel = 0;
	wm_res.attr.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask;

	if (wm_res.fullscreen) {
		XF86VidModeSwitchToMode(wm_res.dpy, wm_res.screen, wm_res.modes[wm_res.mode]);
		XF86VidModeSetViewPort(wm_res.dpy, wm_res.screen, 0, 0);

		/* create a fullscreen window */
		wm_res.attr.override_redirect = True;
		wm_res.win = XCreateWindow(
			wm_res.dpy,
			RootWindow(wm_res.dpy, wm_res.vi->screen),
			0,
			0,
			wm_res.modes[wm_res.mode]->hdisplay,
			wm_res.modes[wm_res.mode]->vdisplay,
			0,
			wm_res.vi->depth,
			InputOutput,
			wm_res.vi->visual,
			CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect,
			&wm_res.attr
		);

		XWarpPointer(wm_res.dpy, None, wm_res.win, 0, 0, 0, 0, 0, 0);
		XMapRaised(wm_res.dpy, wm_res.win);
		XGrabKeyboard(wm_res.dpy, wm_res.win, True, GrabModeAsync, GrabModeAsync, CurrentTime);
		XGrabPointer(wm_res.dpy, wm_res.win, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, wm_res.win, None, CurrentTime);
	}else{
		XSizeHints *hints;
		/* create a window */
		wm_res.win = XCreateWindow(
			wm_res.dpy,
			RootWindow(wm_res.dpy, wm_res.vi->screen),
			0,
			0,
			wm_res.width,
			wm_res.height,
			0,
			wm_res.vi->depth,
			InputOutput,
			wm_res.vi->visual,
			CWBorderPixel | CWColormap | CWEventMask,
			&wm_res.attr
		);

		/* handle wm_delete_events */
		wmDelete = XInternAtom(wm_res.dpy, "WM_DELETE_WINDOW", True);
		XSetWMProtocols(wm_res.dpy, wm_res.win, &wmDelete, 1);
		XMapRaised(wm_res.dpy, wm_res.win);

		/* set window title */
		XStoreName(wm_res.dpy, wm_res.win, wm_res.title);

		/* prevent window resizing */
		hints = XAllocSizeHints();
		hints->min_width = hints->max_width = wm_res.width;
		hints->min_height = hints->max_height = wm_res.height;
		hints->flags = PMinSize | PMaxSize;
		XSetWMNormalHints(wm_res.dpy, wm_res.win, hints);
		XFree(hints);
	}

	wm_res.glxwin = glXCreateWindow(wm_res.dpy, wm_res.fb_cfg, wm_res.win, NULL);

	/* make OpenGL context current */
	if (!glXMakeContextCurrent(wm_res.dpy, wm_res.glxwin, wm_res.glxwin, wm_res.ctx)) {
		glXDestroyContext(wm_res.dpy, wm_res.ctx);
		return 1;
	}

	XGetGeometry(
		wm_res.dpy,
		wm_res.win,
		&winDummy,
		&x,
		&y,
		&w,
		&h,
		&borderDummy,
		&d
	);

	wm_res.width = (int)w;
	wm_res.height = (int)h;

	if (glXIsDirect(wm_res.dpy, wm_res.ctx))
		rtprintf("Using Direct Rendering");

	return 0;
}

/* resize the screen, fullscreen or windowed */
int wm_resize()
{
	if (!wm_has_init)
		return 0;

	XResizeWindow(wm_res.dpy, wm_res.win, wm_res.width, wm_res.height);

	return 0;
}

/* flush graphics through and flip buffers */
int wm_update()
{
	glFlush();
	/* update the screen */
	if (wm_res.dblbuff)
		glXSwapBuffers(wm_res.dpy, wm_res.glxwin);
	return 0;
}

/* destroy the current window */
void wm_destroy()
{
	if (wm_res.ctx) {
		glXMakeContextCurrent(wm_res.dpy, None, None, NULL);

		glXDestroyContext(wm_res.dpy, wm_res.ctx);
		wm_res.ctx = NULL;
		glXDestroyWindow(wm_res.dpy,wm_res.glxwin);
	}
	/* switch back to original desktop resolution if we were in fs */
	if (wm_res.fullscreen) {
		XF86VidModeSwitchToMode(wm_res.dpy, wm_res.screen, &wm_res.deskMode);
		XF86VidModeSetViewPort(wm_res.dpy, wm_res.screen, 0, 0);
	}

}

/* set fullscreen on/off */
void wm_toggle_fullscreen(int fs)
{
	if (fs == wm_res.fullscreen)
		return;

	if (!wm_has_init) {
		wm_res.fullscreen = fs;
		return;
	}
	if (wm_res.ctx) {
		glXMakeContextCurrent(wm_res.dpy, None, None, NULL);
		glXDestroyWindow(wm_res.dpy,wm_res.glxwin);
	}
	/* switch back to original desktop resolution if we were in fs */
	if (wm_res.fullscreen) {
		XF86VidModeSwitchToMode(wm_res.dpy, wm_res.screen, &wm_res.deskMode);
		XF86VidModeSetViewPort(wm_res.dpy, wm_res.screen, 0, 0);
	}
	wm_res.fullscreen = fs;
	/* TODO: this isn't working in glX */
	wm_create();
}

/* use file as a cursor texture */
void wm_cursor(char* file, int width, int height, int offset_x, int offset_y)
{
	Cursor invisibleCursor;
	Pixmap bitmapNoData;
	XColor black;
	static char noData[] = { 0,0,0,0,0,0,0,0 };

	if (!file) {
		if (!wm_res.cursor.mat)
			return;

		wm_res.cursor.mat = NULL;
		XUndefineCursor(wm_res.dpy, wm_res.win);

		return;
	}

	wm_res.cursor.mat = mat_from_image(file);
	wm_res.cursor.w = width;
	wm_res.cursor.h = height;
	wm_res.cursor.x = offset_x;
	wm_res.cursor.y = offset_y;

	if (!wm_res.cursor.mat)
		return;

	black.red = 0;
	black.green = 0;
	black.blue = 0;

	bitmapNoData = XCreateBitmapFromData(wm_res.dpy, wm_res.win, noData, 8, 8);
	invisibleCursor = XCreatePixmapCursor(wm_res.dpy, bitmapNoData, bitmapNoData, &black, &black, 0, 0);
	XDefineCursor(wm_res.dpy, wm_res.win, invisibleCursor);
	XFreeCursor(wm_res.dpy, invisibleCursor);
}

/* grab the mouse */
void wm_grab()
{
	Cursor invisibleCursor;
	Pixmap bitmapNoData;
	XColor black;
	static char noData[] = { 0,0,0,0,0,0,0,0 };

	if (mouse[MGRAB])
		return;

	black.red = 0;
	black.green = 0;
	black.blue = 0;

	bitmapNoData = XCreateBitmapFromData(wm_res.dpy, wm_res.win, noData, 8, 8);
	invisibleCursor = XCreatePixmapCursor(wm_res.dpy, bitmapNoData, bitmapNoData, &black, &black, 0, 0);
	XDefineCursor(wm_res.dpy, wm_res.win, invisibleCursor);
	XFreeCursor(wm_res.dpy, invisibleCursor);

	if (!wm_res.fullscreen)
		XGrabPointer(wm_res.dpy, wm_res.win, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, wm_res.win, None, CurrentTime);

	mouse[MGRAB] = 1;
}

/* stop grabbing the mouse */
void wm_ungrab()
{
	if (!mouse[MGRAB])
		return;

	XUndefineCursor(wm_res.dpy, wm_res.win);
	if (!wm_res.fullscreen)
		XUngrabPointer(wm_res.dpy, CurrentTime);

	mouse[MGRAB] = 0;
}

/* set the window title */
void wm_title(char* title)
{
	if (title) {
		free(wm_res.title);
		wm_res.title = strdup(title);
	}
	if (!wm_has_init)
		return;

	XStoreName(wm_res.dpy, wm_res.win, wm_res.title);
}

#endif
