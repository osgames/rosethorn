/************************************************************************
* collision3d.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2013 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"
#include "rtg_physics.h"

/* a collision has happened
 * TODO: actually handle the collision, also callbacks */
#ifdef RTG_CFG_INSANE_COLLISION
static void collision_apply(mass_t *m1, mass_t *m2)
{
	/*rtprintf(
		"collision: m%d (%f,%f,%f) (%f,%f,%f) (%f,%f,%f) (%f,%f,%f) m%d",
		m1->id,
		m1->velocity.x,
		m1->velocity.y,
		m1->velocity.z,
		m1->force[0].x,
		m1->force[0].y,
		m1->force[0].z,
		m1->force[1].x,
		m1->force[1].y,
		m1->force[1].z,
		m1->force[2].x,
		m1->force[2].y,
		m1->force[2].z,
		m2->id
	);*/

	m1->force[FORCE_CONSTANT].y *= -1.0;
	m1->force[FORCE_PULSE].y = m1->force[FORCE_CONSTANT].y*0.25;
}

static int collision_sameside(v3_t *p1, v3_t *p2, v3_t *a, v3_t *b)
{
	v3_t cp1;
	v3_t cp2;
	v3_t bma;
	v3_t p1a;
	v3_t p2a;
	vect_subtract(b,a,&bma);
	vect_subtract(p1,a,&p1a);
	vect_subtract(p2,a,&p2a);
	vect_crossproduct(&bma, &p1a, &cp1);
	vect_crossproduct(&bma, &p2a, &cp2);
	if (math_dotproduct(&cp1,&cp2) >= 0)
		return 1;
	return 0;
}

static int collision_point_in_poly( v3_t *p, v3_t *a, v3_t *b, v3_t *c)
{
	if (
		collision_sameside(p,a, b,c)
		&& collision_sameside(p,b, a,c)
		&& collision_sameside(p,c, a,b)
	)
		return 1;
	return 0;
}
#endif

#if 0
static int collision_detect_polys(
	v3_t *p1v1,
	v3_t *p1v2,
	v3_t *p1v3,
	v3_t *p2v1,
	v3_t *p2v2,
	v3_t *p2v3
)
{
	if (
		collision_point_in_poly(p2v1,p1v1,p1v2,p1v3)
		&& collision_point_in_poly(p2v2,p1v1,p1v2,p1v3)
		&& collision_point_in_poly(p2v3,p1v1,p1v2,p1v3)
	)
		return 1;

	return 0;
}

static int collision_detect_bounds(aabb_t *b1, quaternion_t *q1, v3_t *s1, aabb_t *b2, quaternion_t *q2, v3_t *s2)
{
	int in[12][3][3] = { /* box */
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		},
		{ /* poly */
			{0,0,0}, /* vertex */
			{0,0,0}, /* vertex */
			{0,0,0} /* vertex */
		}
	};
	v3_t *bb1[2];
	v3_t *bb2[2];
	return 0;
}
#endif

#ifdef RTG_CFG_INSANE_COLLISION
static void collision_detect_mass_fine(mass_t *m1, mass_t *m2, float d)
{
	int c = 0;
	int mi1;
	int mi2;
	int pi1;
	int pi2;
	int *p1;
	int *p2;
	v3_t *v1;
	v3_t *v2;
	v3_t pl1[3];
	v3_t pl2[3];
	quaternion_t q1;
	quaternion_t q2;
	mesh_t **mesh1 = m1->obj->meshes->data;
	mesh_t **mesh2 = m2->obj->meshes->data;

	quat_init_euler(&q1,m1->obj->rot.x,m1->obj->rot.y,m1->obj->rot.z);
	quat_init_euler(&q2,m2->obj->rot.x,m2->obj->rot.y,m2->obj->rot.z);

	for (mi1=0; mi1<m1->obj->meshes->length; mi1++) {
		p1 = mesh1[mi1]->i->data;
		v1 = mesh1[mi1]->v->data;
		for (pi1=0; pi1<mesh1[mi1]->i->length; pi1+=3) {
			pl1[0].x = (v1[p1[pi1]].x*m1->obj->scale.x);
			pl1[0].y = (v1[p1[pi1]].y*m1->obj->scale.y);
			pl1[0].z = (v1[p1[pi1]].z*m1->obj->scale.z);
			quat_rotate(&q1,&pl1[0],&pl1[0]);
			pl1[0].x += m1->pos->x;
			pl1[0].y += m1->pos->y;
			pl1[0].z += m1->pos->z;
			pl1[1].x = (v1[p1[pi1+1]].x*m1->obj->scale.x);
			pl1[1].y = (v1[p1[pi1+1]].y*m1->obj->scale.y);
			pl1[1].z = (v1[p1[pi1+1]].z*m1->obj->scale.z);
			quat_rotate(&q1,&pl1[1],&pl1[1]);
			pl1[1].x += m1->pos->x;
			pl1[1].y += m1->pos->y;
			pl1[1].z += m1->pos->z;
			pl1[2].x = (v1[p1[pi1+2]].x*m1->obj->scale.x);
			pl1[2].y = (v1[p1[pi1+2]].y*m1->obj->scale.y);
			pl1[2].z = (v1[p1[pi1+2]].z*m1->obj->scale.z);
			quat_rotate(&q1,&pl1[2],&pl1[2]);
			pl1[2].x += m1->pos->x;
			pl1[2].y += m1->pos->y;
			pl1[2].z += m1->pos->z;
			for (mi2=0; mi2<m2->obj->meshes->length; mi2++) {
				if (d > mesh1[mi1]->radius+mesh2[mi2]->radius)
					continue;
				p2 = mesh2[mi2]->i->data;
				v2 = mesh2[mi2]->v->data;
				for (pi2=0; pi2<mesh2[mi2]->i->length; pi2+=3) {
					pl2[0].x = (v2[p2[pi2]].x*m2->obj->scale.x);
					pl2[0].y = (v2[p2[pi2]].y*m2->obj->scale.y);
					pl2[0].z = (v2[p2[pi2]].z*m2->obj->scale.z);
					quat_rotate(&q2,&pl2[0],&pl2[0]);
					pl2[0].x += m2->pos->x;
					pl2[0].y += m2->pos->y;
					pl2[0].z += m2->pos->z;
					pl2[1].x = (v2[p2[pi2+1]].x*m2->obj->scale.x);
					pl2[1].y = (v2[p2[pi2+1]].y*m2->obj->scale.y);
					pl2[1].z = (v2[p2[pi2+1]].z*m2->obj->scale.z);
					quat_rotate(&q2,&pl2[1],&pl2[1]);
					pl2[1].x += m2->pos->x;
					pl2[1].y += m2->pos->y;
					pl2[1].z += m2->pos->z;
					pl2[2].x = (v2[p2[pi2+2]].x*m2->obj->scale.x);
					pl2[2].y = (v2[p2[pi2+2]].y*m2->obj->scale.y);
					pl2[2].z = (v2[p2[pi2+2]].z*m2->obj->scale.z);
					quat_rotate(&q2,&pl2[2],&pl2[2]);
					pl2[2].x += m2->pos->x;
					pl2[2].y += m2->pos->y;
					pl2[2].z += m2->pos->z;
					c++;
					if (
						collision_detect_polys(
							&pl1[0],
							&pl1[1],
							&pl1[2],
							&pl2[0],
							&pl2[1],
							&pl2[2]
						)
					) {
	rtprintf(
		"m%d (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)\n"
		"    (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)\n"
		"m%d (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)\n"
		"    (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)",
		m1->id,
		pl1[0].x,
		pl1[0].y,
		pl1[0].z,
		pl1[1].x,
		pl1[1].y,
		pl1[1].z,
		pl1[2].x,
		pl1[2].y,
		pl1[2].z,
		v1[p1[pi1]].x,
		v1[p1[pi1]].y,
		v1[p1[pi1]].z,
		v1[p1[pi1+1]].x,
		v1[p1[pi1+1]].y,
		v1[p1[pi1+1]].z,
		v1[p1[pi1+2]].x,
		v1[p1[pi1+2]].y,
		v1[p1[pi1+2]].z,
		m2->id,
		pl2[0].x,
		pl2[0].y,
		pl2[0].z,
		pl2[1].x,
		pl2[1].y,
		pl2[1].z,
		pl2[2].x,
		pl2[2].y,
		pl2[2].z,
		v2[p2[pi2]].x,
		v2[p2[pi2]].y,
		v2[p2[pi2]].z,
		v2[p2[pi2+1]].x,
		v2[p2[pi2+1]].y,
		v2[p2[pi2+1]].z,
		v2[p2[pi2+2]].x,
		v2[p2[pi2+2]].y,
		v2[p2[pi2+2]].z
	);
						/* TODO: calculate collision angle and velocity */
						collision_apply(m1,m2);
					rtprintf("%d",c);
						return;
					}
				}
			}
		}
	}
					rtprintf("%d",c);
}
#else /* RTG_CFG_INSANE_COLLISION */
static void collision_detect_mass_fine(mass_t *m1, mass_t *m2, float d)
{
	quaternion_t q1;
	quaternion_t q2;

	quat_init_euler(&q1,m1->obj->rot.x,m1->obj->rot.y,m1->obj->rot.z);
	quat_init_euler(&q2,m2->obj->rot.x,m2->obj->rot.y,m2->obj->rot.z);
}
#endif /* RTG_CFG_INSANE_COLLISION */

static void collision_detect_mass(mass_t *e)
{
	int i;
	mass_t **m;
	float d;
	v3_t c[2];
	v3_t a[2];
	c[0].x = e->pos->x+e->radius;
	c[0].y = e->pos->y+e->radius;
	c[0].z = e->pos->z+e->radius;
	c[1].x = e->pos->x-e->radius;
	c[1].y = e->pos->y-e->radius;
	c[1].z = e->pos->z-e->radius;

	m = masses->data;
	for (i=0; i<masses->length; i++) {
		if (m[i]->id != e->id) {
			a[0].x = m[i]->pos->x+m[i]->radius;
			a[1].x = m[i]->pos->x-m[i]->radius;
			if (c[1].x > a[0].x || c[0].x < a[1].x)
				continue;

			a[0].y = m[i]->pos->y+m[i]->radius;
			a[1].y = m[i]->pos->y-m[i]->radius;
			if (c[1].y > a[0].y || c[0].y < a[1].y)
				continue;

			a[0].z = m[i]->pos->z+m[i]->radius;
			a[1].z = m[i]->pos->z-m[i]->radius;
			if (c[1].z > a[0].z || c[0].z < a[1].z)
				continue;

			d = math_distance(e->pos,m[i]->pos);
			if (d > e->radius+m[i]->radius)
				continue;

			collision_detect_mass_fine(e,m[i],d);
		}
	}
}

/* detect collisions */
void collision_detect()
{
	mass_t **m;
	int i;
	if (!masses || !masses->length)
		return;

	m = masses->data;
	for (i=0; i<masses->length; i++) {
		if (m[i]->obj) {
			m[i]->radius = object_radius(m[i]->obj);
		}else{
			m[i]->radius = 0.0;
		}
	}
	for (i=0; i<masses->length; i++) {
		/* an isolated mass won't move when hit, so ignore it */
		if (m[i]->isolated)
			continue;
		collision_detect_mass(m[i]);
	}
}
