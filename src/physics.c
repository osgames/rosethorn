/************************************************************************
* physics.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"
#include "rtg_physics.h"

static int mass_ids = 1;
static array_t *gravity_sources = NULL;
array_t *masses = NULL;

/* create a new mass */
mass_t *mass_create(v3_t *pos, float mass)
{
	int i;
	mass_t *m = malloc(sizeof(mass_t));

	m->id = mass_ids++;
	m->mass = mass;
	m->isolated = 0;
	m->hit = 0;

	m->grav = NULL;

	m->opos.x = 0.0;
	m->opos.y = 0.0;
	m->opos.z = 0.0;

	m->pos = &m->opos;
	if (pos) {
		m->pos->x = pos->x;
		m->pos->y = pos->y;
		m->pos->z = pos->z;
	}

	for (i=0; i<3; i++) {
		m->force[i].x = 0.0;
		m->force[i].y = 0.0;
		m->force[i].z = 0.0;
	}

	m->velocity.x = 0.0;
	m->velocity.y = 0.0;
	m->velocity.z = 0.0;

	if (!masses)
		masses = array_create(RTG_TYPE_PTR);

	array_push_ptr(masses,m);

	return m;
}

/* create a mass, bound to an object */
mass_t *mass_create_obj(object_t *o, float mass)
{
	mass_t *m = mass_create(NULL,mass);

	m->pos = &o->pos;
	m->obj = o;

	return m;
}

/* give a mass a gravitational pull */
int mass_set_gravity_source(mass_t *m, float pull, float weight, float reach)
{
	if (pull == 0.0) {
		int p;
		if (!m->grav)
			return 0;
		p = array_find_ptr(gravity_sources,m);
		if (p > -1)
			array_set_ptr(gravity_sources,NULL,p);
		free(m->grav);
		m->grav = NULL;
		return 0;
	}
	if (!m->grav) {
		m->grav = malloc(sizeof(gravity_t));
		if (!gravity_sources)
			gravity_sources = array_create(RTG_TYPE_PTR);
		array_push_ptr(gravity_sources,m);
	}

	m->grav->pull = pull;
	m->grav->weight = weight;
	m->grav->reach = reach;

	m->grav->offset.x = 0.0;
	m->grav->offset.y = 0.0;
	m->grav->offset.z = 0.0;

	return 0;
}

/* remove the gravitational pull from a mass */
int mass_unset_gravity_source(mass_t *m)
{
	return mass_set_gravity_source(m,0.0,0.0,0.0);
}

/* set the offset of the gravitational pull of a mass */
int mass_set_gravity_offset(mass_t *m, v3_t *offset)
{

	if (!m->grav)
		return 1;
	m->grav->offset.x = offset->x;
	m->grav->offset.y = offset->y;
	m->grav->offset.z = offset->z;

	return 0;
}

/* set the constant force of a mass */
int mass_set_constant_force(mass_t *m, v3_t *v)
{
	m->force[FORCE_CONSTANT].x = v->x;
	m->force[FORCE_CONSTANT].y = v->y;
	m->force[FORCE_CONSTANT].z = v->z;
	return 0;
}

/* set the pulse (decaying) force of a mass */
int mass_set_pulse_force(mass_t *m, v3_t *v)
{
	m->force[FORCE_PULSE].x = v->x;
	m->force[FORCE_PULSE].y = v->y;
	m->force[FORCE_PULSE].z = v->z;
	return 0;
}

/* add to the constant force of a mass */
int mass_add_constant_force(mass_t *m, v3_t *v)
{
	m->force[FORCE_CONSTANT].x += v->x;
	m->force[FORCE_CONSTANT].y += v->y;
	m->force[FORCE_CONSTANT].z += v->z;
	return 0;
}

/* add to the pulse (decaying) force of a mass */
int mass_add_pulse_force(mass_t *m, v3_t *v)
{
	m->force[FORCE_PULSE].x += v->x;
	m->force[FORCE_PULSE].y += v->y;
	m->force[FORCE_PULSE].z += v->z;
	return 0;
}

/* set or unset a mass as isolated (not affected by gravity) */
int mass_set_isolated(mass_t *m, int v)
{
	m->isolated = !!v;
	return 0;
}

/* get thegravity value at a given position */
int gravity_at(v3_t *pos, v3_t *grav)
{
	mass_t **m;
	float c = 0.0;
	float f;
	int i;
	float d = 0.0;
	v3_t t;
	v3_t p;

	grav->x = 0.0;
	grav->y = 0.0;
	grav->z = 0.0;

	if (!gravity_sources || ! gravity_sources->length)
		return 0;

	m = gravity_sources->data;

	for (i=0; i<gravity_sources->length; i++) {
		/* get the distance */
		p.x = m[i]->pos->x+m[i]->grav->offset.x;
		p.y = m[i]->pos->y+m[i]->grav->offset.y;
		p.z = m[i]->pos->z+m[i]->grav->offset.z;
		f = math_distance(pos,&p);
		if (f > m[i]->grav->reach)
			continue;

		c += 1.0;

		p.x = m[i]->pos->x+m[i]->grav->offset.x;
		p.y = m[i]->pos->y+m[i]->grav->offset.y;
		p.z = m[i]->pos->z+m[i]->grav->offset.z;

		d = m[i]->grav->pull*m[i]->grav->weight;
		/* find the vector for the pull */
		vect_create(pos,&p,&t);

		/* and apply the gravity */
		grav->x += t.x*d;
		grav->y += t.y*d;
		grav->z += t.z*d;
	}

	grav->x /= c;
	grav->y /= c;
	grav->z /= c;

	return 0;
}

/* updated physics effects for this frame */
void physics_apply()
{
	int c;
	int i;
	int k;
	mass_t **m;
	v3_t g;
	float f;
	float elapsed = (anim_factor*60.0)/1000.0;

	if (!masses || !masses->length)
		return;

	collision_detect();

	c = (int)(elapsed/0.1)+1;
	m = masses->data;

	if (c != 0)
		elapsed /= c;

	for (i=0; i<c; i++) {
		for (k=0; k<masses->length; k++) {
			if (!m[k] || m[k]->isolated)
				continue;
			if (!m[k]->hit) {
				gravity_at(m[k]->pos,&g);
				m[k]->force[FORCE_CONSTANT].x += g.x*elapsed;
				m[k]->force[FORCE_CONSTANT].y += g.y*elapsed;
				m[k]->force[FORCE_CONSTANT].z += g.z*elapsed;
			}
			m[k]->hit = 0;

			m[k]->velocity.x = m[k]->force[FORCE_PULSE].x;
			m[k]->velocity.y = m[k]->force[FORCE_PULSE].y;
			m[k]->velocity.z = m[k]->force[FORCE_PULSE].z;
			m[k]->velocity.x += m[k]->force[FORCE_CONSTANT].x;
			m[k]->velocity.y += m[k]->force[FORCE_CONSTANT].y;
			m[k]->velocity.z += m[k]->force[FORCE_CONSTANT].z;

			if (m[k]->force[FORCE_PULSE].x != 0.0) {
				f = 0.01*elapsed;
				if (m[k]->force[FORCE_PULSE].x > 0.0) {
					if (m[k]->force[FORCE_PULSE].x > f) {
						m[k]->force[FORCE_PULSE].x -= f;
					}else{
						m[k]->force[FORCE_PULSE].x = 0.0;
					}
				}else{
					if (m[k]->force[FORCE_PULSE].x < -f) {
						m[k]->force[FORCE_PULSE].x += f;
					}else{
						m[k]->force[FORCE_PULSE].x = 0.0;
					}
				}
			}

			if (m[k]->force[FORCE_PULSE].y != 0.0) {
				f = 0.01*elapsed;
				if (m[k]->force[FORCE_PULSE].y > 0.0) {
					if (m[k]->force[FORCE_PULSE].y > f) {
						m[k]->force[FORCE_PULSE].y -= f;
					}else{
						m[k]->force[FORCE_PULSE].y = 0.0;
					}
				}else{
					if (m[k]->force[FORCE_PULSE].y < -f) {
						m[k]->force[FORCE_PULSE].y += f;
					}else{
						m[k]->force[FORCE_PULSE].y = 0.0;
					}
				}
			}

			if (m[k]->force[FORCE_PULSE].z != 0.0) {
				f = 0.01*elapsed;
				if (m[k]->force[FORCE_PULSE].z > 0.0) {
					if (m[k]->force[FORCE_PULSE].z > f) {
						m[k]->force[FORCE_PULSE].z -= f;
					}else{
						m[k]->force[FORCE_PULSE].z = 0.0;
					}
				}else{
					if (m[k]->force[FORCE_PULSE].z < -f) {
						m[k]->force[FORCE_PULSE].z += f;
					}else{
						m[k]->force[FORCE_PULSE].z = 0.0;
					}
				}
			}

			m[k]->pos->x += m[k]->velocity.x*elapsed;
			m[k]->pos->y += m[k]->velocity.y*elapsed;
			m[k]->pos->z += m[k]->velocity.z*elapsed;
		}
	}
}
