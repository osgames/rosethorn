/************************************************************************
* ui.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

#include <stdarg.h>

ui_t ui = {
	0,
	NULL,
	NULL
};

/* command setter for ui values */
void ui_element_setter(char* sel, char* value)
{
	char buff[1024];
	char* a;
	style_rule_t *st;
	style_rule_t *p = ui.styles;

	a = strstr(sel,".style.");
	if (!a)
		return;

	*a = 0;
	a += 7;

	sprintf(buff,"%s {%s:%s;}",sel,a,value);
	*a = '.';

	st = style_parse(buff);
	if (p) {
		while (p->next) {
			p = p->next;
		}
		p->next = st;
	}else{
		ui.styles = st;
	}
	style_refresh(ui.styles,ui.elements);
}

/* draw the ui */
void ui_render()
{
	widget_t *w;
	widget_t *wc;
	int x;
	int y;
	int l;
	int h;
	int bx;
	int by;
	int bw;
	int bh;

	w = containers;
	while (w) {
		if (w->style.visible)
			ui_widget_draw(w);
		w = w->next;
	}

	wc = containers;
	while (wc) {
		if (wc->style.visible) {
			w = wc->child;
			while (w) {
				if (w->hover && w->parent && w->parent->hover && w->htext) {
					l = print_length(w->style.ttfont_face,w->style.ttfont_size,w->htext);
					h = print_height(w->style.ttfont_face,w->style.ttfont_size,w->htext);
					x = mouse[0]+10;
					y = mouse[1];
					if (mouse[1] > w->style.ttfont_size) {
						y -= w->style.ttfont_size;
						if (mouse[0] > (wm_res.width-l))
							x -= l+10;
					}else{
						x -= l+10;
					}

					bx = x-1;
					by = y;
					bw = l+2;
					bh = h;

					if (w->style.has_ttbg)
						draw2d_rect(&w->style.ttbg,bx,by,bw,bh);

					if (w->style.has_ttborder) {
						draw2d_line(&w->style.ttborder,bx,by,bx+bw,by);
						draw2d_line(&w->style.ttborder,bx,by,bx,by+bh );
						draw2d_line(&w->style.ttborder,bx+bw,by,bx+bw,by+bh);
						draw2d_line(&w->style.ttborder,bx,by+bh,bx+bw,by+bh);
					}

					render_color_push(&w->style.ttcolor);
					print2d(x,y,w->style.ttfont_face,w->style.ttfont_size,w->htext);
					render_color_pop();
				}
				w = w->next;
			}
		}
		wc = wc->next;
	}
}

/* event handler for the msgbox */
static int ui_msg_event(widget_t *w)
{
	msgbox_data_t *wd = &w->parent->data->msg;
	if (w->id == wd->btn1->id) {
		if (wd->btn1_func)
			wd->btn1_func(w);
	}else if (w->id == wd->btn2->id) {
		if (wd->btn2_func)
			wd->btn2_func(w);
	}else{
		return EVENT_UNHANDLED;
	}

	ui_widget_free(w->parent);

	return EVENT_HANDLED;
}

/* create a popup message box */
void ui_msg(int type, char* txt, int (*func)(), ...)
{
	va_list ap;
	widget_t *con;
	style_rule_t *s = ui.styles;

	if (!(type == UIMSG_OC || type == UIMSG_OK || type == UIMSG_YN) || !txt) {
		rtprintf(CN_RTG "Invalid message type: %d",type);
		return;
	}

	con = ui_widget_container();
	while (s) {
		if (!strcmp(s->selector,"*.!.*"))
			break;
		s = s->next;
	}
	if (s)
		style_merge(&s->data,con);
	con->style.w = 300;
	con->style.h = 120;
	con->style.x = UI_ALIGN_CENTRE;
	con->style.y = UI_ALIGN_MIDDLE;
	s = ui.styles;
	while (s) {
		if (!strcmp(s->selector,"msgbox.!.*"))
			break;
		s = s->next;
	}
	if (s)
		style_merge(&s->data,con);
	con->style.visible = 1;
	con->data = malloc(sizeof(widget_data_t));
	con->data->msg.container = con;
	con->data->msg.btn1_func = func;
	con->data->msg.btn2_func = NULL;

	con->data->msg.txt = ui_widget_create(UI_WIDGET_LABEL,con);
	con->data->msg.txt->style.w = 300;
	con->data->msg.txt->style.x = 0;
	con->data->msg.txt->style.text_align = UI_ALIGN_CENTRE;
	con->data->msg.txt->style.y = 20;
	ui_widget_value(con->data->msg.txt,txt);

	con->data->msg.btn1 = ui_widget_create(UI_WIDGET_BUTTON,con);
	con->data->msg.btn1->events->mclick = &ui_msg_event;

	con->data->msg.btn2 = ui_widget_create(UI_WIDGET_BUTTON,con);
	con->data->msg.btn2->events->mclick = &ui_msg_event;

	s = ui.styles;
	while (s) {
		if (!strcmp(s->selector,"*.button.*"))
			break;
		s = s->next;
	}
	if (s) {
		style_merge(&s->data,con->data->msg.btn1);
		style_merge(&s->data,con->data->msg.btn2);
	}

	con->data->msg.btn1->style.x = 290-con->data->msg.btn1->style.w;
	con->data->msg.btn1->style.y = 65;
	con->data->msg.btn2->style.x = con->data->msg.btn1->style.x-10-con->data->msg.btn2->style.w;
	con->data->msg.btn2->style.y = 65;

	if (type == UIMSG_OC) {
		va_start(ap,func);
		*(void**)(&con->data->msg.btn2_func) = va_arg(ap,int (*));
		va_end(ap);
		ui_widget_value(con->data->msg.btn1,"OK");
		ui_widget_value(con->data->msg.btn2,"Cancel");
	}else if (type == UIMSG_OK) {
		con->data->msg.btn2->style.visible = 0;
		ui_widget_value(con->data->msg.btn1,"OK");
	}else if (type == UIMSG_YN) {
		va_start(ap,func);
		*(void**)(&con->data->msg.btn2_func) = va_arg(ap,int (*));
		va_end(ap);
		ui_widget_value(con->data->msg.btn1,"Yes");
		ui_widget_value(con->data->msg.btn2,"No");
	}
}

/* event handler for the optbox */
static int ui_opt_event(widget_t *w)
{
	optbox_data_t *wd = &w->parent->data->opb;
	optbox_btn_t *btn = wd->buttons;

	while (btn) {
		if (btn->btn->id == w->id)
			break;
		btn = btn->next;
	}

	if (!btn) {
		return EVENT_UNHANDLED;
	}

	btn->func(w);

	ui_widget_free(w->parent);

	return EVENT_HANDLED;
}

/* create an option select popup box */
void ui_opt(char* txt, ...)
{
	va_list ap;
	widget_t *con;
	int (*f)(widget_t*);
	char* t;
	optbox_btn_t *b;
	int y = 95;

	if (!txt || !txt[0])
		return;
	va_start(ap,txt);
	t = (char*)va_arg(ap,char*);
	if (!t)
		return;
	*(void**)(&f) = va_arg(ap,int (*));
	if (!f)
		return;

	con = ui_widget_create(UI_WIDGET_CONTAINER,NULL);
	con->data = malloc(sizeof(widget_data_t));
	con->style.w = 200;
	con->style.x = UI_ALIGN_CENTRE;
	con->style.y = UI_ALIGN_MIDDLE;
	con->style.visible = 1;

	con->data->opb.container = con;
	con->data->opb.txt = ui_widget_create(UI_WIDGET_LABEL,con);
	con->data->opb.txt->style.x = 100-((print_length(1,SML_FONT,txt))/2);
	if (con->data->opb.txt->style.x < 0)
		con->data->opb.txt->style.x = 0;
	con->data->msg.txt->style.y = 20;
	ui_widget_value(con->data->opb.txt,txt);

	con->data->opb.buttons = malloc(sizeof(optbox_btn_t));
	con->data->opb.buttons->func = f;
	con->data->opb.buttons->btn = ui_widget_create(UI_WIDGET_BUTTON,con);
	con->data->opb.buttons->btn->style.x = 63;
	con->data->opb.buttons->btn->style.y = 65;
	con->data->opb.buttons->btn->events->mclick = &ui_opt_event;
	ui_widget_value(con->data->opb.buttons->btn,t);
	b = con->data->opb.buttons;

	con->data->opb.buttons->next = NULL;

	while ((t = (char*)va_arg(ap,char*)) != NULL && (*(void**)(&f) = va_arg(ap,int (*))) != NULL) {
		b->next = malloc(sizeof(optbox_btn_t));
		b->next->func = f;
		b->next->btn = ui_widget_create(UI_WIDGET_BUTTON,con);
		b->next->btn->style.x = 63;
		b->next->btn->style.y = y;
		b->next->btn->events->mclick = &ui_opt_event;
		ui_widget_value(b->next->btn,t);
		b = b->next;
		b->next = NULL;
		y += 30;
	}

	va_end(ap);
	con->style.h = y;
}
