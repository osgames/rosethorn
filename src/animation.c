/************************************************************************
* animation.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

static int anim_ids = 1;
static int anim_instance_ids = 1;
animation_t *animations = NULL;
anim_instance_t *anim_instances = NULL;

/* load animations from file */
void anim_load(char* file)
{
	xml_tag_t *x = file_load_xml(file);
	if (!x)
		return;

	anim_load_xml(x);

	xml_free(x);
}

/* load animations from string */
void anim_load_str(char* str)
{
	xml_tag_t *x;

	x = xml_parse(str,NULL);

	anim_load_xml(x);

	xml_free(x);
}

/* load animations from xml data */
void anim_load_xml(xml_tag_t *x)
{
	char* af;
	int frames;
	int frame_h;
	int frame_w;
	int fps;
	int alt;
	float speed;
	animation_t *a;
	xml_tag_t *t = x;

	while (t) {
		if (!strcmp(t->tag,"anim")) {
			if (!(af = xml_attribute(t,"file")))
				continue;
			frames = xml_attribute_int(t,"frames");
			frame_w = xml_attribute_int(t,"width");
			frame_h = xml_attribute_int(t,"height");
			fps = xml_attribute_int(t,"fps");
			alt = xml_attribute_int(t,"alt");
			speed = xml_attribute_float(t,"speed");
			a = anim_create(af,frames,frame_h,frame_w,fps,alt,speed);
			if (a)
				a->id = xml_attribute_int(t,"id");
		}
		t = t->next;
	}
}

/* create a new animation from file image with frames frames per loop of size
 * frame_w x frame_h at fps frames per second with alt alternative sequences */
animation_t *anim_create(char* file, int frames, int frame_h, int frame_w, int fps, int alt, float speed)
{
	animation_t *a;
	int l;
	int f;

	a = malloc(sizeof(animation_t));
	a->id = anim_ids++;
	a->mat = mat_from_image(file);
	if (!a->mat) {
		free(a);
		return NULL;
	}
	a->frames = frames;
	a->frame_w = frame_w;
	a->frame_h = frame_h;
	a->fps = (float)fps/60.0;
	a->speed = speed;
	a->alt = alt;
	a->frame_data = malloc(sizeof(sprite_t*)*alt);

	for (l=0; l<alt; l++) {
		a->frame_data[l] = malloc(sizeof(sprite_t)*frames);
		for (f=0; f<frames; f++) {
			a->frame_data[l][f].x = f;
			if (f)
				a->frame_data[l][f].x *= frame_w;
			a->frame_data[l][f].y = (l*frame_h);
			a->frame_data[l][f].w = frame_w;
			a->frame_data[l][f].h = frame_h;
			a->frame_data[l][f].mat = a->mat;
		}
	}

	animations = list_push(&animations,a);

	return a;
}

animation_t *anim_get(int id)
{
	animation_t *a = animations;
	while (a) {
		if (a->id == id)
			return a;
		a = a->next;
	}
	return NULL;
}

/* spawn a new animation of type using sequence alt, continuing loop times
 * starting at bx/by and moving to ex/ey */
anim_instance_t *anim_spawn(int type, int alt, int loop, int bx, int by, int ex, int ey)
{
	anim_instance_t *a;
	animation_t *an = animations;
	while (an) {
		if (an->id == type)
			break;
		an = an->next;
	}

	if (!an || alt >= an->alt)
		return NULL;

	a = malloc(sizeof(anim_instance_t));
	a->anim = an;
	a->alt = alt;
	a->u = 0;
	a->id = anim_instance_ids++;
	a->name[0] = 0;
	a->loop = loop;
	a->frame = 0;
	a->distance = 0;
	a->total = 0;
	a->bx = bx;
	a->by = by;
	a->ex = ex;
	a->ey = ey;
	a->speed = an->speed;

	if (!(a->bx == a->ex && a->by == a->ey)) {
		ex -= bx;
		if (ex < 0)
			ex *= -1;
		ey -= by;
		if (ey < 0)
			ey *= -1;
		a->total = math_sqrt(ex*ex + ey*ey);
	}

	anim_instances = list_push(&anim_instances,a);

	return a;
}

/* mark an animation instance for removal */
void anim_remove(anim_instance_t *a)
{
	a->loop = 0;
}

/* find the screen position of an animation instance */
void anim_get_position(anim_instance_t *a, int *x, int *y)
{
	/* static animations */
	if (a->bx == a->ex && a->by == a->ey) {
		*x = a->bx;
		*y = a->by;
	/* moving animations */
	}else{
		if (a->distance >= a->total) {
			a->distance = a->total;
			*x = a->ex;
			*y = a->ey;
		}else{
			float d;
			d = (1.0/a->total)*a->distance;
			*x = ((a->ex-a->bx)*d)+a->bx;
			*y = ((a->ey-a->by)*d)+a->by;

			a->distance += (((float)a->speed/(float)a->anim->fps)*anim_factor);
		}
	}
}

/* draw all visible animation instances */
void anim_render()
{
	anim_instance_t *a = anim_instances;
	anim_instance_t *na;
	int x;
	int y;

	if (!anim_instances)
		return;

	while (a) {
		/* animation has ended, remove the instance */
		if (!a->loop) {
			na = a;
			a = a->next;
			anim_instances = list_remove(&anim_instances,na);
			free(na);
			continue;
		/* only draw non-unit animations, units are drawn in game_render() */
		}else if (!a->u) {
			anim_get_position(a,&x,&y);
			anim_draw(a,x,y);
		}
		a = a->next;
	}
}

/* draw an animation instance */
void anim_draw(anim_instance_t *a, int x, int y)
{
	int f;

	if (!a)
		return;

	if (x > -a->anim->frame_w && y > -a->anim->frame_h && x < wm_res.width+a->anim->frame_w && y < wm_res.height+a->anim->frame_h)
		draw2d_sprite(&a->anim->frame_data[a->alt][(int)a->frame],x,y,NTV_SIZE,NTV_SIZE);

	if (a->anim->frames == 1)
		return;

	a->frame += a->anim->fps*anim_factor;
	f = (int)a->frame;
	if (f >= a->anim->frames) {
		a->frame = 0;
		if (a->loop > 0 && a->total == a->distance)
			a->loop--;
	}
}
