/************************************************************************
* events.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

static int jset = 0;
event_t current_event;
widget_t *setter = NULL;

int mouse[5] = {0,0,0,0,0};
int rmouse[2] = {0,0};
int key[255];
int button[5] = {0,0,0,0,0};
int shift = 0;
int ctrl = 0;

event_action_t *events = NULL;

/* initialise configured events */
int events_init()
{
	int i;

	for (i=0; i<255; i++) {
		key[i] = 0;
	}

	return 0;
}

/* free events memory */
void events_exit()
{
	event_action_t *e;
	while (events) {
		e = events;
		events = e->next;
		if (e->com)
			free(e->com);
		free(e);
	}
}

/* set the key bind for an event */
void event_set_bind(char* name, char* value)
{
	event_action_t *e;
	size_t i = strlen(name)-1;
	int f = 0;
	int k;
	int t;

	if (!value) {
		event_remove(name);
		return;
	}

	k = key_to_int(value);
	t = key_to_type(value);

	while (isspace(name[i]) && i) {
		name[i] = 0;
		i--;
	}
	i = strlen(value)-1;
	while (isspace(value[i]) && i) {
		value[i] = 0;
		i--;
	}

	e = events;
	while (e) {
		if (!strcmp(name,e->name)) {
			sprintf(e->bind,"%s",value);
			e->type = t;
			e->sym = k;
			f = 1;
		}else if (k && e->sym == k && e->type == t) {
			strcpy(e->bind,"???");
			e->type = 0;
			e->sym = 0;
		}
		e = e->next;
	}

	if (!f) {
		event_action_t *ne = malloc(sizeof(event_action_t));

		if (!ne)
			return;

		ne->type = t;
		ne->sym = k;
		ne->func = NULL;
		ne->r_func = NULL;
		ne->a_func = NULL;
		ne->com = strdup(name);
		sprintf(ne->bind,"%s",value);
		sprintf(ne->name,"%s",name);

		events = list_push(&events,ne);
	}
}

/* set the action function for an event */
void event_set_func(char* name, void (*func)())
{
	event_action_t *e = events;
	while (e) {
		if (!strcmp(name,e->name)) {
			if (e->com) {
				free(e->com);
				e->com = NULL;
			}
			e->func = func;
		}
		e = e->next;
	}
}

/* set the release function for an event */
void event_set_r_func(char* name, void (*func)())
{
	event_action_t *e = events;
	while (e) {
		if (!strcmp(name,e->name)) {
			if (e->com) {
				free(e->com);
				e->com = NULL;
			}
			e->r_func = func;
		}
		e = e->next;
	}
}

/* set the active function for an event */
void event_set_a_func(char* name, void (*func)())
{
	event_action_t *e = events;
	while (e) {
		if (!strcmp(name,e->name)) {
			if (e->com) {
				free(e->com);
				e->com = NULL;
			}
			e->a_func = func;
		}
		e = e->next;
	}
}

/* set the command for an event */
void event_set_com(char* name, char* com)
{
	event_action_t *e = events;
	while (e) {
		if (!strcmp(name,e->name)) {
			if (e->com) {
				free(e->com);
				e->com = NULL;
			}
			e->com = strdup(com);
			if (e->func)
				e->func = NULL;
			if (e->r_func)
				e->r_func = NULL;
			if (e->a_func)
				e->a_func = NULL;
		}
		e = e->next;
	}
}

/* remove an event */
void event_remove(char* name)
{
	event_action_t *e = events;
	while (e) {
		if (!strcmp(name,e->name)) {
			events = list_remove(&events,e);
			if (e->com)
				free(e->com);
			free(e->name);
			if (e->bind)
				free(e->bind);
			free(e);
			break;
		}
		e = e->next;
	}
}

/* create a new event action */
void event_create(char* name, char* bind, char* com, void (*func)(), void (*r_func)(), void (*a_func)())
{
	event_action_t *e = events;
	int k = key_to_int(bind);
	int t = key_to_type(bind);

	while (e) {
		if (!strcmp(name,e->name)) {
			break;
		}
		e = e->next;
	}

	if (!e) {
		e = malloc(sizeof(event_action_t));

		if (!e)
			return;

		e->type = t;
		e->sym = k;
		e->func = NULL;
		e->r_func = NULL;
		e->a_func = NULL;
		e->com = strdup(name);
		sprintf(e->bind,"%s",bind);
		sprintf(e->name,"%s",name);

		events = list_push(&events,e);
	}

	if (com) {
		event_set_com(name, com);
	}else if (func || r_func || a_func) {
		if (e->com) {
			free(e->com);
			e->com = NULL;
		}
		if (func)
			event_set_func(name,func);
		if (r_func)
			event_set_r_func(name,r_func);
		if (a_func)
			event_set_a_func(name,a_func);
	}
}

/* set the event control */
void event_set_control(int type, int sym)
{
	event_action_t *e;
	char* k = sym_to_string(type,sym);
	if (!k)
		return;

	e = events;
	while (e) {
		if (!strcmp(setter->data->opt.com,e->name)) {
			cmd_execf("set sys.control(%s) %s",setter->data->opt.com,k);
			break;
		}
		e = e->next;
	}

	if (!e)
		cmd_execf("set sys.control(%s) %s",setter->data->opt.com,k);

	ui_widget_value(setter,k);
	setter = NULL;
}

/* check if a key is active (pressed) */
int event_key_active(char* name)
{
	event_action_t *e = events;
	while (e) {
		if (!strcmp(e->name,name)) {
			if (e->type == CTRL_KEYBOARD && key[e->sym]) {
				return 1;
			}
			return 0;
		}
		e = e->next;
	}
	return 0;
}

/* check if an event is active */
int event_active(char* ev)
{
	event_action_t *e;
	if (!ev)
		return 0;
	e = events;
	while (e) {
		if (!strcmp(e->name,ev))
			break;
		e = e->next;
	}

	if (!e)
		return 0;

	if (e->type == CTRL_KEYBOARD && e->sym > -1 && e->sym < 255 && key[e->sym])
		return 1;
	if (e->type == CTRL_MOUSE && e->sym > -1 && e->sym < 5 && button[e->sym])
		return 1;
	return 0;
}

/* handle mouse events */
void events_mouse()
{
	event_action_t *e;
	if (setter && current_event.type == RTE_BUTTON_DOWN) {
		event_set_control(CTRL_MOUSE,current_event.button);
		if (current_event.button == RTM_LEFT_BUTTON)
			jset = 1;
		return;
	}else if (jset && current_event.type == RTE_BUTTON_UP && current_event.button == RTM_LEFT_BUTTON) {
		jset = 0;
		return;
	}else if (ui_events()) {
		return;
	}else if (rtg_state < RTG_STATE_PAUSED) {
		return;
	}

	if (current_event.type == RTE_BUTTON_DOWN) {
		e = events;
		while (e) {
			if (e->type == CTRL_MOUSE && e->sym == current_event.button) {
				if (e->com) {
					cmd_exec(e->com);
				}else if (e->func) {
					e->func();
				}
				break;
			}
			e = e->next;
		}
	}else if (current_event.type == RTE_BUTTON_UP) {
		e = events;
		while (e) {
			if (e->type == CTRL_MOUSE && e->sym == current_event.button) {
				if (e->r_func) {
					e->r_func();
				}
				break;
			}
			e = e->next;
		}
	}else if (current_event.type == RTE_MOUSE_MOTION) {
		e = events;
		while (e) {
			if (e->type == CTRL_MOUSE && e->sym == RTM_MOTION) {
				if (e->com) {
					cmd_exec(e->com);
				}else if (e->func) {
					e->func();
				}
				break;
			}
			e = e->next;
		}
	}
}

/* handle key press events */
void events_key_down()
{
	event_action_t *e;
	if (setter) {
		event_set_control(CTRL_KEYBOARD,current_event.sym);
		return;
	}else if (ui_events()) {
		return;
	}else if (rtg_state < RTG_STATE_PAUSED) {
		return;
	}

	e = events;
	while (e) {
		if (e->type == CTRL_KEYBOARD && e->sym == current_event.sym) {
			if (e->com) {
				cmd_exec(e->com);
			}else if (e->func) {
				e->func();
			}
			break;
		}
		e = e->next;
	}
}

/* handle key release events */
void events_key_up()
{
	event_action_t *e;
	if (ui_events()) {
		return;
	}else if (rtg_state < RTG_STATE_PAUSED) {
		return;
	}

	e = events;
	while (e) {
		if (e->type == CTRL_KEYBOARD && e->sym == current_event.sym) {
			if (e->r_func) {
				e->r_func();
			}
			break;
		}
		e = e->next;
	}
}
