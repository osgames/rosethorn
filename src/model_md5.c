/************************************************************************
* model_md5.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

typedef struct md5_hierarchy_s {
	int parent;
	int flags;
	int start;
} md5_hierarchy_t;

/* calculate the baseframe vertices */
static void model_md5_calc_frame(model_t *mdl)
{
	int i;
	int j;
	int k;
	int l;
	int wb;
	int we;
	skeleton_t *sk;
	frame_t* fr;
	mesh_t *m;
	weight_t *wt;
	joint_t *jt;
	v3_t vt;
	v3_t v;

	sk = array_get_ptr(mdl->skeletons,0);
	fr = array_get_ptr(sk->frames,0);

	for (k=0; k<mdl->meshes->length; k++) {
		m = array_get_ptr(mdl->meshes,k);
		for (i=0; i<m->m->length; i += 2) {
			v.x = 0.0;
			v.y = 0.0;
			v.z = 0.0;

			wb = array_get_int(m->m,i);
			we = array_get_int(m->m,i+1)+wb;

			/* Calculate final vertex to draw with weights */
			for (j=wb; j<we; j++) {
				wt = array_get_ptr(m->w,j);
				jt = array_get_ptr(fr->joints,wt->joint);

				/* Calculate transformed vertex for this weight */
				quat_rotate(&jt->rot, &wt->pos, &vt);

				v.x += (jt->pos.x + vt.x) * wt->bias;
				v.y += (jt->pos.y + vt.y) * wt->bias;
				v.z += (jt->pos.z + vt.z) * wt->bias;
			}

			l = (i/2)*3;
			array_set_float(m->v,v.x,l);
			array_set_float(m->v,v.y,l+1);
			array_set_float(m->v,v.z,l+2);
		}
	}
}

static int model_load_md5_frame(model_t *mdl, md5_hierarchy_t *h, array_t *base, frame_t *fr, float *f)
{
	joint_t *j;
	joint_t *bj;
	joint_t *p;
	v3_t pos;
	quaternion_t q;
	int i;
	int k;

	for (i=0; i<base->length; i++) {
		bj = array_get_ptr(base,i);
		j = malloc(sizeof(joint_t));
		array_push_ptr(fr->joints,j);

		k = 0;

		j->pos.x = bj->pos.x;
		j->pos.y = bj->pos.y;
		j->pos.z = bj->pos.z;

		q.x = bj->rot.x;
		q.y = bj->rot.y;
		q.z = bj->rot.z;

		if (h[i].flags & 1)
			j->pos.x = f[h[i].start+k++];

		if (h[i].flags & 2)
			j->pos.y = f[h[i].start+k++];

		if (h[i].flags & 4)
			j->pos.z = f[h[i].start+k++];

		if (h[i].flags & 8)
			q.x = f[h[i].start+k++];

		if (h[i].flags & 16)
			q.y = f[h[i].start+k++];

		if (h[i].flags & 32)
			q.z = f[h[i].start+k++];

		quat_computew(&q);

		/* Has parent? */
		if (h[i].parent > -1) {
			p = array_get_ptr(fr->joints,h[i].parent);

			/* Add positions */
			quat_rotate(&p->rot,&j->pos,&pos);
			j->pos.x = pos.x + p->pos.x;
			j->pos.y = pos.y + p->pos.y;
			j->pos.z = pos.z + p->pos.z;

			/* Concatenate rotations */
			quat_multiply(&p->rot,&q,&j->rot);

			quat_normalise(&j->rot);
		}else{
			j->rot.x = q.x;
			j->rot.y = q.y;
			j->rot.z = q.z;
			j->rot.w = q.w;

		}
	}
	return 0;
}

/* load an md5 model */
model_t *model_load_md5(file_t *f)
{
	char file[255];
	char* d;
	char* p;
	char* e;
	int mc;
	int c;
	int s;
	int i;
	model_t* mdl;
	joint_t *j;
	array_t *ja;
	weight_t *w;
	frame_t *fr;
	skeleton_t *sk;
	mesh_t *m = NULL;
	GLfloat fv[4];
	int iv[4];

	if (strncmp((char*)f->data,"MD5Version 10",13))
		return NULL;

	mdl = model_create();
	if (!mdl)
		return NULL;

	mdl->step = object_advance_frame;

	mdl->skeletons = array_create(RTG_TYPE_PTR);
	sk = malloc(sizeof(skeleton_t));
	sk->fps = 0.4; /* 24 fps */
	sk->name = strdup("baseframe");
	sk->frames = array_create(RTG_TYPE_PTR);
	array_push_ptr(mdl->skeletons,sk);

	p = (char*)f->data;
	e = (char*)f->data + f->len;
	c = 0;
	mc = 0;
	s = 0;

	while (p != e) {
		if (strncmp(p,"//",2)) {
			if (!s) {
				if (!strncmp(p,"numJoints",9)) {
					c++;
				}else if (!strncmp(p,"numMeshes",9)) {
					c++;
				}
				if (c == 2)
					s = 3;
			}else if (s == 1) { /* joints */
				if (*p == '}') {
					s = 3;
				}else if (*p == '"') {
					j = malloc(sizeof(joint_t));
					sscanf(p,"%s %d ( %f %f %f ) ( %f %f %f ) %*s",
						file,
						&c,
						&j->pos.x,
						&j->pos.y,
						&j->pos.z,
						&j->rot.x,
						&j->rot.y,
						&j->rot.z
					);
					quat_computew(&j->rot);
					array_push_ptr(ja,j);
				}
			}else if (s == 2) { /* mesh */
				if (!m) {
					m = mesh_create(GL_TRIANGLES);
					m->t = array_create(RTG_TYPE_FLOAT);
					m->w = array_create(RTG_TYPE_PTR);
					m->m = array_create(RTG_TYPE_INT);
					array_push_ptr(mdl->meshes,m);
				}
				if (*p == '}') {
					m = NULL;
					s = 3;
					mc++;
				}else if (!strncmp(p,"shader",6)) {
					sscanf(p,"shader \"%s\"",file);
					d = strchr(file,'"');
					if (d)
						*d = 0;
					if (!file[0]) {
						/* if no material was specified, just give it a plain white texture */
						if (!m->mat) {
							m->mat = mat_from_dif3ub(255,255,255);
						}
					}else{
						if (!m->mat) {
							m->mat = mat_from_image(file);
							if (!m->mat)
								m->mat = mat_from_dif3ub(255,255,255);
						}else{
							m->mat->tex = tex_from_image(file);
						}
					}
				}else if (!strncmp(p,"vert",4)) {
					sscanf(p,"vert %d ( %f %f ) %d %d",
						&i,
						&fv[0], /* u */
						&fv[1], /* v */
						&iv[0], /* weight */
						&iv[1]  /* weight count */
					);
					i *= 2;
					array_set_float(m->t,fv[0],i);
					array_set_float(m->t,1.0-fv[1],i+1);
					array_set_int(m->m,iv[0],i);
					array_set_int(m->m,iv[1],i+1);
				}else if (!strncmp(p,"tri",3)) {
					sscanf(p,"tri %d %d %d %d",
						&i,
						&iv[0],
						&iv[1],
						&iv[2]
					);
					i *= 3;
					array_set_int(m->i,iv[0],i);
					array_set_int(m->i,iv[1],i+1);
					array_set_int(m->i,iv[2],i+2);
				}else if (!strncmp(p,"weight",3)) {
					w = malloc(sizeof(weight_t));
					sscanf(p,"weight %d %d %f ( %f %f %f )",
						&i,
						&w->joint,
						&w->bias,
						&w->pos.x,
						&w->pos.y,
						&w->pos.z
					);
					array_set_ptr(m->w,w,i);
				}
			}else if (s == 3) {
				if (!strncmp(p,"joints",6)) {
					s = 1;
					c = 0;
					fr = malloc(sizeof(frame_t));
					fr->pre_vertex = NULL;
					ja = array_create(RTG_TYPE_PTR);
					fr->joints = ja;
					array_push_ptr(sk->frames,fr);
				}else if (!strncmp(p,"mesh",4)) {
					s = 2;
					c = 0;
				}
			}
		}

		while (*p != '\n') {
			p++;
		}
		while (isspace(*p)) {
			p++;
		}
	}

	d = strrchr(f->name,'.');
	if (!d)
		return NULL;
	*d = 0;
	sprintf(file,"%s.md5anim",f->name);
	*d = '.';

	if (!model_load_md5_anim(mdl,file)) {
		model_free(mdl);
		return NULL;
	}

	model_md5_calc_frame(mdl);

	return mdl;
}

/* load a new md5 animation into a model */
int model_load_md5_anim(model_t *mdl, char* file)
{
	array_t *ja;
	array_t *base;
	skeleton_t *sk;
	md5_hierarchy_t *h;
	frame_t *fr;
	joint_t *j;
	char* p;
	char* e;
	char* n;
	int s = 0;
	int i;
	float *a;
	file_t *f = file_load(file);

	if (!f)
		return -1;

	p = (char*)f->data;
	e = (char*)f->data + f->len;

	base = array_create(RTG_TYPE_PTR);

	sk = malloc(sizeof(skeleton_t));
	sk->name = strdup("");
	sk->frames = array_create(RTG_TYPE_PTR);
	array_push_ptr(mdl->skeletons,sk);

	while (p != e) {
		if (strncmp(p,"//",2)) {
			if (!s) {
				if (!strncmp(p,"numFrames",9)) {
					sscanf(p,"numFrames %d",&i);
				}else if (!strncmp(p,"numJoints",9)) {
					sscanf(p,"numJoints %d",&i);
					h = malloc(sizeof(md5_hierarchy_t)*i);
				}else if (!strncmp(p,"frameRate",9)) {
					sscanf(p,"frameRate %d",&i);
					sk->fps = (float)i/60.0;
				}else if (!strncmp(p,"numAnimatedComponents",21)) {
					sscanf(p,"numAnimatedComponents %d",&i);
					a = malloc(sizeof(float)*i);
				}else if (!strncmp(p,"hierarchy",9)) {
					s = 1;
					i = 0;
				}else if (!strncmp(p,"bounds",6)) {
					s = 4;
				}else if (!strncmp(p,"baseframe",9)) {
					s = 2;
					i = 0;
				}else if (!strncmp(p,"frame",5)) {
					s = 3;
					sscanf(p,"frame %d",&i);
					fr = malloc(sizeof(frame_t));
					fr->pre_vertex = NULL;
					ja = array_create(RTG_TYPE_PTR);
					fr->joints = ja;
					array_push_ptr(sk->frames,fr);
				}
			}else if (p[0] == '}') {
				s = 0;
			}else if (s == 1) { /* hierarchy */
				sscanf(p,"%*s %d %d %d %*s",
					&h[i].parent,
					&h[i].flags,
					&h[i].start
				);
				i++;
			}else if (s == 2) { /* baseframe */
				j = malloc(sizeof(joint_t));
				array_push_ptr(base,j);
				sscanf(p,"( %f %f %f ) ( %f %f %f )",
					&j->pos.x,
					&j->pos.y,
					&j->pos.z,
					&j->rot.x,
					&j->rot.y,
					&j->rot.z
				);
				quat_computew(&j->rot);
			}else if (s == 3) { /* frame */
				i = 0;
				do {
					a[i++] = strtof(p,&n);
					p = n;
				} while ((n = strchr(p,'.')) && n < strchr(p,'}'));
				model_load_md5_frame(mdl,h,base,fr,a);
			}else if (s == 4) { /* bounds */
			}

			while (*p != '\n') {
				p++;
			}
			while (isspace(*p)) {
				p++;
			}
		}
	}

	for (i=0; i<base->length; i++) {
		j = array_get_ptr(base,i);
		free(j);
	}

	array_free(base);

	free(h);
	free(a);

	file_free(f);

	return mdl->skeletons->length-1;
}
