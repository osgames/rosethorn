/************************************************************************
* time.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

#ifndef WIN32
static struct timeval start;
#else
static DWORD start;
#define TIME_WRAP_VALUE	(~(DWORD)0)
#endif

/* initialise time */
void time_init()
{
#ifndef WIN32
	gettimeofday(&start, NULL);
#else
	timeBeginPeriod(1);
	start = timeGetTime();
#endif
}

/* get number of milliseconds since start */
unsigned int time_ticks(void)
{
#ifndef WIN32
	unsigned int ticks;
	struct timeval now;
	gettimeofday(&now, NULL);
	ticks = (now.tv_sec-start.tv_sec)*1000+(now.tv_usec-start.tv_usec)/1000;
	return ticks;
#else
	DWORD now = timeGetTime();
	DWORD ticks;
	if (now < start) {
		ticks = (TIME_WRAP_VALUE-start) + now;
	}else{
		ticks = (now - start);
	}

	return ticks;
#endif
}

/* high resolution sleep() */
void delay(unsigned int ms)
{
#ifndef WIN32
	struct timespec elapsed;
	struct timespec tv;
	int was_error;

	/* Set the timeout interval */
	elapsed.tv_sec = ms/1000;
	elapsed.tv_nsec = (ms%1000)*1000000;
	do {
		errno = 0;

		tv.tv_sec = elapsed.tv_sec;
		tv.tv_nsec = elapsed.tv_nsec;
		was_error = nanosleep(&tv, &elapsed);
	} while (was_error && (errno == EINTR));
#else
	Sleep(ms);
#endif
}

/*
 * for animation timing
 * based on hz times per second
 * returns the scale factor for smooth animation
 */
float time_scale(unsigned int last, unsigned int hz)
{
	unsigned int now = time_ticks();
	unsigned int elapsed = now-last;
	float timer = 1000.0/(float)hz;
	return (float)elapsed/timer;
}

/* essentially a frame capper */
unsigned int interval_delay(unsigned int last, unsigned int hz)
{
	if (last) {
		unsigned int now = time_ticks();
		unsigned int elapsed = now-last;
		unsigned int timer = 1000/hz;
		unsigned int idelay;
		/*
		 * If elapsed is greater than timer, this could really mess up
		 * the frame rate due to a negative value rolling over to an
		 * insanely high positive value, so only delay if we have to.
		 */
		if (timer > elapsed) {
			idelay = timer-elapsed;
			delay(idelay);
		}
	}
	return time_ticks();
}

/* get the frames per second */
unsigned int calc_fps(unsigned int prev, unsigned int current)
{
	unsigned int diff = (current-prev);
	if (diff) {
		return 1000/diff;
	}else{
		return 1000;
	}
}
