/************************************************************************
* config.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

typedef struct config_s {
	struct config_s *prev;
	struct config_s *next;
	char* name;
	unsigned int h;
	char* value;
} config_t;

static config_t *config_items = NULL;
static config_t *files = NULL;
int cfg_init = 0;

/* add a file to the config list */
static void s_config_exec(char* file)
{
	config_t *fp = files;
	config_t *f;
	unsigned int h = hash(file);
	while (fp) {
		if (fp->h == h && !strcmp(fp->name,file))
			return;
		fp = fp->next;
	}

	f = malloc(sizeof(config_t));
	if (!f)
		return;

	f->name = strdup(file);
	f->h = h;

	files = list_push(&files,f);
}

/* get the value of a config setting */
char* config_get(char* name)
{
	config_t *c = config_items;
	unsigned int h = hash(name);
	while (c) {
		if (c->h == h && !strcmp(c->name,name))
			return c->value;
		c = c->next;
	}
	return NULL;
}

/* get a config setting as an int value */
int config_get_int(char* name)
{
	config_t *c = config_items;
	unsigned int h = hash(name);
	while (c) {
		if (c->h == h && !strcmp(c->name,name))
			return strtol(c->value,NULL,10);
		c = c->next;
	}
	return 0;
}

/* set the value of a config setting */
void config_set(char* name, char* value)
{
	config_t *c = config_items;
	unsigned int h = hash(name);
	rtprintf(CN_RTG "%s = '%s'",name,value);
	while (c) {
		if (c->h == h && !strcmp(c->name,name)) {
			if (c->value) {
				free(c->value);
				c->value = NULL;
			}
			if (value)
				c->value = strdup(value);
			return;
		}
		c = c->next;
	}

	if (!value)
		return;

	c = malloc(sizeof(config_t));
	c->name = strdup(name);
	c->h = h;
	c->value = strdup(value);

	config_items = list_push(&config_items,c);
}

/* set a config setting to an int value */
void config_set_int(char* name, int value)
{
	char str[20];
	sprintf(str,"%d",value);
	config_set(name,str);
}

/* set and apply a config value */
void config_set_and_apply(char* name, char* value)
{
	config_set(name,value);
	cmd_apply(name,value);
}

/* set and apply a config value as an int */
void config_set_and_apply_int(char* name, int value)
{
	char str[20];
	sprintf(str,"%d",value);
	config_set(name,str);
	cmd_apply(name,str);
}

/* load a config file */
void config_load(char* file)
{
	FILE *f;
	char* fn = file_path(file);
	f = fopen(fn,"r");
	if (f) {
		char name[255];
		char buff[2048];
		int o = 0;
		int r = 1;
		int fl = 0;
		int fp = 0;
		int cmt = 0;
		char c;
		rtprintf(CN_RTG "reading '%s'",file);
		while (r > 0) {
			if (fp == fl) {
				r = fread(buff,1,2048,f);
				fl = r;
				fp = 0;
			}
			if (r < 1)
				goto cm_exec;
			c = buff[fp++];
			if (!o && c == '#') {
				cmt = 1;
			}else if (c == '\n') {
cm_exec:
				if (o && !cmt) {
					name[o] = 0;
					cmd_exec(name);
				}
				o = 0;
				cmt = 0;
			}else if (cmt || o > 254 || (!o && c == ' ')) {
				continue;
			}else{
				name[o++] = c;
			}
		}
		fclose(f);
	}else{
		rtprintf(CN_NOTICE "failed to open '%s'",file);
	}
}

/* initialise configuration, load config files and defaults, etc */
void config_init(int argc, char** argv)
{
	int i;
	char file[255];
#ifndef WIN32
	char* h;
#endif
	config_t *f;

	/* set the setters */
	cmd_add_setter(RTG_SETTER_VFUNC,"sys.control",event_set_bind);
	cmd_add_setter(RTG_SETTER_LITERAL,"wm.width",wm_width_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"wm.height",wm_height_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"wm.framecap",wm_cap_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"wm.fullscreen",wm_fs_setter);
#ifdef RTG3D
	cmd_add_setter(RTG_SETTER_LITERAL,"wm.distance",wm_distance_setter);
#endif
	cmd_add_setter(RTG_SETTER_LITERAL,"sound.enabled",sound_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"sound.effects.volume",sound_effects_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"sound.music.volume",sound_music_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.anisotropic",opengl_anisotropic_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.bilinear",opengl_bilinear_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.trilinear",opengl_trilinear_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.mipmaps",opengl_mipmap_setter);
#ifdef RTG3D
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.shaders",shader_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.backfacecull",opengl_backfacecull_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.particles.enabled",opengl_particles_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"gl.particles.max",opengl_particles_max_setter);
#endif
	cmd_add_setter(RTG_SETTER_VFUNC,"ui.elements",ui_element_setter);

	for (i=1; i<argc; i++) {
		if (!strcmp(argv[i],"exec") && i+2 < argc && !strcmp(argv[i+1],"sys.cfg.load")) {
			s_config_exec(argv[i+2]);
			i += 2;
		}
	}

	config_load("default.cfg");

#ifndef WIN32
	h = getenv("XDG_CONFIG_HOME");
	if (h) {
		sprintf(file,"%s/%s/default.cfg",h,rtg_game_dir);
	}else{
		h = getenv("HOME");
		if (h)
			sprintf(file,"%s/.config/%s/default.cfg",h,rtg_game_dir);
	}
#else
	sprintf(file,"user.cfg");
#endif
	config_load(file);

	f = files;
	while (f) {
		config_load(f->name);
		f = f->next;
	}

	for (i=0; i<argc; i++) {
		if (!strcmp(argv[i],"set") && i+2 < argc) {
			i+=2;
			config_set_and_apply(argv[i+1],argv[i+2]);
		}else if (!strcmp(argv[i],"unset") && i+1 < argc) {
			i+=1;
			config_set_and_apply(argv[i+1],NULL);
		}
	}

	cfg_init = 1;
}

/* save the current config */
void config_save()
{
	char file[1024];
	char* fn;
	FILE *f;
	config_t *c;

	sprintf(file,"default.cfg");
	fn = file_path(file);
	if (file_exists(fn)) {
#ifndef WIN32
		char* home = getenv("HOME");
		sprintf(file,"%s/.config/%s",home,rtg_game_dir);
		dir_create(file);
		sprintf(file,"%s/.config/%s/default.cfg",home,rtg_game_dir);
#else
		sprintf(file,"user.cfg");
#endif
		fn = file;
	}

	f = fopen(fn,"w+");
	if (!f)
		return;

	c = config_items;
	while (c) {
		if (c->value)
			fprintf(f,"set %s %s\n",c->name,c->value);
		c = c->next;
	}

	fclose(f);
}
