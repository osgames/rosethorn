/************************************************************************
* ui_style.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

/* set style default for a widget */
void ui_style_defaults(widget_t *w)
{
	w->style.x = UI_ALIGN_CENTRE;
	w->style.y = 0;
	w->style.w = 0;
	w->style.h = SML_FONT;

	w->style.color.r = 255;
	w->style.color.g = 255;
	w->style.color.b = 255;
	w->style.color.a = 255;

	w->style.border.r = 139;
	w->style.border.g = 139;
	w->style.border.b = 0;
	w->style.border.a = 220;
	w->style.has_border = 0;

	w->style.ttcolor.r = 255;
	w->style.ttcolor.g = 255;
	w->style.ttcolor.b = 255;
	w->style.ttcolor.a = 255;

	w->style.ttbg.r = 20;
	w->style.ttbg.g = 20;
	w->style.ttbg.b = 20;
	w->style.ttbg.a = 200;
	w->style.has_ttbg = 1;

	w->style.has_ttborder = 0;

	w->style.ttfont_face = 1;
	w->style.ttfont_size = SML_FONT;

	if (w->type == UI_WIDGET_CONTAINER) {
		w->style.visible = 0;
	}else{
		w->style.visible = 1;
	}

	w->style.visibility_changed = 2;

	w->style.text_align = UI_ALIGN_CENTRE;
	w->style.font_face = 1;
	w->style.font_size = SML_FONT;

	w->style.bg = NULL;

	if (w->type == UI_WIDGET_HEADER) {
		w->style.h = LGE_FONT;
		w->style.font_size = LGE_FONT;
	}else if (w->type == UI_WIDGET_PBAR) {
		w->style.h = 12;
		w->style.w = 100;
	}else if (w->type == UI_WIDGET_CONTAINER) {
		w->style.has_border = 1;
	}else if (w->type == UI_WIDGET_CHECK) {
		w->style.h = 20;
		w->style.w = 20;
	}else if (w->type == UI_WIDGET_LISTROW) {
		w->style.w = w->parent->style.w;
		w->style.h = 25;
	}else if (w->type == UI_WIDGET_BUTTON) {
		w->style.color.r = 200;
		w->style.color.g = 200;
		w->style.color.b = 200;
		w->style.w = 50;
		w->style.h = 20;
	}else if (w->type == UI_WIDGET_NUM) {
		w->style.w = SML_FONT*2;
	}else if (w->type == UI_WIDGET_HSLIDE) {
		w->style.w = 100;
		w->style.h = 20;
	}
}

/* create a new style rule */
void ui_style_set(char* sel, char* value)
{
	char buff[1024];
	style_rule_t *st;
	style_rule_t *p = ui.styles;

	sprintf(buff,"%s {%s}",sel,value);

	st = style_parse(buff);
	while (p->next) {
		p = p->next;
	}
	p->next = st;
	style_refresh(ui.styles,ui.elements);
}

/* create a new style rule */
void ui_style(char* sel, char* attr, char* value)
{
	char buff[1024];
	sprintf(buff,"%s:%s;",attr,value);
	ui_style_set(sel,buff);
}
