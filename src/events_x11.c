/************************************************************************
* events_x11.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

#ifndef WIN32
/* read and act on events */
void events_main()
{
	XEvent event;
	XComposeStatus compose;
	KeySym keysym;
	char buff[2];
	int l;
	while (XPending(wm_res.dpy) > 0) {
		XNextEvent(wm_res.dpy, &event);
		switch (event.type) {
		case Expose:
			if (event.xexpose.count != 0)
				break;
			break;
		case ConfigureNotify:
			if ((event.xconfigure.width != wm_res.width) || (event.xconfigure.height != wm_res.height)) {
				wm_res.width = event.xconfigure.width;
				wm_res.height = event.xconfigure.height;
				wm_resize();
			}
			break;
		case ButtonPress:
			current_event.type = RTE_BUTTON_DOWN;
			if (event.xbutton.button == 1) {
				button[RTM_LEFT_BUTTON] = 1;
				current_event.button = RTM_LEFT_BUTTON;
				events_mouse();
			}else if (event.xbutton.button == 2) {
				button[RTM_MIDDLE_BUTTON] = 1;
				current_event.button = RTM_MIDDLE_BUTTON;
				events_mouse();
			}else if (event.xbutton.button == 3) {
				button[RTM_RIGHT_BUTTON] = 1;
				current_event.button = RTM_RIGHT_BUTTON;
				events_mouse();
			}else if (event.xbutton.button == 4) {
				current_event.button = RTM_SCROLL_UP;
				events_mouse();
			}else if (event.xbutton.button == 5) {
				current_event.button = RTM_SCROLL_DOWN;
				events_mouse();
			}
			break;
		case ButtonRelease:
			current_event.type = RTE_BUTTON_UP;
			if (event.xbutton.button == 1) {
				button[RTM_LEFT_BUTTON] = 0;
				current_event.button = RTM_LEFT_BUTTON;
				events_mouse();
			}else if (event.xbutton.button == 2) {
				button[RTM_MIDDLE_BUTTON] = 0;
				current_event.button = RTM_MIDDLE_BUTTON;
				events_mouse();
			}else if (event.xbutton.button == 3) {
				button[RTM_RIGHT_BUTTON] = 0;
				current_event.button = RTM_RIGHT_BUTTON;
				events_mouse();
			}
			break;
		case MotionNotify:
			current_event.type = RTE_MOUSE_MOTION;
			current_event.x = event.xmotion.x;
			current_event.y = event.xmotion.y;
			rmouse[0] = current_event.x-mouse[0];
			rmouse[1] = current_event.y-mouse[1];
			mouse[0] = current_event.x;
			mouse[1] = current_event.y;
			if (rmouse[0] > -100 && rmouse[0] < 100 && rmouse[1] > -100 && rmouse[1] < 100) {
				events_mouse();
				if (mouse[MGRAB]) {
					if (
						current_event.x < 5
						|| current_event.x > (wm_res.width-5)
					)
						XWarpPointer(wm_res.dpy, None, wm_res.win, 0, 0, 0, 0, wm_res.width/2, current_event.y);
					if (
						current_event.y < 5
						|| current_event.y > (wm_res.height-5)
					)
						XWarpPointer(wm_res.dpy, None, wm_res.win, 0, 0, 0, 0, current_event.x, wm_res.height/2);
				}
			}
			break;
		case KeyPress:
			current_event.type = RTE_KEY_DOWN;
			current_event.sym = XLookupKeysym(&event.xkey, 0);
			if (current_event.sym == RTK_Shift_L || current_event.sym == RTK_Shift_R) {
				shift = 1;
				break;
			}
			if (current_event.sym == RTK_Control_L || current_event.sym == RTK_Control_R)
				ctrl = 1;

			l = XLookupString(&event.xkey, buff, 2, &keysym, &compose);
			if (l) {
				current_event.ch = buff[0];
			}else{
				current_event.ch = 0;
			}

			if (current_event.sym > 255) {
				events_key_down();
			}else if (!key[current_event.sym]) {
				key[current_event.sym] = 1;
				events_key_down();
			}
			break;
		case KeyRelease:
			if (XEventsQueued(wm_res.dpy, QueuedAfterReading)) {
				XEvent nev;
				XPeekEvent(wm_res.dpy, &nev);
				if (
					nev.type == KeyPress
					&& nev.xkey.time == event.xkey.time
					&& nev.xkey.keycode == event.xkey.keycode
				) {
					break;
				}
			}
			current_event.type = RTE_KEY_UP;
			current_event.sym = XLookupKeysym(&event.xkey, 0);
			if (current_event.sym == RTK_Shift_L || current_event.sym == RTK_Shift_R) {
				shift = 0;
				break;
			}
			if (current_event.sym == RTK_Control_L || current_event.sym == RTK_Control_R)
				ctrl = 0;
			if (current_event.sym < 256) {
				key[current_event.sym] = 0;
			}
			events_key_up();
			break;
		case MappingNotify:
			XRefreshKeyboardMapping(&event.xmapping);
			break;
		case ClientMessage:
			if (strstr(XGetAtomName(wm_res.dpy, event.xclient.message_type),"WM_PROTOCOLS"))
				rtg_state = RTG_STATE_EXIT;
			break;
		default:;
		}
	}
	if (rtg_state >= RTG_STATE_PLAY && !focused_widget) {
		event_action_t *e = events;
		while (e) {
			if (event_active(e->name)) {
				if (e->a_func) {
					e->a_func();
				}
			}
			e = e->next;
		}
	}
}
#endif
