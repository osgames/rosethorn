/************************************************************************
* net_udp.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_net.h"

/* create a new udp client connection */
net_connection_t *udp_client_connect(char* host, char* port)
{
	net_connection_t *n = net_connection();
	n->type = RTG_NET_UDP;
	n->host = strdup(host);
	n->port = strdup(port);

	udp_client_reconnect(n);

	return n;
}

/* reconnect a close udp connection */
int udp_client_reconnect(net_connection_t *n)
{
	int max_retries = config_get_int("net.max_retries");
	if (!max_retries)
		max_retries = 5;
	if (n->state == RTG_NET_OPEN || n->tries > max_retries)
		return 2;

	n->tries++;

	if (n->state == RTG_NET_UNUSED) {
		int f = config_get_int("net.ipv4");
		int s = config_get_int("net.ipv6");
		memset(&n->hints, 0, sizeof(n->hints));
		if (s == f) {
			n->hints.ai_family = AF_UNSPEC;
		}else if (s) {
			n->hints.ai_family = AF_INET6;
		}else if (f) {
			n->hints.ai_family = AF_INET;
		}
		n->hints.ai_socktype = SOCK_DGRAM;

		/* resolve hostname */
		if (getaddrinfo(n->host, n->port, &n->hints, &n->addr)) {
			rtprintf(CN_DEBUG "Unable to resolve host '%s'",n->host);
			return 1;
		}

		n->state = RTG_NET_CLOSED;
	}

	/* open socket */
	if ((n->fd = socket(n->addr->ai_family, n->addr->ai_socktype, n->addr->ai_protocol)) == -1) {
		rtprintf(CN_DEBUG "Unable to reconnect to host %s",n->host);
		return 1;
	}

	/* connect to server */
	if (connect(n->fd, n->addr->ai_addr, n->addr->ai_addrlen)) {
		shutdown(n->fd,2);
		rtprintf(CN_DEBUG "Unable to reconnect to host %s",n->host);
		return 1;
	}

	n->state = RTG_NET_OPEN;
	n->tries = 0;

	return 0;
}

/* create a new udp server connection */
net_connection_t *udp_host_connect(char* host, char* port)
{
	net_connection_t *n = net_connection();
	if (!host)
		host = "*";

	n->type = RTG_NET_UDP_HOST;
	n->tries = 0;
	n->host = strdup(host);
	n->port = strdup(port);
	n->peers = array_create(RTG_TYPE_PTR);

	if (udp_host_reconnect(n)) {
		net_close(n);
		return NULL;
	}

	return n;
}

/* start listening for udp clients */
int udp_host_reconnect(net_connection_t *n)
{
	int max_retries = config_get_int("net.max_retries");
	if (!max_retries)
		max_retries = 5;
	if (n->state == RTG_NET_OPEN || n->tries > max_retries)
		return 2;

	n->tries++;

	if (n->state == RTG_NET_UNUSED) {
		char* nhost = NULL;
		int f = config_get_int("net.ipv4");
		int s = config_get_int("net.ipv6");

		memset(&n->hints, 0, sizeof(n->hints));
		if (s == f) {
			n->hints.ai_family = AF_UNSPEC;
		}else if (s) {
			n->hints.ai_family = AF_INET6;
		}else if (f) {
			n->hints.ai_family = AF_INET;
		}
		n->hints.ai_socktype = SOCK_DGRAM;
		n->hints.ai_flags = AI_PASSIVE;

		if (strcmp(n->host,"*"))
			nhost = n->host;

		/* resolve hostname */
		if (getaddrinfo(nhost, n->port, &n->hints, &n->addr)) {
			rtprintf(CN_DEBUG "Unable to resolve host '%s'",n->host);
			return 1;
		}

		n->state = RTG_NET_CLOSED;
	}

	/* open socket */
	if ((n->fd = socket(n->hints.ai_family, n->hints.ai_socktype, n->hints.ai_protocol)) == -1) {
		rtprintf(CN_DEBUG "Unable to open port %u",n->port);
		return 1;
	}
	/* bind to the socket */
	if ((bind(n->fd, n->addr->ai_addr,sizeof(*n->addr->ai_addr))) < 0) {
		rtprintf(CN_DEBUG "Unable to bind port %u",n->port);
		return 1;
	}

	n->state = RTG_NET_OPEN;
	n->tries = 0;

	return 0;
}

/* write data to a udp connection */
int udp_write(net_connection_t *n, void *buff, unsigned int size)
{
	int r = 0;
	if (n->state == RTG_NET_OPEN) {

		if (n->type == RTG_NET_UDP_HOST) {
			r = (int)sendto(n->fd, buff, size, 0, (struct sockaddr*)&n->remote_addr, n->remote_addr_len);
		}else{
			r = (int)write(n->fd,buff,size);
		}
		if (r < 1) {
			rtprintf(CN_DEBUG "failed to write to connection %d (%d)",n->fd,errno);
			if (n->type != RTG_NET_UDP_HOST)
				shutdown(n->fd,2);
			n->state = RTG_NET_CLOSED;
		}
	}
	return r;
}

static int udp_fill_buff(net_connection_t *n)
{
	unsigned char buff[1024];
	int r;
	int l = n->buff_end-n->buff_start;
	if (!n->buff_start && l == 1024)
		return 0;

	memcpy(buff,n->buff+n->buff_start,l);

	errno = 0;
	if (n->type == RTG_NET_UDP_HOST) {
		r = recvfrom(n->fd, buff, 1024-l, 0, (struct sockaddr *)&n->remote_addr, &n->remote_addr_len);
	}else{
		r = (int)recv(n->fd,buff,1024-l,0);
	}
	if (r < 1) {
#ifndef WIN32
		if (errno != EAGAIN && errno != EWOULDBLOCK) {
			if (n->type != RTG_NET_UDP_HOST)
				shutdown(n->fd,2);
			n->state = RTG_NET_CLOSED;
		}
#else
		if (n->type != RTG_NET_UDP_HOST)
			shutdown(n->fd,2);
		n->state = RTG_NET_CLOSED;
#endif
	}else{
		l += r;
	}

	memcpy(n->buff,buff,l);

	n->buff_start = 0;
	n->buff_end = l;

	return 0;
}

/* read data from a udp connection */
int udp_read(net_connection_t *n, void *buff, unsigned int size)
{
	if (n->state != RTG_NET_OPEN)
		return 0;

	udp_fill_buff(n);
	if (size > n->buff_end)
		size = n->buff_end;

	if (size < 1)
		return 0;

	memcpy(n->buff,buff,size);
	n->buff_start += size;

	return size;
}

/* read a line from a udp connection */
int udp_readline(net_connection_t *n, void *buf, unsigned int size)
{
	unsigned int i;
	char* buff = buf;
	if (n->state != RTG_NET_OPEN)
		return 0;

	for (i=0; i<size; i++) {
		if (n->buff_start >= n->buff_end)
			udp_fill_buff(n);
		if (n->buff_start >= n->buff_end)
			break;

		buff[i] = n->buff[n->buff_start++];
		if (buff[i] == '\n') {
			buff[i] = 0;
			break;
		}else if (i > size-2) {
			buff[i+1] = 0;
			break;
		}
	}
	return i;
}

/* broadcast data to all peers of a udp host connection */
int udp_broadcast(net_connection_t *n, void *buff, unsigned int size)
{
	int i;
	net_connection_t **p;
	if (!n || n->state != RTG_NET_OPEN || !n->peers || !n->peers->length)
		return 0;

	p = n->peers->data;
	for (i=0; i<n->peers->length; i++) {
		if (!p[i])
			continue;
		udp_write(p[i],buff,size);
	}

	return i;
}

/* accept new connections to a udp host */
int udp_accept(net_connection_t *n)
{
	net_connection_t *c;
	struct sockaddr_in *s4[2];
	struct sockaddr_in6 *s6[2];
	array_t *a = net_select(0,0,n);
	if (!a)
		return -1;

	if (!a->length) {
		array_free(a);
		return -1;
	}

	c = net_connection();
	c->type = n->type;
	c->state = RTG_NET_OPEN;
	c->fd = n->fd;

	c->buff_end = recvfrom(n->fd, c->buff, 1, 0, (struct sockaddr *)&c->remote_addr, &c->remote_addr_len);

	if (c->buff_end > -1) {
		int i;
		net_connection_t **p = n->peers->data;
		for (i=0; i<n->peers->length; i++) {
			if (p[i] && c->remote_addr.ss_family == p[i]->remote_addr.ss_family) {
				if (c->remote_addr.ss_family == AF_INET) {
					s4[0] = (struct sockaddr_in*)&c->remote_addr;
					s4[1] = (struct sockaddr_in*)&p[i]->remote_addr;
					if (s4[0]->sin_addr.s_addr == s4[1]->sin_addr.s_addr) {
						c->fd = -1;
						net_close(c);
						return i;
					}
				}else if (c->remote_addr.ss_family == AF_INET6) {
					s6[0] = (struct sockaddr_in6*)&c->remote_addr;
					s6[1] = (struct sockaddr_in6*)&p[i]->remote_addr;
					if (!memcmp(s6[0]->sin6_addr.s6_addr,s6[1]->sin6_addr.s6_addr,16)) {
						c->fd = -1;
						net_close(c);
						return i;
					}
				}
			}
		}
		for (i=0; i<n->peers->length; i++) {
			if (!p[i]) {
				p[i] = c;
				return i;
			}
		}
		array_push_ptr(n->peers,c);
		return n->peers->length-1;
	}

	c->fd = -1;
	net_close(c);
	return -1;
}
