/************************************************************************
* render.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"
#include "rtg_render.h"
#ifdef RTG3D
#include "rtg_physics.h"
#endif

int ticks = 0;
int tticks;
int tpf;
int fticks;
int font_init = 0;

static int render_color = -1;

static color_t color_stack[10] = {
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255},
	{255,255,255,255}
};

/* get the current colour */
color_t *render_get_color()
{
	if (render_color < 0)
		return NULL;

	return &color_stack[render_color];
}

/* start using a colour */
void render_color_push(color_t *c)
{
	if (render_color < 9) {
		render_color++;
		color_stack[render_color].r = c->r;
		color_stack[render_color].g = c->g;
		color_stack[render_color].b = c->b;
		color_stack[render_color].a = c->a;
	}
}

/* stop using the last pushed colour */
void render_color_pop()
{
	if (render_color > -1)
		render_color--;
}

/* clear the frame */
void render_clear()
{
	if (!ticks)
		ticks = time_ticks();
	tpf = 1000/wm_res.frame_cap;
	sound_process();
	events_main();
}

/* render to frame */
void render()
{
	ui_render();

	if (wm_res.cursor.mat)
		render2d_quad_mat(wm_res.cursor.mat,mouse[CUR_X]+wm_res.cursor.x,mouse[CUR_Y]+wm_res.cursor.y,wm_res.cursor.w,wm_res.cursor.h);

	glEnableClientState(GL_VERTEX_ARRAY);
	/* enable 2D texturing and clear the screen */
	glEnable(GL_TEXTURE_2D);
	glClearColor(0, 0, 0, 1.0);
	/* smooth blending so the hud layer doesn't hide the game layer */
	glEnable(GL_BLEND);
	glEnable(GL_LINE_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glViewport(0,0,wm_res.width,wm_res.height);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#ifdef RTG3D

	glShadeModel(GL_SMOOTH);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	/* 3D! */
	gluPerspective(45.0,(GLfloat)wm_res.width/(GLfloat)wm_res.height,0.1,wm_res.distance);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	/* depth testing, duh! */
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	physics_apply();
	/* lighting */
	light();

	camera_centre();
	render3d();
	shader_maybe_disable();
#endif

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* disable lighting */
	glDisable(GL_LIGHTING);
	/* 2D means minimal z level */
	glOrtho(0, wm_res.width, wm_res.height, 0, -1, 1);
	/* use modelview */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	render2d();

	glDisableClientState(GL_VERTEX_ARRAY);
	wm_update();

	/* calculate a scale factor for smooth animations */
	tticks = interval_delay(ticks,wm_res.frame_cap);
	wm_res.fps = calc_fps(ticks,tticks);
	anim_factor = time_scale(ticks,60);
	ticks = tticks;

#ifdef RTG3D
	shader_maybe_enable();
#endif
}
