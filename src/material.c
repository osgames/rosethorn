/************************************************************************
* material.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

static material_t *mat_list = NULL;
static int mat_ids = 0;

/*
typedef struct material_s {
	int id;
	char name[100];
	char has_amb;
	char has_dif;
	char has_spc;
	unsigned int options;
	texture_t *tex;
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
} material_t;
*/

/* find or create a material called 'name' */
material_t *mat_find_or_create(char* name)
{
	material_t *mat = mat_list;
	while (mat) {
		if (!strcmp(mat->name,name))
			return mat;
		mat = mat->next;
	}

	mat = mat_create();

	strcpy(mat->name,name);

	return mat;
}

/* create a new blank material */
material_t *mat_create()
{
	material_t *mat = malloc(sizeof(material_t));
	mat->id = mat_ids++;
	mat->name[0] = 0;
	mat->has_amb = 0;
	mat->has_dif = 0;
	mat->has_spc = 0;
	mat->options = 0;
	mat->tex = NULL;
	mat->next = NULL;
	mat->ambient[0] = 1.0;
	mat->ambient[1] = 1.0;
	mat->ambient[2] = 1.0;
	mat->ambient[3] = 1.0;
	mat->specular[0] = 1.0;
	mat->specular[1] = 1.0;
	mat->specular[2] = 1.0;
	mat->specular[3] = 1.0;
	mat->diffuse[0] = 1.0;
	mat->diffuse[1] = 1.0;
	mat->diffuse[2] = 1.0;
	mat->diffuse[3] = 1.0;

	mat_list = list_push(&mat_list,mat);

	return mat;
}

/* free a material */
void mat_free(material_t *mat)
{
	if (!mat)
		return;

	mat_list = list_remove(&mat_list,mat);

	if (mat->tex)
		tex_free(mat->tex);

	free(mat);
}

/* create a material from a texture */
material_t *mat_from_tex(texture_t *tex)
{
	material_t *mat = mat_find_or_create(tex->name);
	if (mat->tex)
		return mat;
	mat->tex = tex;
	strcpy(mat->name,tex->name);

	return mat;
}

/* create a material from pixel data */
material_t *mat_from_pixels(image_t *p)
{
	material_t *mat = mat_create();
	mat->tex = tex_from_pixels(p);
	strcpy(mat->name,mat->tex->name);

	return mat;
}

/* update a material from pixel data */
material_t *mat_update_pixels(material_t *mat, image_t *p)
{
	if (!mat)
		return mat_from_pixels(p);

	mat->tex = tex_update_pixels(mat->tex,p);

	return mat;
}

/* create a material from an image */
material_t *mat_from_image(char* file)
{
	material_t *mat = mat_find_or_create(file);
	if (mat->tex)
		return mat;
	mat->tex = tex_from_image(file);
	if (!mat->tex) {
		mat_free(mat);
		return NULL;
	}
	strcpy(mat->name,mat->tex->name);

	return mat;
}

/* create a material from float diffuse values */
material_t *mat_from_dif3f(GLfloat r, GLfloat g, GLfloat b)
{
	char buff[100];
	material_t *mat;
	snprintf(buff,100,"rgb-%f-%f-%f",r,g,b);

	mat = mat_find_or_create(buff);
	if (mat->has_dif)
		return mat;

	mat->diffuse[0] = r;
	mat->diffuse[1] = g;
	mat->diffuse[2] = b;

	mat->has_dif = 1;

	strcpy(mat->name,buff);

	return mat;
}

/* create a material from unsigned byte diffuse values */
material_t *mat_from_dif3ub(unsigned char r, unsigned char g, unsigned char b)
{
	GLfloat rf = (GLfloat)r/255.0;
	GLfloat gf = (GLfloat)g/255.0;
	GLfloat bf = (GLfloat)b/255.0;

	return mat_from_dif3f(rf,gf,bf);
}

/* create a material from a color */
material_t *mat_from_color(color_t *c)
{
	char buff[100];
	material_t *mat;
	GLfloat rf = (GLfloat)c->r/255.0;
	GLfloat gf = (GLfloat)c->g/255.0;
	GLfloat bf = (GLfloat)c->b/255.0;
	GLfloat af = (GLfloat)c->a/255.0;
	snprintf(buff,100,"rgba-%.3f-%.3f-%.3f-%.3f",rf,gf,bf,af);

	mat = mat_find_or_create(buff);
	if (mat->has_dif)
		return mat;

	mat->diffuse[0] = rf;
	mat->diffuse[1] = gf;
	mat->diffuse[2] = bf;
	mat->diffuse[3] = af;

	mat->has_dif = 1;

	strcpy(mat->name,buff);

	return mat;
}

/* use a material */
void mat_use(material_t *mat)
{
	if (mat->tex) {
		if (mat->tex->state != 1)
			tex_generate(mat->tex);
		glGetError();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,mat->tex->tex);
		if (glGetError() != GL_NO_ERROR)
			mat->tex->state = 0;
	}else{
		glDisable(GL_TEXTURE_2D);
	}

	if (mat->has_dif) {
		glColor4f(mat->diffuse[0],mat->diffuse[1],mat->diffuse[2],mat->diffuse[3]);
	}else{
		glColor4f(1.0,1.0,1.0,1.0);
	}

	/* options */
	opengl_backfacecull_setter((mat->options&RTG_MATOPT_BFCULL));

	return;
	/* specular and ambient doesn't work well (if at all) with
	 * glDrawElements() so ignore them
	 * TODO: a better solution */

	if (mat->has_spc) {
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat->specular);
	}else{
		GLfloat spc[4] = {1.0,1.0,1.0,1.0};
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spc);
	}

	if (mat->has_amb) {
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat->ambient);
	}else{
		GLfloat amb[4] = {1.0,1.0,1.0,1.0};
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	}
}

/* set the name of a material */
void mat_name(material_t *mat, char* name)
{
	strncpy(mat->name,name,100);
}

/* set the texture of a material */
void mat_tex(material_t *mat, texture_t *tex)
{
	mat->tex = tex;
}

/* set the texture of a material from an image file */
void mat_tex_file(material_t *mat, char* file)
{
	mat->tex = tex_from_image(file);
	strcpy(mat->name,mat->tex->name);
}

/* set the ambient values of a material from float values */
void mat_amb3f(material_t *mat, GLfloat r, GLfloat g, GLfloat b)
{
	mat->ambient[0] = r;
	mat->ambient[1] = g;
	mat->ambient[2] = b;

	mat->has_amb = 1;
}

/* set the ambient values of a material from unsigned byte values */
void mat_amb3ub(material_t *mat, unsigned char r, unsigned char g, unsigned char b)
{
	mat->ambient[0] = (GLfloat)r/255.0;
	mat->ambient[1] = (GLfloat)g/255.0;
	mat->ambient[2] = (GLfloat)b/255.0;

	mat->has_amb = 1;
}

/* set the diffuse values of a material from float values */
void mat_dif3f(material_t *mat, GLfloat r, GLfloat g, GLfloat b)
{
	mat->diffuse[0] = r;
	mat->diffuse[1] = g;
	mat->diffuse[2] = b;

	mat->has_dif = 1;
}

/* set the diffuse values of a material from unsigned byte values */
void mat_dif3ub(material_t *mat, unsigned char r, unsigned char g, unsigned char b)
{
	mat->diffuse[0] = (GLfloat)r/255.0;
	mat->diffuse[1] = (GLfloat)g/255.0;
	mat->diffuse[2] = (GLfloat)b/255.0;

	mat->has_dif = 1;
}

/* set the specular values of a material from float values */
void mat_spc3f(material_t *mat, GLfloat r, GLfloat g, GLfloat b)
{
	mat->specular[0] = r;
	mat->specular[1] = g;
	mat->specular[2] = b;

	mat->has_spc = 1;
}

/* set the specular values of a material from unsigned byte values */
void mat_spc3ub(material_t *mat, unsigned char r, unsigned char g, unsigned char b)
{
	mat->specular[0] = (GLfloat)r/255.0;
	mat->specular[1] = (GLfloat)g/255.0;
	mat->specular[2] = (GLfloat)b/255.0;

	mat->has_spc = 1;
}

/* set the alpha value of a material from float data */
void mat_alf(material_t *mat, GLfloat d)
{
	mat->diffuse[3] = d;
	mat->has_dif = 1;
}

/* set the alpha values of a material from unsigned byte data */
void mat_alub(material_t *mat, unsigned char d)
{
	mat->diffuse[3] = (GLfloat)d/255.0;
	mat->has_dif = 1;
}

/* set the options for a material, return the previous options */
unsigned int mat_options(material_t *mat, unsigned int opt)
{
	unsigned int o = mat->options;
	if (opt != RTG_MATOPT_GET)
		mat->options = opt;
	return o;
}
