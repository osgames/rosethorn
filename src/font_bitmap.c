/************************************************************************
* font_bitmap.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

static int font_bitmap_make_char(font_t *f, int ch, int width, int height, texture_t *tex)
{
	int x;
	int y;
	int sw = (float)width*(30.0/(float)height);

	/* find the character */
	x = tex->px->w/width;
	y = (ch/x)*height;
	x = (ch%x)*width;

	rtprintf("font_bitmap_make_char('%s',%d,%d,%d,'%s') %d %d %d",f->token,ch,width,height,tex->name,x,y,sw);

	/* make a display list */
	glNewList(f->list_base+ch,GL_COMPILE);
	glBindTexture(GL_TEXTURE_2D,f->textures[0]);
	glPushMatrix();

	/* and the actual rendering */
	glBegin(GL_QUADS);
		glTexCoord2d(tex->xf*x,tex->yf*(y+height));
		glVertex2f(0,30);
		glTexCoord2d(tex->xf*x,tex->yf*y);
		glVertex2f(0,0);
		glTexCoord2d(tex->xf*(x+width),tex->yf*y);
		glVertex2f(sw,0);
		glTexCoord2d(tex->xf*(x+width),tex->yf*(y+height));
		glVertex2f(sw,30);
	glEnd();
	glPopMatrix();
	glTranslatef(sw,0,0);

	/* done! */
	glEndList();
	f->widths[ch] = sw;

	return 0;
}


/* load a bitmap font file */
int font_load_bitmap(font_t *f, char* file)
{
	xml_tag_t *x;
	xml_tag_t *t;
	unsigned char i;
	texture_t *tex;
	int r = 0;
	int w = 0;
	int h = 0;
	char* ff = NULL;

	x = file_load_xml(file);
	if (!x)
		return 1;

	t = x;
	while (t) {
		if (!strcmp(t->tag,"font")) {
			w = xml_attribute_int(t,"width");
			h = xml_attribute_int(t,"height");
			ff = xml_attribute(t,"texture");
			break;
		}
		t = t->next;
	}

	if (!w || !h || !ff)
		return 1;

	/* the opengl lists */
	f->list_base = glGenLists(128);
	if (!f->list_base)
		return 1;
	if (glGetError() != GL_NO_ERROR)
		return 1;

	tex = tex_from_image(ff);
	if (!tex)
		return 1;
	f->textures[0] = tex->tex;

	/* create the characters */
	for (i=0; i<128; i++) {
		if (!r) {
			r = font_bitmap_make_char(f,i,w,h,tex);
		}else{
			font_bitmap_make_char(f,i,w,h,tex);
		}
	}

	/* done! */
	xml_free(x);

	return r;
}
