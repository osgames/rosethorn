/************************************************************************
* ui_list.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

#include <stdarg.h>

/* create new ui list widget */
widget_t *ui_widget_list_create(widget_t *parent, int (*call_back)(widget_t *), int cols, ...)
{
	va_list ap;
	int i;
	int width;
	char* name;
	widget_t *w;
	width = 0;

	w = ui_widget_create(UI_WIDGET_LIST,parent);
	w->data->lst.col_cnt = cols;
	w->data->lst.columns = malloc(sizeof(list_column_t)*cols);
	w->data->lst.click = call_back;

	va_start(ap,cols);
	for (i=0; i<cols; i++) {
		w->data->lst.columns[i].width = va_arg(ap,int);
		width += w->data->lst.columns[i].width;
		name = va_arg(ap,char*);
		w->data->lst.columns[i].name = strdup(name);
	}
	va_end(ap);

	w->style.w = width;

	return w;
}

/* add a row to a ui list */
widget_t *ui_widget_list_push(widget_t *parent, ...)
{
	va_list ap;
	int i;
	char* name;
	widget_t *w;

	w = ui_widget_create(UI_WIDGET_LISTROW,parent);
	va_start(ap,parent);
	for (i=0; i<parent->data->lst.col_cnt; i++) {
		name = va_arg(ap,char*);
		w->data->row.values[i] = strdup(name);
	}
	va_end(ap);

	parent->data->lst.rows++;

	return w;
}

/* delete all rows from a ui list */
void ui_widget_list_clear(widget_t *w)
{
	widget_t *wh;
	if (w->type != UI_WIDGET_LIST)
		return;
	while (w->child) {
		wh = w->child;
		w->child = wh->next;
		ui_widget_free(wh);
	}

	w->data->lst.rows = 0;
}
