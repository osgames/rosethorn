/************************************************************************
* model_3ds.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

#define READINT() *((int*)(f->data+f->pos)); f->pos += 4
#define READUCHAR() *((unsigned char*)(f->data+f->pos)); f->pos += 1
#define READSHORT() *((short*)(f->data+f->pos)); f->pos += 2
#define READUSHORT() *((unsigned short*)(f->data+f->pos)); f->pos += 2
#define READFLOAT() *((float*)(f->data+f->pos)); f->pos += sizeof(float)

#define M3DS_MAIN	0x4D4D
#define M3DS_EDITOR	0x3D3D
#define M3DS_MESH	0x4100
#define M3DS_MATBLOCK	0xAFFF
#define M3DS_MATNAME	0xA000
#define M3DS_TEXTURE	0xA200
#define M3DS_OBJECT	0x4000
#define M3DS_VERTEX	0x4110
#define M3DS_FACE	0x4120
#define M3DS_FACETEX	0x4130
#define M3DS_MAPCOORD	0x4140
#define M3DS_TEXIMAGE	0xA300
#define M3DS_AMBIENT	0xA010
#define M3DS_DIFFUSE	0xA020
#define M3DS_SPECULAR	0xA030
#define M3DS_COLOR_FLT	0x0010
#define M3DS_COLOR_24	0x0011

/* load a 3ds model */
model_t *model_load_3ds(file_t *f)
{
	model_t *mdl;
	mesh_t *mh = NULL;
	mesh_t *m = NULL;
	material_t *mat = NULL;
	unsigned short id;
	int len;
	int i;
	int l;
	/*color_t c = {128,128,128,255};*/
	unsigned short vsp;
	unsigned short vs;
	float vf;

	mdl = model_create();

	while (f->pos < f->len) {
		id = READUSHORT();
		len = READINT();
		rtprintf("id: 0x%.4hX",id);

		switch (id) {
		case M3DS_MESH:
			m = mesh_create(GL_TRIANGLES);
			m->t = array_create(RTG_TYPE_FLOAT);
			array_push_ptr(mdl->meshes,m);
		case M3DS_MAIN:
		case M3DS_EDITOR:
		case M3DS_MATBLOCK:
		case M3DS_TEXTURE:
			break;
		case M3DS_FACETEX:
			rtprintf("facetex: '%s'",(char*)(f->data+f->pos));
			mat = mat_find_or_create((char*)(f->data+f->pos));
			if (mat)
				m->mat = mat;
			if (strstr((char*)(f->data+f->pos),"hair")) {
				mh = m;
				array_pop_ptr(mdl->meshes);
			}
			file_seek(f,len-6,SEEK_CUR);
			break;
		case M3DS_OBJECT:
			mdl->name = strdup((char*)(f->data+f->pos));
			f->pos += strlen(mdl->name)+1;
			break;
		case M3DS_VERTEX:
			l = READUSHORT();
			for (i=0; i<l; i++) {
				vf = READFLOAT();
				array_push_float(m->v,vf);
				vf = READFLOAT();
				array_push_float(m->v,vf);
				vf = READFLOAT();
				array_push_float(m->v,vf);
			}
			break;
		case M3DS_FACE:
			l = READUSHORT();
			for (i=0; i<l; i++) {
				vs = READUSHORT();
				array_push_int(m->i,vs);
				vsp = READUSHORT();
				vs = READUSHORT();
				array_push_int(m->i,vs);
				array_push_int(m->i,vsp);
				file_seek(f,2,SEEK_CUR);
			}
			break;
		case M3DS_MAPCOORD:
			l = READUSHORT();
			for (i=0; i<l; i++) {
				vf = READFLOAT();
				array_push_float(m->t,vf);
				vf = 1.0-READFLOAT();
				array_push_float(m->t,vf);
			}
			break;
		case M3DS_MATNAME:
			mat = mat_find_or_create((char*)(f->data+f->pos));
			file_seek(f,len-6,SEEK_CUR);
			break;
		case M3DS_TEXIMAGE:
			mat->tex = tex_from_image((char*)(f->data+f->pos));
			file_seek(f,len-6,SEEK_CUR);
			break;
/* XXX: these are disabled, as they never seem to work right
		case M3DS_AMBIENT:
			vs = READUSHORT();
			if (vs == 0x0010) {
				c.r = 255*READFLOAT();
				c.g = 255*READFLOAT();
				c.b = 255*READFLOAT();
				mat_amb3ub(mat,c.r,c.g,c.b);
				file_seek(f,(len-(3*sizeof(float)))-8,SEEK_CUR);
			}else if (vs == 0x0011) {
				c.r = READUCHAR();
				c.g = READUCHAR();
				c.b = READUCHAR();
				mat_amb3ub(mat,c.r,c.g,c.b);
				file_seek(f,len-11,SEEK_CUR);
			}else{
				file_seek(f,len-8,SEEK_CUR);
			}
			break;
		case M3DS_DIFFUSE:
			vs = READUSHORT();
			if (vs == 0x0010) {
				c.r = 255*READFLOAT();
				c.g = 255*READFLOAT();
				c.b = 255*READFLOAT();
				mat_dif3ub(mat,c.r,c.g,c.b);
				file_seek(f,(len-(3*sizeof(float)))-8,SEEK_CUR);
			}else if (vs == 0x0011) {
				c.r = READUCHAR();
				c.g = READUCHAR();
				c.b = READUCHAR();
				mat_dif3ub(mat,c.r,c.g,c.b);
				file_seek(f,len-11,SEEK_CUR);
			}else{
				file_seek(f,len-8,SEEK_CUR);
			}
			break;
		case M3DS_SPECULAR:
			vs = READUSHORT();
			if (vs == 0x0010) {
				c.r = 255*READFLOAT();
				c.g = 255*READFLOAT();
				c.b = 255*READFLOAT();
				mat_spc3ub(mat,c.r,c.g,c.b);
				file_seek(f,(len-(3*sizeof(float)))-8,SEEK_CUR);
			}else if (vs == 0x0011) {
				c.r = READUCHAR();
				c.g = READUCHAR();
				c.b = READUCHAR();
				mat_spc3ub(mat,c.r,c.g,c.b);
				file_seek(f,len-11,SEEK_CUR);
			}else{
				file_seek(f,len-8,SEEK_CUR);
			}
			break;*/
		default:
			 file_seek(f,len-6,SEEK_CUR);
		}
	}

	if (mh)
		array_push_ptr(mdl->meshes,mh);

	for (i=0; i<mdl->meshes->length; i++) {
		m = ((mesh_t**)mdl->meshes->data)[i];
		mesh_normalise(m);
	}

	return mdl;
}
