/************************************************************************
* main.c
* rtg - rosethorn game engine config
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include <stdio.h>

#define CFG_STATIC	0x01
#define CFG_LIBS	0x02
#define CFG_FLAGS	0x04
#define CFG_3D		0x08
#define CFG_SERVER	0x10

void usage()
{
	printf(
		"Usage:\n %s-config [--cflags] [--libs] [--static] [--2d | --3d | --server] [--version]\n",
		PACKAGE
	);
}

int main(int argc, char** argv)
{
	int i;
	unsigned int f = 0;
	for (i=0; i<argc; i++) {
		if (!strcmp(argv[i],"--libs")) {
			f |= CFG_LIBS;
		}else if (!strcmp(argv[i],"--version")) {
			printf("%s\n",VERSION);
			return 0;
		}else if (!strcmp(argv[i],"--cflags")) {
			f |= CFG_FLAGS;
		}else if (!strcmp(argv[i],"--static")) {
			f |= CFG_STATIC;
		}else if (!strcmp(argv[i],"--2d")) {
			f &= ~CFG_3D;
			f &= ~CFG_SERVER;
		}else if (!strcmp(argv[i],"--3d")) {
			f |= CFG_3D;
		}else if (!strcmp(argv[i],"--server")) {
			f |= CFG_SERVER;
		}else if (!strcmp(argv[i],"--help")) {
			usage();
			return 0;
		}
	}

	if (!f) {
		usage();
		return 0;
	}

	if ((f&CFG_FLAGS)) {
		printf("-I%s %s ",INCDIR,CFLAGS);
		if ((f&CFG_3D)) {
			printf("-DRTG3D=1 ");
		}else if ((f&CFG_SERVER)) {
			printf("-DRTGSERVER=1 ");
		}else{
			printf("-DRTG2D=1 ");
		}
	}

	if ((f&CFG_STATIC)) {
		if ((f&CFG_3D)) {
			printf("%s %s/librtg3d.a ",CLIBS,LIBDIR);
		}else if ((f&CFG_SERVER)) {
			printf("%s %s/librtgserver.a ",CLIBS,LIBDIR);
		}else{
			printf("%s %s/librtg2d.a ",CLIBS,LIBDIR);
		}
	}else if ((f&CFG_LIBS)) {
		if ((f&CFG_3D)) {
			printf("-lrtg3d ");
		}else if ((f&CFG_SERVER)) {
			printf("-lrtgserver ");
		}else{
			printf("-lrtg2d ");
		}
	}

	printf("\n");

	return 0;
}
