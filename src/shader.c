/************************************************************************
* shader.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

static int shader_state = 1;
static shader_t *shaders = NULL;
static GLuint shader_program = 0;

static shader_t *shader_create(char* file, GLenum type)
{
	shader_t *s = malloc(sizeof(shader_t));
	s->src = file_load(file);

	if (!s->src) {
		free(s);
		return NULL;
	}

	s->type = type;
	s->in_use = 0;
	s->id = opengl_create_shader(type);

	if (!s->id) {
		file_free(s->src);
		free(s);
		return NULL;
	}

	opengl_shader_source(s->id, 1, (const GLchar**)&s->src->data,NULL);

	opengl_compile_shader(s->id);

	shaders = list_push(&shaders,s);

	return s;
}

/* TODO: there has to be a better way of doing this */
/* internal shader toggler */
void shader_maybe_disable()
{
	shader_t *s = shaders;
	if (!shader_state || !shader_program)
		return;
	while (s) {
		if (s->in_use)
			opengl_detach_shader(shader_program,s->id);
		s = s->next;
	}
	opengl_link_program(shader_program);
	opengl_use_program(shader_program);
}

/* internal shader toggler */
void shader_maybe_enable()
{
	shader_t *s = shaders;
	if (!shader_state)
		return;
	if (!shader_program)
		shader_program = opengl_create_program();
	while (s) {
		if (s->in_use)
			opengl_attach_shader(shader_program,s->id);
		s = s->next;
	}
	opengl_link_program(shader_program);
	opengl_use_program(shader_program);
}

/* command shader setter */
void shader_setter(char* value)
{
	shader_state = !!strtol(value,NULL,10);
	if (shader_state) {
		if (!shader_program) {
			shader_t *s = shaders;
			shader_program = opengl_create_program();
			while (s) {
				if (s->in_use)
					opengl_attach_shader(shader_program,s->id);
				s = s->next;
			}
			opengl_link_program(shader_program);
		}
		opengl_use_program(shader_program);
	}else{
		if (!shader_program)
			return;
		opengl_delete_program(shader_program);
		shader_program = 0;
	}
}

/* find a shader by id */
shader_t *shader_find(int id)
{
	shader_t *s = shaders;
	while (s) {
		if (s->id == id)
			return s;
		s = s->next;
	}
	return NULL;
}

/* create a new vertex shader */
shader_t *shader_create_vertex(char* file)
{
	return shader_create(file,GL_VERTEX_SHADER);
}

/* create a new fragment shader */
shader_t *shader_create_fragment(char* file)
{
	return shader_create(file,GL_FRAGMENT_SHADER);
}

/* free a shader */
void shader_free(shader_t *s)
{
	if (!s)
		return;

	if (s->in_use && shader_program) {
		opengl_detach_shader(shader_program,s->id);
		opengl_link_program(shader_program);
		if (shader_state)
			opengl_use_program(shader_program);
	}

	file_free(s->src);
	free(s);
}

/* use a shader */
int shader_add(shader_t *s)
{
	if (!s || s->in_use)
		return 1;

	if (!shader_program)
		shader_program = opengl_create_program();

	opengl_attach_shader(shader_program,s->id);

	s->in_use = 1;

	opengl_link_program(shader_program);

	if (shader_state)
		opengl_use_program(shader_program);

	return 0;
}

/* stop using a shader */
int shader_remove(shader_t *s)
{
	if (!s || !s->in_use)
		return 1;

	if (!shader_program)
		shader_program = opengl_create_program();

	opengl_detach_shader(shader_program,s->id);

	s->in_use = 0;

	opengl_link_program(shader_program);

	if (shader_state)
		opengl_use_program(shader_program);

	return 0;
}
