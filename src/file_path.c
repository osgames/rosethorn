/************************************************************************
* file_path.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"

static int path_length = 0;
static char* path = NULL;
static array_t* base_path = NULL;

static char* ext_list[][2] = {
	{"tga","images"},
	{"png","images"},
	{"jpg","images"},
	{"bmp","images"},
	{"ttf","fonts"},
	{"xml","xml"},
	{"ui","xml"},
	{"ogg","sounds"},
	{"wav","sounds"},
	{"rta","assets"},
	{"lua","scripts"},
	{"obj","models"},
	{"mtl","models"},
	{"md5mesh","models"},
	{"md5anim","models"},
	{"3ds","models"},
	{"ms3d","models"},
	{"rtm","models"},
	{"vert","shaders"},
	{"frag","shaders"},
	{"glsl","shaders"},
	{"cfg",NULL},
	{NULL,NULL}
};

/* get the full path of a file */
char* file_path(char* filename)
{
	char* p;
	char* t;
	char* tp;
	int pl;
	int i;
	int l;
	int k;

	if (!base_path || !base_path->length)
		rtg_path_add(NULL);

	if (!filename || filename[0] == '/')
		return filename;

	t = strrchr(filename,'.');
	/* this should be a directory */
	if (!t) {
		for (k=base_path->length-1; k>-1; k--) {
			p = array_get_string(base_path,k);
			l = strlen(p)+strlen(filename)+2;

			if (l > path_length) {
				pl = l+10;
				tp = realloc(path,pl);
				if (!tp)
					return NULL;

				path = tp;
				path_length = pl;
			}

			sprintf(path,"%s/%s",p,filename);
			if (dir_exists(path))
				return path;
		}

		p = array_get_string(base_path,0);
		l = strlen(p)+strlen(filename)+2;

		if (l > path_length) {
			pl = l+10;
			tp = realloc(path,pl);
			if (!tp)
				return NULL;

			path = tp;
			path_length = pl;
		}

		sprintf(path,"%s/%s",p,filename);

		return path;
	}

	t++;

	for (i=0; ext_list[i][0]; i++) {
		if (!strcmp(ext_list[i][0],t))
			break;
	}

	t = ext_list[i][1];

	if (!t) {
		for (k=base_path->length-1; k>-1; k--) {
			p = array_get_string(base_path,k);
			l = strlen(p)+strlen(filename)+2;

			if (l > path_length) {
				pl = l+10;
				tp = realloc(path,pl);
				if (!tp)
					return NULL;

				path = tp;
				path_length = pl;
			}

			sprintf(path,"%s/%s",p,filename);
			if (file_exists(path))
				return path;
		}

		p = array_get_string(base_path,0);
		l = strlen(p)+strlen(filename)+2;

		if (l > path_length) {
			pl = l+10;
			tp = realloc(path,pl);
			if (!tp)
				return NULL;

			path = tp;
			path_length = pl;
		}

		sprintf(path,"%s/%s",p,filename);

		return path;
	}

	for (k=base_path->length-1; k>-1; k--) {
		p = array_get_string(base_path,k);
		l = strlen(p)+strlen(t)+strlen(filename)+3;

		if (l > path_length) {
			pl = l+10;
			tp = realloc(path,pl);
			if (!tp)
				return NULL;

			path = tp;
			path_length = pl;
		}

		sprintf(path,"%s/%s/%s",p,t,filename);
		if (file_exists(path))
			return path;
	}

	p = array_get_string(base_path,0);
	l = strlen(p)+strlen(t)+strlen(filename)+3;

	if (l > path_length) {
		pl = l+10;
		tp = realloc(path,pl);
		if (!tp)
			return NULL;

		path = tp;
		path_length = pl;
	}

	sprintf(path,"%s/%s/%s",p,t,filename);

	return path;
}

void rtg_path_add_home()
{
#ifndef WIN32
	char* h = getenv("XDG_DATA_HOME");
	if (h) {
		char dir[1024];
		sprintf(dir,"%s/%s",h,rtg_game_dir);
		rtg_path_add(dir);
	}else{
		h = getenv("HOME");
		if (h) {
			char dir[1024];
			sprintf(dir,"%s/.local/share/%s",h,rtg_game_dir);
			rtg_path_add(dir);
		}
	}
#else
	/* TODO: windows */
#endif
}

/* add a path to game data */
void rtg_path_add(char* p)
{
	if (!base_path)
		base_path = array_create(RTG_TYPE_STRING);

	if (!p) {
		array_push_string(base_path,"data");
		if (strcmp(GAMEDATA,"data")) {
			int l = strlen(GAMEDATA)+strlen(rtg_game_dir)+2;
			p = alloca(l);
			sprintf(p,"%s/%s",GAMEDATA,rtg_game_dir);
			array_push_string(base_path,p);
		}
	}else{
		array_push_string(base_path,p);
	}

	if (!path) {
		path = malloc(1024);
		path_length = 1024;
	}
}

/* set the path to game data */
void rtg_path_set(char* p)
{
	if (base_path) {
		array_free(base_path);
		base_path = NULL;
	}

	rtg_path_add(p);
}

/* a simple file type checker */
char* file_type(char* file)
{
	static char r[100];
	int i;
	char* t = strrchr(file,'.');
	if (!t)
		return NULL;
	t++;

	for (i=0; ext_list[i][0]; i++) {
		if (!strcmp(ext_list[i][0],t))
			break;
	}

	t = ext_list[i][1];

	if (!t)
		return NULL;

	i = strlen(t);
	strcpy(r,t);
	r[i] = 0;

	return r;
}
