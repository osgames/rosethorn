/************************************************************************
* font_ttf.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H
#include FT_TRIGONOMETRY_H

/* create a display list for a character */
static int font_ttf_make_char(font_t *f, FT_Face face, int ch)
{
	int i;
	int j;
	int k;
	int width;
	int height;
	float x;
	float y;
	int b;
	int t;
	int m;
	FT_Glyph glyph;
	FT_BitmapGlyph bitmap_glyph;
	GLubyte* expanded_data;

	/* load and get the glyph */
	if (FT_Load_Glyph(face,FT_Get_Char_Index(face,ch),FT_LOAD_DEFAULT))
		return 1;

	if (FT_Get_Glyph(face->glyph,&glyph))
		return 1;

	/* make a bitmap of the glyph */
	FT_Glyph_To_Bitmap(&glyph,ft_render_mode_normal,0,1);
	bitmap_glyph = (FT_BitmapGlyph)glyph;

	/* get the next power of 2 size for the character's texture */
	width = math_next_pow2(bitmap_glyph->bitmap.width);
	height = math_next_pow2(bitmap_glyph->bitmap.rows);

	expanded_data = alloca(2 * width * height);

	/* now copy the bitmap data over, and add the padding */
	for (j=0; j<height; j++) {
		for (i=0; i<width; i++){
			k = 2*(i+(j*width));
			expanded_data[k] = 255;
			k++;
			if (i >= bitmap_glyph->bitmap.width || j >= bitmap_glyph->bitmap.rows) {
				expanded_data[k] = 0;
			}else{
				expanded_data[k] = bitmap_glyph->bitmap.buffer[i + bitmap_glyph->bitmap.width*j];
			}
		}
	}

	/* now make a texture out of it */
	glBindTexture(GL_TEXTURE_2D, f->textures[ch]);

	b = opengl_has_bilinear();
	t = opengl_has_trilinear();
	m = opengl_has_mipmap();
	if (t) {
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		if (m) {
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		}else{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
	}else if (b) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}else{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	if (opengl_has_anisotropic()) {
		float ma = opengl_max_anisotropic();
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, ma);
	}

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_LUMINANCE_ALPHA,GL_UNSIGNED_BYTE,expanded_data);
	if (glGetError() != GL_NO_ERROR)
		return 1;

	if (m)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 4, width, height , GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data);

	/* and make a display list */
	glNewList(f->list_base+ch,GL_COMPILE);
	glBindTexture(GL_TEXTURE_2D,f->textures[ch]);
	glPushMatrix();

	glTranslatef(-bitmap_glyph->left,28-bitmap_glyph->top,0);

	/* this is a bit of fiddling so we render the character, but not the padding */
	x = (float)bitmap_glyph->bitmap.width / (float)width;
	y = (float)bitmap_glyph->bitmap.rows / (float)height;

	/* and the actual rendering */
	glBegin(GL_QUADS);
		glTexCoord2d(0,y);
		glVertex2f(0,bitmap_glyph->bitmap.rows);
		glTexCoord2d(0,0);
		glVertex2f(0,0);
		glTexCoord2d(x,0);
		glVertex2f(bitmap_glyph->bitmap.width,0);
		glTexCoord2d(x,y);
		glVertex2f(bitmap_glyph->bitmap.width,bitmap_glyph->bitmap.rows);
	glEnd();
	glPopMatrix();
	glTranslatef(face->glyph->advance.x >> 6 ,0,0);

	/* done! */
	glEndList();
	f->widths[ch] = (face->glyph->advance.x >> 6)+1;
	FT_Done_Glyph(glyph);

	return 0;
}

/* load a true type font file */
int font_load_ttf(font_t *f, char* file)
{
	FT_Face face;
	FT_Library library;
	unsigned char i;
	int r = 0;
	unsigned int h = 30<<6;
	char* fn = file_path(file);

	if (FT_Init_FreeType(&library))
		return 1;

	if (FT_New_Face(library,fn,0,&face))
		return 1;

	FT_Set_Char_Size(face,h,h,72,72);

	/* the opengl bits for textures and lists */
	f->list_base = glGenLists(128);
	if (!f->list_base)
		return 1;
	glGenTextures(128,f->textures);
	if (glGetError() != GL_NO_ERROR)
		return 1;

	/* create the characters */
	for (i=0; i<128; i++) {
		if (!r) {
			r = font_ttf_make_char(f,face,i);
		}else{
			font_ttf_make_char(f,face,i);
		}
	}

	/* done! */
	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return r;
}
