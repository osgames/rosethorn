/************************************************************************
* sound.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_sound.h"
#include "rtg_render.h"

static struct {
	int caninit;
	int init;
	int effect_volume;
	int music_volume;
	int enabled;
	float effect_fade;
	float effect_fadev;
	float music_fade;
	float music_fadev;
	unsigned int flags;
	ALCdevice *device;
	ALCcontext *context;
} sound_data = {
	0,
	0,
	255,
	255,
	1,
	-1.0,
	0.0,
	-1.0,
	0.0,
	0,
	NULL,
	NULL
};

static sound_t *effects = NULL;
static sound_t *music = NULL;
static sound_instance_t *playing = NULL;
static sound_instance_t mplaying = {
	0,
	NULL
};

/* initialise sound */
int sound_init()
{
	sound_data.caninit = 1;
	if (!sound_data.enabled)
		return 0;

	/* initialise audio */
	sound_data.device = alcOpenDevice(NULL);
	if (!sound_data.device)
		return 1;

#ifdef RTG_HAVE_OGG
	if (alcIsExtensionPresent(sound_data.device, "EXT_vorbis"))
		sound_data.flags |= RTG_SOUND_VORBIS;
#endif

	sound_data.context = alcCreateContext(sound_data.device, NULL);

	if (!sound_data.context) {
		alcCloseDevice(sound_data.device);
		return 1;
	}

	if (!alcMakeContextCurrent(sound_data.context) || (alcGetError(sound_data.device) != ALC_NO_ERROR)) {
 		alcDestroyContext(sound_data.context);
		alcCloseDevice(sound_data.device);
		return 1;
	}

	sound_data.init = 1;

	return 0;
}

/* exit sound */
void sound_exit()
{
	if (sound_data.init) {
		sound_stop(0);
		alcMakeContextCurrent(NULL);
		alcDestroyContext(sound_data.context);
		alcCloseDevice(sound_data.device);
	}
}

/* do stuff */
void sound_process()
{
	ALint state;
	float v;
	sound_instance_t *d;
	sound_instance_t *i;
	if (sound_data.effect_fade > -0.5) {
		v = (sound_data.effect_fade/(240-wm_res.frame_cap))*anim_factor;
		sound_data.effect_fadev -= v*((float)sound_data.effect_volume/255.0);
		if (sound_data.effect_fadev < 0.01)
			sound_stop_effects(0);
	}
	i = playing;
	while (i) {
		alGetSourcei(i->id, AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING) {
			d = i;
			i = i->next;
			playing = list_remove(&playing,d);
			alDeleteSources(1, &d->id);
			free(d);
			continue;
		}
		if (sound_data.effect_fade > -0.5)
			alSourcef(i->id, AL_GAIN, sound_data.effect_fadev);
		i = i->next;
	}
	if (sound_data.music_fade > -0.5) {
		v = (sound_data.music_fade/(240-wm_res.frame_cap))*anim_factor;
		sound_data.music_fadev -= v*((float)sound_data.music_volume/255.0);
		if (sound_data.music_fadev < 0.01) {
			sound_stop_music(0);
		}else{
			alSourcef(mplaying.id, AL_GAIN, sound_data.music_fadev);
		}
	}
}

/* get/set sound state */
int sound_state(int v)
{
	if (v == 1) {
		sound_data.enabled = 1;
		if (sound_data.caninit && !sound_data.init)
			sound_init();
	}else if (!v) {
		sound_data.enabled = 0;
		if (sound_data.init)
			sound_exit();
	}

	return sound_data.enabled;
}

/* get/set sound effect volume */
int sound_volume_effects(int v)
{
	if (v > -1) {
		if (v > 255)
			v = 255;
		sound_data.effect_volume = v;

		if (sound_data.init) {
			sound_instance_t *i = playing;
			while (i) {
				alSourcef(i->id, AL_GAIN, (float)sound_data.effect_volume/255.0);
				i = i->next;
			}
		}
	}
	return sound_data.effect_volume;
}

/* get/set sound music volume */
int sound_volume_music(int v)
{
	if (v > -1) {
		if (v > 255)
			v = 255;
		sound_data.music_volume = v;

		if (sound_data.init && mplaying.id)
			alSourcef(mplaying.id, AL_GAIN, (float)sound_data.music_volume/255.0);
	}
	return sound_data.music_volume;
}

/* load a sound identified by token from a file */
static sound_t *sound_load(char* file, char* token)
{
	file_t *f;
	sound_t *e = malloc(sizeof(sound_t));
	e->file = strdup(file);
	e->token = strdup(token);
	e->data = NULL;
	e->d_len = 0;

	f = file_load(file);

#ifdef RTG_HAVE_OGG
	if (sound_is_ogg(f)) {
		if (sound_load_ogg(f,e))
			goto sound_load_fail;
	}else
#endif
#ifdef RTG_HAVE_WAV
	if (sound_is_wav(f)) {
		if (sound_load_wav(f,e))
			goto sound_load_fail;
	}else
#endif
	{
		goto sound_load_fail;
	}

	file_free(f);

	return e;

sound_load_fail:
	file_free(f);
	free(e->file);
	free(e->token);
	if (e->data)
		free(e->data);
	free(e);

	rtprintf(CN_ERROR "failed to load sound data for file '%s'",file);

	return NULL;
}

/* load a sound effect identifed by token */
int sound_load_effect(char* file, char* token)
{
	sound_t *e = sound_load(file,token);
	if (!e)
		return 1;

	effects = list_push(&effects,e);
	return 0;
}

/* load music identifed by token */
int sound_load_music(char* file, char* token)
{
	sound_t *e = sound_load(file,token);
	if (!e)
		return 1;
	music = list_push(&music,e);
	return 0;
}

/* unload a sound effect */
void sound_free_effect(char* token)
{
	sound_t *e = effects;
	if (!effects)
		return;

	while (e) {
		if (!strcmp(e->token,token))
			break;
		e = e->next;
	}

	if (!e)
		return;

	effects = list_remove(&effects,e);

	alDeleteBuffers(1,&e->id);

	free(e->data);
	free(e->file);
	free(e->token);
	free(e);
}

/* unload a music */
void sound_free_music(char* token)
{
	sound_t *e = music;
	if (!music)
		return;

	while (e) {
		if (!strcmp(e->token,token))
			break;
		e = e->next;
	}

	if (!e)
		return;

	music = list_remove(&music,e);

	alDeleteBuffers(1,&e->id);

	free(e->data);
	free(e->file);
	free(e->token);
	free(e);
}

/* play sound effect */
int sound_play_effect(char* token)
{
	sound_instance_t *i;
	sound_t *e = effects;
	if (!sound_data.enabled || !sound_data.init || !effects)
		return 1;

	while (e) {
		if (!strcmp(e->token,token))
			break;
		e = e->next;
	}

	if (!e)
		return 1;

	i = malloc(sizeof(sound_instance_t));

	alGenSources(1, &i->id);
	alSourcei(i->id, AL_BUFFER, e->id);
	alSourcei(i->id, AL_SOURCE_RELATIVE, AL_TRUE);
	alSource3f(i->id, AL_POSITION, 0, 0, 0);
	alSource3f(i->id, AL_VELOCITY, 0, 0, 0);
	alSourcei(i->id, AL_LOOPING, AL_FALSE);
	alSourcef(i->id, AL_GAIN, (float)sound_data.effect_volume/255.0);
	alSourcePlay(i->id);

	if (alGetError() != AL_NO_ERROR) {
		alDeleteSources(1, &i->id);
		free(i);
		return 1;
	}

	playing = list_push(&playing,i);

	return 0;
}

/* play music */
int sound_play_music(char* token)
{
	sound_t *e = music;
	if (!sound_data.enabled || !sound_data.init || !music)
		return 1;

	while (e) {
		if (!strcmp(e->token,token))
			break;
		e = e->next;
	}

	if (!e)
		return 1;

	if (mplaying.id) {
		alSourceStop(mplaying.id);
		alDeleteSources(1, &mplaying.id);
		mplaying.id = 0;
	}

	alGenSources(1, &mplaying.id);
	alSourcei(mplaying.id, AL_BUFFER, e->id);
	alSourcei(mplaying.id, AL_SOURCE_RELATIVE, AL_TRUE);
	alSource3f(mplaying.id, AL_POSITION, 0, 0, 0);
	alSource3f(mplaying.id, AL_VELOCITY, 0, 0, 0);
	alSourcei(mplaying.id, AL_LOOPING, AL_TRUE);
	alSourcef(mplaying.id, AL_GAIN, (float)sound_data.music_volume/255.0);
	alSourcePlay(mplaying.id);

	if (alGetError() != AL_NO_ERROR) {
		alDeleteSources(1, &mplaying.id);
		mplaying.id = 0;
		return 1;
	}

	return 0;
}

/* stop sound effects, optionally fading out in fade seconds */
void sound_stop_effects(int fade)
{
	if (fade) {
		sound_data.effect_fade = fade;
		sound_data.effect_fadev = (float)sound_data.effect_volume/255.0;
	}else{
		sound_instance_t *i = playing;
		while (i) {
			playing = i->next;
			alSourceStop(i->id);
			alDeleteSources(1, &i->id);
			free(i);
			i = playing;
		}
		playing = NULL;
		sound_data.effect_fade = -1.0;
	}
}

/* stop music, optionally fading out in fade seconds */
void sound_stop_music(int fade)
{
	if (fade) {
		sound_data.music_fade = fade;
		sound_data.music_fadev = (float)sound_data.music_volume/255.0;
	}else if (mplaying.id) {
		alSourceStop(mplaying.id);
		alDeleteSources(1, &mplaying.id);
		mplaying.id = 0;
		sound_data.music_fade = -1.0;
	}
}

/* stop all sounds, optionally fading out in fade seconds */
void sound_stop(int fade)
{
	sound_stop_effects(fade);
	sound_stop_music(fade);
}

/* command setter for sound state */
void sound_setter(char* value)
{
	int v = strtol(value,NULL,10);
	sound_state(v);
}

/* command setter for sound effects volume */
void sound_effects_setter(char* value)
{
	int v = strtol(value,NULL,10);
	sound_volume_effects(v);
}

/* command setter for music volume */
void sound_music_setter(char* value)
{
	int v = strtol(value,NULL,10);
	sound_volume_music(v);
}
