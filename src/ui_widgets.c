/************************************************************************
* ui_widgets.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

#include <stdarg.h>

widget_t *containers = NULL;
static int next_widget_id = 1;

/* create a container */
widget_t *ui_widget_container()
{
	widget_t *w = containers;

	while (w) {
		if (!w->style.visible && !w->child && !w->x)
			return w;
		w = w->next;
	}

	w = ui_widget_create(UI_WIDGET_CONTAINER,NULL);

	return w;
}

/* create a new widget */
widget_t *ui_widget_create(int type, widget_t *parent)
{
	widget_t *w;
	int i;

	if (!(type > UI_WIDGET_NULL && type < UI_WIDGET_VOID))
		return NULL;

	if ((!parent && type != UI_WIDGET_CONTAINER) || (parent && type == UI_WIDGET_CONTAINER))
		return NULL;

	w = malloc(sizeof(widget_t));

	w->parent = parent;
	w->type = type;
	w->hover = 0;
	w->id = next_widget_id++;
	w->htext = NULL;
	w->child = NULL;
	w->text = NULL;
	w->events = NULL;
	w->data = NULL;
	w->x = NULL;

	w->events = malloc(sizeof(widget_events_t));
	w->events->kdown = NULL;
	w->events->kup = NULL;
	w->events->mclick = NULL;
	w->events->mdown = NULL;
	w->events->mmove = NULL;
	w->events->mup = NULL;
	w->events->sdown = NULL;
	w->events->sup = NULL;
	w->events->show = NULL;
	w->events->hide = NULL;

	ui_style_defaults(w);

	if (type == UI_WIDGET_PBAR) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->pbr.blind = 0;
		w->data->pbr.max = 100;
		w->data->pbr.value = 50;
		w->data->pbr.colour.r = 200;
		w->data->pbr.colour.g = 0;
		w->data->pbr.colour.b = 0;
		w->data->pbr.colour.a = 255;
	}else if (type == UI_WIDGET_CONSOLE) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->con.lines = NULL;
		w->data->con.command[0] = 0;
		w->data->con.cchar = 0;
	}else if (type == UI_WIDGET_NUM) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->num.value = 0;
		w->data->num.min = 0;
		w->data->num.max = 0;
	}else if (type == UI_WIDGET_CHECK) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->chk.checked = 0;
	}else if (type == UI_WIDGET_HSLIDE) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->sld.min = 0;
		w->data->sld.max = 100;
		w->data->sld.value = 50;
		w->data->sld.sld = NULL;
	}else if (type == UI_WIDGET_LIST) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->lst.col_cnt = 0;
		w->data->lst.rows = 0;
		w->data->lst.offset = 0;
		w->data->lst.columns = NULL;
		w->data->lst.click = NULL;
	}else if (type == UI_WIDGET_LISTROW) {
		w->data = malloc(sizeof(widget_data_t));
		w->data->row.values = malloc(sizeof(char*)*parent->data->lst.col_cnt);
		for (i=0; i<parent->data->lst.col_cnt; i++) {
			w->data->row.values[i] = NULL;
		}
		parent->data->lst.rows++;
	}

	ui_script_events(w);

	if (parent) {
		parent->child = list_push(&parent->child,w);
	}else{
		containers = list_push(&containers,w);
	}

	return w;
}

/* find a widget by it's numeric id */
widget_t *ui_widget_find(int id)
{
	widget_t *c = containers;
	widget_t *w;

	while (c) {
		if (c->id == id)
			return c;

		w = c->child;
		while (w) {
			if (w->id == id)
				return w;
			w = w->next;
		}
		c = c->next;
	}

	return NULL;
}

/* free memory of a widget */
void ui_widget_free(widget_t *w)
{
	if (!w)
		return;

	if (w->type != UI_WIDGET_CONTAINER) {
		if (w->events)
			free(w->events);

		if (w->text)
			free(w->text);

		if (w->style.bg)
			free(w->style.bg);
	}

	if (w->type == UI_WIDGET_CONTAINER || w->type == UI_WIDGET_LIST) {
		widget_t *wh = w->child;
		while (w->child) {
			wh = w->child->next;
			ui_widget_free(w->child);
			w->child = wh;
		}
		if (w->type == UI_WIDGET_CONTAINER) {
			w->style.visible = 0;
			return;
		}
	}

	if (w->data) {
		if (w->type == UI_WIDGET_CONSOLE) {
			console_line_t *l;
			while ((l = list_pull(&w->data->con.lines))) {
				free(l);
			}
		}else if (w->type == UI_WIDGET_LISTROW) {
			int i;
			for (i=0; i<w->parent->data->lst.col_cnt; i++) {
				if (w->data->row.values[i])
					free(w->data->row.values[i]);
			}
			free(w->data->row.values);
		}else if (w->type == UI_WIDGET_LIST) {
			int i;
			for (i=0; i<w->data->lst.col_cnt; i++) {
				if (w->data->lst.columns[i].name)
					free(w->data->lst.columns[i].name);
			}
			free(w->data->lst.columns);
		}
		free(w->data);
	}

	if (focused_widget && w->id == focused_widget->id)
		focused_widget = NULL;

	free(w);
}

/* set/get a widget's text value */
char* ui_widget_value(widget_t* w, char* v, ...)
{
	char d[2048];
	va_list ap;

	if (!w)
		return NULL;

	if (v) {
		va_start(ap,v);
		if (!strchr(v,'%')) {
			strcpy(d,v);
		}else if (!strcmp(v,"%d")) {
			int i = va_arg(ap,int);
			sprintf(d,"%d",i);
		}else{
			vsnprintf(d,2048,v,ap);
		}
		va_end(ap);
	}

	if (
		w->type == UI_WIDGET_LABEL
		|| w->type == UI_WIDGET_BUTTON
		|| w->type == UI_WIDGET_TEXT
		|| w->type == UI_WIDGET_TEXTBOX
		|| w->type == UI_WIDGET_HEADER
	) {
		if (v) {
			if (w->text)
				free(w->text);

			w->text = strdup(d);
		}
		if (!w->text) {
			w->text = malloc(1);
			w->text[0] = 0;
		}
		return w->text;
	}else if (w->type == UI_WIDGET_NUM) {
		if (v)
			w->data->num.value = strtol(d,NULL,10);
		if (w->data->num.min || w->data->num.max) {
			if ((w->data->num.min || w->data->num.min < w->data->num.max) && w->data->num.min > w->data->num.value)
				w->data->num.value = w->data->num.min;
			if ((w->data->num.max || w->data->num.min < w->data->num.max) && w->data->num.max < w->data->num.value)
				w->data->num.value = w->data->num.max;
		}
		if (!w->text)
			w->text = malloc(20);
		sprintf(w->text,"%d",w->data->num.value);
		return w->text;
	}else if (w->type == UI_WIDGET_HSLIDE) {
		if (v)
			w->data->sld.value = strtol(d,NULL,10);
		if (w->data->sld.min || w->data->sld.max) {
			if ((w->data->sld.min || w->data->sld.min < w->data->sld.max) && w->data->sld.min > w->data->sld.value)
				w->data->sld.value = w->data->sld.min;
			if ((w->data->sld.max || w->data->sld.min < w->data->sld.max) && w->data->sld.max < w->data->sld.value)
				w->data->sld.value = w->data->sld.max;
		}
		if (!w->text)
			w->text = malloc(20);
		sprintf(w->text,"%d",w->data->sld.value);
		return w->text;
	}else if (w->type == UI_WIDGET_PBAR) {
		if (v)
			w->data->pbr.value = strtol(d,NULL,10);
		if (w->data->pbr.max && w->data->pbr.max < w->data->pbr.value)
			w->data->pbr.value = w->data->pbr.max;
		if (!w->text)
			w->text = malloc(20);
		sprintf(w->text,"%d",w->data->pbr.value);
		return w->text;
	}else if (w->type == UI_WIDGET_CHECK) {
		if (v) {
			if (!d[0] || d[0] == '0') {
				w->data->chk.checked = 0;
			}else{
				w->data->chk.checked = 1;
			}
		}
		if (!w->text)
			w->text = malloc(20);
		sprintf(w->text,"%d",w->data->chk.checked);
		return w->text;
	}else if (w->type == UI_WIDGET_CONSOLE) {
		return w->data->con.command;
	}
	return NULL;
}

/* set a widget value */
void ui_widget_set_value(char* sel, char* val, ...)
{
	widget_t *w;
	char d[100];
	va_list ap;
	array_t *a = style_select(sel);
	if (!a)
		return;

	va_start(ap,val);
	vsnprintf(d,100,val,ap);
	va_end(ap);

	while ((w = array_pop_ptr(a))) {
		ui_widget_value(w,d);
	}

	array_free(a);
}

/* get a widget value */
char* ui_widget_get_value(char* sel)
{
	widget_t *w;
	char* v;
	array_t *a = style_select(sel);
	if (!a)
		return NULL;

	if (!a->length)
		return NULL;

	w = array_get_ptr(a,0);

	v = ui_widget_value(w,NULL);

	array_free(a);

	return v;
}

/* set a widget's tool tip (hover text) */
void ui_widget_tooltip(widget_t *w, char* str, ...)
{
	char d[100];
	va_list ap;

	if (w->htext)
		free(w->htext);

	w->htext = NULL;

	if (str) {
		va_start(ap,str);
		vsnprintf(d,100,str,ap);
		va_end(ap);
		w->htext = strdup(d);
	}
}

/* set the background surface for an image */
void ui_widget_setbg(widget_t *w, char* file)
{

	material_t *m = mat_from_image(file);
	if (!m)
		return;

	if (!w->style.bg)
		w->style.bg = malloc(sizeof(sprite_t));

	w->style.bg->x = 0;
	w->style.bg->y = 0;
	w->style.bg->w = m->tex->w;
	w->style.bg->h = m->tex->h ;
	w->style.bg->mat = m;

	w->style.h = w->style.bg->h ;
	w->style.w = w->style.bg->w;
	if (w->type == UI_WIDGET_CHECK)
		w->style.w /= 2;
}

/* set the background surface for an image */
void ui_widget_setbg_sprite(widget_t *wh, material_t *mat, int x, int y, int w, int h)
{
	if (!wh->style.bg)
		wh->style.bg = malloc(sizeof(sprite_t));

	wh->style.bg->x = x;
	wh->style.bg->y = y;
	wh->style.bg->w = w;
	wh->style.bg->h = h;
	wh->style.bg->mat = mat;

	wh->style.h = wh->style.bg->h;
	wh->style.w = wh->style.bg->w;
	if (wh->type == UI_WIDGET_CHECK)
		wh->style.w /= 2;
}

/* draw a widget, and it's child widgets */
void ui_widget_draw(widget_t *w)
{
	int x;
	int y;
	char* t;
	rect_t dest = {0,0,0,0};

	if (!w->style.visible)
		return;

	/* get the position */
	x = ui_widget_position_x(w);
	y = ui_widget_position_y(w);

	if (x >= wm_res.width || y >= wm_res.height || (x+w->style.w) <= 0 || (y+w->style.h ) <= 0)
		return;

	dest.x = x;
	dest.y = y;

	/* power bar, draw the actual powerbar part */
	if (w->type == UI_WIDGET_PBAR) {
		float wi = (float)w->data->pbr.value/(float)w->data->pbr.max;
		dest.w = (int)(wi*w->style.w);
		dest.h = (w->style.h -4);
		dest.x += 2;
		dest.y += 2;
		draw2d_rect(&w->data->pbr.colour,dest.x,dest.y,dest.w,dest.h);
		dest.x = x;
		dest.y = y;
	}

	/* draw a border if it has one */
	if (w->style.has_border) {
		draw2d_line(&w->style.border,dest.x-1,dest.y,dest.x+w->style.w,dest.y);
		draw2d_line(&w->style.border,dest.x,dest.y-1,dest.x,dest.y+w->style.h );
		draw2d_line(&w->style.border,dest.x+w->style.w+1,dest.y,dest.x+w->style.w+1,dest.y+w->style.h+1 );
		draw2d_line(&w->style.border,dest.x-1,dest.y+w->style.h+1 ,dest.x+w->style.w,dest.y+w->style.h+1 );
	}

	/* draw the background, but not for special case checkbox */
	if (w->style.bg) {
		draw2d_sprite(w->style.bg,x,y,w->style.w,w->style.h);
	/* special cases for no bg */
	}else if (w->type == UI_WIDGET_HSLIDE) {
		dest.x = x;
		dest.y = y+(w->style.h /2);
		dest.w = w->style.w;
		dest.h = 1;
		draw2d_line(&w->style.border,dest.x,dest.y,dest.x+w->style.w,dest.y);
	}else if (w->type == UI_WIDGET_CHECK) {
		if (w->data->chk.checked) {
			print2d(x,y+((w->style.h /2)-6),w->style.font_face,SML_FONT,"yes");
		}else{
			print2d(x,y+((w->style.h /2)-6),w->style.font_face,SML_FONT,"no");
		}
	}

	/* draw text */
	if (
		(t = ui_widget_value(w,NULL))
		&& (
			w->type != UI_WIDGET_PBAR
			|| !w->data->pbr.blind
		)
		&& w->type != UI_WIDGET_CHECK
		&& w->type != UI_WIDGET_CONSOLE
		&& w->type != UI_WIDGET_HSLIDE
		&& w->type != UI_WIDGET_TEXTBOX
	) {
		int tx = x;
		int ty = y+((w->style.h /2)-(w->style.font_size/2));
		color_t *c = &w->style.color;
		int l = print_length(w->style.font_face,w->style.font_size,t);
		if (w->style.text_align == UI_ALIGN_CENTRE) {
			tx = (x+(w->style.w/2))-(l/2);
		}else if (w->style.text_align == UI_ALIGN_RIGHT) {
			tx += (w->style.w-l);
		}

		render_color_push(c);

		if (
			focused_widget
			&& focused_widget->id == w->id
			&& (
				w->type == UI_WIDGET_TEXT
				|| w->type == UI_WIDGET_NUM
			) && (time_ticks()/500)%2) {
			printf2d(tx,ty,w->style.font_face,w->style.font_size,"%s_",t);
		}else{
			print2d(tx,ty,w->style.font_face,w->style.font_size,t);
		}

		render_color_pop();
	}

	if (w->type == UI_WIDGET_HSLIDE) {
		dest.x = x+(((float)w->style.w/(w->data->sld.max-w->data->sld.min))*(float)w->data->sld.value);
		dest.y = y;
		if (w->data->sld.sld) {
			dest.y = (y+(w->style.h /2))-(w->data->sld.sld->h /2);
			dest.x -= (w->data->sld.sld->w/2);
			draw2d_sprite(w->data->sld.sld,dest.x,dest.y,NTV_SIZE,NTV_SIZE);
		}else{
			dest.h = w->style.h ;
			dest.w = 1;
			draw2d_line(&w->style.color,dest.x,dest.y,dest.x,dest.y+w->style.h );
		}
		dest.x = x;
		dest.y = y;
	/* container: draw children */
	}else if (w->type == UI_WIDGET_CONTAINER) {
		widget_t *cw;
		cw = w->child;
		while (cw) {
			ui_widget_draw(cw);
			cw = cw->next;
		}
	/* list: draw children */
	}else if (w->type == UI_WIDGET_LIST) {
		int dx;
		int wx;
		int i;
		int k;
		widget_t *n;
		dx = dest.x;
		w->data->lst.y = w->style.font_size;
		n = w->child;
		render_color_push(&w->style.color);
		for (i=0; i<w->data->lst.col_cnt; i++) {
			if (w->data->lst.columns[i].name) {
				for (
					wx=5, k=0;
					wx < w->data->lst.columns[i].width
					&& w->data->lst.columns[i].name[k];
					wx+=(w->style.font_size/2),k++
				) {
					print_char2d(
						dx+wx,
						dest.y,
						w->style.font_face,
						w->style.font_size,
						w->data->lst.columns[i].name[k]
					);
				}
			}
			dx += w->data->lst.columns[i].width;
		}
		render_color_pop();
		if (w->style.has_border)
			draw2d_line(
				&w->style.border,
				dest.x,
				dest.y+w->style.font_size,
				dest.x+w->style.w,
				dest.y+w->style.font_size
			);
		for (i=0; i<w->data->lst.offset; i++) {
			if (!n)
				return;
			n = n->next;
		}
		while (w->data->lst.y < w->style.h -15 && n) {
			ui_widget_draw(n);
			n = n->next;
			w->data->lst.y += w->style.font_size;
		}
	/* list row: contents depend on parent */
	}else if (w->type == UI_WIDGET_LISTROW) {
		int dx;
		int wx;
		int i;
		int k;
		dx = dest.x;
		render_color_push(&w->style.color);
		for (i=0; i<w->parent->data->lst.col_cnt; i++) {
			if (w->data->row.values[i]) {
				for (wx=5, k=0; wx < w->parent->data->lst.columns[i].width && w->data->row.values[i][k]; wx+=(w->style.font_size/2),k++) {
					print_char2d(dx+wx,dest.y,w->style.font_face,SML_FONT,w->data->row.values[i][k]);
				}
			}
			dx += w->parent->data->lst.columns[i].width;
		}
		render_color_pop();
	/* text box: draw text, respect line breaks */
	}else if (w->type == UI_WIDGET_TEXTBOX && w->text) {
		char* ln;
		char* s;
		int l;
		int cx = x;
		int cy = y;
		int i;
		int cw;
		cw = w->style.w/(w->style.font_size/2);
		ln = alloca(cw+1);
		render_color_push(&w->style.color);

		l = strlen(w->text);
		for (i=0; i<l;) {
			while (isspace(w->text[i])) {
				i++;
			}
			strncpy(ln,w->text+i,cw+1);
			s = strrchr(ln,'\n');
			if (s) {
				*s = 0;
			}else{
				int k = i+strlen(ln);
				if (w->text[k] && w->text[k] != ' ') {
					s = strrchr(ln,' ');
					if (s)
						*s = 0;
				}
			}
			i += strlen(ln);
			if (w->style.text_align == UI_ALIGN_RIGHT) {
				cx = (x+w->style.w)-print_length(w->style.font_face,w->style.font_size,ln);
			}else if (w->style.text_align == UI_ALIGN_CENTRE) {
				cx = (x+(w->style.w/2))-(print_length(w->style.font_face,w->style.font_size,ln)/2);
			}
			print2d(cx,cy,w->style.font_face,w->style.font_size,ln);
			cy += w->style.font_size;
		}
		render_color_pop();
	/* console: insanity follows */
	}else if (w->type == UI_WIDGET_CONSOLE) {
		int cw;
		int ch;
		int o = 0;
		cw = w->style.w/(w->style.font_size/2);
		ch = w->style.h/w->style.font_size;
		/* check there's something to print */
		if (w->data->con.lines) {
			console_line_t *l;
			int m = ch-2;
			int c = 0;
			int ll;
			l = w->data->con.lines;
			while (l) {
				l = l->next;
				c++;
			}
			if (c > m) {
				while (c > m) {
					l = list_pull(&w->data->con.lines);
					if (!l)
						break;
					free(l);
					c--;
				}
			}
			l = w->data->con.lines;
			while (l) {
				ll = strlen(l->str);
				if (cw < ll)
					l->str[cw] = 0;
				print2d(x,y+o,w->style.font_face,w->style.font_size,l->str);
				o += w->style.font_size;
				l = l->next;
			}
		}
		/* make long commands wrap */
		if (w->data->con.cchar > cw+1) {
			char c = w->data->con.command[cw];
			w->data->con.command[cw] = 0;
			print2d(x,(y+w->style.h )-(w->style.font_size*2),w->style.font_face,w->style.font_size,w->data->con.command);
			w->data->con.command[cw] = c;
			print2d(x,(y+w->style.h )-w->style.font_size,w->style.font_face,w->style.font_size,w->data->con.command+cw);
			if (focused_widget && focused_widget->id == w->id && (time_ticks()/500)%2) {
				print2d(x,(y+w->style.h )-w->style.font_size,w->style.font_face,w->style.font_size,"_");
			}
		}else{
			if (focused_widget && focused_widget->id == w->id && (time_ticks()/500)%2) {
				printf2d(x,(y+w->style.h )-w->style.font_size,w->style.font_face,w->style.font_size,"%s_",w->data->con.command);
			}else{
				print2d(x,(y+w->style.h )-w->style.font_size,w->style.font_face,w->style.font_size,w->data->con.command);
			}
		}
	}
}

/* get the absolute x position of a widget */
int ui_widget_position_x(widget_t *w)
{
	int x = 0;
	if (w->style.x == UI_ALIGN_MOUSE) {
		return mouse[0];
	}else if (w->style.x == UI_ALIGN_MOUSE_CENTRE) {
		return mouse[0]-(w->style.w/2);
	}else if (w->parent) {
		x = ui_widget_position_x(w->parent);
		if (w->style.x == UI_ALIGN_CENTRE) {
			x += (w->parent->style.w/2);
			x -= (w->style.w/2);
		}else if (w->style.x == UI_ALIGN_RIGHT) {
			x += w->parent->style.w;
			x -= w->style.w;
		}else{
			x += w->style.x;
		}
		return x;
	}

	if (w->style.x == UI_ALIGN_CENTRE) {
		x = (wm_res.width-w->style.w)/2;
	}else if (w->style.x == UI_ALIGN_RIGHT) {
		x = wm_res.width-w->style.w;
	}else{
		x = w->style.x;
	}
	return x;
}

/* get the absolute y position of a widget */
int ui_widget_position_y(widget_t *w)
{
	int y = 0;
	if (w->style.y == UI_ALIGN_MOUSE) {
		return mouse[1];
	}else if (w->style.y == UI_ALIGN_MOUSE_CENTRE) {
		return mouse[1]-(w->style.h /2);
	}else if (w->parent) {
		y = ui_widget_position_y(w->parent);
		if (w->type == UI_WIDGET_LISTROW) {
			return y+w->parent->data->lst.y;
		}
		if (w->style.y == UI_ALIGN_MIDDLE) {
			y += (w->parent->style.h /2);
			y -= (w->style.h /2);
		}else if (w->style.y == UI_ALIGN_BOTTOM) {
			y += w->parent->style.h ;
			y -= w->style.h ;
		}else{
			y += w->style.y;
		}
		return y;
	}

	if (w->style.y == UI_ALIGN_MIDDLE) {
		y = (wm_res.height/2)-(w->style.h /2);
	}else if (w->style.y == UI_ALIGN_BOTTOM) {
		y = wm_res.height-w->style.h ;
	}else{
		y = w->style.y;
	}

	return y;
}

