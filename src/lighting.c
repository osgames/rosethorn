/************************************************************************
* lighting.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"


struct light_s {
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
	GLfloat mat_ambient[4];
	GLfloat mat_diffuse[4];
	GLfloat mat_specular[4];
	GLfloat mat_shininess[4];
	GLfloat mat_emission[4];
} light_data = {
	{0.5,0.5,0.5,1.0},
	{1.0,1.0,1.0,1.0},
	{1.0,1.0,1.0,1.0},
	{0.0,100.0,0.0,0.0},
};

/* initialise lighting */
void light()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_data.ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_data.diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_data.specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_data.position);

	/* GLfloat tmp[4] = {0.1,0.1,0.1,0.1}; */
	/* glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,tmp); */

	/* tmp[0] = 0.0; */
	/* tmp[1] = 0.0; */
	/* tmp[2] = 0.0; */
	/* glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,tmp); */

	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
}

/* set the light position */
void light_position(float x, float y, float z)
{
	light_data.position[0] = x;
	light_data.position[1] = y;
	light_data.position[2] = z;
}
