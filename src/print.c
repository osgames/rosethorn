/************************************************************************
* print.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

#include <stdarg.h>

extern int font_init;
extern material_t *alphabet[256];
void load_font(void);

/* printf for rosethorn! print data to game and system consoles */
void rtprintf(char* fmt,...)
{
	char type = 0;
	char buff[1024];
	va_list ap;
	va_start(ap, fmt);
	if (*fmt < ' ') {
		type = *fmt;
		fmt++;
#ifndef RTG_CFG_DEBUG
		if (type == 4) {
			va_end(ap);
			return;
		}
#endif
	}
	vsnprintf(buff, 1024, fmt, ap);
	va_end(ap);

	if (type) {
		if (type == 1) {
			printf("ERROR: ");
		}else if (type == 2) {
			printf("NOTICE: ");
		}else if (type == 3) {
			printf("DEBUG: ");
		}else if (type == 4) {
			printf("RTG: ");
		}
	}
	printf("%s\n",buff);

	if (type != 3)
		ui_widget_console_write(NULL,buff);
}

/* print formatted text to screen */
void printf2d(int x, int y, int font, int size, char* fmt, ...)
{
	char buff[1024];
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buff, 1024, fmt, ap);
	va_end(ap);

	print2d(x,y,font,size,buff);
}

/* print text to screen */
void print2d(int x, int y, int font, int size, char* str)
{
	if (!str || !str[0])
		return;
	render2d_text(x,y,font,size,str);
}

/* print a single character */
void print_char2d(int x, int y, int font, int size, char c)
{
	char ch[2] = {0,0};
	ch[0] = c;
	render2d_text(x,y,font,size,ch);
}

/* get the length of a printed string */
int print_length(int font, int size, char* fmt, ...)
{
	char buff[1024];
	int i;
	int ch;
	int r;
	int cl;
	font_t *f;
	va_list ap;

	if (!fmt)
		return 0;

	f = font_get(1);

	va_start(ap, fmt);
	vsnprintf(buff, 1024, fmt, ap);
	va_end(ap);

	cl = 0;
	r = 0;

	for (i=0; buff[i]; i++) {
		ch = buff[i];
		if (buff[i] == '\n') {
			if (r > cl)
				cl = r;
			r = 0;
		}else if (buff[i] > -1 && buff[i] < 127) {
			r += (((float)f->widths[(int)ch]/30.0)*size);
		}
	}

	if (r > cl)
		cl = r;

	return cl;
}

/* get the length of a printed string */
int print_height(int font, int size, char* fmt, ...)
{
	char buff[1024];
	int i;
	int r;
	va_list ap;

	if (!fmt)
		return 0;

	size += 3;

	va_start(ap, fmt);
	vsnprintf(buff, 1024, fmt, ap);
	va_end(ap);

	r = size;

	for (i=0; buff[i]; i++) {
		if (buff[i] == '\n')
			r += size;
	}

	return r;
}
