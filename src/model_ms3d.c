/************************************************************************
* model_ms3d.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2013 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_render.h"

typedef struct ms3d_header_s {
	char id[10];
	int version;
} __attribute__((packed)) ms3d_header_t;

typedef struct ms3d_vertex_s {
	unsigned char flags;
	float vertex[3];
	char bone_id;
	unsigned char ref_count;
} __attribute__((packed)) ms3d_vertex_t;

typedef struct ms3d_poly_s {
	unsigned short int flags;
	unsigned short int indices[3];
	float normals[3][3];
	float s[3];
	float t[3];
	unsigned char smoothing_group;
	unsigned char group;
} __attribute__((packed)) ms3d_poly_t;

typedef struct ms3d_material_s {
	char name[32];
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float emissive[4];
	float shininess;
	float transparency;
	unsigned char mode;
	char texture[128];
	char alphamap[128];
} __attribute__((packed)) ms3d_material_t;

typedef struct ms3d_joint_s {
	unsigned char flags;
	char name[32];
	char parent[32];
	float rotation[3];
	float translation[3];
	unsigned short int numRotationKeyframes;
	unsigned short int numTranslationKeyframes;
} __attribute__((packed)) ms3d_joint_t;

model_t *model_load_ms3d(file_t *f)
{
	model_t *mdl;
	mesh_t *m;
	material_t *mat;
	ms3d_header_t fh;
	int c;
	int i;
	int j;
	int k;
	int l;
	int n;
	v3_t v[3];
	v2_t tc;

	ms3d_vertex_t *verts;
	int vc;
	ms3d_poly_t *polys;
	int pc;
	ms3d_material_t msmat;

	file_read(f,sizeof(ms3d_header_t),&fh);

	if (strncmp(fh.id,"MS3D000000",10))
		return NULL;
	if (fh.version < 3)
		return NULL;

	mdl = model_create();

	vc = file_read_short(f);
	verts = alloca(sizeof(ms3d_vertex_t)*vc);

	for (i=0; i<vc; i++) {
		file_read(f,sizeof(ms3d_vertex_t),&verts[i]);
	}

	pc = file_read_short(f);
	polys = alloca(sizeof(ms3d_poly_t)*pc);

	for (i=0; i<pc; i++) {
		file_read(f,sizeof(ms3d_poly_t),&polys[i]);
	}

	c = file_read_short(f);
	for (i=0; i<c; i++) {
		m = mesh_create_material(NULL);
		array_push_ptr(mdl->meshes,m);
		/* skip flags and name */
		file_seek(f,34,SEEK_CUR);

		j = file_read_short(f);

		/* try and turn ms3d's mesh format into something usable */
		for (k=0; k<j; k++) {
			/* get the poly index */
			l = file_read_short(f);

			/* for each index */
			for (n=0; n<3; n++) {
				/* get the vertex */
				v[0].x = verts[polys[l].indices[n]].vertex[0];
				v[0].y = verts[polys[l].indices[n]].vertex[1];
				v[0].z = verts[polys[l].indices[n]].vertex[2];

				/* get the normal */
				v[1].x = polys[l].normals[n][0];
				v[1].y = polys[l].normals[n][1];
				v[1].z = polys[l].normals[n][2];

				tc.x = polys[l].s[n];
				tc.y = polys[l].t[n];

				/* push the poly point onto the mesh */
				mesh_push_polypoint_n(m,&v[0],&v[1],&tc);
			}
		}

		/* material index stored temporarily */
		m->option = file_read_char(f);
	}

	c = file_read_short(f);
	for (i=0; i<c; i++) {
		file_read(f,sizeof(ms3d_material_t),&msmat);
		mat = mat_from_image(msmat.texture);
		if (!mat) {
			mat = mat_from_dif3f(msmat.diffuse[0],msmat.diffuse[1],msmat.diffuse[2]);
		}else{
			mat_dif3f(mat,msmat.diffuse[0],msmat.diffuse[1],msmat.diffuse[2]);
		}
		mat_alf(mat,msmat.diffuse[3]);
		mat_amb3f(mat,msmat.ambient[0],msmat.ambient[1],msmat.ambient[2]);
		mat_spc3f(mat,msmat.specular[0],msmat.specular[1],msmat.specular[2]);
		for (k=0; k<mdl->meshes->length; k++) {
			m = array_get_ptr(mdl->meshes,k);
			if (m->option == i)
				m->mat = mat;
		}
	}
	for (k=0; k<mdl->meshes->length; k++) {
		m = array_get_ptr(mdl->meshes,k);
		m->option = 0;
	}

	return mdl;
}
