/************************************************************************
* events_w32.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_ui.h"

#ifdef WIN32
/* oh god w32, wtf */
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	short buff[3];
	BYTE ks[256];
	switch (uMsg)
	{
		case WM_ACTIVATE:
			return 0;
		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
			case SC_SCREENSAVE:
			case SC_MONITORPOWER:
				return 0;
			}
			break;
		}
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;
		case WM_KEYDOWN:
		{
			current_event.type = RTE_KEY_DOWN;
			current_event.sym = wParam;
			if (current_event.sym == RTK_Shift_L || current_event.sym == RTK_Shift_R || current_event.sym == VK_SHIFT) {
				shift = 1;
			}else if (current_event.sym == RTK_Control_L || current_event.sym == RTK_Control_R || current_event.sym == VK_CONTROL) {
				ctrl = 1;
			}

			current_event.ch = 0;

			GetKeyboardState(ks);
			if (ToUnicode(wParam, MapVirtualKey(wParam,0), ks, (LPWSTR)buff, 3, 0) == 1)
				current_event.ch = buff[0];


			if (current_event.sym > 255) {
				events_key_down();
			}else if (!key[current_event.sym]) {
				key[current_event.sym] = 1;
				events_key_down();
			}
			return 0;
		}
		case WM_KEYUP:
		{
			current_event.type = RTE_KEY_UP;
			current_event.sym = wParam;
			if (current_event.sym == RTK_Shift_L || current_event.sym == RTK_Shift_R || current_event.sym == VK_SHIFT) {
				shift = 0;
			}else if (current_event.sym == RTK_Control_L || current_event.sym == RTK_Control_R || current_event.sym == VK_CONTROL) {
				ctrl = 0;
			}
			if (current_event.sym < 256) {
				key[current_event.sym] = 0;
			}
			events_key_up();
			return 0;
		}
		case WM_SIZE:
		{
			wm_res.width = LOWORD(lParam);
			wm_res.height = HIWORD(lParam);
			return 0;
		}
		case WM_LBUTTONDOWN:
		{
			button[RTM_LEFT_BUTTON] = 1;
			current_event.type = RTE_BUTTON_DOWN;
			current_event.button = RTM_LEFT_BUTTON;
			events_mouse();
			return 0;
		}
		case WM_LBUTTONUP:
		{
			button[RTM_LEFT_BUTTON] = 0;
			current_event.type = RTE_BUTTON_UP;
			current_event.button = RTM_LEFT_BUTTON;
			events_mouse();
			return 0;
		}
		case WM_MBUTTONDOWN:
		{
			button[RTM_MIDDLE_BUTTON] = 1;
			current_event.type = RTE_BUTTON_DOWN;
			current_event.button = RTM_MIDDLE_BUTTON;
			events_mouse();
			return 0;
		}
		case WM_MBUTTONUP:
		{
			button[RTM_MIDDLE_BUTTON] = 0;
			current_event.type = RTE_BUTTON_UP;
			current_event.button = RTM_MIDDLE_BUTTON;
			events_mouse();
			return 0;
		}
		case WM_RBUTTONDOWN:
		{
			button[RTM_RIGHT_BUTTON] = 1;
			current_event.type = RTE_BUTTON_DOWN;
			current_event.button = RTM_RIGHT_BUTTON;
			events_mouse();
			return 0;
		}
		case WM_RBUTTONUP:
		{
			button[RTM_RIGHT_BUTTON] = 0;
			current_event.type = RTE_BUTTON_UP;
			current_event.button = RTM_RIGHT_BUTTON;
			events_mouse();
			return 0;
		}
		case WM_MOUSEMOVE:
		{
			current_event.type = RTE_MOUSE_MOTION;
			current_event.x = LOWORD(lParam);
			current_event.y = HIWORD(lParam);
			rmouse[0] = mouse[0]-current_event.x;
			rmouse[1] = mouse[1]-current_event.y;
			mouse[0] = current_event.x;
			mouse[1] = current_event.y;
			events_mouse();
			return 0;
		}
		case WM_MOUSEWHEEL:
		{
			current_event.type = RTE_BUTTON_DOWN;
			if (wParam == VK_XBUTTON1) {
				current_event.button = RTM_SCROLL_UP;
				events_mouse();
			}else if (wParam == VK_XBUTTON2) {
				current_event.button = RTM_SCROLL_DOWN;
				events_mouse();
			}
			return 0;
		}
	}

	/* pass unhandled messages to DefWindowProc */
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}


/* read and act on events */
void events_main()
{
	MSG msg;
	while (PeekMessage(&msg,NULL,0,0,PM_REMOVE)) {
		if (msg.message == WM_QUIT) {
			rtg_state = RTG_STATE_EXIT;
		}else{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	if (rtg_state >= RTG_STATE_PLAY && !focused_widget) {
		event_action_t *e = events;
		while (e) {
			if (event_active(e->name)) {
				if (e->la_func[0]) {
					script_event(e->la_func[1]);
				}else if (e->a_func) {
					e->a_func();
				}
			}
			e = e->next;
		}
	}
}
#endif
