/************************************************************************
* net.c
* rtg - rosethorn game engine
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "rtg.h"
#include "rtg_net.h"

#include <stdarg.h>

static net_connection_t *net_connections = NULL;
static int net_ids = 0;
#ifdef WIN32
static int w32_sock_startup = 0;
WSADATA wsaData;
#endif

static struct {
	int (*reconnect)(net_connection_t *n);
	int (*write)(net_connection_t *n, void *buff, unsigned int size);
	int (*read)(net_connection_t *n, void *buff, unsigned int size);
	int (*readline)(net_connection_t *n, void *buf, unsigned int size);
	int (*broadcast)(net_connection_t *n, void *buff, unsigned int size);
	int (*accept)(net_connection_t *n);
} net_funcs[4] = {
	{
		tcp_client_reconnect,
		tcp_write,
		tcp_read,
		tcp_readline,
		NULL,
		NULL
	},
	{
		udp_client_reconnect,
		udp_write,
		udp_read,
		udp_readline,
		NULL,
		NULL
	},
	{
		tcp_host_reconnect,
		tcp_write,
		tcp_read,
		tcp_readline,
		tcp_broadcast,
		tcp_accept
	},
	{
		udp_host_reconnect,
		udp_write,
		udp_read,
		udp_readline,
		udp_broadcast,
		udp_accept
	}
};

/* create a new net connection */
net_connection_t *net_connection()
{
	net_connection_t *n = malloc(sizeof(net_connection_t));

	n->state = RTG_NET_UNUSED;
	n->tries = 0;
	n->type = RTG_NET_UNUSED;
	n->host = NULL;
	n->port = NULL;
	n->id = net_ids++;
	n->buff_start = 0;
	n->buff_end = 0;
	n->peers = NULL;

#ifdef WIN32
	if (!w32_sock_startup) {
		WSAStartup(MAKEWORD(2, 2), &wsaData);
		w32_sock_startup = 1;
	}
#endif

	net_connections = list_push((void**)&net_connections,n);

	return n;
}

/* find a net connection */
net_connection_t *net_fetch(int id)
{
	net_connection_t *n = net_connections;

	while (n) {
		if (n->id == id)
			break;
		n = n->next;
	}

	return n;
}

/* get the state of a net connection */
int net_state(net_connection_t *n)
{
#ifndef WIN32
	int fopts;
	if (!n)
		return RTG_NET_CLOSED;
	if (n->state == RTG_NET_OPEN) {
		if (fcntl(n->fd, F_GETFL, &fopts) < 0) {
			close(n->fd);
#else
	/* windows 'almost the same, but not quite' urgh */
	unsigned long fopts;
	if (!n)
		return RTG_NET_CLOSED;
	if (n->state == RTG_NET_OPEN) {
		if (ioctlsocket(n->fd, FIONREAD, &fopts) < 0) {
			closesocket(n->fd);
#endif
			n->state = RTG_NET_CLOSED;
		}
	}
	return n->state;
}

/* close a net connection */
void net_close(net_connection_t *n)
{
	if (!n)
		return;

	if (net_state(n) == RTG_NET_OPEN)
		shutdown(n->fd,2);
#ifndef WIN32
	close(n->fd);
#else
	closesocket(n->fd);
#endif

	list_remove((void**)&net_connections,n);
	freeaddrinfo(n->addr);
	if (n->host)
		free(n->host);
	if (n->port)
		free(n->port);
	if (n->peers) {
		net_connection_t *p;
		while ((p = array_pop_ptr(n->peers))) {
			net_close(p);
		}
		array_free(n->peers);
	}
	free(n);
}

/* disconnect a net connection */
void net_disconnect(net_connection_t *n)
{
	if (net_state(n) == RTG_NET_OPEN)
		shutdown(n->fd,2);
#ifndef WIN32
	close(n->fd);
#else
	closesocket(n->fd);
#endif
	if (n->peers) {
		net_connection_t *p;
		while ((p = array_pop_ptr(n->peers))) {
			net_close(p);
		}
		array_free(n->peers);
		n->peers = NULL;
	}

	n->state = RTG_NET_CLOSED;
}

/* write data to a net connection */
int net_write(net_connection_t *n, void *buff, unsigned int size)
{
	return net_funcs[n->type].write(n,buff,size);
}

/* write a string to a net connection */
int net_write_string(net_connection_t *n, char* fmt, ...)
{
	char buff[1024];
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buff, 1024, fmt, ap);
	va_end(ap);

	return net_funcs[n->type].write(n,buff,strlen(buff));
}

/* read data from a net connection */
int net_read(net_connection_t *n, void *buff, unsigned int size)
{
	return net_funcs[n->type].read(n,buff,size);
}

/* read a line from a net connection */
int net_readline(net_connection_t *n, void *buff, unsigned int size)
{
	return net_funcs[n->type].readline(n,buff,size);
}

/* broadcast data to all clients of a net connection */
int net_broadcast(net_connection_t *n, void *buff, unsigned int size)
{
	if (net_funcs[n->type].broadcast)
		return net_funcs[n->type].broadcast(n,buff,size);
	return 0;
}

/* accept new connection from a net connection */
int net_accept(net_connection_t *n)
{
	if (net_funcs[n->type].accept)
		return net_funcs[n->type].accept(n);
	return -1;
}

/* wait for data on a net connection */
array_t *net_select(unsigned int msec, int reconnect, net_connection_t *n)
{
	array_t *r;
	array_t *a = array_create(RTG_TYPE_PTR);

	array_push_ptr(a,n);

	r = net_select_array(msec,reconnect,a);

	array_free(a);

	return r;
}

/* wait for data on an array of net connections */
array_t *net_select_array(unsigned int msec, int reconnect, array_t *connections)
{
	fd_set rfds;
	struct timeval tv;
	int m = 0;
	int cont = 1;
	int i;
	int s;
	int st;
	int rl;
	unsigned int usec = 1000;
	array_t *a = array_copy(connections);
	net_connection_t **n = a->data;
	array_t *r = array_create(RTG_TYPE_PTR);

	if (!a)
		return r;
	if (!r) {
		array_free(a);
		return r;
	}

	if (!msec)
		msec = 100;

	usec *= msec;

	tv.tv_sec = 0;
	tv.tv_usec = usec;

	for (i=0; i<a->length; i++) {
		if (n[i]->buff_end > n[i]->buff_start) {
			array_push_ptr(r,n[i]);
			n[i] = NULL;
		}
	}

	if (r->length == a->length) {
		array_free(a);
		return r;
	}

	rl = r->length;

	while (cont) {
		if (!tv.tv_usec)
			break;
		FD_ZERO(&rfds);
		m = 0;
		for (i=0; i<a->length; i++) {
			if (n[i] && net_state(n[i]) == RTG_NET_OPEN) {
				FD_SET(n[i]->fd, &rfds);
				if (n[i]->fd > m)
					m = n[i]->fd;
			}
		}
		if (!m) {
			array_free(a);
			array_free(r);
			return NULL;
		}
		m++;

		s = select(m, &rfds, NULL, NULL, &tv);
		if (s < 1) {
			if (reconnect) {
				for (i=0; i<a->length; i++) {
					if (n[i] && net_state(n[i]) == RTG_NET_CLOSED)
						net_funcs[n[i]->type].reconnect(n[i]);
				}
			}
		}else{
			for (i=0; i<a->length; i++) {
				if (!n[i])
					continue;
				st = net_state(n[i]);
				switch (st) {
				case RTG_NET_OPEN:
					if (FD_ISSET(n[i]->fd,&rfds))
						array_push_ptr(r,n[i]);
					break;
				case RTG_NET_CLOSED:
					if (reconnect)
						net_funcs[n[i]->type].reconnect(n[i]);
					break;
				default:;
				}
			}
			if (r->length > rl)
				break;
		}
	}

	array_free(a);

	return r;
}

/* wait for data from a host's peers */
array_t *net_select_peers(net_connection_t *n, unsigned int msec)
{
	int r;
	array_t *a;

	switch (n->type) {
	case RTG_NET_TCP_HOST:
		return net_select_array(msec,0,n->peers);
	case RTG_NET_UDP_HOST:
		r = udp_accept(n);
		if (r < 0)
			return NULL;
		a = array_create(RTG_TYPE_PTR);
		array_push_ptr(a,array_get_ptr(n->peers,r));
		return a;
	default:;
	}
	return NULL;
}
