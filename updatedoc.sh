#!/bin/sh
########################################################################
# updatedoc.sh
# html source code documentation generator
# Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
########################################################################

PROJECT="RoseThorn Game Library"
DESTDIR=data/docs/html
SRCDIR=src
INCDIR=inc

mkdir -p $DESTDIR/{tmp,src,inc}
mkdir -p $DESTDIR/tmp/{$SRCDIR,$INCDIR,struct}
rm -f $DESTDIR/{$SRCDIR,$INCDIR}/*

# the css
echo 'Creating base pages'
cat > $DESTDIR/style.css << EOF
body,html {width:100%; height:100%; margin:0; padding:0; background:#E1E1E1; color:#000000; font-family:sans-serif; font-size:12px;}
html {height:100%;}
h1 {margin:0; padding:20px; line-height:20px; text-align:center; background:#6F96A2; color:#FFFFFF; font-weight:bold;}
h3 {margin:20px 0; padding:10px; text-align:left; text-indent:10px; background:#6F96A2; color:#FFFFFF; font-weight:bold;}
.footer {margin:0; margin-top:10px; text-align:center; height:30px; font-size:10px; line-height:30px; background:#6F96A2; color:#FFFFFF; clear:both; position:relative; z-index:10; margin-top:-30px;}
.index h3 {width:600px;}
.footer a {color:#FFFFFF;}
.content {margin:15px; padding-bottom:30px;}
.wrapper {height:auto; min-height:100%;}
.code {margin-top:0; padding:5px; background:#9F9F9F; color:#FFFFFF;}
.source {margin-top:0; padding:5px; background:#9F9F9F; color:#FFFFFF;}
.function {color:#000000; text-decoration:none;}
.keyword {color:#FFE800; font-weight:bold;}
.bracket {color:#000000; font-weight:bold;}
.pointer {color:#000000; font-weight:bold;}
.quote {color:#0000FF;}
.comment {color:#D7D7D7;}
.comment .pointer {color:#D7D7D7; font-weight:normal;}
.ppdir {color:#FF003C;}
.about {font-weight:bold; margin:0;}
ul {list-style:none; padding:0; margin:0; overflow:hidden; height:auto; width:700px;}
ul li {float:left; overflow:hidden; height:auto;}
ul li a {height:15px; line-height:15px; float:left; width:200px; display:block;}
EOF

echo 'Searching function definitions'
# look for functions, and preceeding comments in source files
grep -B 2 '[a-zA-Z0-9_](.*)$' $SRCDIR/*.c | grep -v '\(	\|{\|}\|(C)\|at your\|#\|Free Software\|License\)' | grep $SRCDIR | sed -e 's/\/\*//' -e 's/\*\///' -e 's/-/:/' | awk '{print $2 >"'$DESTDIR'/tmp/" $1 ".html"}' FS=':'

echo 'Searching function prototypes'
# look for functions and the 'defined in X.c' comments in header files
grep -B 1 '\(defined\|(\)' $INCDIR/*.h | grep -v '\(	\|{\|}\|#\)' | grep $INCDIR | sed -e 's/\/\*//' -e 's/\*\///' -e 's/-/:/' | awk '{print $2 >"'$DESTDIR'/tmp/" $1 ".html"}' FS=':'

echo 'Generating macro index'
cat > $DESTDIR/macros.html << EOF
<html>
<head>
	<title>$PROJECT - Macros</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div class="wrapper">
	<h1>$PROJECT - Macros</h1>
<div class="content">
	<p><a href="index.html">Back to Index</a></p>
EOF

echo 'Search macros'
# look for macros in header files
grep '\(#define\)' $INCDIR/*.h | grep -v '\({\|}\)' | grep $INCDIR | sed -e 's/#define\([\t ]*\)\([A-Za-z_0-9]*\).*/\2/' | awk '{print $2}' FS=':' | grep -v '^_.*' | awk '!_[$1]++' > macros.tmp
P=""
echo 'Generating macro index html'
# and put them in a file
while IFS= read; do
	PF=`echo "${REPLY}" | awk '{print $1}' FS='_'`
	if [ "$P" = "" ]; then
		P="${PF}"
		cat >> $DESTDIR/macros.html << EOF
	<p class="code">

EOF
	elif [ ! "${PF}" = "$P" ]; then
		P="${PF}"
		cat >> $DESTDIR/macros.html << EOF
	</p>
	<p class="code">

EOF
	fi
	cat >> $DESTDIR/macros.html << EOF
		${REPLY}<br />
EOF
done < macros.tmp

rm macros.tmp

cat >> $DESTDIR/macros.html << EOF
	</p>
</div>
</div>
<div class="footer">This document is licensed under The <a target="_blank" href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a>.</div>
</body>
</html>
EOF

echo 'Searching structs'
# find the structs in headers, and put them in a temporary file
for I in `ls $INCDIR` ; do
	I=$INCDIR/$I
	LC=`wc -l $I | awk '{print $1}'`

	F=`echo $I | awk '{print $2}' FS='/' | sed -e 's/\./_/'`
	T=`grep -nm 1 'typedef struct' $I | awk '{print $1}' FS=':'`
	if [ "$T" = "" ]; then
		continue
	fi
	rm -f $DESTDIR/tmp/struct/$F.html

	while [ ! "$T" = "" ]; do
		B=`tail -n +$T $I | grep -nm 1 '}' | awk '{print $1}' FS=':'`

		A=`expr $T + $B - 1`
		head -n $A $I | tail -n $B >> $DESTDIR/tmp/struct/$F.html
		echo "#####" >> $DESTDIR/tmp/struct/$F.html

		T=`expr $T + $B`
		TT=`tail -n +$T $I | grep -nm 1 'typedef struct' | awk '{print $1}' FS=':'`
		if [ "$TT" = "" ]; then
			break;
		fi
		T=`expr $T + $TT - 1`
	done
done

echo 'Generating file index'
cat > $DESTDIR/index.html << EOF
<html>
<head>
	<title>$PROJECT - File List</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body class="index">
<div class="wrapper">
	<h1>$PROJECT</h1>
<div class="content">
	<h3>Macros</h3>
	<a href="macros.html">Macros</a>
	<h3>Header Files</h3>
	<ul>
EOF

echo 'Creating header index'
# process the header data
for I in `ls $DESTDIR/tmp/$INCDIR` ; do
	F=`echo $I | awk '{print $1 "_" $2}' FS='.'`
	OF=`echo $I | awk '{print $1 "." $2}' FS='.'`
	cat > $DESTDIR/inc_$F.html << EOF
<html>
<head>
	<title>$PROJECT - ${OF}</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div class="wrapper">
	<h1>$PROJECT - ${OF}</h1>
<div class="content">
	<p><a href="index.html">Back to Index</a></p>
	<p>Reference for <a href="inc/$F.html">$OF</a></p>
EOF
# if the file contains struct, put them at the start of the file
	SF=$DESTDIR/tmp/struct/$F.html
	if [ -f $SF ]; then
		cat >> $DESTDIR/inc_$F.html << EOF
	<h3>Structs</h3>

EOF
		echo "$I - Sorting structs"
		T=`grep -nm 1 'typedef struct' $SF | awk '{print $1}' FS=':'`
		while [ ! "$T" = "" ]; do
			B=`tail -n +$T $SF | grep -nm 1 '}' | awk '{print $1}' FS=':'`

			A=`expr $T + $B - 1`
			S=`head -n $A $SF | tail -n $B`
# a bit of syntax highlighting
			S=`echo "${S}" | sed -e 's/\([GL]*int \|char[ \*]\|void\|unsigned\|struct\|[GL]*float \|GLuint\|GLenum\|typedef\)/<span class=\"keyword\">\1<\/span>/g'`
			S=`echo "${S}" | sed -e 's/\([a-z0-9_][a-z0-9_][a-z0-9_]*\)_\([ts]\)\([ ;]\)/<span class=\"keyword\">\1_\2<\/span>\3/g'`
			S=`echo "${S}" | sed -e 's/\([)(}{]\)/<span class=\"bracket\">\1<\/span>/g'`
			S=`echo "${S}" | sed -e 's/\(#ifdef\|#ifndef\|#endif\|#else\|#elif\|#if\)/<span class=\"ppdir\">\1<\/span>/g'`
			S=`echo "${S}" | sed -e 's/;.*/;/g'`
			S=`echo "${S}" | sed -e 's/\t/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g' -e 's/\*/<span class=\"pointer\">*<\/span>/g'`
			S=`echo "$S" | sed ':a;N;$!ba;s/\n/<br \/>\n/g'`
			cat >> $DESTDIR/inc_$F.html << EOF
	<p class="code">$S</p>

EOF

			T=`expr $T + $B`
			TT=`tail -n +$T $SF | grep -nm 1 'typedef struct' | awk '{print $1}' FS=':'`
			if [ "$TT" = "" ]; then
				break;
			fi
			T=`expr $T + $TT - 1`
		done
	fi
	cat >> $DESTDIR/inc_$F.html << EOF
	<h3 id="funcs">Functions</h3>

EOF
	echo "$I - Sorting functions"
# then functions
	while IFS= read; do
		if [ "${REPLY}" = "" ]; then
			continue
		fi
		T=`echo "${REPLY}" | grep -v 'extern '`
		if [ "$T" = "" ]; then
			continue
		fi
# look for function declarations
		T=`echo "${REPLY}" | grep '[a-z0-9_](.*);$'`
		if [ "$T" = "" ]; then
			T=`echo "${REPLY}" | grep 'defined'`
# look for 'defined in X.c' comments
			if [ "$T" = "" ]; then
				cat >> $DESTDIR/inc_$F.html << EOF
	${REPLY}<br />

EOF
			else
				T=`echo "${REPLY}" | sed -e 's/defined in //'`
				SF=`echo "${T}" | sed -e 's/\./_/' -e 's/ //g'`
				cat >> $DESTDIR/inc_$F.html << EOF
	<p class="about">Defined in <a href="src_${SF}.html">${T}</a></p>

EOF
			fi
		else
# a bit of syntax highlighting
			FN=`echo "${REPLY}" | sed -e 's/.*\( \|\*\)\([a-z0-9_]*\)(.*/\2/g'`
			FT=`echo "${FN}" | grep -v ' '`
			if [ "${FT}" = "" ]; then
				continue
			fi
			T=`echo "${REPLY}" | sed -e "s/\([a-zA-Z0-9_]*(\)/<a class=\"function\" href=\"src_$SF.html#${FN}\">\1<\/a>/g"`
			T=`echo "${T}" | sed -e 's/\([GL]*int \|GLuint\|GLenum\|char[ \*]\|void\|unsigned\|struct\|[GL]*float \)/<span class=\"keyword\">\1<\/span>/g'`
			T=`echo "${T}" | sed -e 's/\([a-z0-9_]*\)_t /<span class=\"keyword\">\1_t <\/span>/g'`
			T=`echo "${T}" | sed -e 's/\([)(]\)/<span class=\"bracket\">\1<\/span>/g'`
			T=`echo "${T}" | sed -e 's/;//g' -e 's/\*/<span class=\"pointer\">*<\/span>/g'`
			cat >> $DESTDIR/inc_$F.html << EOF
	<p class="code">$T</p>

EOF
		fi
	done < $DESTDIR/tmp/$INCDIR/$I
	cat >> $DESTDIR/inc_$F.html << EOF
</div>
</div>
<div class="footer">This document is licensed under The <a target="_blank" href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a>.</div>
</body>
</html>
EOF
	cat >> $DESTDIR/index.html << EOF
	<li><a href="inc_$F.html">$OF</a></li>
EOF
done

	cat >> $DESTDIR/index.html << EOF
	</ul>
	<h3>Source Files</h3>
	<ul>
EOF

echo 'Indexing source files'
# process source files
for I in `ls $DESTDIR/tmp/$SRCDIR` ; do
	grep -B 2 '[a-z0-9_](.*)$' $DESTDIR/tmp/$SRCDIR/$I | sed -e 's/--//g' -e 's/()/(void)/g' | sed ':a;N;$!ba;s/\n \*//g' > src.tmp
	F=`echo $I | awk '{print $1 "_" $2}' FS='.'`
	OF=`echo $I | awk '{print $1 "." $2}' FS='.'`
	cat > $DESTDIR/src_$F.html << EOF
<html>
<head>
	<title>$PROJECT - ${OF}</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div class="wrapper">
	<h1>$PROJECT - ${OF}</h1>
<div class="content">
	<p><a href="index.html">Back to Index</a></p>
	<p>Reference for <a href="src/$F.html">$OF</a></p>
EOF
	echo "$I - Sorting functions"
	while IFS= read; do
		if [ "${REPLY}" = "" ]; then
			continue
		fi
# ignore static things
		T=`echo "${REPLY}" | grep -v 'static '`
		if [ "$T" = "" ]; then
			continue
		fi
# find function definitions
		T=`echo "${REPLY}" | grep '[a-z0-9_](.*)$'`
		if [ "$T" = "" ]; then
			cat >> $DESTDIR/src_$F.html << EOF
	<p class="about">${REPLY}</p>

EOF
		else
# a bit of syntax highlighting
			FN=`echo "${REPLY}" | sed -e 's/.*\( \|\*\)\([a-z0-9_]*\)(.*/\2/g'`
			FT=`echo "${FN}" | grep -v ' '`
			if [ "${FT}" = "" ]; then
				continue
			fi
			T=`echo "${REPLY}" | sed -e 's/\([a-zA-Z0-9_]*(\)/<span class=\"function\">\1<\/span>/g'`
			T=`echo "${T}" | sed -e 's/\(int \|char[ \*]\|void\|unsigned\|struct\|float \)/<span class=\"keyword\">\1<\/span>/g'`
			T=`echo "${T}" | sed -e 's/\([a-z0-9_]*\)_t /<span class=\"keyword\">\1_t <\/span>/g'`
			T=`echo "${T}" | sed -e 's/\([)(]\)/<span class=\"bracket\">\1<\/span>/g'`
			cat >> $DESTDIR/src_$F.html << EOF
	<p id="${FN}" class="code">$T</p>

EOF
		fi
	done < src.tmp
	cat >> $DESTDIR/src_$F.html << EOF
</div>
</div>
<div class="footer">This document is licensed under The <a target="_blank" href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a>.</div>
</body>
</html>
EOF
	cat >> $DESTDIR/index.html << EOF
	<li><a href="src_$F.html">$OF</a></li>
EOF
done

echo 'Generating syntax highlighting'
# now lets give all the source files a bit of syntax highlighting
for I in `ls $SRCDIR/*.c` ; do
	F=`echo $I | awk '{print $1 "_" $2}' FS='.'`
	OF=`echo $I | awk '{print $1 "." $2}' FS='.'`
	cat > $DESTDIR/$F.html << EOF
<html>
<head>
	<title>$PROJECT - ${OF} (source)</title>
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<div class="wrapper">
	<h1>$PROJECT - ${OF}</h1>
<div class="content">
	<p><a href="../index.html">Back to Index</a></p>
	<p>Full source for <a href="../src_$F.html">$OF</a></p>
	<p class="source">
EOF

	S=`cat $I | sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g' -e 's/"\([^"]*\)"/<span class="quote">"\1"<\/span>/g' -e 's/\&lt;\([a-zA-Z0-9\/\.]*\)\&gt;/<span class="quote">\&lt;\1\&gt;<\/span>/' -e 's/\/\*\(.*\)\*\//<span class="comment">\/*\1*\/<\/span>/g' -e 's/\([a-zA-Z0-9_]*(\)/<span class=\"function\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\([GL]*int \|char[ \*]\|void\|unsigned\|struct\|[GL]*float \|GLuint\|GLenum\|typedef\|static\|NULL\|extern\)/<span class=\"keyword\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\([a-z0-9_][a-z0-9_][a-z0-9_]*\)_\([ts]\)\([ ;]\)/<span class=\"keyword\">\1_\2<\/span>\3/g'`
	S=`echo "${S}" | sed -e 's/\([)(}{]\)/<span class=\"bracket\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\(#ifdef\|#ifndef\|#endif\|#else\|#elif\|#if\|#define\|#include\|#undef\)/<span class=\"ppdir\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\t/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g' -e 's/\*/<span class=\"pointer\">*<\/span>/g'`
	echo "$S" | sed ':a;N;$!ba;s/\n/<br \/>\n/g' >> $DESTDIR/$F.html
	cat >> $DESTDIR/$F.html << EOF
		</pre></p>
</div>
</div>
<div class="footer">This document is licensed under The <a target="_blank" href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a>.</div>
</body>
</html>
EOF
done

echo 'Generating header syntax highlighting'
# now lets give all the header files a bit of syntax highlighting
for I in `ls $INCDIR/*.h` ; do
	F=`echo $I | awk '{print $1 "_" $2}' FS='.'`
	OF=`echo $I | awk '{print $1 "." $2}' FS='.'`
	cat > $DESTDIR/$F.html << EOF
<html>
<head>
	<title>$PROJECT - ${OF} (source)</title>
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<div class="wrapper">
	<h1>$PROJECT - ${OF}</h1>
<div class="content">
	<p><a href="../index.html">Back to Index</a></p>
	<p>Full source for <a href="../inc_$F.html">$OF</a></p>
	<p class="source">
EOF
	S=`cat $I | sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g' -e 's/"\([^"]*\)"/<span class="quote">"\1"<\/span>/g' -e 's/\&lt;\([a-zA-Z0-9\/\.]*\)\&gt;/<span class="quote">\&lt;\1\&gt;<\/span>/' -e 's/\/\*\(.*\)\*\//<span class="comment">\/*\1*\/<\/span>/g' -e 's/\([a-zA-Z0-9_]*(\)/<span class=\"function\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\([GL]*int \|char[ \*]\|void\|unsigned\|struct\|[GL]*float \|GLuint\|GLenum\|typedef\|static\|NULL\|extern\)/<span class=\"keyword\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\([a-z0-9_][a-z0-9_][a-z0-9_]*\)_\([ts]\)\([ ;]\)/<span class=\"keyword\">\1_\2<\/span>\3/g'`
	S=`echo "${S}" | sed -e 's/\([)(}{]\)/<span class=\"bracket\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\(#ifdef\|#ifndef\|#endif\|#else\|#elif\|#if\|#define\|#include\|#undef\)/<span class=\"ppdir\">\1<\/span>/g'`
	S=`echo "${S}" | sed -e 's/\t/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g' -e 's/\*/<span class=\"pointer\">*<\/span>/g'`
	echo "$S" | sed ':a;N;$!ba;s/\n/<br \/>\n/g' >> $DESTDIR/$F.html
	cat >> $DESTDIR/$F.html << EOF
		</pre></p>
</div>
</div>
<div class="footer">This document is licensed under The <a target="_blank" href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a>.</div>
</body>
</html>
EOF
done

echo 'Cleaning up'
# clean up
rm -f src.tmp
rm -rf $DESTDIR/tmp

cat >> $DESTDIR/index.html << EOF
	</ul>
</div>
</div>
<div class="footer">This document is licensed under The <a target="_blank" href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a>.</div>
</body>
</html>
EOF

echo 'Done'
