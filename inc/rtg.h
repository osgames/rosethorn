#ifndef _RTG_H
#define _RTG_H

#include "rtg_server.h"

#ifndef WIN32
#include <X11/keysym.h>
#include "keys_x11.h"
#endif

enum {
	RTE_BUTTON_DOWN,
	RTE_BUTTON_UP,
	RTE_KEY_DOWN,
	RTE_KEY_UP,
	RTE_MOUSE_MOTION
};

#define CTRL_MOUSE 0
#define CTRL_KEYBOARD 1
#define SYM_MOTION -1

#define CUR_X 0
#define CUR_Y 1
#define PRE_X 2
#define PRE_Y 3
#define MGRAB 4

/* defined in main.c */
extern float anim_factor;

/* defined in print.c */
void printf2d(int x, int y, int font, int size, char* fmt, ...);
void print2d(int x, int y, int font, int size, char* str);
void print_char2d(int x, int y, int font, int size, char c);
int print_length(int font, int size, char* fmt, ...);
int print_height(int font, int size, char* fmt, ...);

/* defined in wm.c */
void wm_width_setter(char* value);
void wm_height_setter(char* value);
void wm_cap_setter(char* value);
void wm_fs_setter(char* value);
#ifdef RTG3D
void wm_distance_setter(char* value);
#endif
void wm_capture(char* file);

/* defined in wm_*.c */
int wm_init(void);
void wm_exit(void);
int wm_create(void);
int wm_resize(void);
int wm_update(void);
void wm_destroy(void);
void wm_toggle_fullscreen(int fs);
void wm_title(char* title);
void wm_cursor(char* file, int width, int height, int offset_x, int offset_y);
void wm_grab(void);
void wm_ungrab(void);

/* defined in events.c */
extern int events_cnt;
extern int mouse[5];
extern int rmouse[2];
extern int key[255];
extern int button[5];
extern int ctrl;
extern int shift;
extern event_t current_event;
extern event_action_t *events;
void grab_mouse(void);
void ungrab_mouse(void);
int events_init(void);
void events_exit(void);
int event_active(char* ev);
void event_set_bind(char* name, char* value);
void event_set_func(char* name, void (*func)());
void event_set_r_func(char* name, void (*func)());
void event_set_com(char* name, char* com);
void event_create(char* name, char* bind, char* com, void (*func)(), void (*r_func)(), void (*a_func)());
void event_remove(char* name);
void event_set_control(int type, int sym);
int event_key_active(char* name);
void events_mouse(void);
void events_key_down(void);
void events_key_up(void);

/* defined in events_*.c */
#ifdef WIN32
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
#endif
void events_main(void);

/* defined in kmap.c */
char* sym_to_string(int type, int sym);
int key_to_type(char* k);
int key_to_int(char* k);

/* defined in sound.c */
int sound_init(void);
void sound_exit(void);
void sound_process(void);
int sound_state(int v);
int sound_volume_effects(int v);
int sound_volume_music(int v);
int sound_load_effect(char* file, char* token);
int sound_load_music(char* file, char* token);
void sound_free_effect(char* token);
void sound_free_music(char* token);
int sound_play_effect(char* token);
int sound_play_music(char* token);
void sound_stop_effects(int fade);
void sound_stop_music(int fade);
void sound_stop(int fade);
void sound_setter(char* value);
void sound_effects_setter(char* value);
void sound_music_setter(char* value);

#endif
