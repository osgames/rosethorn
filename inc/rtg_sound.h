#ifndef _RTG_SOUND_H
#define _RTG_SOUND_H

#include "rtg.h"

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

typedef struct sound_s {
	struct sound_s *prev;
	struct sound_s *next;
	char* file;
	char* token;
	ALenum format;
	ALsizei freq;
	ALuint id;
	char* data;
	int d_len;
} sound_t;

typedef struct sound_instance_s {
	struct sound_instance_s *prev;
	struct sound_instance_s *next;
	ALuint id;
} sound_instance_t;

#define RTG_SOUND_VORBIS	0x01

/* defined in sound_ogg.c */
int sound_is_ogg(file_t *f);
int sound_load_ogg(file_t *f, sound_t *e);

/* defined in sound_wav.c */
int sound_is_wav(file_t *f);
int sound_load_wav(file_t *f, sound_t *e);

#endif
