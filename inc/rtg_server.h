#ifndef _RTG_SERVER_H
#define _RTG_SERVER_H

#define _XOPEN_SOURCE 900
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef WIN32
#include <unistd.h>
#include <alloca.h>
#include <libgen.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include <pthread.h>
#else
/* the next few lines mean we need winXP or better,
 * better being non-windows, really */
#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#endif
#define _WIN32_WINNT 0x0501
#include "keys_w32.h"
#include <windows.h>
#include <malloc.h>
#endif

#ifndef M_PI
#define M_PI	3.14159
#endif

#define CN_ERROR	"\1"
#define CN_NOTICE	"\2"
#define CN_INFO		""
#define CN_DEBUG	"\3"
#define CN_RTG		"\4"

#define PI_OVER_180	0.0174532925

#define RTG_INIT_ALL		0xFF
#define RTG_INIT_ANIM		0x01
#define RTG_INIT_UI		0x02
#define RTG_INIT_SOUND		0x04

#define RTG_STATE_EXIT		0x00
#define RTG_STATE_MENU		0x01
#define RTG_STATE_PAUSED	0x02
#define RTG_STATE_PLAY		0x0F

#define RTG_SETTER_LITERAL	0x00
#define RTG_SETTER_VFUNC	0x01

#define RTG_TYPE_NONE	0
#define RTG_TYPE_INT	1
#define RTG_TYPE_STRING	2
#define RTG_TYPE_FLOAT	3
#define RTG_TYPE_PTR	4

typedef struct array_s {
	unsigned int type;
	unsigned int length;
	unsigned int size;
	void *data;
} array_t;

typedef struct file_s {
	char *name;
	unsigned char* data;
	int len;
	int pos;
} file_t;

#include "rtgconfig.h"

typedef struct ref_s {
	struct ref_s *prev;
	struct ref_s *next;
	void *ref;
} ref_t;

typedef struct event_s {
	int type;
	int x;
	int y;
	int sym;
	int button;
	char ch;
} event_t;

typedef struct event_action_s {
	struct event_action_s *prev;
	struct event_action_s *next;
	char name[255];
	char bind[255];
	int type;
	int sym;
	void (*func)();
	void (*r_func)();
	void (*a_func)();
	char* com;
} event_action_t;

typedef struct thread_s {
	struct thread_s *prev;
	struct thread_s *next;
#ifndef WIN32
	pthread_t thread;
	pthread_attr_t attr;
#else
	HANDLE thread;
#endif
	unsigned int state;
	int exit;
	void *(*func)();
	array_t *args;
} thread_t;

typedef struct mutex_s {
	struct mutex_s *prev;
	struct mutex_s *next;
#ifndef WIN32
	pthread_mutexattr_t attr;
	pthread_mutex_t mut;
#else
	CRITICAL_SECTION mut;
#endif
} mutex_t;

typedef struct xml_attribute_s {
	struct xml_attribute_s *prev;
	struct xml_attribute_s *next;
	char* name;
	char* value;
} xml_attribute_t;

typedef struct xml_tag_s {
	struct xml_tag_s *prev;
	struct xml_tag_s *next;
	char* tag;
	xml_attribute_t *attributes;
	char* text;

	void* data;

	struct xml_tag_s *child;
	struct xml_tag_s *parent;
} xml_tag_t;

typedef struct color_s {
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
} color_t;

typedef struct v3_s {
	float x;
	float y;
	float z;
} __attribute__((packed)) v3_t;

typedef struct v2_s {
	float x;
	float y;
} __attribute__((packed)) v2_t;

/* defined in main.c */
extern int rtg_state;
extern char* rtg_game_name;
extern char* rtg_game_dir;
int rtg_init(int argc, char** argv, unsigned int flags, char* name);
void rtg_exit(void);

/* defined in print.c */
void rtprintf(char* fmt,...);

/* defined in file.c */
int file_exists(char* p);
file_t *file_load(char *file);
xml_tag_t *file_load_xml(char* file);
void file_free(file_t *file);
int file_find(file_t *file, int offset, unsigned char value);
size_t file_read(file_t *f, size_t l, void* dst);
int file_read_int(file_t *f);
unsigned int file_read_uint(file_t *f);
short int file_read_short(file_t *f);
char file_read_char(file_t *f);
float file_read_float(file_t *f);
int file_seek(file_t *f, size_t offset, int origin);
size_t file_tell(file_t *f);
void *file_get(file_t *f);
int dir_exists(char* p);
int dir_create(char* p);

/* defined in file_path.c */
void rtg_path_add(char* p);
void rtg_path_add_home(void);
void rtg_path_set(char* p);
char* file_path(char* file);
char* file_type(char* file);

/* defined in config.c */
char* config_get(char* name);
int config_get_int(char* name);
void config_set(char* name, char* value);
void config_set_int(char* name, int value);
void config_set_and_apply(char* name, char* value);
void config_set_and_apply_int(char* name, int value);
void config_load(char* file);
void config_init(int argc, char** argv);
void config_save(void);

/* defined in xml.c */
xml_tag_t *xml_parse(char* str, xml_tag_t *p);
void xml_free(xml_tag_t *t);
void xml_print(xml_tag_t *t, int s, FILE *f);
xml_tag_t *xml_tag_create(char* t, xml_tag_t *p);
char* xml_attribute(xml_tag_t *tag, char* name);
int xml_attribute_int(xml_tag_t *tag, char* name);
float xml_attribute_float(xml_tag_t *tag, char* name);
void xml_attribute_write(xml_tag_t *t, char* name, char* value);
xml_tag_t *xml_search_attribute(xml_tag_t *tag, char* name, char* value);
int xml_child_count(xml_tag_t *tag);

/* defined in cmd.c */
int cmd_add_setter(int type, char* name, void (*func)());
int cmd_apply(char* name, char* value);
void cmd_exec(char* str);
void cmd_execf(char* str, ...);

/* defined in math.c */
int math_next_pow2(int a);
float math_sqrt(float n);
int math_rand_range(int low, int high);
float math_rand_rangef(float low, float high);

/* defined in time.c */
void time_init(void);
unsigned int time_ticks(void);
void delay(unsigned int ms);
float time_scale(unsigned int last, unsigned int hz);
unsigned int interval_delay(unsigned int last, unsigned int hz);
unsigned int calc_fps(unsigned int prev, unsigned int current);

/* defined in array.c */
array_t *array_create(unsigned int type);
array_t *array_copy(array_t *a);
int array_cmp(array_t *a1, array_t *a2);
void array_free(array_t *a);
int array_push_int(array_t *a, unsigned int v);
int array_push_float(array_t *a, float v);
int array_push_string(array_t *a, char* v);
int array_push_ptr(array_t *a, void *v);
int array_push_color(array_t *a, color_t *c);
int array_push_v3t(array_t *a, v3_t *v);
int array_push_v2t(array_t *a, v2_t *v);
int array_set_int(array_t *a, unsigned int v, int i);
int array_set_float(array_t *a, float v, int i);
int array_set_string(array_t *a, char* v, int i);
int array_set_ptr(array_t *a, void* v, int i);
unsigned int array_pop_int(array_t *a);
float array_pop_float(array_t *a);
char* array_pop_string(array_t *a);
void *array_pop_ptr(array_t *a);
unsigned int array_get_int(array_t *a, int i);
float array_get_float(array_t *a, int i);
char* array_get_string(array_t *a, int i);
void *array_get_ptr(array_t *a, int i);
int array_find_int(array_t *a, unsigned int v);
int array_find_float(array_t *a, float v);
int array_find_string(array_t *a, char* v);
int array_find_ptr(array_t *a, void *v);
int array_remove_string(array_t *a, char* v);
array_t *array_split(char* str, char* s);
char* array_join(array_t *a, char* glue, int start);

/* defined in list.c */
void *list_push(void *list, void *el);
void *list_append(void *list, void *el);
void *list_last(void *list);
void *list_shove(void *list, void *el);
void *list_remove(void *list, void *el);
void *list_pop(void *list);
void *list_pull(void *list);
void *list_insert(void *list, void *el, void *n);
void *list_insert_cmp(void *list, void *el, int (*list_cmp)(void *e1, void *e2));

/* defined in thread.c */
thread_t *thread_create(void *(*func)(thread_t *t), array_t *args);
void thread_free(thread_t *t);
void thread_exit(thread_t *t, int state);
void thread_stop(thread_t *t);
int thread_wake(thread_t *t);
void thread_wait(thread_t *t);
mutex_t *mutex_create(void);
void mutex_free(mutex_t *m);
void mutex_lock(mutex_t *m);
int mutex_trylock(mutex_t *m);
void mutex_unlock(mutex_t *m);
void mutex_unlock_complete(mutex_t *m);

/* defined in crypto.c */
unsigned int hash(char* str);
unsigned int sum(char* str);
char* base64_encode(char* str);
char* base64_decode(char* str);

#endif
