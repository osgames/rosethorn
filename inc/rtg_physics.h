#ifndef _RTG_PHYSICS_H
#define _RTG_PHYSICS_H

#include "rtg_render.h"

#define EARTH_GRAVITY	9.81
#define EARTH_WEIGHT	1.0
#define EARTH_REACH	7000000.0

#define FORCE_ACTIVE	0
#define FORCE_CONSTANT	1
#define FORCE_PULSE	2

#ifndef EVENT_UNHANDLED
#define EVENT_UNHANDLED	0
#endif
#ifndef EVENT_HANDLED
#define EVENT_HANDLED	1
#endif

typedef struct gravity_s {
	float weight;
	float pull;
	float reach;
	v3_t offset;
} gravity_t;

typedef struct mass_s {
	int id;
	int isolated;
	object_t *obj;
	float mass;
	v3_t opos;
	v3_t *pos;
	v3_t velocity;
	v3_t force[3];
	int hit;
	float radius;
	gravity_t *grav;
} mass_t;

/* defined in physics.c */
extern array_t *masses;
mass_t *mass_create(v3_t *pos, float mass);
mass_t *mass_create_obj(object_t *o, float mass);
int mass_set_gravity_source(mass_t *m, float pull, float weight, float reach);
int mass_unset_gravity_source(mass_t *m);
int mass_set_gravity_offset(mass_t *m, v3_t *offset);
int mass_set_constant_force(mass_t *m, v3_t *v);
int mass_set_pulse_force(mass_t *m, v3_t *v);
int mass_add_constant_force(mass_t *m, v3_t *v);
int mass_add_pulse_force(mass_t *m, v3_t *v);
int mass_set_isolated(mass_t *m, int v);
int gravity_at(v3_t *pos, v3_t *grav);
void physics_apply(void);

/* defined in collision3d.c */
void collision_detect(void);

#endif
