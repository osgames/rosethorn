#ifndef _RTG_RENDER_H
#define _RTG_RENDER_H

#include "rtg.h"
#include "rtg_surface.h"

#ifndef WIN32
#include <X11/Xlib.h>
#include <GL/glx.h>
#include <X11/extensions/xf86vmode.h>
#endif

#define RD_NONE		0
#define RD_LINE		1
#define RD_QUAD 	2
#define RD_TEXT		3

#define AFLOAT(__a,__i) (((float*)(__a))[__i])
#define AUINT(__a,__i) (((unsigned int*)(__a))[__i])

typedef struct cursor_s {
	material_t *mat;
	int w;
	int h;
	int x;
	int y;
} cursor_t;

typedef struct res_s {
	int height;
	int width;
	int fullscreen;
	int frame_cap;
	int fps;
	int max_height;
	int max_width;
	char* title;
#ifdef RTG3D
	GLfloat distance;
#endif
#ifndef WIN32
	Display *dpy;
	int screen;
	Window win;
	GLXWindow glxwin;
	GLXContext ctx;
        GLXFBConfig fb_cfg;
	XSetWindowAttributes attr;
	int dblbuff;
	XF86VidModeModeInfo deskMode;
	XF86VidModeModeInfo **modes;
	XVisualInfo *vi;
	int mode;
	int mode_count;
#else
	HDC hDC;
	HGLRC hRC;
	HWND hWnd;
	HINSTANCE hInstance;
#endif
	cursor_t cursor;
} res_t;
extern res_t wm_res;

typedef struct font_s {
	char* token;
	GLuint textures[128];
	GLuint list_base;
	int widths[128];
} font_t;

#ifdef RTG3D

typedef struct aabb_s {
	v3_t upper;
	v3_t lower;
} aabb_t;

struct object_s;

typedef struct vbo_s {
	int state;
	GLuint colours;
	GLuint vertices;
	GLuint normals;
	GLuint texcoords;
	GLuint indices;
} vbo_t;

typedef struct mesh_s {
	material_t *mat;
	vbo_t vbo;
	color_t *col;
	array_t *c;	/* colours */
	array_t *v;	/* vertices */
	array_t *n;	/* normals */
	array_t *t;	/* texcoords */
	array_t *i;	/* indices */
	array_t *w;	/* weights */
	array_t *m;	/* weight map */
	GLenum mode;
	int option;
	float radius;
	aabb_t bounds;
} mesh_t;

typedef struct model_s {
	struct model_s *prev;
	struct model_s *next;
	char* name;
	void *data;
	array_t *meshes;
	array_t *skeletons;
	int (*step)(struct object_s *);
} model_t;

typedef struct anim_state_s {
	int skeleton;
	int frame;
	float value;
} anim_state_t;

typedef struct object_s {
	struct object_s *prev;
	struct object_s *next;
	model_t *m;
	array_t *meshes;
	int ignore;
	int drop;
	int id;
	v3_t pos;
	v3_t rot;
	v3_t scale;
	float radius;
	aabb_t bounds;
	array_t *abounds;
	anim_state_t anim;
} object_t;

typedef struct quaternion_s {
	float w;
	float x;
	float y;
	float z;
} __attribute__((packed)) quaternion_t;

typedef struct weight_s {
	int joint;
	float bias;
	v3_t pos;
} __attribute__((packed)) weight_t;

typedef struct joint_s {
	v3_t pos;
	quaternion_t rot;
} __attribute__((packed)) joint_t;

typedef struct skeleton_s {
	char* name;
	float fps;
	array_t *frames;
} __attribute__((packed)) skeleton_t;

typedef struct pre_vertex_s {
	array_t *vertex;
	GLuint vbo;
} pre_vertex_t;

typedef struct frame_s {
	array_t *joints;
	array_t *pre_vertex;
} frame_t;

typedef struct camera_s {
	float x;
	float y;
	float z;
	float yaw;
	float pitch;
} camera_t;

typedef struct shader_s {
	struct shader_s *prev;
	struct shader_s *next;
	file_t *src;
	GLuint id;
	GLuint type;
	int in_use;
} shader_t;

#endif /* RTG3D */

/* defined in render.c */
color_t *render_get_color(void);
void render_color_push(color_t *c);
void render_color_pop(void);
void render_clear(void);
void render(void);

/* defined in render2d.c */
void render2d(void);
void render2d_line(color_t *c, int x, int y, int ex, int ey);
void render2d_quad_color(color_t *c, int x, int y, int w, int h);
void render2d_quad_mat(material_t *m, int x, int y, int w, int h);
void render2d_quad_mat_vt(material_t *m, int v[8], float t[8]);
void render2d_text(int x, int y, int font, int size, char* str);

#ifdef WIN32
typedef char GLchar;
typedef size_t GLsizeiptr;
#endif

/* defined in opengl.c */
void opengl_anisotropic_setter(char* value);
void opengl_bilinear_setter(char* value);
void opengl_trilinear_setter(char* value);
void opengl_mipmap_setter(char* value);
void opengl_backfacecull_setter(int value);
void opengl_particles_setter(char* value);
void opengl_particles_max_setter(char* value);
int opengl_has_anisotropic(void);
float opengl_max_anisotropic(void);
int opengl_has_bilinear(void);
int opengl_has_trilinear(void);
int opengl_has_mipmap(void);
int opengl_has_vbo(void);
int opengl_has_backfaceculling(void);
/* check if particle effects are enabled */
int opengl_has_particles(void);
int opengl_particles_max(void);
int opengl_gen_buffers(GLsizei n, GLuint *buffers);
int opengl_delete_buffers(GLsizei n, const GLuint *buffers);
int opengl_bind_buffer(GLenum target, GLuint buffer);
int opengl_buffer_data(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
GLuint opengl_create_shader(GLenum type);
int opengl_shader_source(GLuint shader, GLsizei count, const GLchar **string, const GLint *length);
int opengl_compile_shader(GLuint shader);
GLuint opengl_create_program(void);
GLuint opengl_attach_shader(GLuint program, GLuint shader);
GLuint opengl_detach_shader(GLuint program, GLuint shader);
GLuint opengl_link_program(GLuint program);
GLuint opengl_use_program(GLuint program);
GLuint opengl_delete_program(GLuint program);

/* defined in font.c */
int font_load(char* file, char* token);
font_t *font_get(int id);
font_t *font_find(char* token);
int font_get_id(char* token);

/* defined in font_ttf.c */
int font_load_ttf(font_t *f, char* file);

/* defined in font_bitmap.c */
int font_load_bitmap(font_t *f, char* file);

#ifdef RTG3D

/* defined in render3d.c */
void render3d(void);
object_t *render3d_object_create(void);
void render3d_object_free(object_t *o);
object_t *render3d_object_find(int id);
object_t *render3d_model(model_t *mod, v3_t *pos);
object_t *render3d_mesh(mesh_t *m, v3_t *pos);

/* defined in math_quaternion.c */
quaternion_t *quat_create_quat(float x, float y, float z, float w);
quaternion_t *quat_create_euler(float x, float y, float z);
quaternion_t *quat_create_axis(v3_t *v, float angle);
void quat_init_quat(quaternion_t *q, float x, float y, float z, float w);
void quat_init_euler(quaternion_t *q, float x, float y, float z);
void quat_init_axis(quaternion_t *q, v3_t *v, float angle);
void quat_normalise(quaternion_t *q);
void quat_get_axis(quaternion_t *q, v3_t *v, float *angle);
void quat_multiply_vector(quaternion_t *q, quaternion_t *rq, v3_t *v);
void quat_multiply(quaternion_t *q1, quaternion_t *q2, quaternion_t *rq);
void quat_print(quaternion_t *q);
void quat_computew(quaternion_t *q);
void quat_rotate(quaternion_t *q, v3_t *in, v3_t *out);

/* defined in math_vector.c */
void vect_create(v3_t *start, v3_t *end, v3_t *v);
float vect_length(v3_t *v);
void vect_normalise(v3_t *v);
float vect_scalarproduct(v3_t *v1, v3_t *v2);
void vect_crossproduct(v3_t *v1, v3_t *v2, v3_t *v3);
void vect_dotproduct(v3_t *v1, v3_t *v2, v3_t *v3);
void vect_subtract(v3_t *v1, v3_t *v2, v3_t *v3);
float vect_diameter(v3_t *v);
float math_dotproduct(v3_t *v1, v3_t *v2);
float math_distance(v3_t *v1, v3_t *v2);
void mesh_normalise(mesh_t *m);
void object_normalise(object_t *o);
float object_radius(object_t *o);
int mesh_bounds(mesh_t *m);
int object_bounds(object_t *o);

/* defined in mesh.c */
mesh_t *mesh_create(GLenum mode);
mesh_t *mesh_create_material(material_t *mat);
mesh_t *mesh_create_color(color_t *c);
void mesh_free(mesh_t *m);
mesh_t *mesh_copy(mesh_t *m);
int mesh_push_point(mesh_t *m, v3_t *v1);
void mesh_push_poly(mesh_t *m, v3_t *v1, v3_t *v2, v3_t *v3);
int mesh_push_polypoint(mesh_t *m, v3_t *v, v2_t *t);
int mesh_push_polypoint_n(mesh_t *m, v3_t *v, v3_t *n, v2_t *t);
void mesh_push_quad(mesh_t *m, v3_t *v1, v3_t *v2, v3_t *v3, v3_t *v4);

/* defined in object.c */
object_t *object_create(v3_t *pos);
void object_rotate(object_t *o, float x, float y, float z);
void object_scale(object_t *o, float x, float y, float z);
void object_add_material(object_t *o, material_t *mat);
void object_add_color(object_t *o, color_t *c);
void object_add_mat_poly(object_t *o, material_t *mat, v3_t *v1, v3_t *v2, v3_t *v3);
void object_add_mat_quad(object_t *o, material_t *mat, v3_t *v1, v3_t *v2, v3_t *v3, v3_t *v4);
void object_set_skeleton(object_t *o, int skel);
void object_set_frame(object_t *o, int frame);
int object_advance_frame(object_t *o);

/* defined in model.c */
model_t *model_create(void);
void model_free(model_t *mdl);
model_t *model_load(char* file);

/* defined in model_obj.c */
model_t *model_load_obj(file_t *f);

/* defined in model_3ds.c */
model_t *model_load_3ds(file_t *f);

/* defined in model_md5.c */
model_t *model_load_md5(file_t *f);
int model_load_md5_anim(model_t *mdl, char* file);

/* defined in model_ms3d.c */
model_t *model_load_ms3d(file_t *f);

/* defined in model_rtm.c */
model_t *model_load_rtm(file_t *f);
void model_export(model_t *mdl, char* file);

/* defined in camera.c */
void camera_centre(void);
camera_t *camera_get(void);
void camera_set_pos(v3_t *p);
void camera_set_yaw(float yaw);
void camera_set_pitch(float pitch);

/* defined in lighting.c */
void light(void);
void light_position(float x, float y, float z);

/* defined in shader.c */
void shader_maybe_disable(void);
void shader_maybe_enable(void);
void shader_setter(char* value);
shader_t *shader_find(int id);
shader_t *shader_create_vertex(char* file);
shader_t *shader_create_fragment(char* file);
void shader_free(shader_t *s);
int shader_add(shader_t *s);
int shader_remove(shader_t *s);

/* defined in particles.c */
int particles_create(color_t *colour, v3_t *pos, v3_t *speed, unsigned int msec, int count, int size);

#endif /* RTG3D */

#endif /* ! _RTG_RENDER_H */
