#ifndef _RTG_UI_H
#define _RTG_UI_H

#include "rtg.h"
#include "rtg_surface.h"
#include "rtg_render.h"

#define UI_ALIGN_LEFT 0
#define UI_ALIGN_CENTRE -1
#define UI_ALIGN_RIGHT -2
#define UI_ALIGN_TOP 0
#define UI_ALIGN_MIDDLE -1
#define UI_ALIGN_BOTTOM -2
#define UI_ALIGN_MOUSE -3
#define UI_ALIGN_MOUSE_CENTRE -4

#define UI_DEFAULT -5

#ifndef EVENT_UNHANDLED
#define EVENT_UNHANDLED	0
#endif
#ifndef EVENT_HANDLED
#define EVENT_HANDLED	1
#endif

enum {
	UI_WIDGET_NULL,
	UI_WIDGET_LABEL,
	UI_WIDGET_BUTTON,
	UI_WIDGET_CONTAINER,
	UI_WIDGET_TEXT,
	UI_WIDGET_NUM,
	UI_WIDGET_CHECK,
	UI_WIDGET_HEADER,
	UI_WIDGET_CONSOLE,
	UI_WIDGET_PBAR,
	UI_WIDGET_TEXTBOX,
	UI_WIDGET_HSLIDE,
	UI_WIDGET_LIST,
	UI_WIDGET_LISTROW,
	/* add new widgets here */
	UI_WIDGET_VOID
};

struct widget_s;

typedef struct msgbox_data_s {
	struct widget_s *container;
	struct widget_s *txt;
	struct widget_s *btn1;
	struct widget_s *btn2;
	int (*btn1_func)(struct widget_s*);
	int (*btn2_func)(struct widget_s*);
} msgbox_data_t;

typedef struct listrow_data_s {
	char** values;
} listrow_data_t;

typedef struct list_column_s {
	char* name;
	int width;
} list_column_t;

typedef struct list_data_s {
	int col_cnt;
	int rows;
	int offset;
	int y;
	int (*click)(struct widget_s *);
	list_column_t *columns;
} list_data_t;

typedef struct optbox_btn_s {
	struct widget_s *btn;
	int (*func)(struct widget_s*);
	struct optbox_btn_s *next;
} optbox_btn_t;

typedef struct optbox_data_s {
	struct widget_s *container;
	struct widget_s *txt;
	optbox_btn_t *buttons;
} optbox_data_t;

typedef struct option_data_s {
	char com[100];
} option_data_t;

typedef struct check_data_s {
	int checked;
} check_data_t;

typedef struct numbox_data_s {
	int value;
	int min;
	int max;
} numbox_data_t;

typedef struct console_line_s {
	struct console_line_s *prev;
	struct console_line_s *next;
	char str[256];
} console_line_t;

typedef struct console_data_s {
	console_line_t *lines;
	char command[256];
	int cchar;
} console_data_t;

typedef struct slide_data_s {
	int value;
	int min;
	int max;
	sprite_t *sld;
} slide_data_t;

typedef struct pbar_data_s {
	int max;
	int value;
	int blind;
	color_t colour;
} pbar_data_t;

typedef union widget_data_u {
	console_data_t con;
	numbox_data_t num;
	check_data_t chk;
	pbar_data_t pbr;
	msgbox_data_t msg;
	optbox_data_t opb;
	option_data_t opt;
	slide_data_t sld;
	list_data_t lst;
	listrow_data_t row;
} widget_data_t;

typedef struct widget_events_s {
	int (*mclick)(struct widget_s *w);
	int (*mdown)(struct widget_s *w);
	int (*mup)(struct widget_s *w);
	int (*sdown)(struct widget_s *w);
	int (*sup)(struct widget_s *w);
	int (*mmove)(struct widget_s *w);
	int (*kdown)(struct widget_s *w);
	int (*kup)(struct widget_s *w);
	int (*show)(struct widget_s *w);
	int (*hide)(struct widget_s *w);
} widget_events_t;

typedef struct widget_style_s {
	int x;
	int y;
	int w;
	int h;
	int visible;
	int visibility_changed;
	sprite_t *bg;
	color_t color;
	color_t border;
	int has_border;
	int text_align;
	int font_face;
	int font_size;
	color_t ttcolor;
	color_t ttbg;
	int has_ttbg;
	color_t ttborder;
	int has_ttborder;
	int ttfont_face;
	int ttfont_size;
} widget_style_t;

typedef struct widget_s {
	struct widget_s *prev;
	struct widget_s *next;
	int type;
	int id;
	char* text;
	int hover;
	char* htext;
	struct xml_tag_s *x;
	widget_style_t style;
	widget_data_t *data;
	widget_events_t *events;
	struct widget_s *parent;
	struct widget_s *child;
} widget_t;

typedef struct style_attribute_s {
	struct style_attribute_s *prev;
	struct style_attribute_s *next;
	char* name;
	char* value;
} style_attribute_t;

typedef struct style_s {
	int x;
	int y;
	int w;
	int h;
	int visible;
	sprite_t *bg;
	int has_bg;
	color_t color;
	int has_color;
	color_t hcolor;
	int has_hcolor;
	color_t border;
	int has_border;
	int text_align;
	int font_face;
	int font_size;
	color_t ttcolor;
	int has_ttcolor;
	color_t ttbg;
	int has_ttbg;
	color_t ttborder;
	int has_ttborder;
	int ttfont_size;
	sprite_t *slide;
	int has_slide;
} style_t;

typedef struct style_rule_s {
	struct style_rule_s *prev;
	struct style_rule_s *next;
	array_t *elements;
	char* selector;
	style_t data;
	style_attribute_t *attributes;
} style_rule_t;

typedef struct ui_s {
	int init;
	xml_tag_t *elements;
	style_rule_t *styles;
} ui_t;

#define SML_FONT 12
#define LGE_FONT 18

#define UIMSG_OK 0
#define UIMSG_YN 1
#define UIMSG_OC 2

/* defined in ui.c */
extern ui_t ui;
void ui_element_setter(char* sel, char* value);
void ui_render(void);
void ui_msg(int type, char* txt, int (*func)(), ...);
void ui_opt(char* txt, ...);
void ui_set_info(int s);

/* define in ui_widgets.c */
extern widget_t *containers;
widget_t *ui_widget_container(void);
widget_t *ui_widget_create(int type, widget_t *parent);
widget_t *ui_widget_find(int id);
void ui_widget_free(widget_t *w);
char* ui_widget_value(widget_t* w, char* v, ...);
void ui_widget_set_value(char* sel, char* val, ...);
char* ui_widget_get_value(char* sel);
void ui_widget_tooltip(widget_t *w, char* str, ...);
void ui_widget_setbg(widget_t *w, char* file);
void ui_widget_setbg_sprite(widget_t *wh, material_t *s, int x, int y, int w, int h);
void ui_widget_draw(widget_t *w);
int ui_widget_position_x(widget_t *w);
int ui_widget_position_y(widget_t *w);

/* define in ui_events.c */
extern widget_t *focused_widget;
int ui_widget_event_numbox_kdown(widget_t *w);
int ui_widget_event_numbox_sdown(widget_t *w);
int ui_widget_event_numbox_sup(widget_t *w);
int ui_widget_event_text_kdown(widget_t *w);
int ui_widget_event_check_mclick(widget_t *w);
int ui_widget_event_console_kdown(widget_t *w);
int ui_widget_event_slide_mmove(widget_t *w);
int ui_widget_event_list_sdown(widget_t *w);
int ui_widget_event_list_sup(widget_t *w);
int ui_widget_event_set(
	char* sel,
	int (*mclick)(struct widget_s *w),
	int (*mdown)(struct widget_s *w),
	int (*mup)(struct widget_s *w),
	int (*sdown)(struct widget_s *w),
	int (*sup)(struct widget_s *w),
	int (*mmove)(struct widget_s *w),
	int (*kdown)(struct widget_s *w),
	int (*kup)(struct widget_s *w),
	int (*show)(struct widget_s *w),
	int (*hide)(struct widget_s *w)
);
int ui_events(void);
int ui_events_container(widget_t *c);
int ui_events_list(widget_t *w);

/* defined in ui_console.c */
void ui_widget_console_write(widget_t *w, char* str);
void ui_widget_console_set(char* sel);

/* defined in ui_list.c */
widget_t *ui_widget_list_create(widget_t *parent, int (*call_back)(widget_t *), int cols, ...);
widget_t *ui_widget_list_push(widget_t *parent, ...);
void ui_widget_list_clear(widget_t *w);

/* defined in ui_xml.c */
xml_tag_t *ui_xml_load(char* fn);

/* defined in ui_style.c */
void ui_style_defaults(widget_t *w);
void ui_style_set(char* sel, char* value);
void ui_style(char* sel, char* attr, char* value);

/* defined in ui_script.c */
void ui_script_events(widget_t *w);
int ui_script_exec(widget_t *w, char* str);

/* defined in style.c */
style_attribute_t *style_parse_rule(style_rule_t *r, char* str);
style_rule_t *style_parse(char* str);
void style_merge(style_t *src, widget_t *dest);
void style_refresh(style_rule_t *rules, xml_tag_t *tags);
int style_strtocolor(char* str, color_t *c);
int style_colortostr(char* str, color_t *c);
int style_strtoint(char* str);
int style_inttostr(char* str, int v, int h);
int style_strtobg(char* str, sprite_t **bg);
int style_bgtostr(char* str, sprite_t *bg);

/* defined in style_select.c */
array_t *style_select(char* selector);

/* defined in exports.c */
void export_function(char* name, int (*f)(widget_t *));
int export_call_ui(char* fn, widget_t *w);

/* defined in ctrl.c */
int ctrl_clear(widget_t *w);
int ctrl_create(widget_t *w, int x, int y);

extern widget_t *setter;

#endif
