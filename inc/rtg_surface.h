#ifndef _RTG_SURFACE_H
#define _RTG_SURFACE_H

#include "rtg.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

/* set a pixel colour on an image */
#define SETPX(img,x,y,clr) (((uint32_t*)img->pixels)[x+(y*img->w)] = clr)
#define SETPXI(img,i,clr) (((uint32_t*)img->pixels)[i] = clr)
/* get a pixel colour from an image */
#define GETPX(img,x,y) (((uint32_t*)img->pixels)[x+(y*img->w)])
#define GETPXI(img,i) (((uint32_t*)img->pixels)[i])

#define SGN(x) ((x<0)?-1:((x>0)?1:0))

/* convert colour struct to pixel */
#define C2P(c) (((((uint32_t)((c).a))&0x000000FF)<<24)|((((uint32_t)((c).r))&0x000000FF)<<16)|((((uint32_t)((c).g))&0x000000FF)<<8)|((((uint32_t)((c).b))&0x000000FF)))

/* get the value of one component of a pixel */
#define RED(clr) (((clr)&0x00FF0000)>>16)
#define GREEN(clr) (((clr)&0x0000FF00)>>8)
#define BLUE(clr) (((clr)&0x000000FF))
#define ALPHA(clr) (((clr)&0xFF000000)>>24)

#define RTG_MATOPT_NONE		0x00
#define RTG_MATOPT_GET		0x01
#define RTG_MATOPT_BFCULL	0x02

typedef struct image_s {
	int w;
	int h;
	unsigned char* pixels;
} image_t;

typedef struct texture_s {
	struct texture_s *prev;
	struct texture_s *next;
	GLuint tex;
	int state;
	int id;
	char name[100];
	int w;
	int h;
	GLfloat xf;
	GLfloat yf;
	image_t *px;
} texture_t;

typedef struct material_s {
	struct material_s *prev;
	struct material_s *next;
	int id;
	char name[100];
	char has_amb;
	char has_dif;
	char has_spc;
	unsigned int options;
	texture_t *tex;
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
} material_t;

typedef struct sprite_s {
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
	material_t *mat;
} sprite_t;

typedef struct rect_s {
	int x;
	int y;
	int w;
	int h;
} rect_t;

typedef struct animation_s {
	struct animation_s *prev;
	struct animation_s *next;
	material_t *mat;
	int id;
	int frames;
	int frame_w;
	int frame_h;
	float fps;
	sprite_t **frame_data;
	float speed;
	int alt;
} animation_t;

typedef struct anim_instance_s {
	struct anim_instance_s *prev;
	struct anim_instance_s *next;
	animation_t *anim;
	int id;
	char name[100];
	int u;
	int alt;
	int loop;
	float speed;
	float frame;
	float distance;
	float total;
	int bx;
	int by;
	int ex;
	int ey;
} anim_instance_t;

#define NTV_SIZE -1

/* defined in image.c */
image_t *image_load(char* file);
image_t *image_load_frommem(file_t *f);
image_t *image_load_fromscreen(int x, int y, int w, int h, int keep_alpha);
image_t *image_copy(image_t *img);
void image_clear(image_t *img);
void image_draw_rect(image_t *img, color_t *c, int width, rect_t *area);
void image_draw_line(image_t *img, color_t *c, int width, rect_t *from, rect_t *to);
void image_fill_rect(image_t *img, color_t *c, rect_t *area);
void image_copy_area(image_t *dest, image_t *src, rect_t *to, rect_t *from);
void image_blit(image_t *dest, image_t *src, rect_t *to, rect_t *from);
void image_colorise(image_t *img, color_t *c, rect_t *area);

/* defined in image_bmp.c */
int image_is_bmp(file_t *f);
int image_load_bmp(file_t *f, image_t *p);
int image_save_bmp(image_t *p, char* file);

/* defined in image_png.c */
int image_is_png(file_t *f);
int image_load_png(file_t *f, image_t *p);
int image_save_png(image_t *p, char* file);

/* defined in image_jpg.c */
int image_is_jpg(file_t *f);
int image_load_jpg(file_t *f, image_t *p);
int image_save_jpg(image_t *p, char* file);

/* defined int image_tga.c */
int image_is_tga(file_t *f);
int image_load_tga(file_t *f, image_t *p);
int image_save_tga(image_t *p, char* file);

/* defined in texture.c */
texture_t *tex_create(void);
int tex_generate(texture_t *tex);
void tex_free(texture_t *tex);
texture_t *tex_from_image(char* file);
texture_t *tex_from_pixels(image_t *px);
texture_t *tex_update_pixels(texture_t *tex, image_t *px);
texture_t *tex_from_rgba(int x, int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a);

/* defined in draw2d.c */
void draw2d_line(color_t *c, int x, int y, int ex, int ey);
void draw2d_rect(color_t *c, int x, int y, int w, int h);
void draw2d_material(material_t *m, int x, int y, int w, int h);
void draw2d_sprite(sprite_t *s, int x, int y, int w, int h);
void draw2d_sprite_angle(sprite_t *s, int x, int y, int w, int h, int angle);

/* defined in draw3d.c */
#ifdef RTG3D
int draw3d_line(color_t *c, GLfloat x, GLfloat y, GLfloat z, GLfloat ex, GLfloat ey, GLfloat ez);
int draw3d_color(color_t *c, GLfloat x, GLfloat y, GLfloat z, GLfloat ex, GLfloat ey, GLfloat ez);
int draw3d_material(material_t *m, GLfloat x, GLfloat y, GLfloat z, GLfloat ex, GLfloat ey, GLfloat ez);
int draw3d_sprite(sprite_t *s, GLfloat x, GLfloat y, GLfloat z, GLfloat ex, GLfloat ey, GLfloat ez);
#endif

/* defined in material.c */
void mat_use(material_t *mat);
material_t *mat_find_or_create(char* name);
material_t *mat_create(void);
void mat_free(material_t *mat);
material_t *mat_from_tex(texture_t *tex);
material_t *mat_from_pixels(image_t *p);
material_t *mat_update_pixels(material_t *mat, image_t *p);
material_t *mat_from_image(char* file);
material_t *mat_from_dif3f(GLfloat r, GLfloat g, GLfloat b);
material_t *mat_from_dif3ub(unsigned char r, unsigned char g, unsigned char b);
material_t *mat_from_color(color_t *c);
void mat_name(material_t *mat, char* name);
void mat_tex(material_t *mat, texture_t *tex);
void mat_tex_file(material_t *mat, char* file);
void mat_amb3f(material_t *mat, GLfloat r, GLfloat g, GLfloat b);
void mat_amb3ub(material_t *mat, unsigned char r, unsigned char g, unsigned char b);
void mat_dif3f(material_t *mat, GLfloat r, GLfloat g, GLfloat b);
void mat_dif3ub(material_t *mat, unsigned char r, unsigned char g, unsigned char b);
void mat_spc3f(material_t *mat, GLfloat r, GLfloat g, GLfloat b);
void mat_spc3ub(material_t *mat, unsigned char r, unsigned char g, unsigned char b);
void mat_alf(material_t *mat, GLfloat d);
void mat_alub(material_t *mat, unsigned char d);
unsigned int mat_options(material_t *mat, unsigned int opt);

/* defined in animation.c */
extern animation_t *animations;
extern anim_instance_t *anim_instances;
int anim_init(void);
void anim_load(char* file);
void anim_load_str(char* str);
void anim_load_xml(xml_tag_t *x);
animation_t *anim_create(char* file, int frames, int frame_h, int frame_w, int fps, int alt, float speed);
animation_t *anim_get(int id);
anim_instance_t *anim_spawn(int type, int alt, int loop, int bx, int by, int ex, int ey);
void anim_remove(anim_instance_t *a);
void anim_render(void);
void anim_draw(anim_instance_t *a, int x, int y);
void anim_get_position(anim_instance_t *a, int *x, int *y);

#endif
