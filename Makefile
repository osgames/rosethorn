CC ?= gcc
CD ?= cd
RM ?= rm
LD ?= ld
W32_CC ?= i686-pc-mingw32-gcc-4.6.3
W32_AR ?= i686-pc-mingw32-ar

TARGET=rtg
PACKAGE=rtg
GRAPHIC=2d
VERSION=0.1.2
ARCH=POSIX

PWD=$(shell pwd)
PREFIX ?= /usr
BINDIR ?= $(PREFIX)/games/bin
DESTDIR ?= $(PREFIX)/games/$(PACKAGE)
DATADIR ?= $(PREFIX)/games
LIBDIR ?= $(PREFIX)/lib
INCDIR ?= $(PREFIX)/include/$(PACKAGE)

EXT ?=

DEP_LIBS=-lopenal -lvorbisfile -lGL -lGLU $(shell freetype-config --libs) -lm -lpng -ljpeg -lXxf86vm -lX11 -lpthread -ldl
CFLAG_DEFS=-DPACKAGE=\"$(PACKAGE)\" -DVERSION=\"$(VERSION)\" -DGAMEDATA=\"$(DATADIR)\"

POSIX_CFLAGS ?= $(shell freetype-config --cflags) -fPIC -Wall -g -Iinc/ -I. $(CFLAG_DEFS) $(CFLAGS) -ansi -pedantic
POSIX_CLIBS ?= $(DEP_LIBS) -g $(CLIBS)
POSIX_LDFLAGS ?= -g $(LDFLAGS)

W32_CFLAGS ?= -Wall -g -Iinc/ -I. -I/usr/i686-mingw32/usr/include/ -Iw32libs/ -Iw32libs/inc/ -mwindows -DWIN32
W32_CLIBS ?= -L$(PWD)/w32libs/lib/  -lpng -llua51 -lOpenAL32 -lfreetype -lz -lwinmm -lmingw32 -lopengl32 -lws2_32 -mwindows
W32_LDFLAGS ?= -g

SRCDIR=src
DISTFILES=inc data README CHANGELOG LICENSE $(SRCDIR) Makefile* rtgconfig.h w32libs
INSTFILES=data/default.cfg
INSTFONTS=data/fonts/font.ttf
INSTIMGS=data/images/ground.png data/images/orc.png data/images/select.bmp data/images/guard1_body_color.bmp data/images/guard1_face_color.bmp data/images/guard1_helmet_color.bmp data/images/stone.png data/images/grass.tga data/images/mud.png data/images/ui.png data/images/iron_grill.png data/images/round_grill.png
INSTSNDS=data/sounds/explosion.ogg data/sounds/explosion.wav
INSTXMLS=data/xml/main3d.ui data/xml/main2d.ui data/xml/animations.xml
INSTMDLS=data/models/bob.rtm
INSTINCS=inc/keys_x11.h inc/rtg.h inc/rtg_server.h inc/rtg_sound.h inc/rtg_surface.h inc/rtg_render.h inc/rtg_ui.h rtgconfig.h
INSTGRP ?= games

COMMONOBJS=$(SRCDIR)/array.o $(SRCDIR)/animation.o $(SRCDIR)/cmd.o $(SRCDIR)/config.o $(SRCDIR)/crypto.o $(SRCDIR)/crypto_base64.o $(SRCDIR)/ctrl.o $(SRCDIR)/draw2d.o $(SRCDIR)/events.o $(SRCDIR)/events_w32.o $(SRCDIR)/events_x11.o $(SRCDIR)/exports.o $(SRCDIR)/file.o $(SRCDIR)/file_path.o $(SRCDIR)/font.o $(SRCDIR)/font_bitmap.o $(SRCDIR)/font_ttf.o $(SRCDIR)/image.o $(SRCDIR)/image_bmp.o $(SRCDIR)/image_jpg.o $(SRCDIR)/image_png.o $(SRCDIR)/image_tga.o $(SRCDIR)/kmap.o $(SRCDIR)/list.o $(SRCDIR)/main.o $(SRCDIR)/material.o $(SRCDIR)/math.o $(SRCDIR)/net.o $(SRCDIR)/net_tcp.o $(SRCDIR)/net_udp.o $(SRCDIR)/opengl.o $(SRCDIR)/print.o $(SRCDIR)/render.o $(SRCDIR)/render2d.o $(SRCDIR)/sound.o $(SRCDIR)/sound_wav.o $(SRCDIR)/sound_ogg.o $(SRCDIR)/style.o $(SRCDIR)/style_select.o $(SRCDIR)/time.o $(SRCDIR)/texture.o $(SRCDIR)/thread.o $(SRCDIR)/ui.o $(SRCDIR)/ui_console.o $(SRCDIR)/ui_events.o $(SRCDIR)/ui_list.o $(SRCDIR)/ui_script.o $(SRCDIR)/ui_style.o $(SRCDIR)/ui_widgets.o $(SRCDIR)/ui_xml.o $(SRCDIR)/wm.o $(SRCDIR)/wm_w32.o $(SRCDIR)/wm_x11.o $(SRCDIR)/xml.o
RTG2DOBJS=
RTG3DOBJS=$(SRCDIR)/camera.o $(SRCDIR)/collision3d.o $(SRCDIR)/draw3d.o $(SRCDIR)/lighting.o $(SRCDIR)/math_quaternion.o $(SRCDIR)/math_vector.o $(SRCDIR)/mesh.o $(SRCDIR)/model.o $(SRCDIR)/model_3ds.o $(SRCDIR)/model_md5.o $(SRCDIR)/model_ms3d.o $(SRCDIR)/model_rtm.o $(SRCDIR)/model_obj.o $(SRCDIR)/object.o $(SRCDIR)/physics.o $(SRCDIR)/particles.o $(SRCDIR)/render3d.o $(SRCDIR)/shader.o
DYNAMICOBJS=$(COMMONOBJS)
STATICOBJS=$(COMMONOBJS)
OBJS=$(COMMONOBJS) $(RTG2DOBJS) $(RTG3DOBJS) $(SRCDIR)/porting/winmain.o
TESTOBJS=$(SRCDIR)/test/main2d.o $(SRCDIR)/test/main3d.o

ifeq "$(ARCH)" "POSIX"
	TARGET_CFLAGS=$(POSIX_CFLAGS)
	TARGET_CLIBS=$(POSIX_CLIBS)
	TARGET_LDFLAGS=$(POSIX_LDFLAGS)
else
	CC=$(W32_CC)
	AR=$(W32_AR)
	EXT=.exe
	STATICOBJS=$(OBJS)
	TARGET_CFLAGS=$(W32_CFLAGS)
	DEP_LIBS=$(W32_CLIBS)
	TARGET_CLIBS=$(W32_CLIBS)
	TARGET_LDFLAGS=$(W32_LDFLAGS)
endif

ifeq "$(GRAPHIC)" "2d"
	TARGET_CFLAGS+=-DRTG2D=1
	STATICOBJS+=$(RTG2DOBJS)
	DYNAMICOBJS+=$(RTG2DOBJS)
else
	TARGET_CFLAGS+=-DRTG3D=1
	STATICOBJS+=$(RTG3DOBJS)
	DYNAMICOBJS+=$(RTG3DOBJS)
endif

LASTARCH=$(shell touch lastarch && cat lastarch)
ifeq "$(LASTARCH)" ""
	LASTARCH=$(ARCH)
endif

LASTGRAPHIC=$(shell touch lastgraphic && cat lastgraphic)
ifeq "$(LASTGRAPHIC)" ""
	LASTGRAPHIC=$(GRAPHIC)
endif

TARGET=rtg$(GRAPHIC)

all: 2d 3d
	$(CC) -o $(PACKAGE)-config $(SRCDIR)/config/main.c -DPACKAGE=\"$(PACKAGE)\" -DVERSION=\"$(VERSION)\" -DCLIBS="\"$(TARGET_CLIBS)\"" -DLIBDIR=\"$(LIBDIR)\" -DINCDIR=\"$(INCDIR)\" -DCFLAGS=\"$(shell freetype-config --cflags)\"

2d:
	@make GRAPHIC=2d default test2d

3d:
	@make GRAPHIC=3d default test3d

ifeq "$(ARCH)" "POSIX"
default: lib$(TARGET).a lib$(TARGET).so
else
default: lib$(TARGET).a $(TARGET).dll
endif

prepare:
ifneq "$(ARCH)" "$(LASTARCH)"
	@$(MAKE) clean
else
ifneq "$(GRAPHIC)" "$(LASTGRAPHIC)"
	@$(MAKE) distclean
endif
endif
	@echo $(ARCH) > lastarch
	@echo $(GRAPHIC) > lastgraphic

test2d: lib$(TARGET).a $(SRCDIR)/test/main2d.o
	$(CC) -o $(TARGET)$(EXT) $(SRCDIR)/test/main2d.o lib$(PACKAGE)2d.a $(DEP_LIBS) -g

test3d: lib$(TARGET).a $(SRCDIR)/test/main3d.o
	$(CC) -o $(TARGET)$(EXT) $(SRCDIR)/test/main3d.o lib$(PACKAGE)3d.a $(DEP_LIBS) -g

lib$(TARGET).a: prepare $(STATICOBJS)
	$(AR) rcs lib$(TARGET).a $(STATICOBJS)

lib$(TARGET).so: prepare $(DYNAMICOBJS)
	$(CC) $(TARGET_LDFLAGS) -o lib$(TARGET).so $(DYNAMICOBJS) $(TARGET_CLIBS) -shared -Wl,-soname,lib$(TARGET).so

$(TARGET).dll: prepare $(DYNAMICOBJS)
	$(CC) -shared $(TARGET_LDFLAGS) -o $(TARGET).dll $(DYNAMICOBJS) $(TARGET_CLIBS)

local:
	make DESTDIR=$(PWD) DATADIR=$(PWD)/data CFLAGS="-DRTG_LOCAL=1" all

dist-base:
	mkdir -p $(PACKAGE)-$(VERSION)
	cp -Rt $(PACKAGE)-$(VERSION) $(DISTFILES)

dist-gz: dist-base
	tar czf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	$(RM) -r $(PACKAGE)-$(VERSION)

dist-bz2: dist-base
	tar cjf $(PACKAGE)-$(VERSION).tar.bz2 $(PACKAGE)-$(VERSION)
	$(RM) -r $(PACKAGE)-$(VERSION)

dist-zip: dist-base
	zip -r $(PACKAGE)-$(VERSION).zip $(PACKAGE)-$(VERSION)
	$(RM) -r $(PACKAGE)-$(VERSION)

dist: distclean dist-bz2

distclean:
	$(RM) lastarch
	$(RM) lastgraphic
	$(RM) $(OBJS)
	$(RM) $(TESTOBJS)

clean: distclean
	$(RM) lib$(PACKAGE)*
	$(RM) $(PACKAGE)-config
	$(RM) $(PACKAGE)3d
	$(RM) $(PACKAGE)2d

install: all
	install --group=$(INSTGRP) -d $(BINDIR)
	install --group=$(INSTGRP) $(PACKAGE)2d $(BINDIR)/$(PACKAGE)2d
	install --group=$(INSTGRP) $(PACKAGE)3d $(BINDIR)/$(PACKAGE)3d
	install --group=$(INSTGRP) $(PACKAGE)-config $(BINDIR)/$(PACKAGE)-config
	install -d $(INCDIR)
	install -t $(INCDIR) $(INSTINCS)
	install --group=$(INSTGRP) -d $(DESTDIR)
	install --group=$(INSTGRP) -t $(DESTDIR) $(INSTFILES)
	install --group=$(INSTGRP) -d $(DESTDIR)/fonts
	install --group=$(INSTGRP) -t $(DESTDIR)/fonts $(INSTFONTS)
	install --group=$(INSTGRP) -d $(DESTDIR)/sounds
	install --group=$(INSTGRP) -t $(DESTDIR)/sounds $(INSTSNDS)
	install --group=$(INSTGRP) -d $(DESTDIR)/models
	install --group=$(INSTGRP) -t $(DESTDIR)/models $(INSTMDLS)
	install --group=$(INSTGRP) -d $(DESTDIR)/images
	install --group=$(INSTGRP) -t $(DESTDIR)/images $(INSTIMGS)
	install --group=$(INSTGRP) -d $(DESTDIR)/xml
	install --group=$(INSTGRP) -t $(DESTDIR)/xml $(INSTXMLS)
	install lib$(PACKAGE)2d.a $(LIBDIR)/lib$(PACKAGE)2d.a
	install lib$(PACKAGE)2d.so $(LIBDIR)/lib$(PACKAGE)2d.so.$(VERSION)
	$(RM) $(LIBDIR)/lib$(PACKAGE)2d.so
	ln -s $(LIBDIR)/lib$(PACKAGE)2d.so.$(VERSION) $(LIBDIR)/lib$(PACKAGE)2d.so
	install lib$(PACKAGE)3d.a $(LIBDIR)/lib$(PACKAGE)3d.a
	install lib$(PACKAGE)3d.so $(LIBDIR)/lib$(PACKAGE)3d.so.$(VERSION)
	$(RM) $(LIBDIR)/lib$(PACKAGE)3d.so
	ln -s $(LIBDIR)/lib$(PACKAGE)3d.so.$(VERSION) $(LIBDIR)/lib$(PACKAGE)3d.so

uninstall:
	$(RM) $(BINDIR)/$(PACKAGE)2d
	$(RM) $(BINDIR)/$(PACKAGE)3d
	$(RM) $(LIBDIR)/lib$(PACKAGE)2d.a
	$(RM) $(LIBDIR)/lib$(PACKAGE)2d.so
	$(RM) $(LIBDIR)/lib$(PACKAGE)2d.so.$(VERSION)
	$(RM) $(LIBDIR)/lib$(PACKAGE)3d.a
	$(RM) $(LIBDIR)/lib$(PACKAGE)3d.so
	$(RM) $(LIBDIR)/lib$(PACKAGE)3d.so.$(VERSION)

fresh: clean all

fresh-local: clean local

$(SRCDIR)/%.o: $(SRCDIR)/%.c rtgconfig.h inc/rtg.h inc/rtg_server.h
	$(CC) $(TARGET_CFLAGS) -o $@ -c $<

$(SRCDIR)/test/main3d.o: $(SRCDIR)/test/main3d.c inc/rtg.h inc/rtg_server.h
	$(CC) $(TARGET_CFLAGS) -DRTG3D=1 -o $@ -c $<


.PHONY: all default prepare test local dist-base dist-gz dist-bz2 dist distclean clean install uninstall fresh fresh-local
