#ifndef _RTG_CONFIG_H
#define _RTG_CONFIG_H

#define RTG_CFG_DEBUG	1

/* comment these out to only support certain image formats */
#define RTG_HAVE_PNG	1
#define RTG_HAVE_BMP	1
/* the windows build doesn't support jpeg at present */
#ifndef WIN32
# define RTG_HAVE_JPG	1
#endif
#define RTG_HAVE_TGA	1

/* comment these out to only support certain font formats */
#define RTG_HAVE_TTF	1
#define RTG_HAVE_BMF	1

/* comment these out to only support certain sound formats */
/* the windows build doesn't support ogg at present */
#ifndef WIN32
# define RTG_HAVE_OGG	1
#endif
#define RTG_HAVE_WAV	1

/* these are a couple of defaults */
#ifndef GAMEDATA
#define GAMEDATA "data"
#endif

#ifndef PACKAGE
#define PACKAGE "rt2d"
#endif

#endif
